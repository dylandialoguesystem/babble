function evalF1Report(dir, id, ontology, scoreLabels, microList, policy, numPos, numNeg, requestClass)
%EVALREPORT Summary of this function goes here
    setup;
    dir_root = fileparts(which('setup.m'));
    filename = sprintf('%s/expF1Report-%s-%s.csv',dir, policy, requestClass);
    path = fullfile(dir_root,filename);
    disp(path);
    
    if exist(path, 'file') == 0
         head = {'/********************************************************************/';
                 '  MacroF1 Experiment';
        	     '  @ Author:  Yanchao Yu';
                 '  @ Version: 0.1';
                 '  @ Date:    April 2016';
         sprintf('  @ Policy Type:  %s ',policy);
         sprintf('  @ TrainSet Size:  %d     TestSet Size:  %d', numPos, numNeg);
                 '/********************************************************************/'};
         writeToRow(path,head);
    end
    if strcmp(requestClass,'color')
         writeToRow(path,{'',''});
         writeToRow(path,{sprintf('/*********************** Test on %d-fold ************************/', id)});
    elseif strcmp(requestClass,'shape')
         writeToRow(path,{'',''});
         writeToRow(path,{sprintf('/*********************** Test on %d-fold ************************/', id)});
    else
        if strcmp(ontology,'color')
            writeToRow(path,{'',''});
            writeToRow(path,{sprintf('/*********************** Test on %d-fold ************************/', id)});
        end
    end
    [~, coulum] = size(scoreLabels);
    
    labels = {'train size'};
    microF1s = {ontology};
%     labels = {};
%     scores = {};
    for i = 1: coulum
        label = scoreLabels(1,i)
        microF1 = microList(1,i);
        
        labels{i+1} = num2str(label);
        microF1s{i+1} = num2str(microF1);
    end
    
    if strcmp(ontology,'color')
        writeToRow(path, labels);
    end
    writeToRow(path, microF1s);
end

function writeToRow(path, data)
    disp(data);
    fid = fopen(path,'a');
    numColumns = size(data,2);
    fmt = repmat('%s,',1,numColumns-1);
    fprintf(fid,[fmt,'%s\n'],data{1:end,:})
    fclose(fid);
end

  
  

