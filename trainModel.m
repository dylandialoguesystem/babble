function trainModel(attr,label,iskernnel)
%TRAINMODEL Summary of this function goes here

cachedata_file = sprintf('data/cache_data.mat');
load(cachedata_file);
feat = vector;
lrTrain( feat, label, attr, iskernnel );

end