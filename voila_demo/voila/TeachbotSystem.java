package voila;

import java.awt.GraphicsEnvironment;
import java.io.IOException;

import babble.dialog.env.DialogueEnvironment;
import babble.dialogue.IncrementalDialogueAgent;
import core.basics.DataShare;
import qmul.ds.dag.UtteredWord;
import voila.gui.RLExptActivity;
import voila.gui.mvc.DataController;
import voila.im.DialogManager;
import voila.im.ParserType;

public class TeachbotSystem extends DialogueEnvironment{
	public DialogManager dm;
	private RLExptActivity console;
	private DataController data_controller;
	
	public static final int DAT_LEVEL = 0;
	public static final int LEXICAL_LEVEL = 1;
	
	private static final String NAME = "system";
	
	public TeachbotSystem(int expIndex, String vsPath, String tranPath, boolean hasAdaptiveThreshold, boolean isClassifierSavable, ParserType parser_type) throws Exception{
		data_controller = new DataController();
		DataShare dataShare = new DataShare();
		
		boolean usingDS = parser_type.equals(ParserType.DS_TTR); 
		// initialize the TeachBot GUI
		console = new RLExptActivity("user", dataShare, usingDS, vsPath, data_controller);
		
		// initialize the dialogue management 
		this.dm = new DialogManager(this.NAME, dataShare, console.getEvalList(), data_controller, hasAdaptiveThreshold, isClassifierSavable, parser_type);
		this.dm.registerTranscriptWriter(tranPath);
		
		// join agent into the participant list
		this.registerIncrementalAgent(console);
		this.registerIncrementalAgent(this.dm);
		
		new Thread(console).start();
	}

	private void registerIncrementalAgent(IncrementalDialogueAgent agent) {
		if(agent != null)
			this.join(agent);
//			this.participants.add(agent);
		
		logger.info("number of participants joint in: " + this.participants.size());
	}
	

	public IncrementalDialogueAgent getInterlocutor(IncrementalDialogueAgent a)
	{
		if (a==this.participants.get(0))
			return this.participants.get(1);
		else if(a==this.participants.get(1))
			return this.participants.get(0);
		else throw new IllegalArgumentException();
	}
	
	@Override
	public boolean wordUttered(UtteredWord w)
	{
		if (w.speaker().equals(participants.get(0).name()))
			w.setAddressee(participants.get(1).name());
		else if (w.speaker().equals(participants.get(1).name()))
			w.setAddressee(participants.get(0).name());
		else
		{
			throw new IllegalArgumentException("speaker of "+w.word()+" is "+w.speaker()+": not part of conversation");
		}
		
		return super.wordUttered(w);
	}
	
	/********************** MAIN FUNCTION * @throws IOException **********************/
	public static void main(String[] args) throws IOException {
		String visual_path = "resource/SidGidal DataSet";
		String transcript_path = "resource/teachbot_transcription";
		boolean usingFurhat = false;
		boolean isClassifierSavable = true;
//		ParserType parser_type = ParserType.DS_TTR;
		
		if (args.length == 1){
			ParserType parser_type = ParserType.valueOf(args[0]);
			
			TeachbotSystem teachbot;
			try {
				teachbot = new TeachbotSystem(TeachbotSystem.DAT_LEVEL, visual_path, transcript_path, usingFurhat, isClassifierSavable, parser_type);
				teachbot.start();
				
				java.awt.GraphicsDevice[] devices = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
	
				   for ( int i = 0; i < devices.length; i++ )
				   {
				      System.out.println( "Device " + i + " width: " + devices[ i ].getDisplayMode().getWidth() );
				      System.out.println( "Device " + i + " height: " + devices[ i ].getDisplayMode().getHeight() );
				   }
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
