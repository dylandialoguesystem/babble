package voila.gui.mvc;

import java.util.LinkedList;
import java.util.List;

import org.json.simple.JSONObject;

import babble.vision.gui.DSTypes;
import qmul.ds.formula.TTRRecordType;
import voila.im.Behavior;

public class DataController {

	public static final String action_speech_in = "speech.input";
	public static final String action_speech_out = "speech.output";
	public static final String action_vision = "vision.update";
	public static final String action_behavior = "behavior.update";
	public static final String action_btncall = "button.call";
	public static final String action_systemagent = "system.agent";
	public static final String classifier_update = "classifier.update";
	public static final String ttr_record_update = "ttr_record_update";
	
	public DataController(){}

	public void setButtonAction(String btn, boolean isExpRunning) {
		notifyObservers(action_btncall, btn + ":" + isExpRunning);
	}
	
	public void setNewVisualObject(String filename){
		notifyObservers(action_vision, filename);
	}
	
	public void setSpeechOutputUpdated(String msg, String selected_action){
		notifyObservers(action_speech_out, selected_action+":"+msg);
	}
	
	public void setSpeechInputUpdated(String msg, String agent_name){
		notifyObservers(action_speech_in, agent_name+":"+msg);
	}
	
	public void setBehaviorUpdated(Behavior behavior){
		notifyObservers(action_behavior, behavior.toString().toLowerCase());
	}

	public void updateClassiferResults(){
		notifyObservers(classifier_update, "");
	}
	
	@SuppressWarnings("unchecked")
	public void updateTTRRecordType(TTRRecordType f, String speeach_act, DSTypes type){
		JSONObject object = new JSONObject();
		if (f != null)
			object.put("ttr_record", f.toString());
		if (speeach_act != null)
			object.put("speeach_act", speeach_act);
		if (type != null)
			object.put("ds_type", type.toString());
		
		notifyObservers(ttr_record_update, object.toJSONString());
	}
	
	/********************** OBSERVER PATTERN ***********************/
	//SUBJECT must be able to register, remove and notify observers
	//list to hold any observers
	private List<Observer> registeredObservers = new LinkedList<Observer>();
	//methods to register, remove and notify observers
	public void registerObserver(Observer obs){
		registeredObservers.add( obs);
	}
	
	public void removeObserver( Observer obs){
		registeredObservers.remove( obs);
	}	
	
	public void notifyObservers(String type, String content){
		for( Observer obs : registeredObservers)
			obs.update(type, content);
	}
}
