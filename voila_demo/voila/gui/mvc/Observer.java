package voila.gui.mvc;

public interface Observer {
	  public void update(String type, String msg);
}
