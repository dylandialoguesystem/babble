package voila.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.FontUIResource;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import org.apache.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import babble.dialog.chatinterface.DialogueConsole;
import babble.dialog.chatinterface.JTextPaneRightJustified;
import babble.dialogue.DSDialogueAgent;
import babble.simulation.SimulateTutor;
import babble.vision.classification.Concept;
import babble.vision.gui.ClassifierPanel;
import babble.vision.gui.DSTypes;
import babble.vision.gui.DrawPanel;
import babble.vision.gui.StatusBar;
import babble.vision.object.VisualObject;
import babble.vision.tools.Constants;
import babble.vision.tools.VSTimer;
import core.basics.DataShare;
import hw.system.dm.SimulateIM;
import hw.system.dm.UttTyps;
import qmul.ds.Utterance;
import qmul.ds.dag.UtteredWord;
import qmul.ds.formula.TTRRecordType;
import qmul.ds.gui.FormulaPanel;
import voila.gui.mvc.DataController;
import voila.gui.mvc.Observer;

/**
 * Build the simulation activity GUI for managing all system components and create a 
 * text-based dialog system with simulated users, this system will simulate the whole 
 * dialog of teaching process between real human tutor and TeachBot system.
 * 
 * @author Yanchao Yu
 */
public class RLExptActivity extends DialogueConsole implements Observer{
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(RLExptActivity.class);
	
	private JTextPane digText;
	private JTextField da_textpanel, speech_act_textpanel;
	private FormulaPanel semPanel, visualPanel, nlgPanel;
	private ChartPanel histPanel;
	private DrawPanel snapPanel;
	private Font ui_font;
	
	private List<JComponent> componentList = new ArrayList<JComponent>();

	private Border regularBorder = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
	private Border highligtBorder = BorderFactory.createLineBorder(Color.RED, 3);
	
	private boolean usingDS = false;
	private boolean hasHightLight = false;
	private boolean isExpRunning = false;
	private DataController data_controller;
	
	private SimpleAttributeSet jpanel_settings;
	
	private int inputMode;
	private final int SPEECH_INPUT = 0;
	private final int TEXT_INPUT = 1;
	
	private DataShare dataShare;
	
	private String visualPath = "/Users/yanchaoyu/Documents/SidGidal DataSet";
	
	public RLExptActivity(String name, DataShare dataShare, boolean usingDS, String visualPath, DataController data_control){
		super(name);
		if(visualPath != null)
			this.visualPath = visualPath;
		
		this.dataShare = dataShare;
		this.usingDS = usingDS;
		this.ui_font = new Font("Courier", Font.PLAIN, Constants.fontSize);
		
		this.inputMode = TEXT_INPUT;
		this.initialGUI(usingDS);
		
		this.data_controller = data_control;
		this.data_controller.registerObserver(this);
		
		statusBar = new StatusBar(this.ui_font);
		console.add(statusBar,BorderLayout.SOUTH);

		console.setExtendedState(JFrame.MAXIMIZED_BOTH); 
		console.setVisible(true);

		this.setTimeout(2000);
//		this.showOnScreen(1,console);
	}

	/**
	 * pack all components together.
	 */
	private static final int width = 1250;
	private static final int height = 950;
	
	private StatusBar statusBar;
	private void initialGUI(boolean usingDS) {
		console.setTitle("VOILA: Visually Optimised Interactive Learning Agent");
		console.setFont(this.ui_font);
		console.setPreferredSize(new Dimension(width + 10, height));
		console.setLayout(new BorderLayout());
		console.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		console.setLocationRelativeTo(null);
		console.setResizable(true);
		
		JPanel mainPanel = new JPanel();
		mainPanel.setPreferredSize(new Dimension(width*3/4 + 5 , height));
		mainPanel.setLayout(new BorderLayout());
		console.add(mainPanel, BorderLayout.CENTER);
		mainPanel.setBackground(Color.red);
		
		mainPanel.add(this.buildConversationBox(usingDS), BorderLayout.CENTER);
		mainPanel.add(this.buildDataList(this.visualPath), BorderLayout.SOUTH);
//		console.add(this.creatVirtualAgent(), BorderLayout.EAST);
		
		console.setJMenuBar(getMainmenu());
		statusBar = new StatusBar();
		console.add(statusBar,BorderLayout.SOUTH);

		if(visuallist != null)
			visuallist.setEnabled(false);
		if(snapPanel != null)
			snapPanel.setEnabled(false);
		if(visualPanel != null)
			visualPanel.setEnabled(false);
		if(classPanel != null)
			classPanel.setEnabled(false);
		
		for(JComponent componenet: this.componentList){
			final JComponent temp = componenet;
    		temp.setBorder(regularBorder);
			temp.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseEntered(java.awt.event.MouseEvent evt) {
                	if(hasHightLight)
                		temp.setBorder(highligtBorder);
                }
                public void mouseExited(java.awt.event.MouseEvent evt) {
                	if(hasHightLight)
                		temp.setBorder(regularBorder);
                }
            });
		}
		
		jpanel_settings = new SimpleAttributeSet();
		StyleConstants.setForeground(jpanel_settings, new Color(0, 102, 51));
		StyleConstants.setFontSize(jpanel_settings, Constants.dlg_fontSize);
		
		console.setVisible(true);
		console.pack();
	}

	private DrawPanel agentPanel;
	private Component creatVirtualAgent() {
		JPanel agentBox = new JPanel();
		agentBox.setLayout(new BorderLayout());
		agentBox.setPreferredSize(new Dimension(console.getWidth()*2/7, console.getHeight()));
		
		agentPanel = new DrawPanel(); 
		agentPanel.setPreferredSize(new Dimension(console.getWidth()*2/7, console.getHeight()));
		agentPanel.setBackground(Color.WHITE);
		agentPanel.addImage("resource/furhat_img_1.png", false);
		agentBox.add(agentPanel,BorderLayout.CENTER);
		
		return agentBox;
	}

	private final int numOfrows = 1;
	private final int numOfcols = 4;
	private final int gap = 5;
	/**
	 * Setup Visual Panel that contains: 
	 * 1) snapshot of the real object,
	 * 2) feature histogram 
	 */
	private JPanel buildVsBox(){
		JPanel vsBox = new JPanel();
		vsBox.setLayout(new GridLayout(numOfrows, 2, gap, gap));
		vsBox.setPreferredSize(new Dimension(width*2/3, height*3/8));
		
		// panel for showing snapshot of the real object
		JPanel snpGroupBox = new JPanel();
		snpGroupBox.setPreferredSize(new Dimension(width/3-10, height*3/8));
		snpGroupBox.setLayout(new BorderLayout());
		TitledBorder snpTitle = BorderFactory.createTitledBorder(null, "Snapshot", TitledBorder.CENTER, TitledBorder.TOP, this.ui_font);
		
		snpGroupBox.setBorder(snpTitle);
		snapPanel = new DrawPanel();
		snapPanel.setPreferredSize(new Dimension(width/3-5, height*3/8));
		snapPanel.setBackground(Color.WHITE);
		snpGroupBox.add(snapPanel, BorderLayout.CENTER);
		vsBox.add(snpGroupBox);
		this.componentList.add(snapPanel);
		
		JPanel histGroupBox = new JPanel();
		histGroupBox.setPreferredSize(new Dimension(new Dimension(width/3-5, height*3/8)));
		histGroupBox.setLayout(new BorderLayout());
		TitledBorder histTitle = BorderFactory.createTitledBorder(null, "Histogram", TitledBorder.CENTER, TitledBorder.TOP, this.ui_font);
		
		histGroupBox.setBorder(histTitle);
		histPanel = new ChartPanel(initialJFreeChart());
		histPanel.setPreferredSize(new Dimension(new Dimension(width/3-5, height*3/8)));
		histGroupBox.add(histPanel, BorderLayout.CENTER);
		vsBox.add(histGroupBox);
		this.componentList.add(histPanel);
		
		return vsBox;
	}
//	
//	/**
//	 * Setup the Visual Box that contains all classification results and the corresponding visual context produced in TTR format
//	 */
//	private void setVisualBox(){
//		JPanel visualBox = new JPanel();
//		visualBox.setPreferredSize(new Dimension(new Dimension(width*1/4-5, height*3/8)));
//		visualBox.setLayout(new BorderLayout());
//		TitledBorder visualTitle = BorderFactory.createTitledBorder("Visual Context");
//		visualBox.setBorder(visualTitle);
//		
////		JTabbedPane ttrTab = new JTabbedPane();
////		ttrTab.setPreferredSize(new Dimension(width*1/4-5, height));
//	    visualPanel =new FormulaPanel();
//	    visualPanel.setBackground(Color.white);
//	    visualPanel.setPreferredSize(new Dimension(width*1/4-5, height*3/8));
//		JScrollPane visualScroll = new JScrollPane(visualPanel);
//		visualScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
//		visualPanel.setContainer(visualScroll);
//		this.componentList.add(visualPanel);
//		
//		clasPanel = new ClassifierPanel(console);
//		clasPanel.setPreferredSize(new Dimension(width/3-5, height*5/8));
//		try {
//			clasPanel.collectClassifierPanels();
//		} catch (IOException e) {
//			logger.error(e.getMessage());
//		}
//		this.componentList.add(clasPanel);
//		
////		
////	    ttrTab.addTab("Visual Context", visualPanel);
////	    ttrTab.addTab("Classification Results", clasPanel);
////	    visualBox.add(ttrTab, BorderLayout.CENTER);
//
//	    visualBox.add(visualPanel, BorderLayout.NORTH);
//	    visualBox.add(clasPanel, BorderLayout.CENTER);
//		
//	    console.add(visualBox, BorderLayout.EAST);
//	}

	/**
	 * Build Semantic Box that contains all dialogue-related GUI components:
	 * 1) dialogue box that shows entire dialogue between the tutor and the learning agent; 
	 * 2) semantic box that displays the semantic representation in the TTR form parsed via the DyLan;
	 * 3) nlg box that shows the semantic representation produced via unification with semantic and visual context;
	 * @return JPanel Semantic Box
	 */
	private JScrollPane digScroll;
	private ClassifierPanel classPanel;
	private JPanel buildConversationBox(boolean usingDS) {
		JPanel overBox = new JPanel();
		overBox.setLayout(new BorderLayout());
		overBox.setPreferredSize(new Dimension(width, height*4/8));
		
		JPanel conversationBox = new JPanel();
		conversationBox.setLayout(new GridLayout(numOfrows, numOfcols, gap, gap));
		conversationBox.setPreferredSize(new Dimension(width*3/4 + 5, height*4/8));

		JPanel dlgGroupBox = new JPanel();
		dlgGroupBox.setPreferredSize(new Dimension(new Dimension(width/4-5, height*4/8)));
		dlgGroupBox.setLayout(new BorderLayout());
		TitledBorder dlgTitle = BorderFactory.createTitledBorder(null, "Dialogue", TitledBorder.CENTER, TitledBorder.TOP, this.ui_font);
		dlgGroupBox.setBorder(dlgTitle);
	    digText =new JTextPane();
	    digText.setEditable(false);
	    digText.setBackground(Color.white);
	    digText.setFont(new Font("Serif", Font.PLAIN , Constants.dlg_fontSize));
	    digText.setPreferredSize(new Dimension(width/4-5, height*4/8));
		digScroll = new JScrollPane(digText);
		digScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		dlgGroupBox.add(digScroll, BorderLayout.CENTER);
		this.componentList.add(digText);	

		JPanel da_panel = new JPanel();
		JLabel daLabel = new JLabel("Selected Act: ");
		daLabel.setLabelFor(da_textpanel);
		da_textpanel =new JTextField();
		da_textpanel.setEditable(false);
		da_textpanel.setBackground(Color.white);
		da_textpanel.setFont(new Font("Serif", Font.PLAIN, Constants.dlg_fontSize));
//		speech_act_textpanel.setPreferredSize(new Dimension(width/4-5, height*4/8));
		JScrollPane da_scroll_panel = new JScrollPane(da_textpanel);
		da_scroll_panel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		da_panel.setLayout(new BorderLayout());
		da_panel.add(daLabel, BorderLayout.WEST);
		da_panel.add(da_scroll_panel, BorderLayout.CENTER);
		dlgGroupBox.add(da_panel, BorderLayout.SOUTH);	
		conversationBox.add(dlgGroupBox, BorderLayout.CENTER);

		JPanel objGroupBox = new JPanel();
		objGroupBox.setPreferredSize(new Dimension(new Dimension(width/4-5, height*4/8)));
		objGroupBox.setLayout(new BorderLayout());
		TitledBorder objTitle = BorderFactory.createTitledBorder(null, "Chosen Visual Object", TitledBorder.CENTER, TitledBorder.TOP, this.ui_font);
		
		objGroupBox.setBorder(objTitle);
		snapPanel = new DrawPanel();
		snapPanel.setPreferredSize(new Dimension(width/3-5, height*3/8));
		snapPanel.setBackground(Color.WHITE);
		snapPanel.setEnabled(false);
		objGroupBox.add(snapPanel, BorderLayout.CENTER);
		this.componentList.add(snapPanel);
		conversationBox.add(objGroupBox);
		
		if(usingDS){
			JPanel semGroupBox = new JPanel();
			semGroupBox.setPreferredSize(new Dimension(new Dimension(width/4-5, height*4/8)));
			semGroupBox.setLayout(new BorderLayout());
			TitledBorder semTitle = BorderFactory.createTitledBorder("Dialogue Context");
			semGroupBox.setBorder(semTitle);
			semPanel =new FormulaPanel();
		    semPanel.setBackground(Color.white);
		    semPanel.setPreferredSize(new Dimension(width/4-5, height*4/8));
			JScrollPane semScroll = new JScrollPane(semPanel);
			semScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		    semPanel.setContainer(semScroll);
		    semGroupBox.add(semPanel, BorderLayout.CENTER);
			this.componentList.add(semPanel);
		    
			JLabel actLabel = new JLabel("Speech Act: ");
		    actLabel.setLabelFor(speech_act_textpanel);
		    speech_act_textpanel =new JTextField();
		    speech_act_textpanel.setEditable(false);
		    speech_act_textpanel.setBackground(Color.white);
		    speech_act_textpanel.setFont(new Font("Serif", Font.PLAIN, Constants.dlg_fontSize));
//		    speech_act_textpanel.setPreferredSize(new Dimension(width/4-5, height*4/8));
		    JScrollPane speech_scroll_panel = new JScrollPane(speech_act_textpanel);
		    speech_scroll_panel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			this.componentList.add(speech_scroll_panel);

			JPanel act_panel = new JPanel();
			act_panel.setLayout(new BorderLayout());
			act_panel.add(actLabel, BorderLayout.WEST);
			act_panel.add(speech_act_textpanel, BorderLayout.CENTER);
			semGroupBox.add(act_panel, BorderLayout.SOUTH);	
			conversationBox.add(semGroupBox);
		}
		
		overBox.add(conversationBox, BorderLayout.CENTER);
		
		JPanel classGroupBox = new JPanel();
		classGroupBox.setPreferredSize(new Dimension(new Dimension(width/8+20, height*4/8)));
		classGroupBox.setLayout(new BorderLayout());
		TitledBorder classTitle = BorderFactory.createTitledBorder(null, "Classification", TitledBorder.CENTER, TitledBorder.TOP, this.ui_font);
		
		classGroupBox.setBorder(classTitle);
		
		classPanel = new ClassifierPanel(console);
		classPanel.setPreferredSize(new Dimension(width/4-5, height*4/8));
		try {
			classPanel.collectClassifierPanels();
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		classPanel.setVisible(true);
		classGroupBox.add(classPanel, BorderLayout.CENTER);
		this.componentList.add(classPanel);
//		conversationBox.add(classGroupBox);
		overBox.add(classGroupBox, BorderLayout.EAST);
		
		return overBox;
	}
	
	/**
	 * Setup Histogram Chart panel for showing the extracted visual features 
	 * from the specific object
	 */
	private XYSeries series;
	private JFreeChart initialJFreeChart(){
		series = new XYSeries("Feature");
		final XYSeriesCollection dataset = new XYSeriesCollection(series);
		
		String plotTitle = "Feature Histogram";
    	String xaxis = "bin #";
    	String yaxis = "val";
    	PlotOrientation orientation = PlotOrientation.VERTICAL;
    	boolean showLegend = true;
    	boolean toolTips = true;
    	boolean urls = false;
    	JFreeChart jchart = ChartFactory.createXYBarChart(plotTitle,xaxis,false,yaxis,dataset,orientation,showLegend,toolTips,urls);
    	
    	return jchart;
	}
	
	/**
	 * Method to get new MenuBar
	 * @return new JMenuBar
	 */
	public JMenuBar getMainmenu(){
		JMenuBar mainMenuBar = new JMenuBar();		
		JMenu sysMenu = new JMenu("System"); 
		sysMenu.setFont(ui_font);
		final JMenuItem switchItem = new JMenuItem("Start");
		switchItem.setFont(ui_font);
		switchItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(!isExpRunning){
					switchItem.setText("Stop");
					isExpRunning = true;
					if(visuallist != null)
						visuallist.setEnabled(true);
					if(snapPanel != null)
						snapPanel.setEnabled(true);
					if(visualPanel != null)
						visualPanel.setEnabled(true);
					if(classPanel != null)
						classPanel.setEnabled(true);
				}
				else{
					switchItem.setText("Start");
					isExpRunning = false;
					resetGUI();
					if(visuallist != null)
						visuallist.setEnabled(false);
					if(snapPanel != null)
						snapPanel.setEnabled(false);
					if(visualPanel != null)
						visualPanel.setEnabled(false);
					if(classPanel != null)
						classPanel.setEnabled(false);
				}
				
				data_controller.setButtonAction("startBtn", isExpRunning);
			}
			
		});
		sysMenu.add(switchItem);
		
		final JMenuItem highlightItem = new JMenuItem("HighLight");
		highlightItem.setFont(ui_font);
		if(this.hasHightLight){
			highlightItem.setText("HighLight");
		}
		highlightItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(highlightItem.getText().equals("Regular")){
					highlightItem.setText("HighLight");
					hasHightLight = false;
				}
				else if(highlightItem.getText().equals("HighLight")){ 
					highlightItem.setText("Regular");
					hasHightLight = true;
				}
			}
			
		});
		sysMenu.add(highlightItem);		
		
		JMenuItem inputModeItem = new JMenuItem("Speech");
		inputModeItem.setFont(ui_font);
		inputModeItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(inputModeItem.getText().equals("Speech")){
					inputModeItem.setText("Text");
					inputMode = SPEECH_INPUT;
					textPane.setEnabled(false);
				}
				else if(inputModeItem.getText().equals("Text")){ 
					inputModeItem.setText("Speech");
					inputMode = TEXT_INPUT;
					textPane.setEnabled(true);
				}
			}
			
		});
		sysMenu.add(inputModeItem);
		
		JMenuItem exitItem = new JMenuItem("Exit");
		exitItem.setFont(ui_font);
		exitItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				int option = JOptionPane.showConfirmDialog(null, "Do you want to exit?", "Exit",JOptionPane.YES_NO_OPTION);
				 if(option == JOptionPane.YES_NO_OPTION){
					 logger.debug("exit the simulation activity.");
					 System.exit(0);
				 }
			}
			
		});
		sysMenu.add(exitItem);		
		mainMenuBar.add(sysMenu);
		
		return mainMenuBar;
	}
	
	private void resetGUI(){
//		histPanel.removeAll();
//		histPanel.revalidate();
//		histPanel.repaint();
		
//		visualPanel.removeAll();
//		visualPanel.revalidate();
//		visualPanel.repaint();
		
		semPanel.removeAll();
		semPanel.revalidate();
		semPanel.repaint();
//		
//		nlgPanel.removeAll();
//		nlgPanel.revalidate();
//		nlgPanel.repaint();

		snapPanel.clear();
		
		if(classPanel != null)
			classPanel.clearClassifierResults();
		
		digText.setText("");
	}
	
	private final int image_width = 145;
	private final int image_height = 140;
	private JList visuallist;
	@SuppressWarnings("unchecked")
	private JPanel buildDataList(String visualPath) {
		try {
			this.loadImages(visualPath);
			
			JPanel dlgPanel = new JPanel();
			dlgPanel.setLayout(new BorderLayout());
			dlgPanel.setPreferredSize(new Dimension(console.getWidth(), 260));
					
			JPanel jpDatList = new JPanel();
			jpDatList.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(5, 5, 5, 5),
                    new EtchedBorder()));
			jpDatList.setLayout(new BorderLayout());
			jpDatList.setPreferredSize(new Dimension(dlgPanel.getWidth(), image_height+48));
			TitledBorder dataListTitle = BorderFactory.createTitledBorder(null, "Visual Instance List", TitledBorder.CENTER, TitledBorder.TOP, this.ui_font);
			
			jpDatList.setBorder(dataListTitle);
			
			this.visuallist = new JList(this.instanceMap.keySet().toArray());
			this.visuallist.setCellRenderer(new MarioListRenderer());
			this.visuallist.setLayoutOrientation(JList.HORIZONTAL_WRAP);
			this.visuallist.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			this.visuallist.setVisibleRowCount(-1);
			this.visuallist.setEnabled(true);
			
			this.visuallist.addMouseListener(new MouseListener(){

				@Override
				public void mouseClicked(MouseEvent e) {
					if(e.getClickCount() == 2){
						if(visuallist != null && !visuallist.isSelectionEmpty()){
							try {
								updateDialog("---- NEW DIALOGUE ----", UttTyps.obj, "");
							} catch (BadLocationException e1) {
								logger.error(e1.getMessage());
							}
							
				    		String value = (String) visuallist.getSelectedValue();
				    		updateImage(value);
				    		updateStatus("You are talking on the image of '" + value +"'...");
	
			    			data_controller.setNewVisualObject(value);
				    	}
				    	else
				    		statusBar.setStatusTip("Please select one of images below!");
					}
				}

				@Override
				public void mousePressed(MouseEvent e) {
				}

				@Override
				public void mouseReleased(MouseEvent e) {
				}

				@Override
				public void mouseEntered(MouseEvent e) {
				}

				@Override
				public void mouseExited(MouseEvent e) {
				}
			});
			
	        JScrollPane scroll = new JScrollPane(this.visuallist);
	        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
	        
	        jpDatList.add(scroll, BorderLayout.CENTER);
	        dlgPanel.add(jpDatList, BorderLayout.NORTH);
	        
	        // text-based chat tool
	        textPane = new JTextPaneRightJustified(this);
			textPane.setPreferredSize(new Dimension(dlgPanel.getWidth(), 70));
			textPane.setFont(new Font("helvitica", Font.LAYOUT_LEFT_TO_RIGHT, 2));
			textPane.setBackground(Color.white);
			textPane.setEditable(true);
			textPane.startFade();
			dlgPanel.add(textPane, BorderLayout.SOUTH);
			
			if(this.inputMode == this.TEXT_INPUT){
				textPane.setEnabled(true);
			}
	        
	        return dlgPanel;
	        
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	private List<String> evalFileList;
	public List<String> getEvalList() {
		return this.evalFileList;
	}
	
	public class MarioListRenderer extends DefaultListCellRenderer {

        Font font = new Font("helvitica", Font.CENTER_BASELINE, 10);

        @Override
        public Component getListCellRendererComponent(
                JList list, Object value, int index,
                boolean isSelected, boolean cellHasFocus) {

            JLabel label = (JLabel) super.getListCellRendererComponent(
                    list, value, index, isSelected, cellHasFocus);
            label.setIcon(instanceMap.get((String) value));
            label.setVerticalTextPosition(JLabel.BOTTOM);
            label.setHorizontalTextPosition(JLabel.CENTER);
            label.setFont(font);
            return label;
        }
    }

	private List<VisualObject> objList;
	private Map<String, ImageIcon> instanceMap;
	private static final int separator = 100;
	private void loadImages(String path) throws IOException{
		if(this.objList == null)
			this.objList = new ArrayList<VisualObject>(); 
		else
			this.objList.clear();
		
		if(this.instanceMap == null)
			this.instanceMap = new HashMap<String, ImageIcon>(); 
		else
			this.instanceMap.clear();
		
	    final File dir = new File(path);
	    
	    // array of supported extensions
	    final String[] EXTENSIONS = new String[]{
	        "jpg", "png", "bmp"
	    };
	    
	    // filter to identify images based on their extensions
	    final FilenameFilter image_filter = new FilenameFilter() {
	        @Override
	        public boolean accept(final File dir, final String name) {
	            for (final String ext : EXTENSIONS) {
	                if (name.endsWith("." + ext)) {
	                    return (true);
	                }
	            }
	            return (false);
	        }
	    };
	    
	    if(dir.exists() && dir.isDirectory()){
	    	List<String> fileList = new ArrayList<String>();
	    	for(final File f : dir.listFiles(image_filter)){
	    		fileList.add(f.getName());
	    	}
	    	Collections.shuffle(fileList);
	    	
	    	this.evalFileList = fileList.subList(0, this.separator);
	    	
	    	int i=0; 
	    	List<String> objFileList = fileList.subList(this.separator,fileList.size());
	    	for(String filename: objFileList){
	    		File f = new File(dir.getPath()+"/"+filename);
	    		BufferedImage img = ImageIO.read(f);
	    		VisualObject obj = new VisualObject(i++, f.getName());
	    		obj.setBufferedImage(img);
	    		 
	    		instanceMap.put(f.getName(), new ImageIcon(img.getScaledInstance(image_width, image_height, Image.SCALE_DEFAULT)));
	    		this.objList.add(obj);
	    	}
	    }
	}

	@Override
	public void update(String type, String msg) {
//		logger.info("type: " + type + " --> contxt: " + msg);
		
		if(type.equals(DataController.action_speech_in)){
			String[] items = msg.trim().split(":");
			String agent_name = items[0];
			String txt = items[1];
			
//			updateSpeechInput(txt, agent_name);
			try {
				updateDialog(txt.trim(), UttTyps.user, "");
			} catch (BadLocationException e) {
				logger.error(e.getMessage());
			}
		}
		
		else if(type.equals(DataController.action_speech_out)){
			String[] items = msg.trim().split(":");
			String selected_action = items[0];
			String txt = items[1];
			
			try {
				updateDialog(txt.trim(), UttTyps.system, selected_action);
			} catch (BadLocationException e) {
				logger.error(e.getMessage());
			}
		}
		
		else if(type.equals(DataController.classifier_update)){
			Map<String, Queue<Concept>> prediction = dataShare.getPredictions();
			this.updateClassifierResults(prediction);
		}
		
		else if(type.equals(DataController.ttr_record_update)){
			try {
				JSONParser parser = new JSONParser();
				JSONObject ttr_json = (JSONObject) parser.parse(msg);

				TTRRecordType f = TTRRecordType.parse((String)ttr_json.get("ttr_record"));
				String speeach_act = (String) ttr_json.get("speeach_act");
				DSTypes ds_type = DSTypes.valueOf((String)ttr_json.get("ds_type"));
				  
				this.updateTTRRecords(f, speeach_act, ds_type);
			} catch (ParseException e) {
				logger.error(e.getMessage());
			}
		}
	}
	
	public void showOnScreen(int screen, JFrame frame ) {
	    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
	    GraphicsDevice[] gd = ge.getScreenDevices();
	    int width = 0, height = 0;
	    if( screen > -1 && screen < gd.length ) {
	        width = gd[screen].getDefaultConfiguration().getBounds().width;
	        height = gd[screen].getDefaultConfiguration().getBounds().height;
	        frame.setLocation(
	            ((width / 2) - (frame.getSize().width / 2)) + gd[screen].getDefaultConfiguration().getBounds().x, 
	            ((height / 2) - (frame.getSize().height / 2)) + gd[screen].getDefaultConfiguration().getBounds().y
	        );
	        frame.setVisible(true);
	        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    } else {
//	        throw new RuntimeException( "No Screens Found" );
	    }
	}

	public void updateTTRRecords(TTRRecordType f, String speeach_act, DSTypes type) {
		// only work for the demo system with DS-TTR parser
		if(usingDS){
			if(f != null){
				logger.info(type.toString()+" :: " + f.toString());
				switch(type){
				case semanticTTR:
					if(semPanel != null)
						semPanel.setFormula(f);
					if(speech_act_textpanel != null)
						speech_act_textpanel.setText(speeach_act);
					break;
				case visualTTR:
					if(visualPanel != null){
						visualPanel.setFormula(f);
					}
					break;
				case contextTTR:
					if(nlgPanel != null){
						nlgPanel.setFormula(f);
					}
					break;
				}
			}
		}
	}
	
	public void updateDialog(String utt, UttTyps speaker, String action) throws BadLocationException {
		String str = null;
		if(digText != null){
			Document doc = this.digText.getDocument();

			if(utt != null){
				String content = utt;
				logger.info("content: " + content);
				str = Constants.NEWLINE + speaker + ": " + content; // VSTimer.getInstance().getTimeStamp() +" -- " + 

				switch(speaker){
					case system:
						StyleConstants.setForeground(jpanel_settings, new Color(0, 102, 51));
						this.digText.getDocument().insertString(doc.getLength(), str, jpanel_settings);
						this.da_textpanel.setText(action);
						break;
					case user:
						StyleConstants.setForeground(jpanel_settings, new Color(42, 20, 195));
						this.digText.getDocument().insertString(doc.getLength(), str, jpanel_settings);
						break;
					case obj:
						StyleConstants.setForeground(jpanel_settings, new Color(34, 15, 173));
						String signal = utt;
						if(doc.getLength() != 0)
							signal = Constants.NEWLINE + Constants.NEWLINE + utt;
						this.digText.getDocument().insertString(doc.getLength(), signal, jpanel_settings);
						break;
					default:
						break;
				}
				
				digText.setCaretPosition(digText.getDocument().getLength());
				
//				digScroll.getViewport().setViewPosition(new Point(0,digText.getDocument().getLength()));
			}
		}
	}

	public void updateStatus(String msg) {
		statusBar.setStatusTip(msg);
	}

	public void updateImage(String value) {
		String imagePath = visualPath + "/" + value;
		snapPanel.addImage(imagePath, true);
		snapPanel.repaint();
		snapPanel.validate();
	}
	
	public void updateClassifierResults(Map<String, Queue<Concept>> prediction) {
		if(this.classPanel != null){
			logger.info("prediction: " + prediction);
			this.classPanel.updateResults_2(prediction);
		}
	}
}
