package voila.vision;

import java.awt.image.BufferedImage;
import java.io.Serializable;

import javax.swing.ImageIcon;

import babble.vision.object.BasicObject;

public class VisualObject extends BasicObject implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private BufferedImage img = null;
	private ImageIcon imageIcon;
	
	public VisualObject(String fileName){
		this.setFileName(fileName);
	}
	
	public void setBufferedImage(BufferedImage img){
		this.img = img;
		this.imageIcon = new ImageIcon(img);
	}
	
	public BufferedImage getBufferedImage(){
		return this.img;
	}
	
	public ImageIcon getImageIcon(){
		return this.imageIcon;
	}
	
	public VisualObject clone(){
		VisualObject obj = new VisualObject(this.getFileName());
		
		return obj;
	}
}
	