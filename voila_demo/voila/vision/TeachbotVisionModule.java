package voila.vision;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import babble.evaluation.TestClassifierCollection;
import babble.evaluation.eval.StrategyEval;
import babble.system.AbstractVisionModule;
import babble.system.ConceptMap;
import babble.vision.classification.Concept;
import babble.vision.classification.IncremClassifier;
import babble.vision.object.BasicObject;
import babble.vision.object.VisualObject;
import babble.vision.tools.MapToolKit;
import babble.vision.tools.MatlabLink;

public class TeachbotVisionModule extends AbstractVisionModule{
	Logger logger = Logger.getLogger(TeachbotVisionModule.class);
	public Map<String, Integer> rlStates = new HashMap<String, Integer>();

	private MatlabLink matLink;
	private Map<String,TestClassifierCollection> classMap = null;
	public StrategyEval strategyEval;
	
	private List<VisualObject> evalVsiualList;
	
	private static final String visual_path = "resource/SidGidal DataSet/";
	public TeachbotVisionModule(){
		this.loadVisualRecords(visual_path);
//		matLink = new MatlabLink();
	}

	private List<VisualObject> visualRecords;
	private Map<String, List<String>> slot_values;
	public List<VisualObject> getRealObjectList(){
		return this.visualRecords;
	}

	public boolean loadVisualRecords(String dir){
		if(this.visualRecords == null)
			this.visualRecords = new ArrayList<VisualObject>();
		else
			this.visualRecords.clear();

		if(slot_values == null)
			slot_values = new HashMap<String, List<String>>();
		else
			slot_values.clear();
			
		BufferedReader br = null;
		try{
			File annotFile = new File(dir + "annotation.txt");
				
			if(annotFile.exists()){
				logger.debug("Read New File!!!");
				FileInputStream fstream = new FileInputStream(annotFile);
				br = new BufferedReader(new InputStreamReader(fstream));
					
				int count = 0;
				String strLine;
				while ((strLine = br.readLine()) != null){
					if(!strLine.trim().equals("")){
						String[] items = strLine.split(",");
						String filename = items[0];
						String description = items[1];
		
						double[] feat = new double[1280];
						String featVal = items[2].trim().toString();
						String[] vals = featVal.split(" ");
						for(int i=0;i<vals.length;i++){
							  if(vals[i].equals("NaN"))
								  feat[i] = 0;
							  else
								  feat[i] = Double.valueOf(vals[i].trim());
						 }
							
						VisualObject record = new VisualObject(count++);
						record.setDirectory(dir);
						record.setFileName(filename);
						record.setLabels(description);
						record.setFeat(feat);
						visualRecords.add(record);
						
						List<String> list_color = slot_values.get("%colorvalue");
						if(list_color == null)
							list_color = new ArrayList<String>();
						list_color.add(((String)record.getSpecificLabel("color")).toLowerCase());
						slot_values.put("%colorvalue", list_color);
						
						List<String> list_shape = slot_values.get("%shapevalue");
						if(list_shape == null)
							list_shape = new ArrayList<String>();
						list_shape.add(((String)record.getSpecificLabel("shape")).toLowerCase());
						slot_values.put("%shapevalue", list_shape);
					}
				}
				
				logger.debug("Visual List [size="+this.visualRecords.size()+"]: " + this.visualRecords);
				
				return true;
			}
		}
		catch(Exception e){
			this.logger.error(e.getMessage());
		}
		
		return false;
	}
	
	public void setEvalVisualList(List<String> fileList){
		if(this.evalVsiualList == null)
			this.evalVsiualList = new ArrayList<VisualObject>();
		
		for(VisualObject obj: visualRecords){
			String filename = obj.getFileName();
			if(fileList.contains(filename))
				this.evalVsiualList.add(obj);
		}
		
		logger.info("evalList size: " + evalVsiualList.size());
	}

	/**
	 * Initial classifiers based on attribute txt file "resource/classifiertypes.txt"
	 */
	public void initialClassifiers(boolean isSavable){
		if(clAttrs == null)
			clAttrs = new ConceptMap();
			
		if(classMap != null){
			classMap.clear();
			classMap = null;
		}
			
		classMap = new HashMap<String,TestClassifierCollection>();
		HashMap<String, List<String>> cptMap =  clAttrs.getOntologies();
		Iterator<Map.Entry<String, List<String>>> iterator = cptMap.entrySet().iterator();
		while(iterator.hasNext()){
			 Map.Entry<String, List<String>> pair = (Map.Entry<String, List<String>>) iterator.next();
			 String ontology = (String)pair.getKey();
			 List<String> cptList = (List<String>)pair.getValue();
			 this.addNewCollection(ontology,cptList, isSavable);
		}
		
		logger.debug("classMap: " + classMap);
		logger.debug("clAttrs: " + clAttrs);
		logger.debug("cptMap: " + cptMap);
		
		logger.info("Initial Classifiers: Done");
	}	
	
	/**
	 * Create new classifier collection based on ontology -- "color", "shape" and etc.
	 * @param ontology is the category of attributes
	 * @param cptList is list of attributes
	 */
	private void addNewCollection(String ontology, List<String> cptList, boolean isSavable){
		TestClassifierCollection collection = new TestClassifierCollection(ontology, this.matLink);
		
		for(String s : cptList){
			IncremClassifier classifier = new IncremClassifier(s, isSavable);
//			// // logger.debug("Classifier: " + ((SGD)classifier.getClassifier()));
			collection.addNewClassifier(s, classifier);
		}
		classMap.put(ontology, collection);
	}
	
	
	@Override
	public Map<String, Integer> getRLStates(BasicObject object) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Map<String, Queue<Concept>> receiveNewObject(String fileName) {
		for(VisualObject obj: this.visualRecords){
			if(obj.getFileName().equals(fileName.trim()))
				return getCLassifictionResults(obj);
		}

		return null;
	}
	
	public Map<String, Queue<Concept>> getCLassifictionResults(){
		if(this.currentObj != null)
			return getCLassifictionResults(this.currentObj);
		return predictMap;
	}
	
	@Override
	public Map<String, Queue<Concept>> getCLassifictionResults(BasicObject vObj) {
		Map<String, Queue<Concept>> results = new HashMap<String, Queue<Concept>>();
		
		this.currentObj = (VisualObject) vObj;
		
		TestClassifierCollection colorClassifiers =this.classMap.get("color");
		TestClassifierCollection shapeClassifiers =this.classMap.get("shape");
		
		if(this.currentObj != null){
			if(colorClassifiers.doClassify(this.currentObj)){
				Map<String, Double> predictMap = colorClassifiers.getResultMap();
				Map<String, Double> sortedMap = MapToolKit.getInstance().sortByComparator(predictMap, true);
				
				Queue<Concept> cptQueue = new LinkedList<Concept>();
				Iterator<Entry<String, Double>> iterator = sortedMap.entrySet().iterator();
				while(iterator.hasNext()){
					Entry<String, Double> pair = iterator.next();
					String attribute = pair.getKey();
					double prob = pair.getValue();
					cptQueue.add(new Concept("color", attribute, prob));
				}
				results.put("color", cptQueue);
			}
			else
				logger.error("cannot classify the visual object of " + this.currentObj.getFileName());
				
			
			if(shapeClassifiers.doClassify(this.currentObj)){
				Map<String, Double> predictMap = shapeClassifiers.getResultMap();
				Map<String, Double> sortedMap = MapToolKit.getInstance().sortByComparator(predictMap, true);
				
				Queue<Concept> cptQueue = new LinkedList<Concept>();
				Iterator<Entry<String, Double>> iterator = sortedMap.entrySet().iterator();
				while(iterator.hasNext()){
					Entry<String, Double> pair = iterator.next();
					String attribute = pair.getKey();
					double prob = pair.getValue();
					cptQueue.add(new Concept("shape", attribute, prob));
				}
				results.put("shape", cptQueue);
			}
			else
				logger.error("cannot classify the visual object of " + this.currentObj.getFileName());
		}
		else
			logger.error("cannot find current visual object...");
		
		return results;
	}
	
	@Override
	public boolean updateClassifier(String ontology, String conceptName) {
		if(this.currentObj instanceof VisualObject){
			
			VisualObject copy = ((VisualObject)this.currentObj).clone();
			copy.clearLabels();
			copy.setLabels(conceptName);
			
			TestClassifierCollection classifiers = this.classMap.get(ontology);
			
			return classifiers.trainModle(copy);
		}
		
		return false;
	}
	
	@Override
	public void reset() {
		this.currentObj = null;
		this.resetClassifier();
	}
	
	public void resetClassifier() {
		TestClassifierCollection colorClassifiers =this.classMap.get("color");
		colorClassifiers.resetClassifier();
		TestClassifierCollection shapeClassifiers =this.classMap.get("shape");
		shapeClassifiers.resetClassifier();
	}

	@Override
	public void eval(int numOfObject, int nfold, double cost) {
	}

	/******************* Unused Fucntion ************************/
	@Override
	public void extractFeature(BufferedImage buff) {
	}
	
//	public static void main(String[] args){
//		TeachbotVisionModule vm = new TeachbotVisionModule();
//	}
}
