package voila.record;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.log4j.Logger;

public class Transcription {
	Logger logger = Logger.getLogger(Transcription.class);
	private static final String dialog_txt_file = "dialogues-";
	private Transcription instance;
	
	private String transcript_path;
	private String path;
	
	public Transcription(String path){
		String localDate = new SimpleDateFormat("yyy_MM_dd").format(Calendar.getInstance().getTime());
		this.transcript_path = path + "/" + localDate +"/";
		logger.info("transcription path is: " + transcript_path);
		
		File dir = new File(this.transcript_path);
		if(!dir.exists())
			dir.mkdirs();
	}
	
	public Transcription getInstance(){
		return this.instance;
	}
	
	public void writeToTranscriptFile(String name, String script) throws IOException{
		File newfile = new File(this.path);
		FileWriter fileWriter = new FileWriter(new File(path), true);
		if(!newfile.exists()){
			newfile.createNewFile();
			fileWriter.write(name + ": " + script+"\r\n");
		}
		else
			fileWriter.append(name + ": " + script+"\r\n");

		fileWriter.flush();
        fileWriter.close();
	}

	public void appendNewLineTotFile() throws IOException {
		File newfile = new File(path);
		FileWriter fileWriter = new FileWriter(new File(path), true);
		if(!newfile.exists()){
			newfile.createNewFile(); 
			fileWriter.write("\r\n");
		}
		else
			fileWriter.append("\r\n");
		
		fileWriter.flush();
        fileWriter.close();
	}
	
	public void startNewExpt(){
		String timeStamp = new SimpleDateFormat("HH-mm-ss").format(Calendar.getInstance().getTime());
		
		this.path = this.transcript_path + dialog_txt_file + timeStamp +".txt"; 
		logger.info("new timeStamp for experiment: " + this.path);
	}
}
