package voila.im;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Random;

import org.apache.log4j.Logger;
import org.yaml.snakeyaml.Yaml;

import babble.dialogue.DSDialogueAgent;
import babble.dialogue.IncrementalDialogueAgent;
import babble.vision.classification.Concept;
import babble.vision.gui.DSTypes;
import babble.vision.object.VisualObject;
import babble.vision.tools.MapToolKit;
import burlap.behavior.singleagent.learning.tdmethods.QLearningStateNode;
import burlap.behavior.valuefunction.QValue;
import burlap.mdp.core.action.Action;
import burlap.mdp.core.state.MutableState;
import burlap.mdp.core.state.State;
import burlap.multilayer.adaptive.AdaptiveBasicState;
import burlap.multilayer.controller.ControlBasicState;
import burlap.multilayer.controller.ControlLearningDomain;
import burlap.multilayer.controller.ControlLearningDomain.ContextType;
import burlap.statehashing.HashableState;
import burlap.statehashing.HashableStateFactory;
import burlap.statehashing.simple.SimpleHashableStateFactory;
import core.basics.ActionName;
import core.basics.Constants;
import core.basics.DataShare;
import core.basics.DataShare.CostType;
import extension.core.SLUResponder;
import ngram.basics.UnAvailableException;
import ngram.core.component.NLGTemplate;
import qmul.ds.Utterance;
import qmul.ds.dag.UtteredWord;
import qmul.ds.formula.TTRRecordType;
import qmul.ds.test.rules.DyLanParser;
import qmul.ds.test.rules.DyLanParser.ParseForm;
import qmul.ds.tree.Tree;
import qmul.ds.tree.label.Label;
import voila.gui.mvc.DataController;
import voila.gui.mvc.Observer;
import voila.record.Transcription;
import voila.vision.TeachbotVisionModule;

public class DialogManager extends IncrementalDialogueAgent implements Observer, Runnable{
	Logger logger = Logger.getLogger(DialogManager.class);
	
	protected Queue<UtteredWord> toWords = new LinkedList<UtteredWord>();
	private static final String qTableName1= "data/Domains/teachbot/rl-policy/adaptivePolicy.ser";
	private static final String qTableName2 = "data/Domains/teachbot/rl-policy/controllerPolicy.ser";
	private static final String templatePath = "data/Domains/teachbot/nlgTemplate.ser";
	private static final String sluPath = "data/Domains/teachbot/sluModel.ser";
	
	public final static String ENGLISHTTRURL = "resource/2017-english-ttr-copula-simple/";
	public final String ACTMAP = "act-mappings.txt";
	
	private HashableStateFactory hashingFactory; 
	private Map<HashableState, QLearningStateNode> qFunctionforAdaptive;
	private Map<HashableState, QLearningStateNode> qFunctionforController;
	
	private AdaptiveBasicState curAState;
	private ControlBasicState curCState;

	private SLUResponder sluResponder;
	private DyLanParser dlParser;
	private Map<String, List<String>> act_map;
	private String pre_action_from_sys = null;
	
	private TeachbotVisionModule vModule;
	private NLGTemplate generator;
	private DataShare dataShare;
	private Transcription transcription;
	private DataController data_controller;

	private Map<ActionName, String> actMap;
	private String[] initiatives = {"tutor","learner"};
	private String[] misunderstand_utts = {"sorry, i guess i misunderstood what you saying.", 
										   "sorry, what did you say?", 
										   "sorry, i don't understand what you said. can you rephrase that?"};
	
	private int binUnit = 10;
	private int numOfInstances = 0;
	private double preAccuracy = 0.0;
	private int deltaAcc = 0;
	private int posiThreshold = 95;
	
	private boolean isSavable = false;
	
	protected VisualObject curObj;
	
	private boolean hasAdaptiveThreshold = true;

	private boolean isExptRunning = false;
	private ParserType parser_type;
	
	public DialogManager(String name, DataShare dataShare, List<String> evalFileList, DataController data_controller, boolean hasAdaptiveThreshold, boolean isSavable, ParserType parser_type) {
		super(name);

		this.isSavable = isSavable;
		this.parser_type = parser_type;
		
		this.initialDM();
		this.initialDialogActions();
		this.registerVisionModel(evalFileList, this.isSavable);

		this.dataShare = dataShare;
		this.hasAdaptiveThreshold = hasAdaptiveThreshold;
		this.data_controller = data_controller;
		this.data_controller.registerObserver(this);
	}

	/************************** Initialization **************************/
	public void registerVisionModel(List<String> list, boolean isSavable) {
		if(vModule == null)
			vModule = new TeachbotVisionModule();
		else
			vModule.reset();
		
		vModule.initialClassifiers(isSavable);
		vModule.setEvalVisualList(list);
	}

	private void initialDM(){
		// load QFucntion for Agent
		this.hashingFactory = new SimpleHashableStateFactory();
		this.loadQTable("adaptive", qTableName1);
		this.loadQTable("controller", qTableName2);
		
		// load nlg template;
		this.generator = new NLGTemplate(templatePath);
		
		// initial a semantic parser
		switch(this.parser_type){
			case DS_TTR:
				this.initialDyLanParser(null);
				break;
			case SIMPLE_SLU:
				// load simple slu model
				this.sluResponder = new SLUResponder();
				boolean isSLULoaded = this.sluResponder.loadSLUModeltoFile(sluPath);
				logger.info("isSLULoaded: " + isSLULoaded);
				break;
		}
	}
	
	private void initialDyLanParser(String english_ttr_url) {
		if(english_ttr_url == null)
			english_ttr_url = this.ENGLISHTTRURL;
		
		dlParser = new DyLanParser(english_ttr_url, null);
		this.act_map = this.loadActMappings(english_ttr_url);
	}
	
	private Map<String, List<String>> loadActMappings(String english_ttr_url) {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		
		BufferedReader br = null;
	    String line = "";
	    String splitBy = ">>";
	        
	    try {
			String path = english_ttr_url + this.ACTMAP;
			br = new BufferedReader(new FileReader(path));
			while ((line = br.readLine()) != null) {
				line = line.trim().toLowerCase();
				if(!line.isEmpty() && !line.startsWith("//")){
					String[] items = line.split(splitBy);
					String key = items[0].trim();
					
					String value = items[1].trim();
					value = value.replace("[", "").replace("]", "");
					String[] arr = value.split(",");
					
					map.put(key, Arrays.asList(arr));
				}
			}
			return map;
		} catch (IOException e) {logger.error("Error: " + e.getMessage());}
		finally{
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
		               e.printStackTrace();
		           }
		       }
		}
		
		return null;
	}
	
	private void initialDialogActions(){
		if(this.actMap == null)
			this.actMap = new HashMap<ActionName, String>();
		else
			this.actMap.clear();
		
		this.actMap.put(ActionName.LISTENING, "listen()");
		this.actMap.put(ActionName.ASKWH_COLOR, "ask(color)");
		this.actMap.put(ActionName.ASKWH_SHAPE, "ask(shape)");
		this.actMap.put(ActionName.POLAR_COLOR, "polar(color)");
		this.actMap.put(ActionName.POLAR_SHAPE, "polar(shape)");
		this.actMap.put(ActionName.POLAR_COLORSHAPE, "polar(color&shape)");
		this.actMap.put(ActionName.ACK, "ack()");
		this.actMap.put(ActionName.ACK_COLOR, "ackrepeat(color)");
		this.actMap.put(ActionName.ACK_SHAPE, "ackrepeat(shape)");
		this.actMap.put(ActionName.UNKNOWN, "donotknow()");
		this.actMap.put(ActionName.UNKNOWN_COLOR, "donotknow(color)");
		this.actMap.put(ActionName.UNKNOWN_SHAPE, "donotknow(shape)");
		this.actMap.put(ActionName.REJECT, "donotknow()");
		this.actMap.put(ActionName.KEEPGO, "keepgoing()");
//		this.actMap.put(ActionName.REPEAT, "nothing()");
//		this.actMap.put(ActionName.NEXT, "next()");
	}
	
	public void registerTranscriptWriter(String path){
		this.transcription = new Transcription(path);
	}

	/********************* Burlap Policy ************************/
	@SuppressWarnings("unchecked")
	private void loadQTable(String type, String path) {
		Yaml yaml = new Yaml();
		try {
			if(type != null && type.equals("adaptive")){
				System.err.println("path of Adaptive : " + path);
				this.qFunctionforAdaptive = (Map<HashableState, QLearningStateNode>)yaml.load(new FileInputStream(path));
//				this.printQFunction(qFunctionforAdaptive);
			}
			
			else if(type != null && type.equals("controller")){
				System.err.println("path of Controller : " + path);
				this.qFunctionforController = (Map<HashableState, QLearningStateNode>)yaml.load(new FileInputStream(path));
				this.printQFunction(qFunctionforController);
			}
			
		} catch(FileNotFoundException e) {
			logger.error(e.getMessage());
		}
	}
	
	private void printQFunction(Map<HashableState, QLearningStateNode> qTable){
		logger.info("Q-Table ---------- (" + qTable.size() + ") ------------");
		Iterator<Entry<HashableState, QLearningStateNode>> iterator = qTable.entrySet().iterator();
		while(iterator.hasNext()){
			Entry<HashableState, QLearningStateNode> entry = iterator.next();
			HashableState state = (HashableState) entry.getKey();	
			
			State ss = state.s();
			if(ss instanceof ControlBasicState)
				logger.info("state: " + ((ControlBasicState)ss).toString());

			if(ss instanceof AdaptiveBasicState)
				logger.info("state: " + ((AdaptiveBasicState)ss).toString());
						
			QLearningStateNode node = entry.getValue();
			String result= "";
			List<QValue> sorted=new ArrayList<QValue>(node.qEntry);
			Map<String, Double> qMap = new HashMap<String, Double>();
			for(QValue qv: sorted)
			{
//				result+=qv.a+"->"+qv.q+", ";	
				qMap.put(qv.a.toString(), qv.q);
			}
			
			qMap = MapToolKit.getInstance().sortByComparator(qMap, true);
			
			Iterator<Entry<String,Double>> q_iterator = qMap.entrySet().iterator();
			int i=0;
			while(q_iterator.hasNext()){
				Entry<String,Double> pair = q_iterator.next();
//				if(i < 2){
					String key = pair.getKey();
					double value = pair.getValue();
					result+= key +"->"+ String.format("%.3f", value) +", ";
//				}
				i++;
			}
			
			result=result.substring(0, result.length()-2)+"}\n";
		}
	}

	
	/**
	 * Search for the best action from the q-value table
	 * @param state current RL state
	 * @return next action
	 */
	private Action getBestActionFromQTable(MutableState state) {
		if(state != null){
			
			QLearningStateNode node = null;
			if(state instanceof AdaptiveBasicState){
				logger.info((AdaptiveBasicState)state);
				HashableState hashState = this.hashingFactory.hashState((AdaptiveBasicState)state);
				logger.debug("this.hashState: " + hashState.toString());
				logger.debug("this.hashState: " + hashState.s());
				
				node = this.qFunctionforAdaptive.get(hashState);
				logger.debug("node: " + node);
			}
				
			else if(state instanceof ControlBasicState){
				System.err.println("ControlBasicState: " + (ControlBasicState)state);
				
				logger.info((ControlBasicState)state);
				HashableState hashState = this.hashingFactory.hashState((ControlBasicState)state);
				logger.debug("this.hashState: " + hashState.toString());
				logger.debug("this.hashState: " + hashState.s());
				
				node = this.qFunctionforController.get(hashState);
				logger.debug("node: " + node);
			}
						
			if(node == null)
				return null;
			
			List<QValue> qList=new ArrayList<QValue>(node.qEntry);
			
			Map<Action, Double> qActMap = new HashMap<Action, Double>();
			for(QValue qv: qList)
				qActMap.put(qv.a, qv.q);
			
			Map<Action, Double> sorted = MapToolKit.getInstance().sortActionMapByComparator(qActMap, true);
			
			return MapToolKit.getInstance().getTopKeyFromActionMap(sorted);
		}
		return null;
	}
	
	/************************ START WITH DIALOGUE MANAGEMENT *******************************/
	private int colorState =  -1;
	private int shapeState = -1;
	private String preDAts = null;
	private String lastAction = "Null()";
	
	
	/**
	 * Initial the State for Dialogue Control MDP based 
	 * on the visual classification results
	 * @param prediction the visual classification results with confidence scores
	 */
	private void initialStatesByPrediction(Map<String, Queue<Concept>> prediction) {
		int colorStateLevel = -1;
		int shapeStateLevel = -1;
		Iterator<Entry<String, Queue<Concept>>> iterator = prediction.entrySet().iterator();
		while(iterator.hasNext()){
			Entry<String, Queue<Concept>> pair = iterator.next();
			String ontology = pair.getKey();
			Concept cpt = pair.getValue().peek();

			double posThreshold =  (double)this.posiThreshold / (double)100;
					
			double score = cpt.getConfidenceScore();
			if(ontology.equals("color")){
				if(score > posThreshold)
					colorStateLevel = 2;
				else if(score <= 0.5)
					colorStateLevel = 0;
				else if(0.5 < score &&  score<= posThreshold)
					colorStateLevel = 1;
			}
			
			else if(ontology.equals("shape")){
				if(score > posThreshold)
					shapeStateLevel = 2;
				else if(score <= 0.5)
					shapeStateLevel = 0;
				else if(0.5 < cpt.getConfidenceScore() &&  score<= posThreshold)
					shapeStateLevel = 1;
			}
		}
		
		if(colorStateLevel == 2){
			Concept cpt = prediction.get("color").peek();
			this.vModule.updateClassifier("color", cpt.getConceptName());
		}
		
		if(shapeStateLevel == 2){
			Concept cpt = prediction.get("shape").peek();
			this.vModule.updateClassifier("shape", cpt.getConceptName());
		}
		
		curCState = new ControlBasicState(colorStateLevel, shapeStateLevel, "Null()", "U");
		logger.debug("initial state: " + curCState);
		
		// either take the initiative or release the floor
		int index = new Random().nextInt(this.initiatives.length);
		String initiative = initiatives[index];
		
		if(initiative.equals("learner")){
			try {
				this.take_initiative(prediction);
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		}
		else{
//			String utterance = "";
			
			this.data_controller.setBehaviorUpdated(Behavior.LOOK_UP);
//	        this.data_controller.setSpeechOutputUpdated(utterance);
		}
	}
	
	@SuppressWarnings("unused")
	private void take_initiative(Map<String, Queue<Concept>> predictions) throws IOException{
		Action bestAction = this.getBestActionFromQTable(this.curCState);
		logger.debug("selected Action's Name = " + bestAction.actionName());
		
		String utterance = "";
		String dAt = "";
		if(bestAction != null){
			dAt = actMap.get(ActionName.valueOf(bestAction.actionName()));
			lastAction = dAt;
			utterance = this.generator.searchUttbyAction(dAt);
			logger.info("dAt: " + dAt + "; best response: " + utterance);
				
			Iterator<Entry<String, Queue<Concept>>> iterator_prediction = predictions.entrySet().iterator();
			while(iterator_prediction.hasNext()){
				Entry<String, Queue<Concept>> entry = iterator_prediction.next();
				String keyword = entry.getKey().toLowerCase();
				Concept cpt = entry.getValue().peek();
				utterance = utterance.replace("%"+keyword+"value", cpt.getConceptName().toLowerCase());
			}
			
			if(dAt.startsWith("polar(") && !utterance.contains("?"))
				utterance += "? ";
			
			if(!dAt.trim().equals("listen")){
				String subUtt = "wait a second,";
				String[] words = subUtt.split(" ");
				for(int i=0; i < words.length; i++){
					this.addWord(words[i]);
				}
				this.addWord("<pause>");
				
				this.data_controller.setSpeechOutputUpdated(subUtt, dAt);
			}
		}
		
		logger.info("DAt: " + dAt + " -- L: " + utterance);
        this.data_controller.setSpeechOutputUpdated(utterance, dAt);
        
		if(this.transcription != null)
			this.transcription.writeToTranscriptFile(this.name, utterance);
		
		String[] words = utterance.split(" ");
		for(int i=0; i < words.length; i++){
			this.addWord(words[i]);
		}
		this.addWord(DSDialogueAgent.RELEASE_TURN);
		logger.info("this.uttered: " + this.toWords);
		this.checkDAtFromUtt_ds("sys", utterance + " ");

		long time=new Date().getTime();
		while(time-lastCharTime<this.delay){
//			logger.info("wait...");
			time=new Date().getTime();
		}
		
		this.data_controller.setBehaviorUpdated(Behavior.LOOK_UP);
	}
	
	
	/**
	 * Check the dialogue act on particular utterance
	 * @param speaker current speaker, either system or human user
	 * @param utterance current utterance
	 * @return dialogue act
	 */
	private String checkDAtFromUtt(String speaker, String utterance){
		data_controller.setSpeechInputUpdated(utterance.replace("\n", ""), "usr");
		
		logger.debug("utterance : " + utterance);
		Queue<String> intentQueue = this.sluResponder.getResponder(utterance);		
		logger.debug("intentQueue : " + intentQueue);
		String dAtFromTutor = this.sluResponder.covertToDAts2(intentQueue);
		logger.debug("dAtFromTutor : " + dAtFromTutor);
		
		return dAtFromTutor;
	}
	
	public String checkDAtFromUtt_ds(String speaker, String utterance) {
		if (speaker.equals("usr"))
			data_controller.setSpeechInputUpdated(utterance.replace("\n", ""), "usr");
		
		Queue<String> speech_acts = new LinkedList<String>();
		TTRRecordType f = null;
		
		String text = speaker + ": " + utterance + "<rt>";
		text = text.replaceAll("  ", " ");
		
		Utterance utt = new Utterance(text);
		logger.debug("=================================================================");
		
		for(int i=0; i< utt.getTotalNumberOfSegments(); i++){
			String segment = utt.getUttSegment(i);
			ParseForm result = null;
			
			if(!segment.contains("<rt>") && !segment.contains(".") && !segment.contains("?"))
				segment += ".";
			
			logger.debug("++++ segment: " + utt.getSpeaker() + ": " + segment);
			String[] words = segment.split(" ");
			for(int j=0; j < words.length; j++){
				String word = utt.getSpeaker() + ": " + words[j];
				result = dlParser.parse(word);
			}
			
			if(result.hasException()){
				logger.warn(result.getException());
			}
			
			Tree resultTree = result.getContxtalTree().clone();
			logger.debug("  --> " + resultTree.getPointedNode());
			if(resultTree.getPointedNode() != null){
				f = (TTRRecordType) resultTree.getPointedNode().getFormula();
				for(Label l: resultTree.getPointedNode()){
					String label_str = l.toString().trim();
					if(label_str.startsWith("sa:") && label_str.contains(utt.getSpeaker())){
						if(!speech_acts.contains(label_str.trim()))
							speech_acts.add(label_str.trim());
						break;
					}
				}
			}
		}
		
		data_controller.updateTTRRecordType(f, speech_acts.toString(), DSTypes.semanticTTR);
		String actions = this.convertToDAts(speech_acts);
		
		return actions;
	}

	private String convertToDAts(Queue<String> queue){
		String dats = "";
    	logger.debug("queue: " + queue);
    	
    	if(queue != null){
    		int index = 0;
    		while(!queue.isEmpty()){
        		String speech_act = queue.poll();
        		// removing the sa tag
        		speech_act = speech_act.replace("sa:", "");
        		// removing speaker
        		speech_act = speech_act.replace("sys,", "").replaceAll("usr,", "")
        				.replaceAll("sys", "").replaceAll("usr", "");
        		// combining different slot values and change ":" to equals
        		speech_act = speech_act.replaceAll(",", "&").replaceAll(":", "=");
        		// combining different slot values
        		speech_act = speech_act.replaceAll(",", "&");
        		// removing all spaces
        		speech_act = speech_act.replaceAll(" ", "");
        		
        		if(this.pre_action_from_sys != null && (this.pre_action_from_sys.startsWith("info") || pre_action_from_sys.startsWith("polar"))){
        			String pre_act = this.pre_action_from_sys.replaceAll("info", "").replaceAll("polar", "");
            		logger.debug(" ---1----> pre_act = " + pre_act);
            		logger.debug(" ---1----> speech act = " + speech_act);
        			
        			if(speech_act.startsWith("info-neg")){
        				String act_str = speech_act.replace("info-neg", "");
                		logger.debug(" ---1----> act_str = " + act_str);
        				if(act_str.trim().equals(pre_act.trim()))
							speech_act = "reject()";
        			}
        			
        			else if(speech_act.startsWith("info")) {
        				String act_str = speech_act.replace("info", "");
                		logger.debug(" ---2----> act_str = " + act_str);
        				if(act_str.trim().equals(pre_act.trim()))
							speech_act = "ack()";
        			}
        		}

        		if(speech_act.startsWith("accept"))
        			speech_act = "ack()";

        		if(speech_act.startsWith("acceptcolor"))
        			speech_act = "ack(color)";

        		if(speech_act.startsWith("acceptshape"))
        			speech_act = "ack(shape)";

        		if(speech_act.startsWith("askcolor"))
        			speech_act = "ask(color)";

        		if(speech_act.startsWith("askshape"))
        			speech_act = "ask(shape)";

        		if(speech_act.startsWith("rejectcolor"))
        			speech_act = "reject(color)";

        		if(speech_act.startsWith("reject"))
        			speech_act = "reject(shape)";

        		if(speech_act.startsWith("openask"))
        			speech_act = "openAsk()";
        		
        		if(speech_act.equals("Repeatrequest"))
        			speech_act = "Repeat()";
        		
        		logger.debug(" ---2----> speech act = " + speech_act);
        			
        		if(index++ == 0)
    				dats += speech_act.substring(0, 1).toUpperCase() + speech_act.substring(1);
    			else
    				dats += "&&" + speech_act.substring(0, 1).toUpperCase() + speech_act.substring(1);
    		}
    		
    		if(dats.startsWith("Ack()&&Reject(shape") || dats.startsWith("Ack()&&Infoneg(shape")){
    			dats = dats.replace("Ack()&&", "ack(color)&&");
    		}
    		else if(dats.startsWith("Ack()&&Reject(color") || dats.startsWith("Ack()&&Infoneg(color")){
    			dats = dats.replace("Ack()&&", "Ack(shape)&&");
    		}
    		
    		logger.debug(" -------> dats = " + dats);
    		
    		return dats;
    	}
    	
    	return null;
	}
	
	
	private ControlBasicState learnInstanceforNewEllipse(ControlBasicState curState, String dAtFromTutor, String dAtFromAgent, Map<String, Queue<Concept>> prediction) throws UnAvailableException {
		if(curCState != null && dAtFromTutor != null && dAtFromAgent != null && prediction != null){
			logger.info("previous dialogue-act from the agent: " + dAtFromAgent);
			logger.info("previous dialogue-act from the user: " + dAtFromTutor);
			
			double totalCost = 0;//dataShare.getScoreByType(CostType.Turn);
			ControlBasicState copy = (ControlBasicState) curCState.copy();
						
			ContextType contextType = null;
			String context = dAtFromAgent.substring(dAtFromAgent.indexOf("("));
			if(context != null){
				if(context.trim().equals("()"))
					contextType = ContextType.U;
				else if(context.trim().equals("(color)"))
					contextType = ContextType.C;
				else if(context.trim().equals("(shape)"))
					contextType = ContextType.S;
				else if(context.trim().equals("(color&shape)"))
					contextType = ContextType.CS;
			}
			logger.debug("contextType: " + contextType);
			
			copy.set(ControlLearningDomain.PRECONTEXT, contextType.toString());
	
			String dAts = ""; 
			String ackContext = null;
			String[] actions = dAtFromTutor.split(Constants.ACTSPLITER);
			for(int i=0; i < actions.length; i++){
				String subAct = actions[i];
					
				if(subAct.contains("Ack(")){
					totalCost += dataShare.getScoreByType(CostType.Acknowledge);
						
					switch(contextType){
						case C:
							copy.set(ControlLearningDomain.COLORSTATE, 2);
							ackContext = "(color)";
	
							if(!dataShare.checkUpdateFlagByOntology("color")){
								Concept cpt0 = prediction.get("color").peek();
								logger.debug("retrain classifiers on " + cpt0.getConceptName());
								
								this.vModule.updateClassifier("color", cpt0.getConceptName());
								dataShare.enableUpdateFlagByOntology("color");
							}
							
							break;
						case CS:
							
							if(subAct.equals("Ack()")){
								if(!dataShare.checkUpdateFlagByOntology("color")){
									Concept cpt1 = prediction.get("color").peek();
									logger.debug("retrain classifiers on " + cpt1.getConceptName());
									
									this.vModule.updateClassifier("color", cpt1.getConceptName());
									dataShare.enableUpdateFlagByOntology("color");
								}
								copy.set(ControlLearningDomain.COLORSTATE, 2);
		
								if(!dataShare.checkUpdateFlagByOntology("shape")){
									Concept cpt2 = prediction.get("shape").peek();
									logger.debug("retrain classifiers on " + cpt2.getConceptName());
									
									this.vModule.updateClassifier("shape", cpt2.getConceptName());
									dataShare.enableUpdateFlagByOntology("shape");
								}
								copy.set(ControlLearningDomain.SHAPESTATE, 2);
								
								ackContext = "(color&shape)";
							}
								
							else if(subAct.equals("Ack(color)")){
								if(!dataShare.checkUpdateFlagByOntology("color")){
									Concept cpt1 = prediction.get("color").peek();
									logger.debug("retrain classifiers on " + cpt1.getConceptName());
									
									this.vModule.updateClassifier("color", cpt1.getConceptName());
									dataShare.enableUpdateFlagByOntology("color");
								}
								copy.set(ControlLearningDomain.COLORSTATE, 2);

								ackContext = "(color)";
							}
							
							else if(subAct.equals("Ack(shape)")){
								if(!dataShare.checkUpdateFlagByOntology("shape")){
									Concept cpt2 = prediction.get("shape").peek();
									logger.debug("retrain classifiers on " + cpt2.getConceptName());
									
									this.vModule.updateClassifier("shape", cpt2.getConceptName());
									dataShare.enableUpdateFlagByOntology("shape");
								}
								copy.set(ControlLearningDomain.SHAPESTATE, 2);

								ackContext = "(shape)";
							}
							break;
						case S:
							copy.set(ControlLearningDomain.SHAPESTATE, 2);
							ackContext = "(shape)";
	
							if(!dataShare.checkUpdateFlagByOntology("shape")){
								Concept cpt3 = prediction.get("shape").peek();
								logger.debug("retrain classifiers on " + cpt3.getConceptName());
								
								this.vModule.updateClassifier("shape", cpt3.getConceptName());
								dataShare.enableUpdateFlagByOntology("shape");
							}
							break;
					default:
						break;
					}
				}
	
				if(subAct.contains("Reject(")){
					totalCost += dataShare.getScoreByType(CostType.Reject);
				}
					
				if(subAct.contains("color=")){
					logger.debug("ackContext: " + ackContext);
					if(ackContext == null || ackContext.equals("(shape)")){
						copy.set(ControlLearningDomain.COLORSTATE, 2);
						String label = subAct.substring(subAct.indexOf("color=")+6,subAct.indexOf(")"));
						logger.debug("color = " + label);
	
						if(!dataShare.checkUpdateFlagByOntology("color")){
							this.vModule.updateClassifier("color", label);
							logger.debug("retrain color classifiers on " + label);
							
							dataShare.enableUpdateFlagByOntology("color");
						}
						
						totalCost += dataShare.getScoreByType(CostType.Inform);
						subAct = subAct.replace("="+label, "");
						logger.debug("subAct: " + subAct + "-- label: " + label);
						
						dAtFromAgent = dAtFromAgent.replace("="+label, "%colorvalue");
					}
					else
						logger.debug("color has been updated");
				}
					
				if(subAct.contains("shape=")){
					logger.debug("ackContext: " + ackContext);
					if(ackContext == null || ackContext.equals("(color)")){
						copy.set(ControlLearningDomain.SHAPESTATE, 2);
						String label = subAct.substring(subAct.indexOf("shape=")+6,subAct.indexOf(")"));
						logger.debug("shape = " + label);
	
						if(!dataShare.checkUpdateFlagByOntology("shape")){
							logger.debug("retrain shape classifiers on " + label);
							
							this.vModule.updateClassifier("shape", label);
							dataShare.enableUpdateFlagByOntology("shape");
						}
						
						totalCost += dataShare.getScoreByType(CostType.Inform);
						subAct = subAct.replace("="+label, "");

						logger.debug("subAct: " + subAct + "-- label: " + label);
						
						dAtFromAgent = dAtFromAgent.replace("="+label, "%shapevalue");
					}
					else
						logger.debug("shape has been updated");
				}

				if(i == 0)
					dAts += subAct;
				else
					dAts += Constants.ACTSPLITER + subAct;
			}
			
			logger.debug("current dAts = " + dAts);
			logger.debug("dAtFromAgent: " + dAtFromAgent + "; dAts: " + dAts);
			
			if(dAtFromAgent.contains("Polar(")){
				if(!(dAts.contains("Ack") || dAts.contains("Reject"))){
					switch(contextType){
					case C:
						if(!dAts.contains("Info(color)")){
							if(!dataShare.checkUpdateFlagByOntology("color")){
								Concept cpt1 = prediction.get("color").peek();
								logger.debug("retrain color classifiers on " + cpt1.getConceptName());
								
								this.vModule.updateClassifier("color", cpt1.getConceptName());
								dataShare.enableUpdateFlagByOntology("color");
							}
							copy.set(ControlLearningDomain.COLORSTATE, 2);
						}
						break;
					case CS:
						if(!dAts.contains("Info(color)")){
							if(!dataShare.checkUpdateFlagByOntology("color")){
								Concept cpt2 = prediction.get("color").peek();
								logger.debug("retrain color classifiers on " + cpt2.getConceptName());
								
								this.vModule.updateClassifier("color", cpt2.getConceptName());
								dataShare.enableUpdateFlagByOntology("color");
							}
							copy.set(ControlLearningDomain.COLORSTATE, 2);
						}
						
						if(!dAts.contains("Info(shape)")){
							if(!dataShare.checkUpdateFlagByOntology("shape")){
								Concept cpt3 = prediction.get("shape").peek();
								logger.debug("retrain shape classifiers on " + cpt3.getConceptName());
							
								this.vModule.updateClassifier("shape", cpt3.getConceptName());
								dataShare.enableUpdateFlagByOntology("shape");
							}
							copy.set(ControlLearningDomain.SHAPESTATE, 2);
						}
						break;
					case S:
						if(!dAts.contains("Info(shape)")){							
							if(!dAts.contains("Info(shape)")){
								if(!dataShare.checkUpdateFlagByOntology("shape")){
									Concept cpt4 = prediction.get("shape").peek();
									logger.debug("retrain shape classifiers on " + cpt4.getConceptName());
									
									this.vModule.updateClassifier("shape", cpt4.getConceptName());
									dataShare.enableUpdateFlagByOntology("shape");
								}
								copy.set(ControlLearningDomain.SHAPESTATE, 2);
							}
						}
						break;
					default:
						break;
					}
				}
			}
			
			if(dAtFromAgent.contains("Polar(color&shape)") && !dAts.contains("Ack(") && dAts.contains("Reject(")){
				logger.debug("############# Here(1)");
				
				if(!dAts.contains("Info(color)")){
					logger.debug("############# Here(2)");
					if(!dataShare.checkUpdateFlagByOntology("color")){
						Concept cpt1 = prediction.get("color").peek();
						logger.debug("retrain color classifiers on " + cpt1.getConceptName());
						
						this.vModule.updateClassifier("color", cpt1.getConceptName());
						
						
						dataShare.enableUpdateFlagByOntology("color");
					}
					copy.set(ControlLearningDomain.COLORSTATE, 2);
				}
				
				if(!dAts.contains("Info(shape)")){
					logger.debug("############# Here(3)");
					if(!dataShare.checkUpdateFlagByOntology("shape")){
						Concept cpt2 = prediction.get("shape").peek();
						logger.debug("retrain shape classifiers on " + cpt2.getConceptName());
						
						this.vModule.updateClassifier("shape", cpt2.getConceptName());
						dataShare.enableUpdateFlagByOntology("shape");
					}
					copy.set(ControlLearningDomain.SHAPESTATE, 2);
				}
			}
			
			dAts = dAts.replace("Info(color)&&Info(shape)", "Info(color&shape)").replace("Info(shape)&&Info(color)", "Info(color&shape)");
			
			// modify the precontext state by checking the latest dialog action
			String[] subactions = dAts.split(Constants.ACTSPLITER);
			logger.debug("processed actions: " + dAts + " -- " + actions.length);
			
			String finalAction = subactions[subactions.length-1];
			logger.debug("finalAction: " + finalAction);
			
			if(finalAction.trim().equals("")){
				throw new UnAvailableException("finalAction can't be empty at any points: " + dAts);
			}
			
			copy.set(ControlLearningDomain.PREDATS, finalAction);
			
			if(finalAction.startsWith("Info") || finalAction.startsWith("Ask") || finalAction.startsWith("Focus") || finalAction.startsWith("Clr")){
				String conditionValue = finalAction.substring(finalAction.indexOf("("));
				
				if(conditionValue.trim().equals("()"))
					contextType = ContextType.U;
				else if(conditionValue.trim().equals("(color)"))
					contextType = ContextType.C;
				else if(conditionValue.trim().equals("(shape)"))
					contextType = ContextType.S;
				else if(conditionValue.trim().equals("(color&shape)"))
					contextType = ContextType.CS;
	
				copy.set(ControlLearningDomain.PRECONTEXT, contextType.toString());
			}
			else if(finalAction.startsWith("ACK"))
				copy.set(ControlLearningDomain.PRECONTEXT, ContextType.U.toString());
			else
				logger.debug("nothing happend.");
	
			// reclassify the object after each turn
			this.reClassifyInstance();
			logger.debug("new prediction: " + this.prediction);
			
			dataShare.addTutorCost(totalCost);
			return (ControlBasicState) copy.copy();
		}
		
		return null;
	}
	
	/**
	 * Find the proper response to the user utterance
	 * @param utt utterance from a human user
	 * @return
	 * @throws IOException
	 * @throws UnAvailableException
	 */
	@SuppressWarnings("unused")
	public Queue<UtteredWord> findResponse(String utt) throws IOException {
		String dAt = "";
		if(utt != null && !utt.isEmpty()){
			// check the dialogue act for the utterance
			String dAtFromUsr = "";
			switch(this.parser_type){
			case DS_TTR:
				dAtFromUsr = this.checkDAtFromUtt_ds("usr", utt + " ");
				break;
			case SIMPLE_SLU:
				dAtFromUsr = this.checkDAtFromUtt("usr", utt);
				break;
			}
			
			logger.info("dAtFromUsr: " + dAtFromUsr + " -- T: " + utt);
			if(this.transcription != null)
				this.transcription.writeToTranscriptFile("usr", utt);
			
			// check the current dialogue state
			ControlBasicState newState = null;
			try {
				newState = learnInstanceforNewEllipse(this.curCState, dAtFromUsr, lastAction, this.prediction);
			} catch (UnAvailableException e) {
				logger.error(e.getMessage());
				return this.performDefaultAction();
			}
			
			if(newState != null)
				this.curCState = newState;
			else
				return this.performDefaultAction();
				
			colorState = (int)curCState.get(ControlLearningDomain.COLORSTATE);
			shapeState = (int)curCState.get(ControlLearningDomain.SHAPESTATE);
			preDAts = (String)curCState.get(ControlLearningDomain.PREDATS);
			logger.debug("current Dialogue State: " + this.curCState);
			
			// find the best response
			String utterance = "";
			if(!(colorState == 2  && shapeState == 2  && !preDAts.trim().contains("Ask(") 
					&& !preDAts.trim().contains("Repeat(") && !preDAts.trim().contains("Check(") 
					&& !preDAts.trim().contains("Reject("))){
				
				// find the best action to perform
				Action bestAction = this.getBestActionFromQTable(curCState);
				
				// check whether the action is available
				//if yes
				if(bestAction != null){
					logger.info("selected Action's Name = " + bestAction.actionName());
					
					dAt = actMap.get(ActionName.valueOf(bestAction.actionName()));
					this.lastAction = dAt;
					utterance = generator.searchUttbyAction(dAt);
					
					if(prediction != null){
						Iterator<Entry<String, Queue<Concept>>> iterator = prediction.entrySet().iterator();
						while(iterator.hasNext()){
							Entry<String, Queue<Concept>> entry = iterator.next();
							String keyword = entry.getKey().toLowerCase();
							Concept cpt = entry.getValue().peek();
				
							utterance = utterance.replace("%"+keyword+"value", cpt.getConceptName().toLowerCase());
						}
					}
					
					if(dAt.startsWith("Polar(") && !utterance.contains("?"))
						utterance += "?";
						
					logger.info(" -- L: " + utterance + " -- " + dAt);
					this.checkDAtFromUtt_ds("sys", utterance + " ");
					this.pre_action_from_sys = dAt.toLowerCase();
					
					if(this.transcription != null)
						this.transcription.writeToTranscriptFile(this.name, utterance);
					
				}
				
				//if no
				else
					return this.performDefaultAction();
			}
			
			else{
				utterance = "thanks. can you show me another instance?";
			}
			
	        data_controller.setSpeechOutputUpdated(utterance, dAt);
	        return convertToUtteredWords(utterance);
		}
		return null;
	}
	
	public Queue<UtteredWord> performDefaultAction(){
		int index = new Random().nextInt(this.misunderstand_utts.length);
		data_controller.setSpeechOutputUpdated(this.misunderstand_utts[index], "out of topic");
	    return convertToUtteredWords(this.misunderstand_utts[index]);
	}

	/****************** Visual Classification Functions ******************/ 
	private int delay = 2000;
	private long lastCharTime = -1; 
	private static final int adjustVal = 5;
	private Map<String, Queue<Concept>> prediction;
	public void visualSceneUpdated(String fileName) {
		if(fileName != null && !fileName.isEmpty()){
			logger.info("file selected is : " + fileName);
			this.resetDM();
			
			lastCharTime = new Date().getTime();
			this.data_controller.setBehaviorUpdated(Behavior.LOOK_DOWN);
			
			//setup the current visual image under the path
			this.prediction = this.vModule.receiveNewObject(fileName);
			if(this.prediction != null){
				
				this.numOfInstances++;
				
				if(this.hasAdaptiveThreshold){
					if(this.numOfInstances % binUnit == 1){
						int trainLevel = Math.floorDiv(this.numOfInstances, binUnit);
	//					logger.info("numOfInstances = " + this.numOfInstances +"; binUnit = " + binUnit + "; trainLevel = " + trainLevel);
	//					logger.info("previous Accuracy = " + this.preAccuracy);
						AdaptiveBasicState aState = new AdaptiveBasicState(trainLevel, this.posiThreshold, this.deltaAcc);
						
						Action action = this.getBestActionFromQTable(aState);
						
						ActionName seletedAct = ActionName.valueOf(action.actionName());
						switch(seletedAct){
							case AdjustDown:
								if(this.posiThreshold > 65)
									this.posiThreshold -= adjustVal;
								break;
							case AdjustUp:
								if(this.posiThreshold < 95)
									this.posiThreshold += adjustVal;
								break;
						}
					}
				}
				
				logger.info("current positive Threshold: " + this.posiThreshold + "; Predicted Results: " + prediction);
				dataShare.setPredictions(prediction);
				this.data_controller.updateClassiferResults();
				
				try {
					if(this.transcription != null)
						this.transcription.appendNewLineTotFile();
					this.initialStatesByPrediction(this.prediction);
				} catch (IOException e) {
					logger.error(e.getMessage());
				}
			}
			else
				logger.error("cannot find the selected image");
		}
	}
	
	private void reClassifyInstance() {
		this.prediction = vModule.getCLassifictionResults();
		dataShare.setPredictions(prediction);
		this.data_controller.updateClassiferResults();
	}
	
	/*************** IncrementalDialogueAgent Functions *****************/
	@Override
	public UtteredWord nextWord() throws InterruptedException {
		logger.debug("This nextWord() is just called."); 

		// add a delay after each word
		long millis = new Random().nextInt(400);
		Thread.sleep(200+millis);
		
		Utterance lastUtterance = this.dialogue_env.getLastUtterance();
		String lastUtt = lastUtterance == null ? "" : lastUtterance.getText().toLowerCase();
		logger.debug("last utt as text: "+lastUtt);
	
		if (this.dialogue_env.hasFloor(this)) {
			logger.info("DM has the floor");
			if (this.toWords != null && this.toWords.isEmpty()) {
				logger.debug("Nothing more to say");
//				if (curTemplate!=null)
//					this.usedTemplates.add(curTemplate);
//				
//				curTemplate=null;
				return new UtteredWord(DSDialogueAgent.RELEASE_TURN, this.name);
			} else 
				return toWords.peek();
			
		} else if (this.dialogue_env.floorIsOpen()) {
			logger.debug("Floor is open");
			
			logger.debug("lastUtterance: " + lastUtterance);
			
			// floor is open and I'm the last speaker. So I must have opened it. 
			if (lastUtterance != null && lastUtterance.getSpeaker().equals(this.name)){
				// The other has generated erroneous do-nothing action.
				logger.debug("I'm the last speaker and floor is open. Other should have said something");

				return new UtteredWord(DSDialogueAgent.RELEASE_TURN, this.name);
			}
			
			// floor is open and the other is the last speaker. 
			else{
				logger.debug("Other released turn. Now generating word");
				
//				while(toWords.isEmpty())
//					wait();
				
				if(isExptRunning)
					try {
						toWords = findResponse(lastUtt);
					} catch (IOException e) {
						logger.error(e.getMessage());
					}
				else
					toWords = frindResponseFromRules(lastUtt);
				
				logger.debug("toWords is:"+ this.toWords);
				if(toWords != null && !toWords.isEmpty()){
					logger.debug("queue words for response utt: " + toWords.peek());
					return toWords.peek();
				}
				else{
					return new UtteredWord(DSDialogueAgent.RELEASE_TURN, this.name);
				}
			}
			
		} else{
			logger.info("Other has the floor. Is his utterance so far recognisable?");
			return new UtteredWord(DSDialogueAgent.WAIT, this.name);
		}
	}
	
	@Override
	public void wordUttered(UtteredWord w) {
		logger.debug("w: " +w.word() + " - " +  w.speaker());
		
		if (!w.speaker().equals(name))
			return;

		if (w.word().equals(DSDialogueAgent.BYE) || w.word().equals(DSDialogueAgent.RELEASE_TURN)){
			return;
		}

		logger.debug("now removing word from queue:" + w);
		if (!this.toWords.peek().equals(w))
			throw new IllegalStateException("top of queue:" + this.toWords.peek() + " but got:" + w);

		this.toWords.remove();
	}

	@Override
	public void reset() {
		// clear the Queue of UtteredWord
		this.toWords = new LinkedList<UtteredWord>();
		this.toWords.clear();
	}
	
	public synchronized void addWord(String word) {
		logger.debug("Adding word:" + word);
		this.toWords.offer(new UtteredWord(word, this.name));
		notifyAll();
	}

	private Queue<UtteredWord> frindResponseFromRules(String lastUtt) {
		String utterance = "I'm VOILA, wellcome to chat with me.";
        data_controller.setSpeechOutputUpdated(utterance, "");
		return convertToUtteredWords(utterance);
	}
	
	private Queue<UtteredWord> convertToUtteredWords(String content){
		if(content != null && !content.trim().isEmpty()){
			Queue<UtteredWord> queue = new LinkedList<UtteredWord>();
			String[] words = content.split(" ");
			for(int i=0; i < words.length; i++)
				queue.add(new UtteredWord(words[i], this.name));
			
			if(!content.contains(DSDialogueAgent.RELEASE_TURN))
				queue.add(new UtteredWord(DSDialogueAgent.RELEASE_TURN, this.name));
			return queue;
		}
		return null;
	}
	
	public void resetDM(){
		if(this.prediction != null)
			this.prediction.clear();
		this.dataShare.reset();
		this.dlParser.initParser();
		this.pre_action_from_sys = null;

//		this.vModule.reset();

		this.curObj = null;
		this.numOfInstances = 0;
		this.posiThreshold = 95;
		this.preAccuracy = 0.0;
		this.deltaAcc = 0;
	}
	
	/**********************************************/
	@Override
	public void update(String type, String contxt) {
//		logger.info("type: " + type + " --> contxt: " + contxt);
		
		if(type.trim().equals(DataController.action_vision)){
			this.visualSceneUpdated(contxt);
		}

		if(type.trim().equals(DataController.action_btncall)){
			String[] items = contxt.split(":");
			String btn_name = items[0];
			this.isExptRunning = Boolean.valueOf(items[1]);
			
			if(this.isExptRunning){
				if(this.transcription != null)
					this.transcription.startNewExpt();
				else
					try {
						throw new UnAvailableException("Class of 'Transcription' isn't instantiated yet");
					} catch (UnAvailableException e) {
						logger.error(e.getMessage());
					}
			}
		}
	}

	@Override
	public void run() {
	}
	
	public enum PolicyType{
		ADAPTIVE,
		CONTROL;
	}
}
