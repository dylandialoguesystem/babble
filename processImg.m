% function vector = processImg(path, bbox)
function vector = processImg(path)
%PROCESSIMG 

setup;

img = imread(path);
image = standardizeImage(img);

[ B , bbox ] = image2edge( image );
subimage = imcrop(image, bbox);
% figure(2)
% imshow(subimage);

feat_bovw = image2bovw( subimage );
feat_color = image2hsv( subimage )' ;
vector = [feat_color;feat_bovw];
disp(vector);

store_in_cache(vector,bbox);
end
 
 %% save feature into cache data file .....
 function store_in_cache(vector,bbox)
      cachedata_file = sprintf('data/cache_data.mat');
      save(cachedata_file,'vector','bbox');
 end
