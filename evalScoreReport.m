function evalScoreReport(dir, ontology, id, acutual, predict, policy, numObj)
%EVALREPORT Summary of this function goes here
    setup;
    dir_root = fileparts(which('setup.m'));
    filename = sprintf('%s/expScoreReport-%s-%s.csv',dir, policy, datestr(now,'mm-dd-yyyy'));
    path = fullfile(dir_root,filename);
    disp(path);
    
    if exist(path, 'file') == 0
         head = {'/********************************************************************/';
                 '  TeachBot Dialog Strategy Experiment';
        	     '  @ Author:  Yanchao Yu';
                 '  @ Version: 0.1';
                 '  @ Date:    March 2016';
         sprintf('  @ Policy Type:  %s ',policy);
                 '/********************************************************************/'};
         writeToRow(path,head);
    end
    
    if strcmp(ontology,'color')
        writeToRow(path,{'',''});
        writeToRow(path,{sprintf('/*********************** Trained Data Size:  %d ************************/', numObj)});
        title = {'','Recog Score'};
        writeToRow(path, title);
    end
    acutual = cell2mat(acutual');
    predict = cell2mat(predict');
    [row,cols] = size(acutual);
    
    score = {};
    for i = 1 : cols
        X = acutual(:,i);
        Y = predict(:,i);
        disp(size(Y));
        info = im2RecgScore(X',Y', ontology);
        score{i} = info.recg_score;
        disp(score{i});
    end
    totalScroe = total(score);
    data = {ontology, num2str(totalScroe)}
    writeToRow(path, data);
end

function value = total(data)
    num = length(data)
    value = 0;

    for i = 1 : num
        if(isnan(data{i}))
            data{i} = 0;
        end
        
        value = value + data{i};
    end
end

function writeToRow(path, data)
    disp(data);
    fid = fopen(path,'a');
    numColumns = size(data,2);
    fmt = repmat('%s,',1,numColumns-1);
    fprintf(fid,[fmt,'%s\n'],data{1:end,:})
    fclose(fid);
end

  
  

