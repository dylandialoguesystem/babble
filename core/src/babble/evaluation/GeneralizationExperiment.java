package babble.evaluation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import babble.dialog.rl.TTRMDPStateEncoding;
import babble.simulation.SemanticsBasedSimulatedDialogueAgent;
import babble.simulation.UtteranceTemplate;
import csli.util.Pair;
import qmul.ds.Dialogue;
import qmul.ds.InteractiveContextParser;
import qmul.ds.Utterance;
import qmul.ds.babi.BabiDialogue;


public class GeneralizationExperiment {
	private TTRMDPStateEncoding stateEncoder = new TTRMDPStateEncoding();
	private String domain;
	private String grammarResource;
	private String agentName;
	private Map<List<Integer>, List<UtteranceTemplate>> triggerTemplates = new HashMap<>();
	InteractiveContextParser parser;
	
	public GeneralizationExperiment(String inDomain, String inGrammarResource, String inAgentName) {
		domain = inDomain;
		grammarResource = inGrammarResource;
		agentName = inAgentName;

		parser = new InteractiveContextParser(
			String.join(File.separator, new String[]{TTRMDPStateEncoding.GRAMMAR_FOLDER, grammarResource}),
			"usr",
			"sys"
		);
	}

	public void train(List<Dialogue> inTrainSet) throws IOException {
		System.out.println("\n\ntraining\n\n");
		// constructing the state space
		stateEncoder.constructFromLoadedDialogues(inTrainSet, domain, grammarResource);
		// generating simulator templates
		SemanticsBasedSimulatedDialogueAgent.generateTemplatesForBothSides(
			inTrainSet,
			domain,
			grammarResource,
			stateEncoder
		);
		SemanticsBasedSimulatedDialogueAgent agent = new SemanticsBasedSimulatedDialogueAgent(
			"sys",
			domain,
			grammarResource,
			stateEncoder
		);
		for (Map.Entry<List<Integer>, List<UtteranceTemplate>> template: agent.loadTemplates(domain).a.entrySet()) {
			List<Integer> triggerState = template.getKey().subList(2, template.getKey().size());
			if (!triggerTemplates.containsKey(triggerState)) {
				triggerTemplates.put(Collections.unmodifiableList(new ArrayList<>(triggerState)), new ArrayList<>());
			}
			triggerTemplates.get(triggerState).addAll(template.getValue());
		}
	}

	public Pair<Double, Double> test(List<Dialogue> inTestSet) throws IOException, InterruptedException {
		int
			totalDialogues = 0,
			correctDialogues = 0,
			totalUtterances = 0,
			correctUtterances = 0;
		for (int index = 0; index != inTestSet.size(); ++index) {
			System.out.println(String.format("Processing dialogue %d out of %d", index + 1, inTestSet.size()));
			Dialogue dialogue = inTestSet.get(index);
			System.out.println(dialogue.toString());

			Pair<Integer, Integer> partialResult = evaluateDialogue(dialogue);
			correctUtterances += partialResult.a;
			totalUtterances += partialResult.b;
			++totalDialogues;
			correctDialogues += (partialResult.a + 1) / (partialResult.b + 1); // integer division intentionally
		}
		return new Pair<>(correctUtterances / (double) totalUtterances, correctDialogues / (double) totalDialogues);
	}


	public static Pair<List<Integer>, List<Integer>> splitDataset(List<Integer> inDataset, int inTrainsetSize) {
		List<Integer> datasetShuffled = new ArrayList<Integer>(inDataset);
		Collections.shuffle(datasetShuffled);
		List<Integer>
			trainSet = datasetShuffled.subList(0, inTrainsetSize),
			testSet = datasetShuffled.subList(inTrainsetSize, datasetShuffled.size());
		return new Pair<>(trainSet, testSet);
	}

	public static List<Dialogue> loadCorpus(String inRoot) throws IOException {
		List<String> filesToProcess = new ArrayList<>();

		List<Dialogue> dataset = new ArrayList<>();
		for (File dialogueFile: new File(inRoot).listFiles()) {
			if (dialogueFile.getName().equals("slot-values")) {
				continue;
			}
			filesToProcess.add(dialogueFile.getAbsolutePath());
		}
		filesToProcess.sort(
			new Comparator<String>() {
				@Override
				public int compare(String o1, String o2) {
					int prefixComparison =  getPrefix(o1).compareTo(getPrefix(o2));
					if (prefixComparison != 0) {
						return prefixComparison;
					}
					return getIndex(o1).compareTo(getIndex(o2));
				}

				public String getPrefix(String inFileName) {
					return inFileName.substring(0, inFileName.lastIndexOf("."));
				}

				public Integer getIndex(String inFileName) {
					return Integer.parseInt(inFileName.substring(inFileName.lastIndexOf(".") + 1, inFileName.length()));
				}
			}
		);

		for (String dialogueFile: filesToProcess) {
			dataset.addAll(BabiDialogue.loadFromBabbleFile(dialogueFile));
		}
		return dataset;
	}

	private Pair<Integer, Integer> evaluateDialogue(Dialogue inDialogue) {
		// debug code
		List<Integer> testTemplate = null;
		for (List<Integer> stateCandidate: triggerTemplates.keySet()) {
			testTemplate = stateCandidate;
			break;
		}
		//

		parser.init();
		int
			totalUtterances = 0,
			correctUtterances = 0;
		for (Utterance utterance: inDialogue) {
			if (utterance.getSpeaker().equals(agentName)) {
				++totalUtterances;
				List<Integer> fullState = stateEncoder.encode(parser.getContext());
				fullState = fullState.subList(2, fullState.size());
				if (fullState.size() != testTemplate.size()) {
					System.err.println(String.format(
						"State sizes don't match: %d (template candidate), %d (target)",
						testTemplate.size(),
						fullState.size()
					));
					System.err.println(utterance.getText());
					System.exit(1);
				}
				List<UtteranceTemplate> templates = getNearestNeighbor(fullState);
				for (UtteranceTemplate template: templates) {
					Utterance candidateUtterance = template.getRandomUtterance(agentName);
					if (candidateUtterance.equals(utterance)) {
						++correctUtterances;
						break;
					}
				}
			}
			parser.parseUtterance(utterance);
		}
		return new Pair<>(correctUtterances, totalUtterances);
	}

	private List<UtteranceTemplate> getNearestNeighbor(List<Integer> inState) {
		List<UtteranceTemplate> result = null;
		int minDistance = 999999;
		for (List<Integer>stateCandidate: triggerTemplates.keySet()) {
			int hammingDistance = 0;
			for (int index = 0; index != inState.size(); ++index) {
				hammingDistance += Math.abs(stateCandidate.get(index) - inState.get(index));
			}
			if (hammingDistance < minDistance) {
				minDistance = hammingDistance;
				result = triggerTemplates.get(stateCandidate);
			}
		}
		return result;
	}

	public static void main(String[] args) throws IOException, InterruptedException {
		String default_domain = "restaurant-search";
		String trainsetRoot = "data/Domains/" + default_domain + "/train";
		String testsetRoot = "data/Domains/" + default_domain + "/test";
		if (args.length == 3) {
			// System.out.println("Usage: GeneralizationExperiment.java <domain>
			// <trainset root> <testset root>");
			default_domain = args[0];
			trainsetRoot = args[1];
			testsetRoot = args[2];

		} else if (args.length == 2) {
			trainsetRoot = args[0];
			testsetRoot = args[1];

		} else if (args.length == 0) {

		} else {
			System.out.println("Usage: GeneralizationExperiment.java <domain> <trainset root> <testset root>");
			return;
		}

		List<Dialogue> trainDialogues = loadCorpus(trainsetRoot), testDialogues = loadCorpus(testsetRoot);
		GeneralizationExperiment experiment = new GeneralizationExperiment(default_domain,
				"2016-english-ttr-restaurant-search", "sys");
		experiment.train(trainDialogues);
		Pair<Double, Double> combinedAccuracy = experiment.test(testDialogues);
		System.out.println(String.format("Per-utterance accuracy: %.3f", combinedAccuracy.a));
		System.out.println(String.format("Per-dialogue accuracy: %.3f", combinedAccuracy.b));
	}

}
