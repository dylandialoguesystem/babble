package babble.evaluation.eval;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import babble.evaluation.TestClassifierCollection;
import babble.vision.classification.VisualClassifier;
import babble.vision.object.BasicObject;
import babble.vision.tools.Constants;
import babble.vision.tools.MatlabLink;

public class StrategyEval extends Evaluation{
	static Logger logger = Logger.getLogger(StrategyEval.class);

	private int numPos = 0;
	private int numNeg = 0;
	
	private double posThreshold = 1.0;
	private double negThreshold = 0.0;
	
	private InitiativeType initialTyp = InitiativeType.LearnerInitial;
	private boolean hasUncertainty = false; 
	private boolean hasContentDependency = false; 
	private boolean hasCorretion = false; 
	
	private MatlabLink matLink;
	
	public StrategyEval(MatlabLink matLink){
		this.matLink = matLink;
		logger.info("matLink : " + (matLink == null));
		
	}
	
	public void initialSetup(InitiativeType initialTyp,
			boolean hasUncertainty, boolean hasContentDependency, boolean hasCorretion){
		this.initialTyp = initialTyp;
		this.hasUncertainty = hasUncertainty;
		this.hasContentDependency = hasContentDependency;
		this.hasCorretion = hasCorretion;
	}
	
	public void setTrnTstNum(int numPos, int numNeg){
		logger.info("num of Train: " + numPos + "  num of Test: " + numNeg);
		this.numPos = numPos; 
		this.numNeg = numNeg;
	}
	
	public void setThresholds(double posThreshold, double negThreshold){
		this.posThreshold = posThreshold;
		this.negThreshold = negThreshold;
	}

	private Map<Integer, Double> costList = new TreeMap<Integer, Double>();
	public void evalClassifiers(int numPoint, int nfold, String string, List<? extends BasicObject> list,
			TestClassifierCollection colorClassifiers, TestClassifierCollection shapeClassifiers, double cost, String filename) {
		double score = 0;
		
		if(list != null){
			double[][] actualMcolor = new double[list.size()][colorClassifiers.size()];
			double[][] actualMshape = new double[list.size()][shapeClassifiers.size()];
			double[][] predMcolor = new double[list.size()][colorClassifiers.size()];
			double[][] predMshape = new double[list.size()][shapeClassifiers.size()];		
			int count = 0;
			for(BasicObject vObj : list){
//				logger.info("TestObjt["+count+"]:" + vObj.getFileName());
				actualMcolor[count] = buildMatrix(vObj.getLabels(), colorClassifiers);
				actualMshape[count]  = buildMatrix(vObj.getLabels(), shapeClassifiers);
					
				if(colorClassifiers.doClassify(vObj)){
					Map<String, Double> predictMap = colorClassifiers.getResultMap();
//					// logger.info("predictMap at Color: " + predictMap);
					predMcolor[count] = buildMatrix(predictMap, colorClassifiers);
				}
				else
					logger.error("cannot classify the visual object of " + vObj.getFileName());
				if(shapeClassifiers.doClassify(vObj)){
					Map<String, Double> predictMap = shapeClassifiers.getResultMap();
//					// logger.info("predictMap at Shape: " + predictMap);
					predMshape[count] = buildMatrix(predictMap, shapeClassifiers);
				}
				else
					logger.error("cannot classify the visual object of " + vObj.getFileName());
				
				count++;
			}
//			Map<String, Integer> evalMatrix_color = computeMatrix(ontology, actualMcolor, predMcolor);
//			double recogScore_color = (Double)caluclateScore("color", evalMatrix_color, -1);
//			Map<String, Integer> evalMatrix_shape = computeMatrix(ontology, actualMshape, predMshape);
//			double recogScore_shape = (Double)caluclateScore("shape", evalMatrix_shape, -1);
			
			Map<String, Integer> evalMatrix_color = computeFMatrix("color", actualMcolor, predMcolor);
			double recogScore_color = caluclateXScore("color", evalMatrix_color, "accuracy");
			Map<String, Integer> evalMatrix_shape = computeFMatrix("shape", actualMshape, predMshape);
			double recogScore_shape = caluclateXScore("shape", evalMatrix_shape, "accuracy");
			
			score = (recogScore_color + recogScore_shape) / 2;
			
			if(numPoint == 0)
				logger.info("Accuracy: " + score);
			
			this.scoreList.put(numPoint, score);
			this.costList.put(numPoint, cost);
		
			logger.info("number of instance now: " + numPoint + ";   in totall: " + numPos);
			if(numPoint >= numPos-1){
				// logger.info(nfold + "'s exp -> recogScoreList : " + recogScoreList);
				Set<Integer> keySet = this.scoreList.keySet();
				int[] scoreLabels = new int[keySet.size()];
				double[] scoreList = new double[keySet.size()];
				double[] cList = new double[keySet.size()];
				int num = 0;
				for(Integer label: keySet){
					scoreLabels[num] = label;
					scoreList[num] = this.scoreList.get(label);
					cList[num] = this.costList.get(label);
					num++;
				}
				
				logger.info("filename: " + filename);
				logger.info("nfold: " + nfold);
				logger.info("scoreLabels: " + scoreLabels);
				logger.info("scoreList: " + scoreList);
				logger.info("cList: " + cList);
				logger.info("initialTyp: " + initialTyp);
				logger.info("hasUncertainty: " + hasUncertainty);
				logger.info("hasContentDependency: " + hasContentDependency);
				logger.info("hasCorretion: " + hasCorretion);
				logger.info("numPos: " + numPos);
				logger.info("numNeg: " + numNeg);
				logger.info("posThreshold: " + posThreshold);
				if(matLink != null)
					matLink.excuteFunc(filename, Constants.simulateExpDir, nfold, scoreLabels, scoreList, cList, 
							this.initialTyp.toString(), this.hasUncertainty, this.hasContentDependency, this.hasCorretion,
							this.numPos, this.numNeg, this.posThreshold);
				else
					logger.error("cannot find matlab link...");
				
				this.scoreList.clear();
				this.costList.clear();
			}
		}
		else
			logger.error("Test set is null...");
	}

	public double[] buildMatrix(Map<String, Double> predictMap, TestClassifierCollection classifiers){
		double[] vector = new double[predictMap.size()];
		int i=0;
		Iterator<Entry<String, VisualClassifier>> iterator = classifiers.getClassifiers().entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, VisualClassifier> pair = (Entry<String, VisualClassifier>) iterator.next();
		    String attr = (String)pair.getKey();
		    vector[i++] = predictMap.get(attr);
		}
		return vector;
	}

	@Override
	public Object caluclateScore(String ontology, Map<String, Integer> evalMatrix, int scoreIndex) {
		int numTP = evalMatrix.get("numTP");
		int numFP = evalMatrix.get("numFP");
		int numTN = evalMatrix.get("numTN");
		int numFN = evalMatrix.get("numFN");
		int numPTP = evalMatrix.get("numPTP");
		int numPFP = evalMatrix.get("numPFP");
		int numPTN = evalMatrix.get("numPTN");
		int numPFN = evalMatrix.get("numPFN");
		
		double score = (numTP + numTN + numPTP*0.5 + numPTN*0.5) - (numFP + numFN + numPFP*0.5 + numPFN*0.5);
		return score;
	}
	
	public double caluclateXScore(String ontology, Map<String, Integer> evalMatrix, String methodName) {
		int numTP = evalMatrix.get("numTP");
		int numFP = evalMatrix.get("numFP");
		int numTN = evalMatrix.get("numTN");
		int numFN = evalMatrix.get("numFN");
		
		double precs = (double)numTP / ( (double)numTP + (double)numFP );
		double rc = (double)numTP / ( (double)numTP + (double)numFN );
		double f1Score = 2 * (precs * rc) / (precs + rc);
		double accuracy = ((double)numTP + (double)numTN) / ( (double)numTP + (double)numFP + (double)numFN + (double)numTN );
	
		if(methodName.equals("precision"))
			return precs;
		else if(methodName.equals("f1Score"))
			return f1Score;
		else if(methodName.equals("accuracy"))
			return accuracy;
		else
			return 0;
	}
	
	@Override
	public Map<String, Integer> computeMatrix(String ontology, double[][] target, double[][] predict) {
		int numTP = 0, numFP = 0, numTN = 0, numFN = 0;
		int numPTP = 0, numPFP = 0, numPTN = 0, numPFN = 0;
		
		for(int i = 0; i < target.length; i++){
			for(int j = 0; j < target[0].length; j++){
				
				if(target[i][j] == 1.0 && predict[i][j] >= this.posThreshold)
					numTP += 1;
				else if(target[i][j] == 1.0 && predict[i][j] < this.negThreshold)
					numFP += 1;
				else if(target[i][j] == 0.0 && predict[i][j] >= this.posThreshold)
					numFN += 1;
				else if(target[i][j] == 0.0 && predict[i][j] < this.negThreshold)
					numTN += 1;
				else if(target[i][j] == 1.0 && (0.5 < predict[i][j]  && predict[i][j] < this.posThreshold))
					numPTP += 1;
				else if(target[i][j] == 1.0 && (predict[i][j] >= this.negThreshold && predict[i][j] <= 0.5))
					numPFP += 1;
				else if(target[i][j] == 0.0 && (predict[i][j] >= this.negThreshold && predict[i][j] <= 0.5))
					numPTN += 1;
				else if(target[i][j] == 0.0 && (0.5 < predict[i][j] && predict[i][j] < this.posThreshold))
					numPFN += 1;
			}
		}
		Map<String, Integer> evalMatrix = new HashMap<String, Integer>();
		evalMatrix.put("numTP", numTP);
		evalMatrix.put("numFP", numFP);
		evalMatrix.put("numTN", numTN);
		evalMatrix.put("numFN", numFN);
		evalMatrix.put("numPTP", numPTP);
		evalMatrix.put("numPFP", numPFP);
		evalMatrix.put("numPTN", numPTN);
		evalMatrix.put("numPFN", numPFN);
		
		return evalMatrix;
	}
	
	public Map<String, Integer> computeFMatrix(String ontology, double[][] target, double[][] predict) {
		int numTP = 0, numFP = 0, numTN = 0, numFN = 0;
		
		for(int i = 0; i < target.length; i++){
			for(int j = 0; j < target[0].length; j++){
				
				if(target[i][j] == 1.0 && predict[i][j] > 0.5)
					numTP += 1;
				else if(target[i][j] == 1.0 && predict[i][j] <= 0.5)
					numFP += 1;
				else if(target[i][j] == 0.0 && predict[i][j] > 0.5)
					numFN += 1;
				else if(target[i][j] == 0.0 && predict[i][j] <= 0.5)
					numTN += 1;
			}
		}
		Map<String, Integer> evalMatrix = new HashMap<String, Integer>();
		evalMatrix.put("numTP", numTP);
		evalMatrix.put("numFP", numFP);
		evalMatrix.put("numTN", numTN);
		evalMatrix.put("numFN", numFN);
		
		return evalMatrix;
	}
	
	public enum InitiativeType {
		TutorInitial,
		LearnerInitial;
	}

	@Override
	public void evalClassifiers(int numPoint, int nfold, String ontology, List<? extends BasicObject> tstList,
			TestClassifierCollection classifiers, int requestIndex, String filename) {
		
		
	}

	@Override
	public void evalClassifiers(String ontology, List<? extends BasicObject> tstList,
			TestClassifierCollection classifiers, int requestIndex,  String filename) {
	}
}
