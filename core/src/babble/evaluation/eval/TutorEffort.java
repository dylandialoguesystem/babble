package babble.evaluation.eval;

public class TutorEffort {
	private static TutorEffort instance;
	
	private TutorEffort(){}
	
	public static TutorEffort getInstance(){
		if(instance == null)
			instance = new TutorEffort();
		return instance;
	}

	public static final double cost_Inform = 1;
	public static final double cost_Correct = 1;
	public static final double cost_Acknow = 0.25;
	public static final double cost_Parse = 0.5;
	public static final double cost_Pruduct = 1;
}
