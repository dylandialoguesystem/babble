package babble.evaluation.eval;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import babble.evaluation.TestClassifierCollection;
import babble.vision.classification.VisualClassifier;
import babble.vision.object.BasicObject;

public abstract class Evaluation {
	
	protected HashMap<String, Boolean> policyflagSettings = new HashMap<String, Boolean>();
	protected HashMap<String, String> policySettings = new HashMap<String, String>();
	
	
	public final int PRECISION = 0;
	public final int RECALL = 1;
	public final int MACROF1 = 2;
	public final int ACCURACY = 3;
	
	protected Map<Integer, Double> scoreList = new TreeMap<Integer, Double>();
	
	public abstract void evalClassifiers(int numPoint, int nfold, String ontology, List<? extends BasicObject> tstList, TestClassifierCollection classifiers, int requestIndex, String filename);
	
	public abstract void evalClassifiers(String ontology, List<? extends BasicObject> tstList, TestClassifierCollection classifiers, int requestIndex, String filename);

	public double[] buildMatrix(List<String> labelList, TestClassifierCollection classifiers){
		HashMap<String, VisualClassifier> classifierMap = classifiers.getClassifiers();
		double[] vector = new double[classifierMap.size()];
		
		int i=0;
		Iterator<Entry<String, VisualClassifier>> iterator = classifierMap.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, VisualClassifier> pair = (Entry<String, VisualClassifier>) iterator.next();
		    String attr = (String)pair.getKey();
		    
		    //logger.debug("Classifier["+attr+"]: " + "data = " + data);
		    if(labelList == null || labelList.isEmpty())
		    	vector[i++] = 0;
		    else{
			    if(labelList.contains(attr))
			    	vector[i++] = 1;
			    else
			    	vector[i++] = 0;
		    }
		}
		return vector;
	}
	
	public abstract Object caluclateScore(String ontology, Map<String, Integer> evalMatrix, int scoreIndex);
	
	public abstract Map<String, Integer> computeMatrix(String ontology, double[][] target, double[][] predict);
	
	public void addNewPolicy(String policyName, boolean flag){
		if(this.policyflagSettings != null){
			this.policyflagSettings.put(policyName, flag);
		}
	}
	
	public void clearFlagSettings(){
		if(this.policyflagSettings != null)
			this.policyflagSettings.clear();
	}

	public void addNewPolicy(String policyName, String value){
		if(this.policySettings != null){
			this.policySettings.put(policyName, value);
		}
	}
	
	public void clearSettings(){
		if(this.policySettings != null)
			this.policySettings.clear();
	}

	public void reset(){
		this.clearFlagSettings();
		this.clearSettings();
	}
}
