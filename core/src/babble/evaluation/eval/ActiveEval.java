package babble.evaluation.eval;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;

import babble.evaluation.TestClassifierCollection;
import babble.simulation.VisualObjRecord;
import babble.vision.object.BasicObject;
import babble.vision.object.VisualObject;
import babble.vision.tools.Constants;
import babble.vision.tools.MapToolKit;
import babble.vision.tools.MatlabLink;

public class ActiveEval extends Evaluation{
	static Logger logger = Logger.getLogger(ActiveEval.class);

	private static int scoreIndex; 
	private static int numPos = 0;
	private static int numNeg = 0;

	public static final int ASKTOP = 0;
	public static final int ASKMORE = 1;
	
	private MatlabLink matLink;
	
	private String policyName = null;
	
	public ActiveEval(MatlabLink matLink){
		this.matLink = matLink;
	}
	
	public void setExptPolicy(String name,String policy){
		logger.info("Request policy: " + policy);
		super.addNewPolicy(name, policy);
		this.policyName = name;
	}
	
	public void setTrnTstNum(int numPos, int numNeg){
		logger.info("num of positive: " + numPos + "  num of Negative: " + numNeg);
		this.numPos = numPos; 
		this.numNeg = numNeg;
	}

	public void setScoreRequest(int scoreIndex){
		this.scoreIndex = scoreIndex;
	}
	
	@Override
	public Object caluclateScore(String ontology, Map<String, Integer> evalMatrix, int scoreIndex) {
		int numTP = evalMatrix.get("numTP");
		int numFP = evalMatrix.get("numFP");
		int numTN = evalMatrix.get("numTN");
		int numFN = evalMatrix.get("numFN");
		
		switch(scoreIndex){
		case PRECISION:
			Double precision = (double)numTP / ( (double)numTP + (double)numFP );
			return precision;
		case RECALL:
			Double recall = (double)numTP / ( (double)numTP + (double)numFN );
			return recall;
		case MACROF1:
			double precs = (double)numTP / ( (double)numTP + (double)numFP );
			double rc = (double)numTP / ( (double)numTP + (double)numFN );
			Double f1Score = 2 * (precs * rc) / (precs + rc);
			
			logger.info("Precision = " + precs + "Recall = " + rc + "Macro-F1 = " + f1Score);
			return f1Score;
		}
		
		return 0;
	}

	@Override
	public Map<String, Integer> computeMatrix(String ontology, double[][] target, double[][] predict) {
		int numTP = 0, numFP = 0, numTN = 0, numFN = 0;
		for(int i = 0; i < target.length; i++){
			for(int j = 0; j < target[0].length; j++){
				
				if(target[i][j] == 1.0 && predict[i][j] == 1.0)
					numTP += 1;
				else if(target[i][j] == 1.0 && predict[i][j] == 0.0)
					numFP += 1;
				else if(target[i][j] == 0.0 && predict[i][j] == 1.0)
					numFN += 1;
				else if(target[i][j] == 0.0 && predict[i][j] == 0.0)
					numTN += 1;
			}
		}
		Map<String, Integer> evalMatrix = new HashMap<String, Integer>();
		evalMatrix.put("numTP", numTP);
		evalMatrix.put("numFP", numFP);
		evalMatrix.put("numTN", numTN);
		evalMatrix.put("numFN", numFN);
		
		return evalMatrix;
	}

	@Override
	public void evalClassifiers(int numPoint, int nfold, String ontology, List<? extends BasicObject> tstList,
			TestClassifierCollection classifiers, int requestIndex, String filename) {
		if(tstList != null){
			double[][] targMatrix = new double[tstList.size()][classifiers.size()];
			double[][] predMatrix = new double[tstList.size()][classifiers.size()];
			
			int count = 0;
			for(BasicObject vObj : tstList){
				if(vObj instanceof VisualObject)
					targMatrix[count] = this.buildMatrix(((VisualObject)vObj).getLabels(), classifiers);
				if(vObj instanceof VisualObjRecord)
					targMatrix[count] = this.buildMatrix(((VisualObjRecord)vObj).getLabels(), classifiers);
				
				
				if(classifiers.doClassify(vObj)){
					Map<String, Double> predictMap = classifiers.getResultMap();
					
					List<String> predictLabels = new ArrayList<String>();
					switch(requestIndex){
					case ASKTOP:
						predictMap = MapToolKit.getInstance().sortByComparator(predictMap, true);
						String predictStr = MapToolKit.getInstance().getTopKey(predictMap);
						double prob1 = predictMap.get(predictStr);
						if(prob1 > 0.5)
							predictLabels.add(predictStr);
						
						break;
					case ASKMORE:
						Iterator<Entry<String, Double>> iterator = predictMap.entrySet().iterator();
						while (iterator.hasNext()) {
							Entry<String, Double> pair = (Entry<String, Double>) iterator.next();
						    String attr = (String)pair.getKey();
						    double prob2 = pair.getValue();
						    
						    if(prob2 > 0.5)
						    	predictLabels.add(attr);
						}
						break;
					}
					
					predMatrix[count] = this.buildMatrix(predictLabels, classifiers);
					
				}
				else
					logger.error("cannot classify the visual object." );
				
				count++;
			}

//			// show matrix
//			for(int i = 0; i < targMatrix.length; i++){
//				for(int j = 0; j < targMatrix[0].length; j++){
//					logger.info("targMatrix["+i+"]["+j+"]: " + targMatrix[i][j]);
//					logger.info("predMatrix["+i+"]["+j+"]: " + predMatrix[i][j]);
//				}
//			}
			
			Map<String, Integer> evalMatrix = computeMatrix(ontology, targMatrix, predMatrix);
			double score = (Double)caluclateScore(ontology, evalMatrix, this.scoreIndex);
			this.scoreList.put(numPoint, score);
			
			if(numPoint >= this.numPos){
				Set<Integer> keySet = this.scoreList.keySet();
				int[] scoreLabel = new int[keySet.size()];
				double[] scoreList = new double[keySet.size()];
				int num = 0;
				for(Integer label: keySet){
					scoreLabel[num] = label;
					scoreList[num] = this.scoreList.get(label);
					num++;
				}
				matLink.excuteFunc("evalF1Report", Constants.simulateExpDir, nfold, ontology, scoreLabel, scoreList, this.policySettings.get(this.policyName), this.numPos, this.numNeg, ontology);
			}
		}
		
	}
	
	@Override
	public void evalClassifiers(String ontology, List<? extends BasicObject> tstList,
			TestClassifierCollection classifiers, int requestIndex, String filename) {
		// TODO Auto-generated method stub
		
	}

	public void evalClassifiers(int countOfObject, int nfold, String requestClass, List<VisualObject> tstList,
			TestClassifierCollection classifiers, int i) {
		
	}

}
