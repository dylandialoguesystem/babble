package babble.evaluation;

import babble.dialog.chatinterface.DialogueConsole;
import babble.dialog.env.DyadicDialogueEnvironment;
import babble.dialogue.IncrementalDialogueAgent;
import babble.simulation.SemanticUserSimulation;

public class ConsoleTester {

	DyadicDialogueEnvironment env;

	public ConsoleTester(IncrementalDialogueAgent other) throws Exception {
		DialogueConsole usr = new DialogueConsole("usr");
		
		// DSQLearningAgent dylan=new DSQLearningAgent("sys",
		// "restaurant-search", "2016-english-ttr-restaurant-search");
		// dylan.loadPolicyFromFile("initial_policy");
		env = new DyadicDialogueEnvironment(usr, other);
		env.start();

	}

	public static void main(String[] a) throws Exception
	{
		SemanticUserSimulation sim = new SemanticUserSimulation(
				"sys", "restaurant-search","2016-english-ttr-restaurant-search");
		
		ConsoleTester tester=new ConsoleTester(sim);
	}

}
