package babble.evaluation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import babble.dialog.env.DyadicDialogueEnvironment;
import babble.dialogue.DSQLearningAgent;
import babble.dialogue.IncrementalDialogueAgent;
import babble.simulation.SimpleReplaySimulatedDialogueAgent;
import csli.util.Pair;
import qmul.ds.Dialogue;
import qmul.ds.Utterance;
import qmul.ds.babi.BabiDialogue;

public class CrossValidationEvaluation {
	public static Pair<Double, Double> eval(
		String inDialoguesRoot,
		int inTrainsetSize,
		int inFoldNumber,
		List<Integer> inDatasetShuffle
	) throws IOException, InterruptedException {
		List<Dialogue> dialogues = GeneralizationExperiment.loadCorpus(inDialoguesRoot);
		GeneralizationExperiment experiment = new GeneralizationExperiment(
			"restaurant-search",
			"2016-english-ttr-restaurant-search",
			"sys"
		);
		List<Integer>
			trainSetIndex = new ArrayList<>(),
			testSetIndex = new ArrayList<>();
		for (int dialogueIndex = 0; dialogueIndex != inDatasetShuffle.size(); ++dialogueIndex) {
			if (
				inFoldNumber * inTrainsetSize <= dialogueIndex &&
				dialogueIndex < (inFoldNumber + 1) * inTrainsetSize
			) {
				trainSetIndex.add(inDatasetShuffle.get(dialogueIndex));
			}
			else {
				testSetIndex.add(inDatasetShuffle.get(dialogueIndex));
			}
		}
		double
			perDialogueAccuracy = 0.0,
			perUtteranceAccuracy = 0.0;
		List<Dialogue>
			trainSet = new ArrayList<>(),
			testSet = new ArrayList<>();
		for (Integer trainDialogueIndex: trainSetIndex) {
			trainSet.add(dialogues.get(trainDialogueIndex));
		}
		for (Integer testDialogueIndex: testSetIndex) {
			testSet.add(dialogues.get(testDialogueIndex));
		}
		experiment.train(trainSet);
		Pair<Double, Double> accuracy = experiment.test(testSet);
		perUtteranceAccuracy += accuracy.a;
		perDialogueAccuracy += accuracy.b;
	
		return new Pair<>(perUtteranceAccuracy, perDialogueAccuracy);
	}

	public static Pair<Double, Double> trainAndEvaluate(
		List<BabiDialogue> inTrainSet,
		List<BabiDialogue> inTestSet
	) throws IOException, InterruptedException {
		// TODO: agent.train(inTrainSet);
		DSQLearningAgent dylan = new DSQLearningAgent(
			"sys",
			"restaurant-search",
			"2016-english-ttr-restaurant-search"
		);
		dylan.loadPolicyFromFile("policy2");
		List<Dialogue> dialogues = new ArrayList<>();
		for (BabiDialogue testDialogue: inTestSet) {
			IncrementalDialogueAgent usr = new SimpleReplaySimulatedDialogueAgent("usr", testDialogue);
			DyadicDialogueEnvironment env = new DyadicDialogueEnvironment(dylan, usr);
			env.start();
			env.join();
			dialogues.add((Dialogue) env.getDialogue().clone());
			env.resetEnvironment();
		}
		List<BabiDialogue> allDialogues = new ArrayList<>(inTestSet);
		allDialogues.addAll(inTrainSet);
		return getAccuracy(dialogues, allDialogues);
	}

	public static Pair<Double, Double> getAccuracy(List<Dialogue> modelOutput, List<BabiDialogue> goldDialogues) {
		int
			correctDialogues = 0,
			correctTurns = 0,
			overallTurns = 0;
		Map<Integer, List<BabiDialogue>> dialoguesGroupedByLength = groupDialoguesByLength(goldDialogues);

		for (Dialogue dialogue: modelOutput) {
			int correctTurnsNumber = 0;
			for (int utteranceIndex = 0; utteranceIndex != dialogue.size(); ++utteranceIndex) {
				Utterance utterance = dialogue.get(utteranceIndex);
				if (!utterance.getSpeaker().equals("sys")) {
					++correctTurnsNumber;
					continue;
				}
				for (BabiDialogue goldDialogue: dialoguesGroupedByLength.get(dialogue.size())) {
					Utterance systemUtterance = goldDialogue.get(utteranceIndex);
					if (systemUtterance.getText().equals(utterance.getText())) {
						++correctTurns;
						break;
					}
				}
			}
			correctDialogues += correctTurnsNumber / dialogue.size(); // integer division is intentional
		}
		return new Pair<>(correctDialogues / (double) modelOutput.size(), correctTurns / (double) overallTurns);
	}

	private static Map<Integer, List<BabiDialogue>> groupDialoguesByLength(List<BabiDialogue> inDialogues) {
		Map<Integer, List<BabiDialogue>> result = new HashMap<>();
		for (BabiDialogue dialogue: inDialogues) {
			Integer size = dialogue.size();
			if (!result.containsKey(size)) {
				result.put(size, new ArrayList<>());
			}
			result.get(size).add(dialogue);
		}
		return result;
	}

	public static List<Integer> readDatasetShuffle(String inFile) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(inFile));
		String line = in.readLine();
		List<Integer> shuffle = Arrays.asList(line.split(" "))
			.stream()
			.map(x -> Integer.parseInt(x))
			.collect(Collectors.toList());
		in.close();
		return shuffle;
	}

	public static void main(String[] args) throws IOException, InterruptedException {
		if (args.length != 4) {
			System.out.println(
				"Usage: CrossValidationEvaluation.java " +
				"<dialogues root> <#train dialogues> <#fold> <dataset shuffle>"
			);
			return;
		}
		String
			dialoguesRoot = args[0],
			datasetShuffleFile = args[3];
		int
			trainingDialoguesNumber = Integer.parseInt(args[1]),
			foldNumber = Integer.parseInt(args[2]);
		List<Integer> datasetShuffle = readDatasetShuffle(datasetShuffleFile);
		Pair<Double, Double> combinedAccuracy = CrossValidationEvaluation.eval(
			dialoguesRoot,
			trainingDialoguesNumber,
			foldNumber,
			datasetShuffle
		);
		System.out.println(String.format("Results for %d train dialogues:", trainingDialoguesNumber));
		System.out.println(String.format("Per-utterance accuracy: %.3f", combinedAccuracy.a));
		System.out.println(String.format("Per-dialogue accuracy: %.3f", combinedAccuracy.b));
	}
}
