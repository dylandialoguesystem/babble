package babble.evaluation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;

import babble.vision.classification.IncremClassifier;
import babble.vision.classification.Concept;
import babble.vision.classification.VisualClassifier;
import babble.vision.object.BasicObject;
import babble.vision.object.VisualObject;
import babble.vision.tools.Constants;
import babble.vision.tools.MapToolKit;
import babble.vision.tools.MatlabLink;
import babble.vision.tools.NormalDistribution;
import weka.core.Instance;
import weka.core.Instances;

/**
 * Group all binary attribute-based classifiers under same category (i.e. color, shape)
 * train classifiers for same object, do classification as well as measure the classifier
 *  reliability using matrix (e.g. RSE and relative entropy)
 * 
 * @author Yanchao Yu
 */
public class TestClassifierCollection{
	static Logger logger = Logger.getLogger(TestClassifierCollection.class);
	
	private final String ontology;
	private HashMap<String, VisualClassifier> classifierList;
	private HashMap<String, Double> evalMap;
	private HashMap<String, Double> resultMap;
	private Map<String, Double> normalizeMap;
	
	public boolean isNormalizing = false;
	
	public TestClassifierCollection(String ontology){
		this.ontology = ontology;
		
		classifierList = new HashMap<String, VisualClassifier>();
	}
	
	private MatlabLink matLink;
	public TestClassifierCollection(String ontology, MatlabLink matLink){
		this.matLink = matLink;
		this.ontology = ontology;
		
		classifierList = new HashMap<String, VisualClassifier>();
	}
	
	public void setNomalization(boolean isNormalizing){
		this.isNormalizing = isNormalizing;
	}
	
	
	/**
	 * Add new single classifier with Classifier object and attribute name
	 * @param attrribute Attribute name
	 * @param classifier an object of specific classifier
	 */
	public void addNewClassifier(String attrribute, VisualClassifier classifier){
		if(classifierList != null){
			if(!classifierList.containsKey(attrribute))
				classifierList.put(attrribute, classifier);
		}
	}
	
	/**
	 * Add a group of binary classifiers with mapped attribute names
	 */
	public void addAllClassifiers(HashMap<String, VisualClassifier> classifierList){
		this.classifierList = classifierList;
	}

	/**
	 * Train the classifiers on same object with attribute-labels
	 * @param feat visual features of specific object
	 * @param labels annotation labels
	 * @return flag whether classifiers are trained successfully
	 */
	public boolean trainModle(VisualObject obj) {
		if(classifierList != null && !classifierList.isEmpty()){
			double[] feat = obj.getFeat();
			List<String> labels = obj.getLabels();
			
			logger.debug("obj lables: " + obj.getDescription());
			
			Iterator<Entry<String, VisualClassifier>> iterator = classifierList.entrySet().iterator();
			while (iterator.hasNext()) {
				 Map.Entry<String, VisualClassifier> pair = (Map.Entry<String, VisualClassifier>) iterator.next();
				 String attr = (String)pair.getKey();
				 VisualClassifier classifier = (VisualClassifier)pair.getValue();
				 
				// check if the classifier is an object of incremental classifier 
				 if(classifier instanceof IncremClassifier){
					 String label = "0";
					 if(labels.contains(attr))
						 label = "1";
							 
//					 logger.debug("label for classifier["+attr+"]: " + label);
					 ((IncremClassifier)classifier).trainOnSingleInstance(feat, label);
				}
				 
//				 this.getEvaluations(obj);
			}
			return true;
		}
		return false;
	}
	
	/**
	 * classify the object through all classifiers
	 * @param obj object with all details
	 * @return
	 */
	public boolean doClassify(BasicObject obj){
		if(evalMap == null)
			evalMap = new HashMap<String, Double>();
		else
			evalMap.clear();
		
		if(resultMap == null)
			resultMap = new HashMap<String,Double>();
		else
			resultMap.clear();
		
		Iterator<Entry<String, VisualClassifier>> iterator = classifierList.entrySet().iterator();
		while (iterator.hasNext()) {
		    Map.Entry<String, VisualClassifier> pair = (Map.Entry<String, VisualClassifier>) iterator.next();
		    String attr = (String)pair.getKey();
		    VisualClassifier classifer = (VisualClassifier)pair.getValue();

			if(classifer instanceof IncremClassifier){
				double[] result = ((IncremClassifier)classifer).predictOnSingleInstance(((VisualObject)obj).getFeat());
				resultMap.put(attr, result[0]);
				
				// Classifier Performance
				logger.debug("Attribute["+attr+"] = " + result[0] + " -- " + result[1]);
				evalMap.put(attr, ((IncremClassifier)classifer).getTotalEntropyForSignleClassifier());
			}
			else
				return false;
		}
//		logger.debug("ResultMap: " + resultMap);
		
		if(isNormalizing)
			normalizeMap = MapToolKit.getInstance().normalize(resultMap);
		else
			normalizeMap = null;
			
		return true;
	}
	
	/**
	 * @return normalized map of attributes and their corresponding predict scores
	 */
	public Map<String, Double> getNormalizedMap(){
		return this.normalizeMap;
	}

	/**
	 * @return map of attributes and their corresponding predict scores
	 */
	public HashMap<String, Double> getResultMap() {
		return this.resultMap;
	}
	
	/**
	 * @return map of classifiers and their corresponding relative entropies
	 */
	public HashMap<String, Double> getEntropyMap(){
		return this.evalMap;
	}
	
	/**
	 * @return List of predict results containing attribute name, probabilities and relative entropy
	 */
	public List<Concept> getResultTypes(){
		List<Concept> predList = new ArrayList<Concept>();

		if((this.evalMap != null && !this.evalMap.isEmpty()) && (this.resultMap != null && !this.resultMap.isEmpty())){
			Iterator<Entry<String, VisualClassifier>> iterator = classifierList.entrySet().iterator();
			while (iterator.hasNext()) {
				 Map.Entry<String, VisualClassifier> pair = (Map.Entry<String, VisualClassifier>) iterator.next();
				 String attr = (String)pair.getKey();
				 
				 Concept pred = new Concept(null,this.ontology,attr);
				 pred.setPredictProb(this.resultMap.get(attr),this.isNormalizing);
				 pred.setEntropy(this.evalMap.get(attr));
				 
				 
				 if(this.isNormalizing)
					 pred.setEntropy(this.resultMap.get(attr));
				 
				 predList.add(pred);
			}
		}
		
		return predList;
	}
	
	/**
	 * @return the best predicted result cross all classifiers
	 */
	public Map<String, Double> normalize(HashMap<String, Double> map){
		if(map != null && !map.isEmpty()){
			Map<String, Double> normalizes = NormalDistribution.getInstance().getProbabilities(map);
			return normalizes;
		}
		return null;
	}
	
	/**
	 * @return flag if the classifier list is empty
	 */
	public boolean isEmpty(){
		return this.classifierList.isEmpty();
	}
	
	/**
	 * @return map of attributes and their visual classifiers 
	 */
	public HashMap<String, VisualClassifier> getClassifiers(){
		return this.classifierList;
	}

	public String getEvaluations(){
		String result = "";
		Iterator<Entry<String, VisualClassifier>> iterator1 = classifierList.entrySet().iterator();
		while (iterator1.hasNext()) {
		    Map.Entry<String, VisualClassifier> pair = (Map.Entry<String, VisualClassifier>) iterator1.next();
		    VisualClassifier classifer = (VisualClassifier)pair.getValue();
		    
		    if(classifer instanceof IncremClassifier){
		    	result += ((IncremClassifier)classifer).evaluate(((IncremClassifier)classifer).wholeTrainSet, 10);
		    }
		}
		return result;
	}
	
	/**
	 * Display the map content in the console
	 * @param map the map of attributes' prediction scores
	 */
	private void printMap(Map<String, Double> map){
		Iterator<Entry<String, Double>> iterator = map.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, Double> entry = (Entry<String, Double>) iterator.next();
			String key = (String)entry.getKey();
			double  value = (double)entry.getValue();
			logger.debug("Key: [" +key+ "] = " + value);
		}
	}
	
	/**
	 * @return sorted entropies across classifiers.
	 */
	public Map<String, Double> getSortedEntropy(){
		HashMap<String, Double> entropies = new HashMap<String, Double>();
		
		Iterator<Entry<String, VisualClassifier>> iterator = classifierList.entrySet().iterator();
		while (iterator.hasNext()) {
		    Map.Entry<String, VisualClassifier> pair = (Map.Entry<String, VisualClassifier>) iterator.next();
		    String attr = (String)pair.getKey();
		    VisualClassifier classifer = (VisualClassifier)pair.getValue();
		    
		    if(classifer instanceof IncremClassifier){
		    	double entropy = ((IncremClassifier)classifer).getTotalEntropyForSignleClassifier();
		    	entropies.put(attr, entropy);
		    }
		}
		
		Map<String, Double> sortedEntropy = MapToolKit.getInstance().sortByComparator(entropies,true);
		sortedEntropy.putAll(entropies);
		return sortedEntropy;
	}
	
//	/**
//	 * @return sorted RMSE across classifiers.
//	 */
//	public Map<String, Double> getSortedRMSE(){
//		HashMap<String, Double> rmses = new HashMap<String, Double>();
//		
//		Iterator iterator = classifierList.entrySet().iterator();
//		while (iterator.hasNext()) {
//		    Map.Entry pair = (Map.Entry) iterator.next();
//		    String attr = (String)pair.getKey();
//		    VisualClassifier classifer = (VisualClassifier)pair.getValue();
//		    
//		    if(classifer instanceof IncremClassifier){
//		    	double rmse = ((IncremClassifier)classifer).getRMSE();
//		    	rmses.put(attr, rmse);
//		    }
//		}
//		
//		Map<String, Double> sortedRMSE = MapToolKit.getInstance().sortByComparator(rmses,true);
//		sortedRMSE.putAll(rmses);
//		return sortedRMSE;
//	}
	

	/******************************* Experiment Methods (EM) ********************************/
	public boolean trainClassifiers(List<VisualObject> objList) {
		int count =0;
		for(VisualObject vObj : objList){
			List<String> labels = vObj.getLabels();
			logger.debug("sample"+count+": obj["+(vObj.getFileName())+"]: " + labels.get(0) +","+ labels.get(1)+";");
			if(!trainModle(vObj))
				return false;
			count++;
		}
		return true;
	}
	
	public boolean trainClassifiers(){
		HashMap<String, VisualClassifier> map = this.getClassifiers();
		Iterator<Entry<String, VisualClassifier>> iterator = map.entrySet().iterator();
		while(iterator.hasNext()){
			VisualClassifier classifier = iterator.next().getValue();
			logger.debug("Attribute["+classifier.getAttribute()+"]:");
			if(classifier instanceof IncremClassifier){
				Instances trainSet = ((IncremClassifier) classifier).getWholeTrainSet();
				
				for(int i=0; i < trainSet.size(); i++){
					Instance instance = trainSet.get(i);
					logger.debug("train classifier at " + i);
					if(!((IncremClassifier) classifier).trainOnSingleInstance(instance))
						return false;
				}
			}
		}
		return true;
	}

	public void testClassifiers(int i, int numPos, List<VisualObject> tstSet, String policy) {
		double[][] actualmatrix = new double[tstSet.size()][classifierList.size()];
		double[][] predsmatrix = new double[tstSet.size()][classifierList.size()];
		
		int count = 0;
		for(VisualObject vObj : tstSet){
			actualmatrix[count] = buildMatrix(vObj.getDescription());
			
			if(doClassify(vObj)){
				Map<String, Double> predictMap = this.getNormalizedMap();
				if(this.isNormalizing)
					predictMap	= this.getNormalizedMap();
				else
					predictMap = this.getResultMap();
				
				predictMap = MapToolKit.getInstance().sortByComparator(predictMap, true);
				String predictStr = MapToolKit.getInstance().getTopKey(predictMap);
					
				logger.debug("Best Result: " + predictStr);
				predsmatrix[count] = buildMatrix(predictStr);
				
//				buildTextFile(count, vObj, predictMap);
			}
			count++;
		}
		Map<String, Integer> smpDistribution = this.getSampleDistribution();
		buildReport(i, actualmatrix, predsmatrix, policy, numPos, tstSet.size(), smpDistribution.toString());
	}

	private String[] clsList;
	public double[] buildMatrix(String data) {
		clsList = new String[classifierList.size()];
		double[] vector = new double[classifierList.size()];
		int i=0;
		Iterator<Entry<String, VisualClassifier>> iterator = classifierList.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, VisualClassifier> pair = (Entry<String, VisualClassifier>) iterator.next();
		    String attr = (String)pair.getKey();
		    clsList[i] = attr;
		    
		    //logger.debug("Classifier["+attr+"]: " + "data = " + data);
		    if(data == null)
		    	vector[i++] = 0;
		    else{
			    if(data.contains(attr))
			    	vector[i++] = 1;
			    else
			    	vector[i++] = 0;
		    }
		}
		return vector;
	}

	public void buildReport(int index, double[][] actual, double[][] pred, String policy, int numPos, int numNeg, String sampleDistribution) {
		matLink.excuteFunc("evalExcelReport", Constants.xlsExpDir, this.ontology, clsList, index, actual, pred, policy, numPos, numNeg, sampleDistribution);
	}

	public void resetClassifier() {
		Iterator<Entry<String, VisualClassifier>> iterator = classifierList.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, VisualClassifier> pair = (Entry<String, VisualClassifier>) iterator.next();
			 VisualClassifier classifer = (VisualClassifier)pair.getValue();
			 
			 // check if the classifier is an object of incremental classifier 
			 if(classifer instanceof IncremClassifier){
				 ((IncremClassifier)classifer).reset();
			}
		}
	}

	public final int ENTROPY = 0;
	public final int RMSE = 1;
	public String getWorstClassifier(int type) {
		switch(type){
			case ENTROPY:
					Map<String, Double> sortEntropy = this.getSortedEntropy();
					return MapToolKit.getInstance().getTopKey(sortEntropy);
//			case RMSE:
//					Map<String, Double> sortRMSE = this.getSortedRMSE();
//					return MapToolKit.getInstance().getTopKey(sortRMSE);
		}
		return null;
	}

	public int size() {
		return classifierList.size();
	}
	
//	private void buildTextFile(int count, VisualObject vObj, Map<String, Double> hashMap){
//		if(!isTitleCreated){
//			writeLinbyLin(Constants.resultReport, createTitle());
//		}
//
//		String report = "";
//		report += String.format("%-5s", String.valueOf(count));
//		report += String.format("%-20s", vObj.getFileName());
//		report += String.format("%-20s", vObj.getDescription());
//		
//		Iterator<Entry<String, VisualClassifier>> iterator = classifierList.entrySet().iterator();
//		while (iterator.hasNext()) {
//			Entry<String, VisualClassifier> pair = (Entry<String, VisualClassifier>) iterator.next();
//			 String attr = (String)pair.getKey();
//			 report += String.format("%-30s", hashMap.get(attr));
//		}
//		
//		writeLinbyLin(Constants.resultReport, report);
//	}
//	
//	private boolean isTitleCreated = false;
//	private String createTitle(){
//		String title ="The Results of Classifiers:\n";
//		title += String.format("%-5s", "ID");
//		title += String.format("%-20s", "Sample ID");
//		title += String.format("%-20s", "Description");
//		
//		Iterator<Entry<String, VisualClassifier>> iterator = classifierList.entrySet().iterator();
//		while (iterator.hasNext()) {
//			 Entry<String, VisualClassifier> pair = (Entry<String, VisualClassifier>) iterator.next();
//			 String attr = (String)pair.getKey();
//			title += String.format("%-30s", attr);
//		}
//		isTitleCreated = true;
//		return title;
//	}
//
//	
//	private void writeLinbyLin(String path, String msg){
//		try {
//			BufferedWriter output = new BufferedWriter(new FileWriter(path, true));
//			output.write(msg + System.getProperty("line.separator"));
//			output.flush();
//			output.close();
//		} catch (IOException e) {
//			logger.error(e.getMessage());
//		}
//	}
	/************************************* END OF EM ******************************************/

	
	public Map<String, Integer> getRequests(){
		return this.requiredClass;
	}
	
	public Map<String, Integer> getSampleDistribution(){
		Map<String, Integer> smapleMap  = new HashMap<String, Integer>();
		Iterator<Entry<String, VisualClassifier>> iterator = classifierList.entrySet().iterator();
		while(iterator.hasNext()){
			VisualClassifier classifier = iterator.next().getValue();
			smapleMap.put(classifier.getAttribute(), ((IncremClassifier)classifier).getNumPosInstances());
//			logger.debug("Classifier["+classifier.getAttribute()+"] getNumPosInstances:" + ((IncremClassifier)classifier).getNumPosInstances());
//			logger.debug("Classifier["+classifier.getAttribute()+"] getNumPosInstances:" + classifier.getNumPosInstances());
//			logger.debug("Classifier["+classifier.getAttribute()+"] getNumOfInstances:" + ((IncremClassifier)classifier).getNumOfInstances());
		}
		
		return smapleMap;
	}
	
	
	private Map<String, Integer> requiredClass = null;
	public boolean checkBalance(int acceptGap) {
		VisualClassifier maxClass = this.getMax();
		VisualClassifier minClass = this.getMin();
//		logger.debug("check Balance: max is " +  maxClass.getAttribute() + "("+maxClass.getNumPosInstances()+")"
//				+ "  min is " +  minClass.getAttribute() + "("+minClass.getNumPosInstances()+")");
		
		int gap = maxClass.getNumPosInstances() - minClass.getNumPosInstances();
		if(gap <= acceptGap){
			requiredClass = null;
			return true;
		}
		else{
			requiredClass = new HashMap<String, Integer>();
			requiredClass.put(minClass.getAttribute(), gap);
			logger.debug("minClass.getAttribute(): " + minClass.getAttribute() + " gap: " + gap); 
			return false;
		}
	}
	
	private VisualClassifier getMax(){
		VisualClassifier attr = null;
		int numOfPos = -1;
		Iterator<Entry<String, VisualClassifier>> iterator = classifierList.entrySet().iterator();
		while(iterator.hasNext()){
			VisualClassifier classifier = iterator.next().getValue();
			if(numOfPos == -1){
				numOfPos = classifier.getNumPosInstances();
				attr = classifier;
			}
			else{
				if(numOfPos < classifier.getNumPosInstances()){
					numOfPos = classifier.getNumPosInstances();
					attr = classifier;
				}
			}	
			logger.debug("check Max: " +  attr.getAttribute() + "("+numOfPos+")");
		}
		
		return attr;
	}
	
	private VisualClassifier getMin(){
		VisualClassifier attr = null;
		int numOfPos = -1;
		Iterator<Entry<String, VisualClassifier>> iterator = classifierList.entrySet().iterator();
		while(iterator.hasNext()){
			VisualClassifier classifier = iterator.next().getValue();
			
			if(numOfPos == -1){
				numOfPos = classifier.getNumPosInstances();
				attr = classifier;
			}
			else{
				if(numOfPos > classifier.getNumPosInstances()){
					numOfPos = classifier.getNumPosInstances();
					attr = classifier;
				}
			}	
			logger.debug("check Min: " +  attr.getAttribute() + "("+numOfPos+")");
		}
		
		return attr;
	}
	
	public Map<String, double[]> getPredictResultsOnPretrainedData(int size){
		Map<String, double[]> predMap = new HashMap<String, double[]>();
		Set<String> attrList = new HashSet<String>();
		
		double[][] predResults = new double[classifierList.size()][size];
//		logger.debug("predResults : " + classifierList.size() + " x " + size);
		int count = 0;
		HashMap<String, VisualClassifier> map = this.getClassifiers();
		for(Entry<String, VisualClassifier> entry : map.entrySet()){
			String attr = entry.getKey();
			attrList.add(attr);
//			logger.debug("Classifier["+count+"]: " + attr);
			VisualClassifier classifier = entry.getValue();
			
			if(classifier instanceof IncremClassifier){
				Instances newInstance = ((IncremClassifier)classifier).getWholeTrainSet();
				double[] preds = ((IncremClassifier)classifier).predictOnInstances(newInstance);
				predResults[count++] = preds;
			}
		}
//		logger.debug("attrList: " + attrList.size());
		
		if(this.isNormalizing)
			predMap = MapToolKit.getInstance().gNormalize(attrList, predResults);
		
		return predMap;
	}
	
	public Map<String, Double> getPredictResultsOnUnlabelData(BasicObject obj){
		if(this.doClassify(obj)){
			return this.normalizeMap;
		}
		
		return null;
	}
}