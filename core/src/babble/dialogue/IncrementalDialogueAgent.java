package babble.dialogue;

import babble.dialog.env.DialogueEnvironment;
import qmul.ds.dag.UtteredWord;

public abstract class IncrementalDialogueAgent{

	
	protected String name;

	protected DialogueEnvironment dialogue_env=null;

	
	public IncrementalDialogueAgent(String name)
	{
		this.name=name;
		
	}
	
	public void join(DialogueEnvironment env)
	{
		this.dialogue_env=env;
	}

	
	public String name() {
		return name;
	}

	
	/**
	 * Override this method for deciding what to output next, given current state of the dialogue.
	 * 
	 * @return the next word to be uttered by agent. null if agent has nothing to say.
	 */
	public abstract UtteredWord nextWord() throws InterruptedException;

	

	/**
	 * Implement this to specify how the agent is to integrate w.
	 * @param w
	 * @return true of the word was successfully integrated.
	 */
	public abstract void wordUttered(UtteredWord w);
	
	/**
	 * implement this to specify how the agent resets itself to its initial state
	 * 
	 */
	public abstract void reset();
	
	public String toString()
	{
		return this.name();
	}
	
	public boolean equals(Object o)
	{
		if (!(o instanceof IncrementalDialogueAgent))
			return false;
		
		return this.name.equals(((IncrementalDialogueAgent)o).name);
	}
	
	public int hashCode()
	{
		return name.hashCode();
	}

	/**
	 * override to specify what an agent should do when another joins the conversation.
	 * @param a
	 */
	public void joinedConversation(IncrementalDialogueAgent a) {
		
		
	}


}
