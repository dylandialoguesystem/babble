package babble.dialogue;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import babble.dialog.rl.TTRMDPStateEncoding;
import babble.simulation.SemanticUserSimulation;
import babble.simulation.UtteranceTemplate;
import qmul.ds.InteractiveContextParser;
import qmul.ds.Utterance;
import qmul.ds.babi.BabiDialogue;
import qmul.ds.dag.UtteredWord;
import qmul.ds.formula.TTRRecordType;

/**
 * Intended to be used as a dialogue agent proper rather than a simulation, but it is
 * identical in implementation to babble.simulation.SemanticUserSimulation - except that it is not used for 
 * RL, so does not monitor the other for partial match. It works with full semantic matching. Generates <wait>
 * if it has nothing to say. 
 * 
 * @author Arash
 *
 */

public class SemanticDialogueAgent extends SemanticUserSimulation {
	

	public SemanticDialogueAgent(String name) {
		super(name);
	}

	public SemanticDialogueAgent(
		String name,
		String domain,
		String grammarResourceName
	) throws IOException {
		super(name,domain,grammarResourceName);
		
	}

	

	
	public UtteredWord nextWord() {
		
		if (this.exceptionCaught)
		{
			return new UtteredWord(DSDialogueAgent.BYE, this.name);
		}
		
		Utterance lastUtterance = this.dialogue_env.getLastUtterance();
		
		
		
		

		if (this.dialogue_env.hasFloor(this)) {
			// I have the floor, am I finished?
			logger.debug("Simulator has the floor");
			if (this.toUtter.isEmpty()) {
				logger.debug("End of the current segment. Trying to load another one");
				List<List<Integer>> matches = findMatchesToCurrentState();
				assert !matches.isEmpty();
				lastSemanticTemplateUsed = matches.isEmpty() ? null : matches.get(0);
				List<UtteranceTemplate> responseTemps = triggerTemplates.get(lastSemanticTemplateUsed);
				Utterance response = responseTemps.get(rand.nextInt(responseTemps.size())).getRandomUtterance(name);
				this.toUtter.addAll(response.getWords());
			}
			return toUtter.peek();
		} else if (this.dialogue_env.floorIsOpen()) {
			// the floor is open.
			logger.debug("Floor is open");
			// I'm the last speaker
			if (lastUtterance != null && lastUtterance.getSpeaker().equals(name)) {
				// floor is open and I'm the last speaker. So I must have opened it.
				// The other only generated <wait> action.
				logger.debug("I'm the last speaker and floor is open");

				if (fullyMatches()) {
					List<List<Integer>> matches = findMatchesToCurrentState();
					lastSemanticTemplateUsed = matches.isEmpty() ? null : matches.get(0);
					List<UtteranceTemplate> responseTemps = triggerTemplates.get(lastSemanticTemplateUsed);
					Utterance response = responseTemps.get(rand.nextInt(responseTemps.size())).getRandomUtterance(name);
					this.toUtter.clear();// TODO: this is dangerous! Having to do this is bad.
					this.toUtter.addAll(response.getWords());
					return toUtter.peek();
				} else{
					logger.error("no full match. Don't know what to say. Just waiting by default");
					return new UtteredWord(DSDialogueAgent.BYE, this.name);
				}
			}

			// if we are here, last utt is from the other, and floor is open.
			List<Integer> state = stateEncoder.encode(parser.getContext());
			
			logger.debug("Checking current state against triggers:\n"+stateEncoder.prettyPrintState(state));
			state = state.subList(2, state.size());
			List<UtteranceTemplate> responseTemps=this.getNearestNeighbor(state);
			Utterance response = responseTemps.get(rand.nextInt(responseTemps.size())).getRandomUtterance(name);
			
			this.toUtter.clear();// TODO: this is dangerous! Having to do this is bad.
			this.toUtter.addAll(response.getWords());
			return toUtter.peek();
//			
//			if (fullyMatches()) {
//				logger.debug("Floor is open with full recognisable utterance from the other");
//				List<List<Integer>> matches = findMatchesToCurrentState();
//				lastSemanticTemplateUsed = matches.isEmpty() ? null : matches.get(0);
//				List<UtteranceTemplate> responseTemps = triggerTemplates.get(lastSemanticTemplateUsed);
//				Utterance response = responseTemps.get(rand.nextInt(responseTemps.size())).getRandomUtterance(name);
//				this.toUtter.clear();// TODO: this is dangerous! Having to do this is bad.
//				this.toUtter.addAll(response.getWords());
//				return toUtter.peek();
//			} else {
//				logger.debug("Other has released floor, but I can't recognise the context of the conversation");
//				//logger.error("Just waiting by default");
//				// invalid utterence by other. signal leaving of dialogue
//		
//				return new UtteredWord(DSDialogueAgent.BYE, this.name);
//			}

		} else {
			logger.debug("Other has the floor. Generating wait.");
			return new UtteredWord(DSDialogueAgent.WAIT, this.name);
//			logger.debug("Other has the floor. Is his utterance so far recognisable?");
//			if (partiallyMatches()) {
//				logger.debug("yes");
//				return new UtteredWord(DSDialogueAgent.WAIT, this.name);
//			} else {
//				logger.debug("no! Leaving dialogue");
//				return new UtteredWord(DSDialogueAgent.BYE, this.name);
//			}
		}
	}


	




	


	
}
