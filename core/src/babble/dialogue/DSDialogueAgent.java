package babble.dialogue;

import java.io.IOException;
import java.util.Comparator;
import java.util.Date;

import org.apache.log4j.Logger;

import babble.dialog.env.DialogueEnvironment;
import babble.dialog.env.DyadicDialogueEnvironment;
import babble.dialog.env.SimulatedDialogueEnvironment;
import babble.dialog.rl.ContextState;
import babble.dialog.rl.TTRMDPStateEncoding;
import babble.simulation.TemplateBasedSimulatedDialogueAgent;
import burlap.behavior.policy.Policy;
import burlap.behavior.singleagent.auxiliary.performance.LearningAlgorithmExperimenter;
import burlap.behavior.singleagent.auxiliary.performance.PerformanceMetric;
import burlap.behavior.singleagent.auxiliary.performance.TrialMode;
import burlap.behavior.valuefunction.QValue;
import burlap.mdp.singleagent.SADomain;
import qmul.ds.InteractiveContextParser;
import qmul.ds.dag.DAG;
import qmul.ds.dag.DAGTuple;
import qmul.ds.dag.GroundableEdge;
import qmul.ds.dag.UtteredWord;

public abstract class DSDialogueAgent extends IncrementalDialogueAgent {

	public static final String BYE="bye";
	public static final String RELEASE_TURN=InteractiveContextParser.RELEASE_TURN;
	public static final String WAIT=InteractiveContextParser.WAIT;
	
	protected Logger logger=Logger.getLogger(DSDialogueAgent.class);
	
	protected Policy policy;
	protected InteractiveContextParser parser;
	
	protected TTRMDPStateEncoding encoding;
	
	protected ContextState curState;
	protected ContextState initialState;
	
	protected SADomain domain;
	
	/**
	 * determines whether unsuccessful parses trigger an unsuccessful terminal state.
	 */
	protected boolean learningMode=false;
	
	
	
	public DSDialogueAgent(String name, String domain, String grammarResource) throws IOException{
		super(name);
		parser=new InteractiveContextParser(TTRMDPStateEncoding.GRAMMAR_FOLDER+grammarResource, name);
		encoding=new TTRMDPStateEncoding(domain, parser.getLexicon());
		initialState=encoding.getInitialState();
		curState=initialState;
		this.domain=encoding.generateDomain();
	}
	
	public void joinedConversation(IncrementalDialogueAgent a)
	{
		if (this!=a)
			parser.getContext().addParticipant(a.name());
	}
	
	public void join(DialogueEnvironment env)
	{
		if (!(env instanceof DyadicDialogueEnvironment))
			throw new IllegalArgumentException("Expected a DyadicDialogueEnvironment, got:"+env.getClass());
		
		this.dialogue_env=env;

		
		
	}
	
	/**
	 * Generate next word according to policy
	 * 
	 * @param env
	 */
	public UtteredWord generateNextWord()
	{
		String nextWord=policy.action(curState).actionName();
		if (generateWord(nextWord)==null)
		{
			logger.error("Policy selects unparsable word");
			return null;
		}
		return new UtteredWord(nextWord, name);
		
	}
	
	/**
	 * Generate a word, and update context accordingly.
	 * @param word
	 */
	public ContextState generateWord(String nextWord)
	{
		UtteredWord nextUttered=new UtteredWord(nextWord, this.name);
		DAG<DAGTuple, GroundableEdge> resultingContext=parser.parseWord(nextUttered);
		if (resultingContext == null)
			return null;
		
		logger.info("Generating word:"+nextWord);
		this.curState=encoding.encodeToBurlapState(parser.getContext());
		logger.info("Resulting state:");
		logger.info(curState);
		return curState;
		
		
		
	}
	
	public ContextState getCurrrentState()
	{
		return curState;
	}

	
	public UtteredWord nextWord() {
		//return next word accoring to policy. The word will be parsed when the environment actually calls the wordUttered() method below.
		String nextWord=policy.action(curState).actionName();
		return new UtteredWord(nextWord,name);
	}

	
	public boolean isInUnsuccessfulTerminalState()
	{
		return encoding.isUnsuccessfulTerminal(this.curState);
	}
	@Override
	public void wordUttered(UtteredWord w) {
		
		if (isInUnsuccessfulTerminalState())
			return;
		
		//if the word is leave dialogue then for now just transition to the unsuccessful terminal state
		if (w.word().equals(BYE))
		{
			logger.info("Parsed: "+BYE);
			logger.info("Setting state to unsuccessful terminal");
			this.setStateToUnsuccessfulTerminal();
			return;
			
		}
		
		DAG<DAGTuple, GroundableEdge> resultingContext=null;
		try{
			resultingContext=parser.parseWord(w);
		}
		catch(Exception e)
		{
			logger.error("Exception thrown by parser. Setting state to unsuccessful terminal.");
			logger.error(e);
			e.printStackTrace();
			this.setStateToUnsuccessfulTerminal();
			return;
		}

		if (resultingContext == null)
		{
			logger.info("Couldn't parse word:"+w);
			
			if (learningMode)
			{
				logger.info("Setting state to unsuccessful terminal");
				this.setStateToUnsuccessfulTerminal();
			}
			else logger.info("... skipping it");
			
			return;
		}
		logger.info("Parsed word:"+w);
		logger.info("Tree:"+parser.getContext().getCurrentTuple().getTree());
		this.curState=encoding.encodeToBurlapState(parser.getContext());
		logger.info("Resulting state: "+encoding.printState(curState));
		return;
		
	}
	
	public void setStateToUnsuccessfulTerminal()
	{
		this.curState=this.getUnsuccessfulTerminalState();
	}
	
	public ContextState getUnsuccessfulTerminalState()
	{
		return encoding.getUnsuccessfulTerminalState();
	}


	@Override
	public void reset() {
		parser.init();
		initialState=encoding.getInitialState();
		curState=initialState;
		
	}
	
	

	public TTRMDPStateEncoding getEncoding() {
		return encoding;
	}
	
	public static void main(String[] s) throws IOException
	{
		DSQLearningAgent dylan=new DSQLearningAgent("Dylan", "shopping-mall", "2016-english-ttr-shopping-mall");
		TemplateBasedSimulatedDialogueAgent sim=new TemplateBasedSimulatedDialogueAgent("usr", "shopping-mall");
		SimulatedDialogueEnvironment env=new SimulatedDialogueEnvironment(dylan, sim);
		
		long time1=new Date().getTime();
		dylan.learn(600, 1.0, 0.01);
		
		
		System.out.println(dylan.printQTable());
		long time2=new Date().getTime();
		
		System.out.println("Done! Training took "+(time2-time1)+" milliseconds");
		dylan.writePolicyToFile("policy2");
		
		
	}
	
	public abstract void writePolicyToFile(String policyFileName) throws IOException;
	

	public abstract void learn(int numEpisodes, double learningRate, double explorationRate);
	
	/**
	 * 
	 * @param policyFileName with no extension. extension specified internally by the implementing class.
	 * @throws IOException
	 */
	public abstract void loadPolicyFromFile(String policyFileName) throws IOException;
	
	public abstract String getPolicyFileExtension();
	/*
	public void plotEpisodes()
	{
		LearningAlgorithmExperimenter exp = new LearningAlgorithmExperimenter(env, 10, 100,
				qLearningFactory, sarsaLearningFactory);

		exp.setUpPlottingConfiguration(500, 250, 2, 1000,
				TrialMode.MOST_RECENT_AND_AVERAGE,
				PerformanceMetric.CUMULATIVE_STEPS_PER_EPISODE,
				PerformanceMetric.AVERAGE_EPISODE_REWARD);

		exp.startExperiment();
		exp.writeStepAndEpisodeDataToCSV("expData");
	}*/
	
	
	class QValueComparator implements Comparator<QValue>
	{

		@Override
		public int compare(QValue o1, QValue o2) {
			if (o1.q <o2.q)
				return +1;
			else if (o1.q>o2.q)
				return -1;
			else
				return 0;
		}
		
	}
	
	
	

}
