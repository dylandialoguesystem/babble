package babble.dialogue;

import babble.dialog.env.NonIncrementalSimulatedDialogueEnvironment;
import babble.dialog.env.SimulatedDialogueEnvironment;
import babble.dialog.rl.BreadthFirstRewardFunction;
import babble.dialog.rl.GoalBasedRewardFunction;
import babble.simulation.SemanticUserSimulation;
/**
 * The main, runnable class used to train a DS dialogue agent. 
 * 
 * Usage: java -jar babble.jar <number-of-episodes> <learning-rate> <exploration-rate>
 * 
 * Can be run just providing the first argument (number of episodes).
 * 
 * @author Arash
 *
 */

public class TrainDialogueAgent {

	public static void main(String[] s) throws Exception {
		// DialogueConsole arash = new DialogueConsole("arash");
		// arash.initalization();
		int episodes=0;
		double explorationRate=0.1;
		double learningRate=1.0;
		
		if (s.length<1)
		{
			System.out.println("Usage: java -jar babble.jar <number-of-episodes> <learning-rate> <exploration-rate>");
			System.out.println("Provide at least the number of episodes to train for");
			return;
		}
		else if (s.length==1)
		{
			episodes=Integer.parseInt(s[0]);
			System.out.println("Training. Assuming exploration rate of 0.1 and learning rate of 1");
		}
		else if (s.length==2)
		{
			System.out.println("Missing arguments.");
			System.out.println("Usage: java -jar babble.jar <number-of-episodes> <learning-rate> <exploration-rate>");
			System.out.println("Provide at least the number of episodes to train for");
			return;
		}
		else if (s.length==3)
		{
			episodes=Integer.parseInt(s[0]);
			explorationRate=Double.parseDouble(s[2]);
			learningRate=Double.parseDouble(s[1]);
			
			
		}
		else
		{
			System.out.println("Usage: java -jar babble.jar <number-of-episodes> <learning-rate> <exploration-rate>");
			return;
		}
			
		
		
		
		
		SemanticUserSimulation sim = new SemanticUserSimulation("usr", "restaurant-search",
				"2016-english-ttr-restaurant-search");

		

		// sim=new TemplateBasedSimulatedDialogueAgent("sim", "shopping-mall");
		DSQLearningAgent dylan = new DSQLearningAgent("sys", "restaurant-search", "2016-english-ttr-restaurant-search");
		// dylan.loadPolicyFromFile("policy2");
		SimulatedDialogueEnvironment env = new NonIncrementalSimulatedDialogueEnvironment(dylan, sim, new BreadthFirstRewardFunction(dylan.getEncoding()));
		
		dylan.learn(episodes, learningRate, explorationRate);
		dylan.writePolicyToFile("initial_policy");

	}

}
