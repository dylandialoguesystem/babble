package babble.dialogue;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import babble.dialog.env.SimulatedDialogueEnvironment;
import babble.dialog.rl.ContextState;
import babble.dialog.rl.DefaultingGreedyQPolicy;
import babble.dialog.rl.HashableContextStateFactory;
import babble.dialog.rl.QLearner;
import babble.dialog.rl.TTRMDPStateEncoding;
import burlap.behavior.singleagent.Episode;
import burlap.behavior.singleagent.learning.tdmethods.QLearningStateNode;
import burlap.behavior.valuefunction.QValue;
import burlap.mdp.core.action.Action;
import burlap.mdp.core.action.SimpleAction;
import burlap.statehashing.HashableState;

public class DSQLearningAgent extends DSDialogueAgent {

	protected static final Pattern Q_TABLE_ROW_PATTERN = Pattern.compile("\\s*\\[(.+)\\]\\s*\\-\\>\\s*\\{(.+)\\}\\s*");

	public DSQLearningAgent(String name, String domain, String grammarResource) throws IOException {
		super(name, domain, grammarResource);

	}

	@Override
	public void writePolicyToFile(String policyFileName) throws IOException {
		logger.info("Writing policy to file: "+TTRMDPStateEncoding.DOMAIN_FOLDER + encoding.getDomainName() + "/" + policyFileName+getPolicyFileExtension());
		if (this.qTable == null)
			throw new IllegalStateException("No policy to write - must learn it first");

		BufferedWriter writer = new BufferedWriter(new FileWriter(
				TTRMDPStateEncoding.DOMAIN_FOLDER + encoding.getDomainName() + "/" +  policyFileName+getPolicyFileExtension()));

		for (HashableState state : qTable.keySet()) {

			QLearningStateNode node = qTable.get(state);
			List<QValue> sorted=new ArrayList<QValue>(node.qEntry);
			Collections.sort(sorted, new QValueComparator());
			String line = state + "->{";
			
			for (QValue qv : sorted) {
				line += qv.a + "->" + qv.q + ", ";
			}
			line = line.substring(0, line.length() - 2) + "}";
			writer.write(line);
			writer.newLine();

		}
		writer.close();

	}

	private Map<HashableState, QLearningStateNode> qTable = null;

	@Override
	public void learn(int numEpisodes, double learningRate, double explorationRate) {
		if (this.dialogue_env==null || !(this.dialogue_env instanceof SimulatedDialogueEnvironment))
			throw new IllegalStateException(
					"Cannot learn without a Burlap Environment, got:" + ((this.dialogue_env==null)? "null":this.dialogue_env.getClass().toString()));

		this.learningMode=true;
		SimulatedDialogueEnvironment env = (SimulatedDialogueEnvironment) this.dialogue_env;

		QLearner learner = new QLearner(this.domain, 0.99, new HashableContextStateFactory(), 0.0, learningRate,
				explorationRate);
		List<Episode> result=new ArrayList<Episode>();
		for (int i = 0; i < numEpisodes; i++) {
			System.out.println("Episode number: "+i);
			Episode e = learner.runLearningEpisode(env);
			result.add(e);
			// e.write(outputPath + "ql_" + i);
			// System.out.println(i + ": " + e.maxTimeStep());

			//System.out.println("Finished learning episode:");
			//System.out.println(e.serialize());
			System.out.println("Dialogue at end of episode:");
			System.out.println(env.getDialogue());
			System.out.println(learner.printQTable(this.encoding));
			System.out.println();
			
			// reset environment for next learning episode
			env.resetEnvironment();
		}
		
		qTable = learner.getQTable();
		this.policy = new DefaultingGreedyQPolicy(learner, new SimpleAction(DSDialogueAgent.WAIT));
		this.learningMode=false;
		Episode.writeEpisodes(result, "data/Domains/"+this.encoding.getDomainName()+"/episodes/", "episode");
	}

	public String printQTable() {
		String result = "Q-Table -------------------------\n";

		for (HashableState state : qTable.keySet()) {
			
			QLearningStateNode node = qTable.get(state);
			result += state + "->{";
			
			
			for (QValue qv : node.qEntry) {
				result += qv.a + "->" + qv.q + ", ";
			}
			result = result.substring(0, result.length() - 2) + "}\n";

		}
		return result;
	}

	@Override
	public void loadPolicyFromFile(String policyFileName) throws IOException {
		if (this.qTable != null)
			logger.warn("Q-Table already loaded. Overwriting it.");

		logger.info("loading policy from file");
		this.qTable = new HashMap<HashableState, QLearningStateNode>();
		BufferedReader reader = new BufferedReader(new FileReader(
				TTRMDPStateEncoding.DOMAIN_FOLDER + encoding.getDomainName() + "/" + policyFileName+ getPolicyFileExtension()));

		String line;
		while ((line = reader.readLine()) != null) {
			if (line.trim().isEmpty()||line.trim().startsWith("//"))
				continue;
				

			Matcher m = Q_TABLE_ROW_PATTERN.matcher(line);
			if (!m.matches()) {
				logger.error("error reading q table row:" + line);
				logger.error("skipping it.");
				continue;
			}
			String state = m.group(1);
			String[] stateSValues = state.split(",");
			List<Integer> intValues = new ArrayList<Integer>();
			for (String sValue : stateSValues) {
				Integer intValue = Integer.parseInt(sValue.trim());
				intValues.add(intValue);

			}
			ContextState cState = new ContextState(intValues);
			logger.debug("Loaded state:" + cState);

			QLearningStateNode current = new QLearningStateNode(cState);

			String actionValueS = m.group(2);
			logger.debug("Loading Q values from string:" + actionValueS);
			String[] actionValues = actionValueS.split(",");
			for (String actionS : actionValues) {
				String[] actionValue = actionS.split("\\-\\>");
				if (actionValue.length != 2)
				{
					reader.close();
					throw new IllegalArgumentException("Bad Q-Table Row:" + line);
				}

				Action action = new SimpleAction(actionValue[0].trim());
				double value = Double.parseDouble(actionValue[1].trim());
				current.addQValue(action, value);
				logger.debug("adding Q-Value:" + action + "->" + value);
			}

			this.qTable.put(cState, current);

		}

		reader.close();
	
		QLearner qlearner = new QLearner(this.domain, 0.99, new HashableContextStateFactory(), 0.0, 1.0, 0.0);
		qlearner.setQTable(this.qTable);
		this.policy = new DefaultingGreedyQPolicy(qlearner, new SimpleAction(DSDialogueAgent.WAIT));
		logger.info("Done loading policy");

	}

	public static void main(String[] s) {
		String line = "[1,2,3,4] -> {arash,siavash} ";
		Matcher m = Q_TABLE_ROW_PATTERN.matcher(line);

		if (m.matches()) {
			System.out.println(m.group(1));
			System.out.println(m.group(2));
		}

	}

	protected static final String POLICY_FILE_EXT = ".qpol";

	@Override
	public String getPolicyFileExtension() {
		return POLICY_FILE_EXT;
	}

}
