package babble.utils;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import babble.dialog.rl.TTRMDPStateEncoding;
import qmul.ds.Dialogue;
import qmul.ds.InteractiveContextParser;
import qmul.ds.babi.BabiDialogue;


public class BabiStateEncodingGenerator {
	private static final int RANDOM_SEED = 273;
	private static final String DOMAIN = "restaurant-search";
	private TTRMDPStateEncoding encoder;
	private InteractiveContextParser parser;
	private String grammarResource;
	

	public BabiStateEncodingGenerator(String inGrammarResource) {
		grammarResource = inGrammarResource;
		encoder = new TTRMDPStateEncoding();
		parser = new InteractiveContextParser(TTRMDPStateEncoding.GRAMMAR_FOLDER + grammarResource);
	}

	public void createStateEncoding(String inDialoguesFile, int trainingDialoguesNumber) throws IOException {
		List<BabiDialogue> dialogues = BabiDialogue.loadFromBabiFile(
			new File(inDialoguesFile).getAbsolutePath(),
			BabiDialogue.getDefaultConfig()
		);
		Collections.shuffle(dialogues, new Random(RANDOM_SEED));
		List<Dialogue> trainingDialogues = dialogues.subList(0, trainingDialoguesNumber)
			.stream()
			.map(x -> (Dialogue)x)
			.collect(Collectors.toList());
		encoder.constructFromLoadedDialogues(trainingDialogues, DOMAIN, grammarResource);
	}

	public void save() throws IOException {
		encoder.writeEncodingToFile();
	}

	public static void main(String[] args) {
		try {
			if (args.length != 2) {
				System.out.println("Usage: BabiStateEncodingGenerator.java <babi file> <#training dialogues>");
				return;
			}
			String srcPath = args[0];
			int trainingDialoguesNumber = Integer.parseInt(args[1]);
			final String grammarResource = "2016-english-ttr-restaurant-search";

			BabiStateEncodingGenerator encodingGenerator = new BabiStateEncodingGenerator(grammarResource);
			encodingGenerator.createStateEncoding(srcPath, trainingDialoguesNumber);
			encodingGenerator.save();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
