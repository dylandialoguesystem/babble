package babble.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import babble.dialog.rl.TTRMDPStateEncoding;
import qmul.ds.InteractiveContextParser;
import qmul.ds.Utterance;
import qmul.ds.babi.BabiDialogue;


public class DSFeatureExtractor {
	private TTRMDPStateEncoding encoder = null;
	private InteractiveContextParser parser = null;

	public DSFeatureExtractor(String domain, String grammarResource) throws IOException {
		encoder = new TTRMDPStateEncoding(domain);
		parser = new InteractiveContextParser(TTRMDPStateEncoding.GRAMMAR_FOLDER + grammarResource);
	}

	private List<BabiDialogue> loadDataset(String srcFile) throws IOException {
		Map<String, Boolean> converterConfig = BabiDialogue.getDefaultConfig();
		converterConfig.put("remove_silence", false);
		converterConfig.put("process_api_calls", true);
		List<BabiDialogue> dialogues = BabiDialogue.loadFromBabiFile(
			new File(srcFile).getAbsolutePath(),
			converterConfig
		);
		return dialogues;
	}

	public List<List<List<Integer>>> extractFeaturesFromCorpus(String srcFile) throws IOException {
		List<BabiDialogue> loadedDialogues = loadDataset(srcFile);

		List<List<List<Integer>>> result = new ArrayList<>();
		for (BabiDialogue dialogue: loadedDialogues) {
			if (dialogue.size() % 2 != 0) {
				System.out.println("Problem dialogue");
				System.exit(0);
			}
			List<List<Integer>> features = extractFeatures(dialogue.subList(0, dialogue.size() - 1));
			Integer[] lastTurnFeatures = new Integer[features.get(0).size()];
			Arrays.fill(lastTurnFeatures, 0);
			features.add(Arrays.asList(lastTurnFeatures));
			result.add(features);
		}
		return result;
	}

	public List<List<Integer>> extractFeatures(List<Utterance> dialogue) {
		List<List<Integer>> result = new ArrayList<>();
		parser.init();
		for (Utterance utterance: dialogue) {
			parser.parseUtterance(utterance);
			List<Integer> allFeatures = encoder.encode(parser.getContext());
			List<Integer> currentTurnFeatures = encoder.getPendingSubstate(allFeatures);
			result.add(currentTurnFeatures);
		}
		return result;
	}

	public static void saveFeatures(List<List<List<Integer>>> inFeatureVectors, String inDstFile) throws IOException {
		BufferedWriter datasetOut = new BufferedWriter(new FileWriter(new File(inDstFile)));

		for (List<List<Integer>> dialogue: inFeatureVectors) {
			if (dialogue.size() % 2 != 0) {
				System.out.println("Problem dialogue");
				System.exit(0);
			}

			for (int turnIndex = 0; turnIndex != dialogue.size(); turnIndex += 2) {
				List<List<Integer>> qaFeatures = dialogue.subList(turnIndex,  turnIndex + 2);
				datasetOut.write(
					String.join(
						"\t",
						qaFeatures.stream().map(x -> x.toString()).collect(Collectors.toList())
					)
				);
			}
			datasetOut.write(
				String.join(
					"\n",
					dialogue.stream().map(x -> x.toString()).collect(Collectors.toList())
			));
			datasetOut.write("\n");
			datasetOut.write("\n");
		}
		datasetOut.close();
	}

	public static void main(String[] args) {
		try {
			String
				srcPath = "corpus/bAbI-dialogue/dialog-bAbI-tasks",
				dstPath = "corpus/bAbI-dialogue/babble-format",
				domain = "restaurant-search",
				grammarResource = "2016-english-ttr-restaurant-search";

			if (args.length == 2) {
				srcPath = args[0];
				dstPath = args[1];
			}
			DSFeatureExtractor extractor = new DSFeatureExtractor(domain, grammarResource);
			List<List<List<Integer>>> features = extractor.extractFeaturesFromCorpus(srcPath);
			saveFeatures(features, dstPath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
