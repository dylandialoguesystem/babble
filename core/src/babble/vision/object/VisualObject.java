package babble.vision.object;

import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.ImageIcon;
import qmul.ds.formula.TTRField;
import qmul.ds.formula.TTRRecordType;

public class VisualObject extends BasicObject implements Serializable{
	private String descrip;
	private String labelColor;
	private String labelShape;

	private BufferedImage img = null;
	private ImageIcon imageIcon;
	
	public VisualObject(int idx) {
		super(idx);
	}

	public VisualObject(int idx, String fileName){
		super(idx);
		this.setFileName(fileName);
	}
	
	public String getDescription(){
		return this.descrip;
	}
	
	public String getSpecificLabel(String type){
		if(type.equals("color"))
			return this.labelColor;
		else if(type.equals("shape"))
			return this.labelShape;
		return null;
	}
	
	public void setLabels(String descrip){
		this.descrip = descrip;
		if(!descrip.equals("")){
			String[] subs = descrip.split(" ");
			labels = null;
			labels = Arrays.asList(subs);
			
			for(String str: labels){
				if(str.equals("red") || str.equals("green") || str.equals("blue") || str.equals("black") 
						|| str.equals("purple") || str.equals("orange"))
					this.labelColor = str;
				else if(str.equals("circle") || str.equals("triangle") || str.equals("square"))
					this.labelShape = str;
			}
		}
		else{
			labels = null;
		}
		setLabels(labels);
	}

	//[pred2==square : cn|p4==class(pred2) : t|pred1==red : cn|p3==subj(pred1, pred2) : t|x1==this : e|head==x1 : e|p2==subj(pred2, x1) : t]
	public void setLabels(TTRRecordType annotation){
		List<TTRField> ttrfList = annotation.getFields();
		descrip = "";
		labels = new ArrayList<String>();
		for(TTRField ttrf: ttrfList){
			if(ttrf.getLabel().toString().contains("pred")){
				labels.add(ttrf.getType().toString());
				descrip += ttrf.getType().toString() +" ";
			}
		}
	}
	
	@Override
	public String toString(){
		return "{"+ this.fileName + ": " + this.descrip + "}";
	}

	public void setBufferedImage(BufferedImage img){
		this.img = img;
		this.imageIcon = new ImageIcon(img);
	}
	
	public BufferedImage getBufferedImage(){
		return this.img;
	}
	
	public ImageIcon getImageIcon(){
		return this.imageIcon;
	}
	
	public VisualObject clone(){
		VisualObject obj = new VisualObject(this.idx, this.getFileName());
		obj.setFileName(this.getFileName());
		obj.setFeat(this.getFeat());
		if(this.img != null)
			obj.setBufferedImage(this.img);
		obj.setLabels(this.getDescription());
		return obj;
	}
}
