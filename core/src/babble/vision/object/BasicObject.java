package babble.vision.object;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BasicObject implements Serializable{
	protected int idx;
	protected double[] feat;
	protected String fileName;
	protected List<String> labels;
	protected String dir;
	
	public BasicObject(){
		
	}
	
	public BasicObject(int idx){
		this.idx = idx;
	}
	
	public BasicObject(int idx, double[] feat){
		this.idx = idx;
		this.feat = feat;
	}
	
	public double[] getFeat(){
		return this.feat;
	}
	
	public String getFileName(){
		return this.fileName;
	}
	
	public List<String> getLabels(){
		return this.labels;
	}
	
	public int getIndex(){
		return this.idx;
	}
	
	public void setFeat(double[] feat){
		this.feat = feat;
	}
	
	public void setIndex(int idx){
		this.idx = idx;
	}

	public void setLabels(List<String> labels){
		this.labels = labels;
	}
	
	public void setLabels(String descrip){
		if(descrip != null && !descrip.equals("")){
			String[] subs = descrip.split(" ");
			labels = null;
			labels = Arrays.asList(subs);
		}
	}

	public void clearLabels(){
		setLabels(new ArrayList<String>());
	}
	
	public void setFileName(String fileName){
		this.fileName = fileName;
	}

	public void setDirectory(String dir) {
		this.dir = dir;
	}
	
	public String getDirectory(){
		return this.dir;
	}
}
