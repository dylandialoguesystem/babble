package babble.vision;

import java.util.List;
import java.util.TreeMap;

import babble.vision.classification.Concept;
import qmul.ds.formula.Formula;

public interface VisualListener {
	public abstract void insertVisualPredicts(Formula f, String ontology, List<Concept> resultMap);

	public abstract void noticeProgress(boolean isCompleted);
}
