package babble.vision;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.*;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import babble.vision.classification.Concept;
import babble.vision.tools.Constants;
import babble.vision.tools.MapToolKit;

/**
 * Class, as an extended HashMap, stores a collection of attribute classifier results in the same category, i.e. shape or color.
 * It will pick up the n-best results from collection and return basicType with all details.
 * 
 * @author Yanchao Yu
 * @param <ATT> attribute name
 * @param <BTR> BasicTTRType containing probabilities, entropy as well as generated TTRRecordType
 */
public class VisualTTRCollection<ATT extends String, PTR extends Concept> extends HashMap<ATT, PTR>{
	Logger logger = Logger.getLogger(VisualTTRCollection.class);
	private static final long serialVersionUID = 1L;
	private double acceptedDiff = 0.01;
	
	public VisualTTRCollection(){}
	
	/**
	 * Add new visual attribute result into collection
	 * @param attribute attribute name
	 * @param basicType basic TTRtype with all information
	 */
	public void addNewAttributeResult(ATT attribute, PTR basicType){
		if(!this.containsKey(attribute))
			this.put(attribute, basicType);
		else
			this.put(attribute, basicType);
	}
	
	public void addAttributeResultCollection(List<PTR> resultList){
		for(PTR pred : resultList){
			ATT attr = (ATT) pred.getConceptName();
			addNewAttributeResult(attr, pred);
		}
	}
	
	/**
	 * @return basic TTRtype of the best result, which is only working on 1-best result now.
	 */
	@SuppressWarnings("unused")
	public List<Concept> extractNBestResults(){
		List<Concept> nBests = new ArrayList<Concept>();
		Map<String, Double> sortedProb = findHihgestAttribute();
		
		int count = 0;
		Iterator<Entry<String, Double>> iterator = sortedProb.entrySet().iterator();
		logger.debug("sortedProb Size: " + sortedProb.size());
		
		while(iterator.hasNext()){
			logger.debug("count: " + (count+1) +"  contants: "+ Constants.nBestResult);
			if(count < Constants.nBestResult){
				Entry<String, Double> pair = iterator.next();
				String attr = pair.getKey();
				double prob = pair.getValue();
				
				if(this.containsKey(attr))
					nBests.add(this.get(attr));
				
				count++;
			}
			else
				break;
		}

		logger.debug("nBests Size: " + nBests.size());
		
		
		return nBests;
	}

	/**
	 * @return sorted list of attribute probabilities, mapped to attribute names
	 */
	private Map<String, Double> findHihgestAttribute(){
		HashMap<String, Double> probList = new HashMap<String, Double>();
		
		List<String> attributeList = new ArrayList<String>();
		Iterator<Entry<ATT, PTR>> iterator = this.entrySet().iterator();
		while(iterator.hasNext()){
			Entry<ATT, PTR> pair = iterator.next();
			String attr = pair.getKey();
			Concept basic = pair.getValue();
			double prob = basic.getConfidenceScore();
			probList.put(attr, prob);
		}
		
		Map<String, Double> sortedMap = MapToolKit.getInstance().sortByComparator(probList, true);
		logger.debug("Descend sortedMap for heighest Attributes: " + sortedMap);
		return sortedMap;
	}

	/**
	 * @return the worst classifier based on calculated relative entropy;
	 */
	public HashMap<String, Double> findWorstClassifier(){
		double maxEntropyValue = -1;
		String maxEntropyAtt = null;
		HashMap<String, Double> worstEntropy = new HashMap<String, Double>();
		
		Iterator<Entry<ATT, PTR>> iterator = this.entrySet().iterator();
		while(iterator.hasNext()){
			Entry<ATT, PTR> pair = iterator.next();
			String attr = pair.getKey();
			Concept basic = pair.getValue();
			if(maxEntropyValue < basic.getEntropy()){
				maxEntropyValue = basic.getEntropy();
				maxEntropyAtt = attr;
			}
		}
		worstEntropy.put(maxEntropyAtt, maxEntropyValue);
		
		return worstEntropy;
	}
}
