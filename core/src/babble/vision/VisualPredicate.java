package babble.vision;

import qmul.ds.formula.Predicate;

public class VisualPredicate  extends Predicate{
	private static final long serialVersionUID = 1L;

	public VisualPredicate(String formula) {
		super(formula);
	}	
	
	public VisualPredicate(VisualPredicate predicate) {
		super(predicate.name);
	}

}
