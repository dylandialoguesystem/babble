package babble.vision.classification;

import org.apache.log4j.Logger;

import qmul.ds.formula.Formula;
import qmul.ds.formula.TTRLabel;
import qmul.ds.formula.TTRRecordType;
import qmul.ds.type.DSType;

/**
 * Class to build the TTRRecordType for specific classifier with probabilities and entropy;
 * only used in Visual Context 
 * 
 * @author Yanchao Yu
 */
public class Concept {
	Logger logger = Logger.getLogger(Concept.class);
	
	private String ontology;
	private String concept;
	private TTRLabel xLabel;
	private double confidence_score;
	private boolean isNormalized = false;
	private double entropy;
	
	public Concept(TTRLabel xLabel, String ontology, String concept){
		this.xLabel = xLabel;
		this.ontology = ontology;
		this.concept = concept;
	}

	public Concept(String ontology, String concept, double confidence_score){
		this.ontology = ontology;
		this.concept = concept;
		this.confidence_score = confidence_score;
	}
	
	/**
	 * @return a TTRRecordType for specific attribute
	 */
	public SubTTRforAttribute generateTTR(){
		TTRRecordType tmp = new TTRRecordType();
		
		TTRLabel pred = tmp.getFreeLabel(new TTRLabel("pred"));
		tmp.add(pred,Formula.create(concept),DSType.cn);
		
		Formula p0 = Formula.create(ontology+"("+pred.toString()+")");
		TTRLabel pLabel = tmp.getFreeLabel(new TTRLabel("p"));
		tmp.add(pLabel,p0,DSType.t);
		if(ontology.equals("shape")){
			pLabel = tmp.getFreeLabel(new TTRLabel("p"));
			tmp.add(pLabel, Formula.create("class"+"("+pred+")"),DSType.t);
		}
		
		Formula predf1 = Formula.create("subj("+pred+","+xLabel+")");
		pLabel = tmp.getFreeLabel(new TTRLabel("p"));
		tmp.add(pLabel,predf1,DSType.t);
		
//		this.logger.debug("SubTTR: " + tmp);
		return new SubTTRforAttribute(new TTRLabel(pred),tmp);
	}

	/**
	 * @return the name of attribute classifier
	 */
	public String getConceptName(){
		return this.concept;
	}

	/**
	 * @return relative entropy 
	 */
	public double getEntropy(){
		return this.entropy;
	}
	
	/**
	 * @return predicted score/probability
	 */
	public double getConfidenceScore(){
		return this.confidence_score;
	}
	
	/**
	 * @return ontology i.e. "color" and "shape"/"class"
	 */
	public String getOntology(){
		return this.ontology;
	}
	
	/**
	 * @return the TTR label for specific object
	 */
	public TTRLabel getTTRLabel(){
		return this.xLabel;
	}

	/**
	 * Set predict score (i.e. probability )
	 * @param predictProb predicted probability
	 */
	public void setPredictProb(double confidence_score, boolean isNormalized){
		this.isNormalized = isNormalized;
		this.confidence_score = confidence_score;
	}

	/**
	 * Set relative entropy for reflecting classifier reliability
	 * @param relativeEntropy relative entropy = sum (p(x) * log(p(x) / q(x))) / numTrain
	 */
	public void setEntropy(double entropy){
		this.entropy = entropy;
	}
	
	/**
	 * Set TTRLabel for specific object
	 * @param xLabel TTR Label
	 */
	public void setTTRLabel(TTRLabel xLabel){
		this.xLabel = xLabel;
	}
	
	public final class SubTTRforAttribute{
		private final TTRLabel label;
		private final TTRRecordType ttr;
		
		public SubTTRforAttribute(TTRLabel label, TTRRecordType ttr){
			this.label = label;
			this.ttr = ttr;
		}
		
		public TTRLabel getPredLabel(){
			return this.label;
		}
		
		public TTRRecordType getSubTTRType(){
			return this.ttr;
		}
	}
	
	@Override
	public String toString(){
		return this.ontology + "(" + this.concept + ") = " + this.confidence_score;
	}
}
