package babble.vision.classification;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import babble.vision.tools.Constants;
import babble.vision.tools.Entropy;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.SGD;
import weka.core.Attribute;
import weka.core.Debug.Random;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ArffLoader;
import weka.core.converters.ConverterUtils.DataSource;

/**
 * this class implements the SGD in the incremental case, which won't perform the iterations through the training instance. 
 * Hence, the learning rate should be reduced significantly, try 0.00001. Moreover, don't mix incremental with batch learning,
 * which consider to build the whole classifier before the learning one-by-one.
 * 
 * @author yy147
 * @TODO: modify the classifier and test in offline experiment; add the evaluation measure method for making the final decision 
 * of requesting new instances. 
 */
public class IncremClassifier extends VisualClassifier{
	static Logger logger = Logger.getLogger(IncremClassifier.class);
	
	protected AbstractClassifier classifier;
	private final String optionSGD = "-F 1 -L 1.95 -R 1.0E-4 -E 500 -C 0.001 -S 2";
	
	private final String pathPretrainArff = "data/classifier/tmp/"+super.getAttribute()+"trainedSampleSet.arff";
	private final String pathTstArff = "data/"+super.getAttribute()+"test.arff";
	private final String pathClassifier = Constants.claDir + super.getAttribute() + ".model";
	
	private static final int SGD = 0;
	
	private boolean savable = false;
	public IncremClassifier(String attr, boolean savable) {
		super(attr);
		this.savable = savable;
		initialModel(SGD);
	}
	
	private Instances instanceBld = null;
	/**
	 * Initial Classification model with empty Instance Structure.
	 */
	@Override
	public boolean initialModel(int typeID){
		final String attr = super.getAttribute();
		File clasPath = new File(pathClassifier);
		
		// initial Instance Structure
		instanceBld = InstanceBuild();
		try {
			// check if the classifier is existed.
			if(clasPath.exists()){
				// load the existing classifier via path
				classifier = (SGD) weka.core.SerializationHelper.read(clasPath.getPath());
			}
			else{
				// set up a new classifier with settings
				classifier = new SGD();
				classifier.setOptions(Utils.splitOptions(optionSGD));
			    
				// initial Classifier with empty Instances 
				classifier.buildClassifier(instanceBld);
			}
			
			File tmp = new File(pathPretrainArff);
			if(tmp.exists()){
				DataSource source = new DataSource(pathPretrainArff);
				wholeTrainSet = source.getDataSet();
				if (wholeTrainSet.classIndex() == -1)
					wholeTrainSet.setClassIndex(wholeTrainSet.numAttributes() - 1);
			}
			else
				wholeTrainSet = new Instances(instanceBld);
			
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return false;
	}

	@Override
	public int getNumPosInstances() {
		if(wholeTrainSet != null && !wholeTrainSet.isEmpty()){
			Instances clone = InstanceBuild();
//			logger.info("classifier("+this.getAttribute()+") - instance size: " + wholeTrainSet.size());
			for(int i=0; i< wholeTrainSet.size(); i++){
				Instance instance = wholeTrainSet.get(i);
//				logger.info("classifier("+this.getAttribute()+") - instance["+i+"] class: " + instance.classValue());
				if(instance.classValue() == 0.0)
					clone.add(instance);
			}
			
			return clone.size();
		}
		return 0;
	}
	
	/**************************** Build Instance Structure (BIS) **************************/
	private int FEATDIMS = 1280;
	private Instances InstanceBuild() {
		ArrayList<Attribute> attributes = buildStructure(FEATDIMS);
		Instances instances = new Instances(super.getAttribute(), attributes, 0);
		instances.setClassIndex(FEATDIMS);
		instances.setRelationName(this.getAttribute());
		return instances;
	}
	
	private final String[] cLass = {"1","0"};
	private ArrayList<Attribute> buildStructure(int featdim) {
		ArrayList<Attribute> fvAttrs = new ArrayList<Attribute>(featdim + 1);
		for(int i=0; i < featdim; i++){
			Attribute attr = new Attribute("numeric("+String.valueOf(i)+")");
			fvAttrs.add(attr);
		}
		
		ArrayList<String> classes = new ArrayList<String>(cLass.length);
		for(int j=0; j<cLass.length; j++){
			classes.add(cLass[j]);
		}
		Attribute classAttr = new Attribute("class", classes);
	
		fvAttrs.add(classAttr);
		return fvAttrs;
	}
	/*************************************** END OF BIS ************************************/
	
	/********************** Train and Predict the Single Instance (TPSI) *******************/
	/**
	 * update classifier weights on single instance, which is created with features and labels
	 * @param feat double array of visual features
	 * @param classIdx index of label, i.e. 1 for positive and 0 for negative
	 * @return flag if update successfully
	 */
	public boolean trainOnSingleInstance(double[] feat, String classIdx){
		try {
			Instance newInstance = createInstance(feat, classIdx);
		
			if(this.classifier != null && newInstance != null)
				if(this.classifier instanceof SGD)
					((SGD)this.classifier).updateClassifier(newInstance);
			else
				return false;
			
			if(this.savable)
				weka.core.SerializationHelper.write(this.pathClassifier, this.classifier);
//			else
//				logger.info("not savable..");

//			logger.info("instance class at " + classIdx);
//			logger.info("instance class at "+this.getAttribute()+": " + newInstance.classValue());
			wholeTrainSet.add(newInstance);
//			this.saveSamples(wholeTrainSet, pathPretrainArff);
//			calculateEntrypy(wholeTrainSet);
			
//			logger.info("classifier["+this.getAttribute()+"]'s  updated Weights:" + classifier.toString());
			
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return false;
	}
	
	public boolean trainOnSingleInstance(Instance instance) {
		try {
			if(this.classifier != null && instance != null){
				if(this.classifier instanceof SGD)
					((SGD)this.classifier).updateClassifier(instance);
				return true;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Predict the labels of single sample.
	 * @param feat visual features of the sample
	 * @return	Predicted results
	 */
	public double[] predictOnSingleInstance(double[] feat){
		// build a new instance based on the extracted visual features.
		Instance newInstance = createInstance(feat, null);
		//logger.debug("Instance: " + newInstance.toString());
			
		try {	
			if(this.classifier != null){
				double[] probs = this.classifier.distributionForInstance(newInstance);
				
//				Evaluation eval = new Evaluation(this.wholeTrainSet);
//				eval.evaluateModel(this.classifier, this.wholeTrainSet);
//				
//				logger.info("Attribute["+this.getAttribute()+"] ErrorRate: " + eval.errorRate() + "priorEntropy: " + eval.priorEntropy());
				
				
				return probs;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}	
			
		return null;
	}
	
	/**
	 * Predict the labels of single sample.
	 * @param newInstance instance of new Object sample with visual features
	 * @return Predicted results
	 */
	public double[] predictOnSingleInstance(Instance newInstance){
		try {	
			if(this.classifier != null){
				double[] probs = this.classifier.distributionForInstance(newInstance);
				
				return probs;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}	
			
		return null;
	}
	

	public double[] predictOnInstances(Instances instances){
		try {	
			if(this.classifier != null){
				double[][] probs = this.classifier.distributionsForInstances(instances);
//				logger.info("Size: " + probs.length +" x "+ probs[0].length);
				
				double[] preds = new double[probs.length];
				for(int i = 0; i < probs.length; i++){
					preds[i] = probs[i][0];
				}
				
				return preds;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}	
			
		return null;
	}

	public double[][] predictOnInstances2(Instances instances){
		try {	
			if(this.classifier != null){
				double[][] probs = this.classifier.distributionsForInstances(instances);
				logger.info("Size: " + probs.length +" x "+ probs[0].length);
				return probs;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}	
			
		return null;
	}
	
	/**
	 * Build single instance with feature and labels, copy the existing Instance structure
	 * @param feat double array of visual features
	 * @param classIdx index of label, i.e. 1 for positive and 0 for negative
	 * @return an object of Instance
	 */
	private Instance createInstance(double[] feat, String classIdx){
		if(feat != null && feat.length != 0){
			Instance instance = new DenseInstance(FEATDIMS+1);
			instance.setDataset(instanceBld);
			
			for(int index = 0; index < FEATDIMS; index++){
				instance.setValue(index, feat[index]);
			}
				
			if(classIdx != null){
				instance.setClassValue(classIdx);}
			else
				instance.setMissing(FEATDIMS);
			
				return instance;
		}
		else
			logger.error("feature unavialable..");
		
		return null;
	}
	/*************************************** END OF TPSI ***********************************/
	
	/***************************** Classifier Evaluations (CE) *****************************/
	public Instances wholeTrainSet;
	
	private double totalEntropy = -1;
	private void calculateEntrypy(Instances instances) throws Exception{
		if(instances.size() > 0){
			double[][] probs = this.classifier.distributionsForInstances(instances);
			
			totalEntropy = 0.0;
			for(int i=0; i< probs.length; i++){
//				// standard Entropy
//				double predictProb1 = probs[i][0];
//				double log2Val1 = Math.log10(predictProb1) / Math.log10(2);
//				double entropy1 = -1 * (predictProb1 * log2Val1);
//
//				double predictProb2 = probs[i][1];
//				double log2Val2 = Math.log10(predictProb2) / Math.log10(2);
//				double entropy2 = -1 * (predictProb2 * log2Val2);
//				
//				totalEntropy += entropy1 + entropy2;
				
				// standard Entropy
				double classValue =  instances.get(i).classValue();
				double[] target = new double[]{0,0};
				if(classValue == 0.0)
					target[1] = 1;
				else if(classValue == 1.0)
					target[0] = 1;
//				logger.info("Target on ["+i+"]: " + target[0] + " - " + target[1] + " ~ index: " +  classValue);
//				logger.info("Estimation on ["+i+"]: " + probs[i][0] + " - " + probs[i][1]);
				
				double entropy = Entropy.getInstance().crossEntropy(target, probs[i]);
				totalEntropy += entropy;
//				logger.info("Entropy: " + entropy);
			}
		}
	}
	
	public double getTotalEntropyForSignleClassifier(){
		logger.debug("Attr["+this.getAttribute()+"] = " + totalEntropy/getNumOfInstances());
//		return totalEntropy/getNumOfInstances();
		return totalEntropy / getNumOfInstances();
	}
	
	public String evaluate(Instances evalInstance, int folds){
		String result = "";
		try {
			Evaluation eval = new Evaluation(evalInstance);
			Random rand = new Random(1);
			eval.crossValidateModel(this.classifier, evalInstance, folds, rand);
			
			result += eval.toSummaryString("\nResults Summary\n=== "+pathClassifier+" ===\n", false) + System.getProperty("line.separator");
			result += eval.toClassDetailsString("\nClass Details\n======\n") + System.getProperty("line.separator");
			result += eval.toMatrixString("\nMatrix\n======\n") + System.getProperty("line.separator");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return result;
	}
	
	/****************************** END OF CE *******************************/
	
	/******************************* Others *********************************/
	public AbstractClassifier getClassifier(){
		return this.classifier;
	}
	
	public Instances getInstanceBld(){
		return this.instanceBld;
	}
	
	public boolean isExist(){
		return this.classifier != null;
	}
	
	public void reset() {
		if(isExist()){
			this.initialModel(SGD);
			this.wholeTrainSet.clear();
			this.totalEntropy = 0.0;
		}
	}
	
	private void saveSamples(Instances instances, String path){
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(path));
			writer.write(instances.toString());
			writer.flush();
			writer.close();
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}
	
	public int getNumOfInstances(){
		if(this.wholeTrainSet != null)
			return this.wholeTrainSet.size();
		return 0;
	}
	
	public Instances getWholeTrainSet(){
		return this.wholeTrainSet;
	}

	/***************************** Main Method *********************************/
//	public static void main(String[] args) throws Exception{
//		IncremClassifier classifier = new IncremClassifier("square");
//		classifier.prepareData();
//		classifier.test();
//	}
	
	/************************ Test Single Classifier ***************************/
	public void test() throws Exception{
		ArffLoader loader = new ArffLoader(); 
		loader.setFile(new File(pathPretrainArff)); 
		Instances structure = loader.getStructure(); 
		structure.setClassIndex(structure.numAttributes()-1);

//	    NaiveBayesUpdateable classifier = new NaiveBayesUpdateable();
//		classifier.setOptions(Utils.splitOptions(optionNB));
		SGD classifier = new SGD();
		classifier.setOptions(Utils.splitOptions(optionSGD));
		
		
		classifier.buildClassifier(structure);
		
		Instance current; 
		// Incrementally update the Classifier 
		while ((current = loader.getNextInstance(structure)) != null) {
			classifier.updateClassifier(current); 
			structure.add(current);
			
			calculateEntrypy(structure);
		} 
		
//		double[][] result = classifier.distributionsForInstances(structure); 
//		for(int i = 0; i< result.length; i++){
//			logger.debug("sample["+i+"]:" + result[i][0] + " -- " + result[i][1]);
//		}
//		
//		try {
//			Evaluation eval = new Evaluation(structure);
//			eval.evaluateModel(classifier, structure);
//			
//			logger.debug(eval.toSummaryString("\nResults Summary\n=== "+pathClassifier+" ===\n", false));
//			logger.debug(eval.toClassDetailsString("\nClass Details\n======\n") );
//			logger.debug(eval.toMatrixString("\nMatrix\n======\n"));
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//		}
	}
	
	public void prepareData() throws IOException{
		instanceBld = InstanceBuild();
		
		File file = new File(Constants.annotPreTrnSet); 
		if(file.exists()){
			FileInputStream fstream = new FileInputStream(file);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String strLine;
			while ((strLine = br.readLine()) != null){
				if(!strLine.trim().equals("")){
					String[] items = strLine.split(",");
					String description = items[1];
					String[] features = items[2].split(" ");
					
					double[] feat = new double[features.length];
					
					for(int i=0; i<features.length; i++){
						feat[i] = Double.valueOf(features[i]);
					}

					String classIdx = "0";
					if(description.contains(super.getAttribute()))
						classIdx = "1";
					
					Instance newInstance = createInstance(feat, classIdx);
					instanceBld.add(newInstance);
				}
			}
			saveSamples(instanceBld, pathTstArff);
		}
	}

	@Override
	public VisualClassifier clone() {
		// TODO Auto-generated method stub
		return null;
	}
}