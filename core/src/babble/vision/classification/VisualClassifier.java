package babble.vision.classification;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;

import babble.vision.tools.Constants;

public abstract class VisualClassifier {
	static Logger logger = Logger.getLogger(VisualClassifier.class);
	
	private double[] objFeat = null;
	private List<Integer> labels = null;
	private String attr;
	private File classifierModel = null;
	
	public VisualClassifier(String attr){
		this.attr = attr;
		classifierModel = new File(Constants.claDir+attr);
	}
	
	public String getAttribute(){
		return this.attr;
	}
	
	public double[]  getFeatures(){
		return this.objFeat;
	}
	
	public List<Integer> getLabels(){
		return this.labels;
	}
	
	public File getClassifierModel(){
		return this.classifierModel;
	}
	
	public abstract VisualClassifier clone();
	
	public abstract boolean initialModel(int typeOfClassifier);

	public abstract int getNumPosInstances();
	
}
