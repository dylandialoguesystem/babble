package babble.vision;

import java.util.Date;
import java.util.List;
import java.util.Timer;

import org.apache.log4j.Logger;

import babble.system.AbstractVisionModule;
import babble.vision.classification.Concept;
import babble.vision.object.VisualObject;
import babble.vision.tools.Constants;
import qmul.ds.formula.Formula;

/**
 * Class creates a task for doing classification on specific object and then updating the visual context. 
 * As this task is defined as a time task, it will be operated every 10s until the task is cancelled in the system
 * 
 * TODO: 1) build a time task with 0s delay and 5s duration
 * 		 2) doing classification on the visual records and providing the best results
 * 		 3) generating/updating the visual context via a visual listener
 * 
 * @author Yanchao YU
 */
public class VisualTTRUpdate implements Runnable{
	
	public volatile boolean runnable = false;
	static Logger logger = Logger.getLogger(VisualTTRUpdate.class);
	
	private static final long DURATION = 30*Constants.UNIT;
	
	private AbstractVisionModule moduel;
	private VisualListener listener;
	
	public VisualTTRUpdate(AbstractVisionModule moduel, VisualListener listener){
		this.moduel = moduel;
		this.listener = listener;
	}

	private VisualRecords<Formula, VisualObject> vsRecords;
	private VisualPAFSet<VisualPredicateArgumentFormula> vpafSet;

	@Override
	public void run() {
		synchronized(this){
			while(runnable){
				vpafSet = moduel.getPredicateArguments();
				vsRecords = moduel.getRecords();
				if(!vsRecords.isEmpty()){
					List<Formula> fs = vsRecords.getFormulas();
					
					for(int i=0; i< fs.size(); i++){
						Formula f = fs.get(i);
						
						for(VisualPredicateArgumentFormula vpaf: vpafSet){
							vpaf.updateArgument(f);
							
							List<Concept> predictList = vpaf.doClassify();
							if(predictList != null){
								if(!predictList.isEmpty())
									listener.insertVisualPredicts(f, vpaf.getOntology(), predictList);
								else
									logger.error("Error: Result Map : "+ predictList);
							}
							else
								logger.error("Result Map unavailable : "+ predictList);
						}
						listener.noticeProgress(true);
						logger.info("Prediction Done at " + new Date());
					}
				}
				else
					logger.error("vsRecords is empty" + new Date());
				
				try {
					Thread.sleep(this.DURATION);
				} catch (InterruptedException e) {
					logger.error(e.getMessage());
				}
			}
			
			if(!runnable){
				logger.info("Visual TTR Update is stoped..");
				clearVSRecords();
			}
		}
	}
	
	public void clearVSRecords(){
		if(vsRecords != null && !vsRecords.isEmpty())
			vsRecords.clear();
	}
}
