package babble.vision.context;

import java.rmi.UnexpectedException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import babble.system.dm.AbstractBabbleDialog;
import qmul.ds.formula.Formula;
import qmul.ds.formula.TTRField;
import qmul.ds.formula.TTRLabel;
import qmul.ds.formula.TTRRecordType;
import qmul.ds.type.DSType;

public abstract class AbstractTargetContext {
	Logger logger = Logger.getLogger(AbstractTargetContext.class);

	protected AbstractBabbleDialog babbleDialog;
	
	protected Map<String,TTRRecordType> qContextMap;
	protected List<String> ontologyList;
	public AbstractTargetContext(AbstractBabbleDialog babbleDialog, List<String> ontologyList){
		this.babbleDialog = babbleDialog;
		this.ontologyList = ontologyList;
	}
	
	public void initial(){
		if(qContextMap == null)
			qContextMap = new HashMap<String,TTRRecordType>();
		else
			qContextMap.clear();
		
		for(String ontology: this.ontologyList){
			String qContextStr = "[x==this:e|pred:cn|p0=="+ontology+"(pred):t|p1==subj(pred,x):t|p2==question(pred):t]";
			TTRRecordType qContext = TTRRecordType.parse(qContextStr);
			if(ontology.trim().equals("shape")){
				qContext.add(new TTRField(qContext.getFreeLabel(new TTRLabel("p0")), DSType.t, Formula.create("class(pred)")));
			}
			
			qContextMap.put(ontology, qContext);
		}
		
		logger.debug("Question Map: " + qContextMap);
	}
	
	
	public abstract boolean answerQuestion(TTRRecordType context);
	
	public abstract RespondTTR buildRespond(VisualContext visualContext);
	
	public abstract RespondTTR buildRespond(TTRRecordType qTTR, VisualContext visualContext, TTRField selQ);
	
	/**
	 * @return flag if all meta variables in goal context has been answered -- so-called idle situation
	 * @throws UnexpectedException 
	 */
	public boolean isIdleSituationByQuestion(){
		return qContextMap.isEmpty();
	}
	
	public int sizeOfQContext(){
		return this.qContextMap.size();
	}
}
