package babble.vision.context;

import qmul.ds.formula.TTRRecordType;

/**
 * RespondTTR class to store all respond-related information, e.g. ttr representation, 
 * falg if generate a question as well as the answer reliability
 * 
 * @author Yanchao Yu
 */
public class RespondTTR {
	protected TTRRecordType respondTTR = null;
	protected boolean isWHQuestion = false;
	protected double reliability; // should be between 0 ~ 1;
	protected double probability; // should be between 0 ~ 1;
	
	public RespondTTR(TTRRecordType respondTTR, boolean isWHQuestion){
		this.respondTTR = respondTTR;
		this.isWHQuestion = isWHQuestion;
	}
	
	/**
	 * setup the probability of answer, only used when Visual context has already get something for PRED;
	 */
	public void setProbability(double probability){
		this.probability = probability;
	}
	
	/**
	 * setup the reliability of answer, only used when Visual context has already get something for PRED;
	 */
	public void setReliability(double reliability){
		this.reliability = reliability;
	}
	
	/**
	 * @return generated respond TTR RecordType
	 */
	public TTRRecordType getRespondTTR(){
		return this.respondTTR;
	}
	
	/**
	 * @return flag if need to respond as question
	 */
	public boolean isWHQuestion(){
		return this.isWHQuestion;
	}
	
	/**
	 * @return reliability of answer from visual contexts
	 */
	public double getReliability(){
		return this.reliability;
	}
	
	/**
	 * @return probability of answer from visual contexts
	 */
	public double getProbability(){
		return this.probability;
	}
}