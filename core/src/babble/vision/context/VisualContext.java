package babble.vision.context;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;

import babble.vision.VisualListener;
import babble.vision.VisualTTRCollection;
import babble.vision.classification.Concept;
import babble.vision.classification.Concept.SubTTRforAttribute;
import babble.vision.gui.AbstractActivityListener;
import babble.vision.gui.DSTypes;
import babble.vision.tools.MapToolKit;
import qmul.ds.formula.Formula;
import qmul.ds.formula.TTRField;
import qmul.ds.formula.TTRLabel;
import qmul.ds.formula.TTRRecordType;
import qmul.ds.type.DSType;

/**
 * Class to store all visual TTR components and generate the final visual context for dialog:
 * 
 * once system receive new classifier results, the non-linguistic context will be updated automatically based on some rules/templates;
 * updated results will be stored in this context class until new object is provided.
 * 
 * TODO: 1) setup the current policy that system only support one object viewed in the scene. 
 * 		 2) store all visual components in the Maps
 * 		 3) setup a set of templates for different visual content.
 * 
 * @author Yanchao Yu
 */
public class VisualContext implements VisualListener{
	Logger logger = Logger.getLogger(VisualContext.class);
	
	// setup the max number of objects in the scene in the current version
	private final int maxNumoofObj = 1;
	
	private HashMap<String, VisualTTRCollection> visualCollection;
	private HashMap<String, Concept> bestPair;
	private HashMap<String, Double> evalMap;
	HashMap<TTRLabel, Formula> nlcSet; 
	private static final String fixedTTR = "[x==s : e|e2==see : es|p0==subj(e2,x):t]"; 

	private int indxOfX = 0;
	
	private TTRRecordType currntContext = null;
	private AbstractActivityListener listener;
	
	private boolean isCompleted = false;
	
	public VisualContext(AbstractActivityListener listener){
		nlcSet = new HashMap<TTRLabel, Formula>();
		visualCollection = new HashMap<String, VisualTTRCollection>();
		bestPair = new HashMap<String, Concept>();
		evalMap = new HashMap<String, Double>();
		this.listener = listener;
	}
	
	/**************** Only For Test ********************/
	public VisualContext(String context){
		TTRRecordType currntContext = TTRRecordType.parse(context);
	}
	/****************** END ****************************/
	
	
	/**
	 * @return flag if the Object list is empty
	 */
	public boolean isEmpty(){
		return nlcSet.isEmpty();
	}
	
	/**
	 * Clear the Context, all status will be stored in the history.
	 */
	public void clearContext(){
		if(nlcSet != null)
			nlcSet.clear();
		
		if(currntContext != null)
			currntContext = null;
		
		if(visualCollection != null && !visualCollection.isEmpty())
			visualCollection.clear();
		
		if(bestPair != null && bestPair.isEmpty())
			bestPair.clear();
		
		indxOfX = 0;
		
		isCompleted = false;
	}
	
	/**
	 * Add all objects in the scene with formulas
	 * @param nlcSet a list of object formula
	 */
	public void addAll(HashMap<TTRLabel, Formula>nlcSet){
		this.nlcSet = nlcSet;
	}
	
	/**
	 * Add a single object TTR into the context
	 * @param pointer formula of the specific object
	 * @param ttr generated TTR representation of the object
	 */
	public void addnewObj(Formula pointer){
		if(nlcSet != null){
			TTRLabel xLabel = null;
			if(!nlcSet.containsValue(pointer)){
				xLabel = new TTRLabel("x" + String.valueOf(indxOfX++));
				nlcSet.put(xLabel, pointer);
			}
			else{
				xLabel = findLabelByF(pointer);
				nlcSet.put(xLabel, pointer);
			}
			
			Formula objf = Formula.create("obj(e2,"+xLabel.toString()+")");
			
//			if(this.currntContext == null)		
			this.currntContext  = null;
			this.currntContext = TTRRecordType.parse(fixedTTR);
			this.bestPair.clear();
			this.visualCollection.clear();
			
			
			logger.debug("Map: " + this.currntContext.getRecord());
			logger.debug("objf: " + objf);
				 
			 if(!this.currntContext.getLabels().contains(xLabel))
				currntContext.add(new TTRLabel(xLabel),pointer,DSType.e);
						 
			 if(!this.currntContext.getRecord().containsValue(objf)){
				 TTRLabel pLabel = currntContext.getFreeLabel(new TTRLabel("p"));
				 currntContext.add(pLabel,objf,DSType.t);
			 }
		}
	}
	
	/**
	 * find TTRLabel of the specific object formula
	 * @param pointer object formula, e.g. "this", "obj1", "obj2"...
	 * @return TTRLabel
	 */
	private TTRLabel findLabelByF(Formula pointer){
		Iterator<Entry<TTRLabel, Formula>> iter = nlcSet.entrySet().iterator();
		while (iter.hasNext()) {
		    Entry<TTRLabel, Formula> entry = (Entry<TTRLabel, Formula>)iter.next();
		    if (entry.getValue().equals(pointer)) {
		        return entry.getKey();
		    }
		}
		return null;
	}
	
	/**
	 * @return Current Visual Context in the form of TTRRecordType
	 */
	public TTRRecordType getCurrentVisualContext(){
		if(this.currntContext != null)
			return this.currntContext;

		return null;
	}

	private Map<String, Double> thresMap = new HashMap<String, Double>();
	@SuppressWarnings("unchecked")
	@Override
	public void insertVisualPredicts(Formula f, String ontology, List<Concept> predList) {
		// once system receive new classifier results, the non-linguistic context will be updated automatically based on some rules/templates;
		// updated results will be stored in this context class until new object is provided.

		logger.debug("Ontology : " + ontology);
		
		double thresshold = (1.0 / (double)predList.size()) + ((1 - (1.0 / (double)predList.size()))/2);
		
		
//		logger.debug("Threshold = " + thresshold + " ~ " + predList.size());
		
		
		TTRLabel xLabel = findLabelByF(f);
		for(Concept pred :  predList){
			pred.setTTRLabel(xLabel);
			logger.debug(pred.getOntology() + "["+pred.getConceptName()+"] : " + pred.generateTTR().getSubTTRType() + " score: " + pred.getConfidenceScore());
			
			String attr = pred.getConceptName();
			evalMap.put(attr, pred.getEntropy());
			thresMap.put(attr, thresshold);
		}
		
//		logger.debug("Threshold Map: " + thresMap);
		
//		logger.info("EVAL MAP: " + evalMap);
		
		VisualTTRCollection<String, Concept> vtCollection = null;
		if(this.visualCollection.containsKey(ontology)){
			vtCollection = this.visualCollection.get(ontology);
		}
		else{
			vtCollection = new VisualTTRCollection<String, Concept>();
			visualCollection.put(ontology, vtCollection);
		}
		vtCollection.addAttributeResultCollection(predList);
		
		checkRule();
	}
	
	private void checkRule() {
		if(this.visualCollection != null && !this.visualCollection.isEmpty()){
			Iterator<Entry<String, VisualTTRCollection>> iterator = this.visualCollection.entrySet().iterator();
			
			while(iterator.hasNext()){
				Entry<String, VisualTTRCollection> entry = iterator.next();
				String ontology = entry.getKey();
				VisualTTRCollection<String, Concept> vtCollection = entry.getValue();
				
				List<Concept> predList = vtCollection.extractNBestResults();
				logger.debug("predList Size: " + predList.size());
				
				for(Concept pred: predList){
					bestPair.put(ontology, pred);
				}
			}
			
			generateFinalContext();
		}
		else
			logger.error("Visual Collection is unavaible...");
	}
	
	/**
	 * Generate the final visual context by merging subTTR
	 */ 
	private void generateFinalContext() {
		if(bestPair != null){
			Iterator<Entry<String, Concept>> iterator = this.bestPair.entrySet().iterator();
			while(iterator.hasNext()){
				Entry<String, Concept> entry = iterator.next();
				String ontology = entry.getKey();
				Concept predTTr = entry.getValue();
				logger.info("ontology: " + ontology + " -- " + predTTr.getConceptName() + ": " + predTTr.getConfidenceScore());
				
				SubTTRforAttribute sta = predTTr.generateTTR();
				logger.debug("SubTTR: " + sta);
				
				TTRLabel oldLabel = sta.getPredLabel();
				TTRRecordType subTTR = sta.getSubTTRType();
				
				if(!subTTR.subsumes(currntContext)){

					logger.debug("Cannot subsumed: " + subTTR + " -- " + currntContext);
					 
					TTRLabel newLabel = currntContext.getFreeLabel(new TTRLabel("pred"));

					logger.debug("oldLabel: " + oldLabel + " -- newLabel: " + newLabel );
					 
					for(TTRField ttrf: subTTR.getFields()){
						TTRLabel label = ttrf.getLabel();
						if(label.toString().contains("pred")){
							ttrf.setLabel(newLabel);
						}
						else if(label.toString().contains("p")){
							label = currntContext.getFreeLabel(new TTRLabel("p"));
							String type = ttrf.getType().toString();
							 
							if(oldLabel != null)
								 type = type.replace(oldLabel.toString(), newLabel.toString());
							
							ttrf.setLabel(label);
							ttrf.setType(Formula.create(type));
						}
						currntContext.add(ttrf);
					}
				}
				
				 logger.debug("final Visual Context: " + currntContext);
				 this.listener.updateTTRRecords(currntContext, DSTypes.visualTTR);
			}
		}
	}

	/**
	 * @return List of attribute and corresponding relative entropy
	 */
	public HashMap<String, Double> getEntropyMAp() {
		return evalMap;
	}
	
	/**
	 * @return normalized entropy of binary attribute classifier
	 */
	public double getEntropyByAttribute(String attr){
		if(evalMap != null && !evalMap.isEmpty()){
			if(evalMap.containsKey(attr))
				return evalMap.get(attr);
		}
		return -1;
	}
	
	/**
	 * @return confidence threshold on specific attribute
	 */
	public double getThresholdByAttribute(String attr){
		if(thresMap != null && !thresMap.isEmpty()){
			if(thresMap.containsKey(attr))
				return thresMap.get(attr);
		}
		return 1;
	}
	
	/**
	 * @return confidence score on specific attribute (i.e. normalized probability)
	 */
	public double getConfidenceScoreByAttribute(String attr){
		if(bestPair != null && !bestPair.isEmpty()){ 
			logger.debug("BestPair: " + bestPair);
			
			Set<String> keySey = bestPair.keySet();
			for(String key: keySey){
				Concept pred = bestPair.get(key);
				if(pred.getConceptName().equals(attr.trim()))
					return pred.getConfidenceScore();
			}
		}
		else
			logger.error("the Best Pair is unavailable... " + bestPair);
		
		return 0;
	}
	
	/**
	 * @return worst classifier upon the evaluation socres (e.g. entropy or root mean square error)
	 */
	public String getWorstClassifier(){
		System.out.println("evalMap = " + evalMap);
		
		if(evalMap != null && !evalMap.isEmpty()){
			Map<String, Double> sortEvalMap = MapToolKit.getInstance().sortByComparator(evalMap, true);
			logger.debug("SORTED ENTROPY MAP: " + sortEvalMap);
			logger.debug("WORST ENTROPY ATTR: " + MapToolKit.getInstance().getTopKey(sortEvalMap));
			return MapToolKit.getInstance().getTopKey(sortEvalMap);
		}
		
		return null;
	}

	public Concept getAttributByOntology(String ontology) {
		return bestPair.get(ontology);
	}

	@Override
	public void noticeProgress(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}
	
	public boolean isCompleted(){
		return this.isCompleted;
	}
}
