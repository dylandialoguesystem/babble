package babble.vision.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

public class StatusBar extends JPanel{
	private JLabel welcomedate;
    private JLabel msgLabel;
	
    public StatusBar(){
    	this.setLayout(new BorderLayout());//frame layout
    	msgLabel = new JLabel(" " + "Good Day!", JLabel.LEFT);
    	msgLabel.setForeground(Color.black);
    	msgLabel.setToolTipText("Tool Tip Here");
    	welcomedate = new JLabel();
    	welcomedate.setOpaque(true);//to set the color for jlabel
    	welcomedate.setBackground(Color.black);
    	welcomedate.setForeground(Color.WHITE);
        this.setLayout(new BorderLayout());
        this.setBorder(javax.swing.BorderFactory.createEtchedBorder());
       // this.setBackground(Color.LIGHTGRAY);
        this.add(msgLabel, BorderLayout.WEST);
        this.add(welcomedate, BorderLayout.EAST);
        updateTimetip();
    }
    
    public StatusBar(Font font) {
    	this.setLayout(new BorderLayout());//frame layout
    	msgLabel = new JLabel(" " + "Good Day!", JLabel.LEFT);
    	msgLabel.setFont(font);
    	msgLabel.setToolTipText("Tool Tip Here");
    	welcomedate = new JLabel();
    	welcomedate.setFont(font);
    	welcomedate.setOpaque(true);//to set the color for jlabel
    	welcomedate.setBackground(Color.black);
    	welcomedate.setForeground(Color.WHITE);
        this.setLayout(new BorderLayout());
        this.setBorder(javax.swing.BorderFactory.createEtchedBorder());
       // this.setBackground(Color.LIGHTGRAY);
        this.add(msgLabel, BorderLayout.WEST);
        this.add(welcomedate, BorderLayout.EAST);
        updateTimetip();
	}

	private Timer timer;
    private void updateTimetip(){
    	timer = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				java.util.Date now = new java.util.Date();
                String time = DateFormat.getDateTimeInstance().format(now);
                welcomedate.setText(time);
                welcomedate.setToolTipText("Today is " + time);
			}
        });
    	timer.start();
    }
    
    public void setStatusTip(String msg){
    	if(msg != null && !msg.isEmpty()){
    		this.msgLabel.setText(msg);
    		
    		msgLabel.validate();
    		msgLabel.repaint();
    	}
    }

}
