package babble.vision.gui;

import java.awt.image.BufferedImage;
import java.util.Map;

import hw.system.dm.UttTyps;
import qmul.ds.formula.TTRRecordType;

public interface AbstractActivityListener {
	public abstract void updateStatus(String msg);
	public abstract void updateImage(BufferedImage img);
	public abstract void updateHist(double[] hist);
	public abstract void updateClassifierResults(Map<String, Double> classifierMap);
	public abstract void updateDialog(String utt, UttTyps type, boolean isStopped);
	public abstract void updateTTRRecords(TTRRecordType f, DSTypes type);
}
