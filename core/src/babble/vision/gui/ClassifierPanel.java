package babble.vision.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.StyleConstants;

import babble.vision.classification.Concept;
import babble.vision.tools.Constants;

public class ClassifierPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	private Map<String, JPanel> claPanelMap = new HashMap<String, JPanel>();
	private JFrame frame; 
	
	public ClassifierPanel(JFrame frame){
		this.frame = frame;
	};
	
	@SuppressWarnings("resource")
	public void collectClassifierPanels() throws IOException{
		File file = new File(Constants.classifierType);
		
		if(file.exists()){
			BufferedReader br = null;
			FileInputStream fstream = new FileInputStream(file);
			br = new BufferedReader(new InputStreamReader(fstream));
			
			String strLine;
			while ((strLine = br.readLine()) != null){
				if(!strLine.trim().equals("")){
					String[] items = strLine.split(":");
					
					String clasName = items[1];
					JPanel clasPanel = createNewPanel(clasName);
					
					if(!claPanelMap.containsKey(clasName))
						claPanelMap.put(clasName, clasPanel);
				}
			}
			br.close();
			
			drawPanels();
		}			
	}

	private int numOfClassifiers = 0;
	private void drawPanels() {
		numOfClassifiers = claPanelMap.size();
		
		this.removeAll();
		this.setLayout(new GridLayout(numOfClassifiers,1,0,15));
		
		Iterator entries = claPanelMap.entrySet().iterator();
		while (entries.hasNext()) {
		  Entry pair = (Entry) entries.next();
		  String className = (String) pair.getKey();
		  JPanel clasPanel = (JPanel)pair.getValue();
		  
		  this.add(clasPanel);
		}
		
		this.validate();
		this.repaint();
	}

	private JPanel createNewPanel(String clasName) {
		JPanel tmp = new JPanel();
		tmp.setLayout(new BorderLayout());
		JLabel label = new JLabel(clasName.toUpperCase() + ":");
		JTextField result = new JTextField(); 
		result.setEditable(false);
		result.setBackground(Color.white);
		result.setHorizontalAlignment(JTextField.CENTER);
		
		tmp.add(label, BorderLayout.WEST);
		tmp.add(result, BorderLayout.CENTER);
		
		return tmp;
	}
	
	public void addNewClassifierPanel(String clasName){
		if(!claPanelMap.containsKey(clasName)){
			JPanel clasPanel = createNewPanel(clasName);
			claPanelMap.put(clasName, clasPanel);
			
			drawPanels();
		}
	}
	
	public void removeClassifierPanel(String clasName){
		if(claPanelMap.containsKey(clasName)){
			claPanelMap.remove(clasName);
			drawPanels();
		}
	}
	
	public void updateResults(Map<String, Double> classifierMap){
		Iterator<Entry<String, Double>> entries = classifierMap.entrySet().iterator();
		while (entries.hasNext()) {
		  Entry<String, Double> pair = entries.next();
		  String className = (String) pair.getKey();
		  double clasPanel = (double)pair.getValue();
		  
		  updateValue(className, clasPanel, Color.BLACK);
		}
		
		this.validate();
		this.repaint();
	}
	
	public void updateResults_2(Map<String, Queue<Concept>> prediction){
		if(prediction != null){
			Iterator<Entry<String, Queue<Concept>>> entries = prediction.entrySet().iterator();
			while (entries.hasNext()) {
			  Entry<String, Queue<Concept>> pair = entries.next();
			  
			  int index = 0;
			  Queue<Concept> queue = pair.getValue();
			  for(Concept cpt: queue){
				  String classifier_name = cpt.getConceptName();
				  double score = cpt.getConfidenceScore(); 
				  
				  if(index++ == 0)
					  updateValue(classifier_name, score, Color.RED);
				  else
					  updateValue(classifier_name, score, Color.BLACK);
			  }
			}
		}
		this.validate();
		this.repaint();
	}
	
	private void updateValue(String clasName, double score, Color font_color){
		if(claPanelMap.containsKey(clasName)){
			 JPanel clasPanel = claPanelMap.get(clasName);
			 Component com = clasPanel.getComponent(1);
			 if(com instanceof JTextField){
				 ((JTextField)com).setFont(new Font("Courier", Font.BOLD, Constants.fontSize));
				 
				 if(score < 0)
					 ((JTextField)com).setText("");
				 else
					 ((JTextField)com).setText(String.valueOf(Math.floor(score * 1000) / 1000));
				 ((JTextField)com).setForeground(font_color);
			 }
			 else{
				 JOptionPane.showMessageDialog(this.frame,
						    "This component isn't JTextField.",
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
			 }
		}
	}
	
	public void clearClassifierResults(){
		Set<String> classifierList = claPanelMap.keySet();
		for(String attr: classifierList){
			updateValue(attr, -1.0, Color.BLACK);
		}
	}
	
}
