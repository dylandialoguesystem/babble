package babble.vision.gui;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
/**
 * Build a paint panel for showing an existing picture based on panel's size
 * 
 * @author Yanchao Yu
 */
public class DrawPanel extends JPanel{
	private static final long serialVersionUID = 1L;
	private BufferedImage image;
	private float rate = 1;
	private boolean isResize = false;
    
	public DrawPanel(){}
	
	/**
	 * showing a new image using filename 
	 * @param filename path of the specific image
	 */
	public void addImage(String filename, boolean isResize){	
		try {                
			image = ImageIO.read(new File(filename));
			this.rate = this.calculateDimensionRatio(image);
        } catch (IOException ex) {
        	System.out.println(ex.getMessage());
        }
	}
	
	/**
	 * showing a new image using an already created bufferimage
	 * @param image
	 */
	public void addImage(BufferedImage image, boolean isResize){
		this.image = image;
		this.rate = this.calculateDimensionRatio(image);
	}
	
	/**
	 * remove and whiten all pictures showing currently
	 */
	public void clear() {
		image = null;
        repaint();
    }
	
	private float calculateDimensionRatio(BufferedImage img) {
		int height = img.getHeight();
		int width = img.getWidth();
		
		return (float)width / (float)height;
	}  
	
	
	@Override
	protected void paintComponent(Graphics g) {	
		super.paintComponent(g);
		if(isResize)
		    g.drawImage(image, 0, 0, (int)(getHeight()*this.rate), getHeight(), this);  
		else
			g.drawImage(image, 0, 0, getWidth(), getHeight(), this);          
	}
}
