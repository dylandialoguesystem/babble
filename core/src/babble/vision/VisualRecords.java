package babble.vision;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import babble.vision.object.BasicObject;
import qmul.ds.formula.Formula;
import qmul.ds.formula.TTRRecordType;
import qmul.ds.type.DSType;

public class VisualRecords <F extends Formula, O extends BasicObject> extends HashMap<F,O>{
	private static final long serialVersionUID = 1L;
	
	public VisualRecords(){}

	public void addNewRecord(F f, O newObj){
		if(f != null && newObj != null){
			if(this.containsKey(f))
				replaceRecord(f,newObj);
			else
				this.put(f, newObj);
		}
	}
	
	public O getObject(F f){
		if(this.containsKey(f))
			return this.get(f);
		return null;
	}
	
	public int getNumOfRecords(){
		return this.size();
	}
	
	public void removeRecord(F f){
		if(this.containsKey(f))
			this.remove(f);
	}
	
	public void replaceRecord(F f, O newObj){
		if(this.containsKey(f))
			this.put(f, newObj);
	}
	
	public void clearRecords(){
		if(!this.isEmpty()){
			this.clear();
		}
	}
	
	public TTRRecordType getTTRRecordType(F f){
		TTRRecordType ttr = null;
		if(this.containsKey(f)){
			BasicObject obj = (BasicObject)this.get(f);
			ttr = new TTRRecordType(f.toString(),"this",DSType.e.toString());//"o"+String.valueOf(obj.getIndex())
		}
		return ttr;
	}
	
	public List<TTRRecordType> getTTRRecordType(){
		List<TTRRecordType> ttrs = new ArrayList<TTRRecordType>();
		Iterator iterator = this.entrySet().iterator();
	    while (iterator.hasNext()) {
	        Map.Entry pair = (Map.Entry)iterator.next();
	        Formula f = (Formula) pair.getKey();
	        BasicObject obj = (BasicObject) pair.getValue();
	        TTRRecordType ttr = new TTRRecordType(f.toString(),"this", DSType.e.toString());//"o"+String.valueOf(obj.getIndex())
	        ttrs.add(ttr);
	    }
	    return ttrs;
	}
	
	public ArrayList<Formula> getFormulas(){
		ArrayList<Formula> fs = new ArrayList<Formula>();
		Iterator iterator = this.entrySet().iterator();
	    while (iterator.hasNext()) {
	        Map.Entry pair = (Map.Entry)iterator.next();
	        Formula f = (Formula) pair.getKey();
	        fs.add(f);
	    }
	    return fs;
	}
}
