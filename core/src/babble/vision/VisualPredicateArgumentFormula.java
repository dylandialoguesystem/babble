package babble.vision;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import babble.simulation.VisualObjRecord;
import babble.system.AbstractVisionModule;
import babble.vision.classification.IncremClassifier;
import babble.vision.classification.Concept;
import babble.vision.classification.VisualClassifier;
import babble.vision.gui.AbstractActivityListener;
import babble.vision.object.BasicObject;
import babble.vision.object.VisualObject;
import babble.vision.tools.MapToolKit;
import qmul.ds.formula.Formula;
import qmul.ds.formula.Predicate;
import qmul.ds.formula.PredicateArgumentFormula;

public class VisualPredicateArgumentFormula extends PredicateArgumentFormula{
	private static final long serialVersionUID = 1L;
	
	static Logger logger = Logger.getLogger(VisualPredicateArgumentFormula.class);
	private String ontology = "";

	private HashMap<String, VisualClassifier> classifierList;
	
	private AbstractVisionModule svs; 
	private List<Concept> predList;
	private AbstractActivityListener listener; 
	
	public VisualPredicateArgumentFormula(Predicate predicate, ArrayList<Formula> arguments, AbstractVisionModule visionModule, List<String> cptList, AbstractActivityListener listener, boolean savable) {
		super(predicate, arguments);
		this.ontology = predicate.getName();
		this.listener = listener;
		
		this.svs = visionModule;
		initialClassifier(ontology,cptList, savable);
	}
	
	/**
	 * Initial all classifiers using the list of attributes on the specific category
	 * @param ontology attribute category
	 * @param cptList list of attribute concepts
	 */
	private void initialClassifier(String ontology, List<String> cptList, boolean savable) {
		if(classifierList == null)
			classifierList = new HashMap<String, VisualClassifier>();
		
		for(String cpt: cptList){
			addNewClassifier(cpt, savable);
		}
	}
	
	/**
	 * Create and add new classifier for the specific attribute concept on category
	 * @param cpt attribute concept
	 * @return flag of create classifier successfully
	 */
	public boolean addNewClassifier(String cpt, boolean savable){
		if(classifierList != null){
			if(!classifierList.containsKey(cpt))
				classifierList.put(cpt, new IncremClassifier(cpt, savable));
			else
				return false;
			
			return true;
		}
		return false;
	}

	/**
	 * update with new formulas
	 * @param f a new formula
	 */
	public void updateArgument(Formula f) {
		if(!this.arguments.isEmpty())
			this.arguments.clear();
		
		this.arguments.add(f);
	}

	/**
	 * Train classifiers on specific object sample
	 * @param obj a sample
	 * @return flag of if train classifiers successfully
	 */
	public boolean trainModle(VisualObject obj) {
		if(classifierList != null && !classifierList.isEmpty()){
			double[] feat = obj.getFeat();
			List<String> labels = obj.getLabels();
			boolean isSameOntology = checkSameOntology(labels);
			
//			logger.info("~~~~~~~~~~~~~~~~~Obj Labels: " + obj.getDescription() +"  -- same Ontology?? " + isSameOntology + " ("+this.ontology+")");
			if(isSameOntology){
				Iterator iterator = classifierList.entrySet().iterator();
				while (iterator.hasNext()) {
					 Map.Entry pair = (Map.Entry) iterator.next();
					 String attr = (String)pair.getKey();
					 VisualClassifier classifier = (VisualClassifier)pair.getValue();
					 
					// check if the classifier is an object of incremental classifier 
					 if(classifier instanceof IncremClassifier){
						 String label = "0";
						 if(labels.contains(attr))
							 label = "1";

//						logger.info("~~~~~~~~~~~~~~~~~Learning: " + attr +"  -- leanring label(" + label + ") ~~ "+classifier.getAttribute());
						 ((IncremClassifier)classifier).trainOnSingleInstance(feat, label);
					}
				}
			}
			return true;
		}
		
		return false;
	}
	
	/**
	 * Check if it the classifier ontology is same with the labels 
	 */
	private boolean checkSameOntology(List<String> labels){
		for(String label : labels){
			if(classifierList.keySet().contains(label))
				return true;
		}
		return false;
	}
	
	
	private VisualRecords<Formula, VisualObject> vsRecords;
	public List<Concept> doClassify(){
		if(this.classifierList != null && !classifierList.isEmpty()){
			vsRecords = svs.getRecords();
			if(arguments != null && !arguments.isEmpty()){
				Formula f = arguments.get(0);
				
				if(vsRecords.containsKey(f)){
					BasicObject obj = vsRecords.get(f);
					if(doClassify(obj)){
						predList = getResultTypes();

//						logger.error("Listener: " + this.listener == null );
						this.listener.updateClassifierResults(getResultMap());
					}
					else
						logger.error("Cannot recognize this object");
				}
				else
					logger.error("Cannot find this formula: " + f);
			}
			else
				logger.error("Cannot find an object: " + ontology);
		}
		
		return predList;
	}
	
	public boolean doClassify(BasicObject obj){
		if(evalMap == null)
			evalMap = new HashMap<String, Double>();
		else
			evalMap.clear();
		
		if(resultMap == null)
			resultMap = new HashMap<String,Double>();
		else
			resultMap.clear();
		
		Iterator iterator = classifierList.entrySet().iterator();
		while (iterator.hasNext()) {
		    Map.Entry pair = (Map.Entry) iterator.next();
		    String attr = (String)pair.getKey();
		    VisualClassifier classifer = (VisualClassifier)pair.getValue();

			if(classifer instanceof IncremClassifier){
				double[] result = ((IncremClassifier)classifer).predictOnSingleInstance(((VisualObject)obj).getFeat());
				
				resultMap.put(attr, result[0]);
				evalMap.put(attr, ((IncremClassifier)classifer).getTotalEntropyForSignleClassifier());
			}
			else
				return false;
		}
		
		if(isNormalizing)
			resultMap = MapToolKit.getInstance().normalize((HashMap)resultMap);
			
		return true;
	}
	
	/**
	 * @return the specific predict
	 */
	public List<Concept> getPredict(){
		return this.predList;
	}

	/**
	 * @return the predicate argument
	 */
	public Formula getPredicateArgument(){
		return this.evaluate();
	}
	
	/**
	 * @return the ontology of attribute concepts -- attribute category
	 */
	public String getOntology(){
		return ontology;
	}
	
	/**
	 * @return collection of classifiers on the same category
	 */
	public HashMap<String, VisualClassifier> getClassifiers(){
		return this.classifierList;
	}
	
	public Map<String, Integer> getSampleDistribution(){
		Map<String, Integer> smapleMap  = new HashMap<String, Integer>();
		Iterator<Entry<String, VisualClassifier>> iterator = classifierList.entrySet().iterator();
		while(iterator.hasNext()){
			VisualClassifier classifier = iterator.next().getValue();
			smapleMap.put(classifier.getAttribute(), ((IncremClassifier)classifier).getNumPosInstances());
//			logger.info("Classifier["+classifier.getAttribute()+"] getNumPosInstances:" + ((IncremClassifier)classifier).getNumPosInstances());
//			logger.info("Classifier["+classifier.getAttribute()+"] getNumPosInstances:" + classifier.getNumPosInstances());
//			logger.info("Classifier["+classifier.getAttribute()+"] getNumOfInstances:" + ((IncremClassifier)classifier).getNumOfInstances());
		}
		
		return smapleMap;
	}
	
	/************************** Classifier Performance ******************************/
	private Map<String, Double> evalMap;
	private Map<String, Double> resultMap;
	public boolean isNormalizing = true;

	/**
	 * @return map of attributes and their corresponding predict scores
	 */
	public Map<String, Double> getResultMap() {
		return this.resultMap;
	}
	
	/**
	 * @return map of classifiers and their corresponding relative entropies
	 */
	public Map<String, Double> getEvalMap(){
		return this.evalMap;
	}
	
	/**
	 * @return List of predict results containing attribute name, probabilities and relative entropy
	 */
	public List<Concept> getResultTypes(){
		List<Concept> predList = new ArrayList<Concept>();

		if((this.evalMap != null && !this.evalMap.isEmpty()) && (this.resultMap != null && !this.resultMap.isEmpty())){
			Iterator iterator = classifierList.entrySet().iterator();
			while (iterator.hasNext()) {
				 Map.Entry pair = (Map.Entry) iterator.next();
				 String attr = (String)pair.getKey();
				 
				 Concept pred = new Concept(null,this.ontology,attr);
				 pred.setPredictProb(this.resultMap.get(attr),this.isNormalizing);
				 pred.setEntropy(this.evalMap.get(attr));
				 
				 predList.add(pred);
			}
		}
		
		return predList;
	}
}	
