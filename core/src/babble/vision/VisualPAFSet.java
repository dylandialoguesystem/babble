package babble.vision;

import java.util.ArrayList;

/**
 * Class to build a collection of Visual predicate argument formula, 
 * which contains several visual classifiers for particular category.
 * 
 * @author Yanchao Yu
 *
 * @param <VPAF> visual predicate-argument formula
 */
public class VisualPAFSet<VPAF extends VisualPredicateArgumentFormula> extends ArrayList<VPAF> {
	private static final long serialVersionUID = 1L;

	public VisualPAFSet(){}
	
	
	/**
	 * Add new formula into the collection for specific category.
	 * @param vpaf visual predicate-argument formula
	 */
	public void addVisualPAF(VPAF vpaf){
		if(!this.contains(vpaf))
			this.add(vpaf);
	}
	
	/**
	 * Add a set of formulas into the collection
	 * @param vpafs a list of formulas
	 */
	public void addVisualPAFs(ArrayList<VPAF> vpafs){
		for(VPAF vpaf: vpafs){
			this.add(vpaf);
		}
	}
	
	/**
	 * remove the specific visual predicate-argument formula
	 * @param vpaf visual predicate-argument formula
	 */
	public void removeVisualPAFs(VPAF vpaf){
		if(!this.isEmpty() && this.contains(vpaf))
			this.remove(vpaf);
	}
	
	/**
	 * clear the formula collection
	 */
	public void clearVisualPAFs(){
		if(!this.isEmpty())
			this.clear();
	}
	
	/**
	 * @return the size of formula collection
	 */
	public int getNumOfVPAFs(){
		return this.size();
	}
}
