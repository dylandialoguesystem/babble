package babble.vision.tools;

/**
 * Enum of Target TTR Types
 * @author Yanchao Yu
 */
public enum TargetTTRType{
	answer,
	donotKnow,
	question;
}