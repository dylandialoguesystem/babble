package babble.vision.tools;

public class Constants {
	private Constants instance;
	
	private Constants(){}
	
	public Constants getInstance(){
		if(instance == null)
			instance = new Constants();
		return instance;
	}
	
	public static final String simulateUser = "demo.tst";
	public static final String simulateUserID = "001";

	public final static int recordUpdatems = 000;
	public final static int ttrUpdatems = 300;
	public final static int predClassifyms = 800;
	public final static int camerams = 10000;
	public static final String systemName = "system v1";

	public static String parserFilename = "../dsttr/resource/2016-english-ttr-attribute-learning";
	public static String localparserFilename = "resource/2016-english-ttr-attribute-learning";
	public static int waitPeriod = 120000;
	
	
	/**
	 * Classifer Settings
	 */
	public static String claDir = "data/classifier/";
	
	public String getClassifierPath(String attribute){
		return claDir+attribute+"cls.mat";
	}
	
	/**
	 * Keyboard Control
	 */
	public static String SLASH = "/";
	public static String SPACE = " ";
	public static String NEWLINE = "\r\n";
//	public static String NEWLINE = "<br>";
	public static final CharSequence ELLIPSIS = "$";
	public static final String ACTSPLITER = "&&";
	public static final String DOT = ".";
	public static final int fontSize = 16;
	public static final int dlg_fontSize = 17;

	/**
	 * Experiments Settings
	 */
	public static String exptRoot = "data/Experiments";
	public static String expriment = "offline";
	public static String resultsDir = exptRoot + SLASH + expriment + SLASH + "results" + SLASH;
	public static String simulateExpDir = exptRoot + SLASH + "simulate";
	public static String xlsExpDir =  exptRoot + SLASH + "xlsFolder";
	public static String simulateReport = simulateExpDir + SLASH + "simulateresults.txt";
	public static String rlDir = "data/RLPolicy/";
	
	/**
	 * Data Set Settings
	 */
	public static String recourceRoot = "resource";
	public static String instanceDir = "2016-instances";
	public static String classifierType = recourceRoot + SLASH + "classifier-types.txt";
	public static String picFilename = "data/template.jpg";
	public static String simuldaPath = recourceRoot + SLASH + "simulData.txt";

	public static String trnSetDir = recourceRoot + SLASH + instanceDir  + SLASH + "trainset" + SLASH;
	public static String annotTrnSet = trnSetDir + SLASH + "annotation.txt";
	
	public static String tstSetDir = recourceRoot + SLASH + instanceDir + SLASH + "tstset" + SLASH;
	public static String annotTstSet = tstSetDir + SLASH + "annotation.txt";
	
	public static String preTrnDir = recourceRoot + SLASH + instanceDir + SLASH + "pretrain-instances" + SLASH;
	public static String annotPreTrnSet= preTrnDir + SLASH + "annotation.txt";
	

	public static String visualAttDir = "2016-visualsamples";
	public static String visualAttFile = recourceRoot + SLASH + visualAttDir  + SLASH + "resource-data.txt";

	
	/**
	 * TimeOut Settings
	 */
	public static final long UNIT = 1000;
	
	/**
	 * Vision Module Settings
	 */
	public static final int nBestResult = 1;
	
	/**
	 * DyLan Module Settings
	 */
	public static final String TUTOR = "T:";
	public static final String SYS = "S:";
	

	public static String configFile = recourceRoot + "configuration.txt";
}
