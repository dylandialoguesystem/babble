package babble.vision.tools;

import java.util.Date;

public class VSTimer {
	private static VSTimer isntance;

	private VSTimer(){}
	
	public static VSTimer getInstance(){
		if(isntance == null)
			isntance = new VSTimer();
		
		return isntance;
	}

	/**
	 * @return time stamp in the format of "HH:mm:ss"
	 */
	public String getTimeStamp(){
		return new java.text.SimpleDateFormat("HH:mm:ss").format(new Date());
	}
	
	/**
	 * @return time stamp in the format of "dd/mm/yy"
	 */
	public String getDate(){
		return new java.text.SimpleDateFormat("dd/mm/yy").format(new Date());
	}
	
	/**
	 * @return current time in milliseconds.
	 */
	public long getCurrentTime(){
		return new Date().getTime();
	}
}
