package babble.vision.tools;

import java.awt.EventQueue;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.log4j.Logger;

import matlabcontrol.MatlabConnectionException;
import matlabcontrol.MatlabInvocationException;
import matlabcontrol.MatlabProxy;
import matlabcontrol.MatlabProxyFactory;
import matlabcontrol.MatlabProxyFactoryOptions;

public class MatlabLink {
	private MatlabProxyFactory factory;
	static Logger logger = Logger.getLogger(MatlabLink.class);
	
	//Proxy to communicate with MATLAB
	private final AtomicReference<MatlabProxy> proxyHolder = new AtomicReference<MatlabProxy>();
	private static final String STATUSDISCONNECTED = "Connection Status: Disconnected",
			STATUSCONNECTING = "Connection Status: Connecting",
			STATUSCONNECTEDEXISTING = "Connection Status: Connected (Existing)",
			STATUSCONNECTEDLAUNCHED = "Connection Status: Connected (Launched)";
	
	public MatlabLink(){
		initalMactlab();
	}
	
	private void initalMactlab(){
		//Create proxy factory
        MatlabProxyFactoryOptions options = new MatlabProxyFactoryOptions.Builder()
                .setUsePreviouslyControlledSession(true)
                //.setMatlabLocation(Constants.matlabDir)
                .build();
        factory = new MatlabProxyFactory(options);
        connectMatlabProxy();
	}
	
	public AtomicReference<MatlabProxy> geyProxyHolder(){
		return this.proxyHolder;
	}
	
	private boolean connectMatlabProxy(){
		try
        {
            //Request a proxy
            factory.requestProxy(new MatlabProxyFactory.RequestCallback()
            {
                @Override
                public void proxyCreated(final MatlabProxy proxy)
                {
                    proxyHolder.set(proxy);
                
                    proxy.addDisconnectionListener(new MatlabProxy.DisconnectionListener()
                    {
                        @Override
                        public void proxyDisconnected(MatlabProxy proxy)
                        {
                            proxyHolder.set(null); 
                            //Visual update
                            EventQueue.invokeLater(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                	logger.error("Matlab Disconnected!");
                                }
                            });
                        }
                    });
            
                    //Visual update
                    EventQueue.invokeLater(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            String status;
                            if(proxy.isExistingSession())
                                status = STATUSCONNECTEDEXISTING;
                            else
                                status = STATUSCONNECTEDLAUNCHED;
                            logger.info(status);
                        }
                    });
                }
            });
            return true;
        }
        catch(MatlabConnectionException exc)
        {
        	exc.getStackTrace();
        }
		return false;
	}
	
//	public void excuteFunc(String method, Mat data){
//		try {
//			geyProxyHolder().get().feval(method,data);
//		} catch (MatlabInvocationException e) {
//            logger.error(e.getMessage());
//		}
//	}

	public void excuteFunc(String method, double[][] data1, double[][] data2, double[][] data3) {
		try {
			geyProxyHolder().get().feval(method,data1,data2,data3);
		} catch (MatlabInvocationException e) {
            logger.error(e.getMessage());
		}
	}

	public void excuteFunc(String method, String para) {
		try {
			geyProxyHolder().get().feval(method,para);
		} catch (MatlabInvocationException e) {
            logger.error(e.getMessage());
		}
	}

	public double[] excuteFunc(String method, String para,double[] data) {
		try {
			Object[] objs = geyProxyHolder().get().returningFeval(method,1,para,data);
//			for(int i=0; i<objs.length;i++){
//				Object obj = objs[i];
//				System.out.println("##Object: " + obj);
//				double[] s = (double[])objs[i];
//				for(int j=0; j < s.length; j++){
//					System.out.println("##s: " + s[j]);
//				}
//			}
			double[] result = (double[])objs[0];
			
			return result;
		} catch (MatlabInvocationException e) {
            logger.error(e.getMessage());
		}
		
		return null;
	}
	
	public double[] excuteFunc2(String method, String para) {
		try {
			Object[] objs = geyProxyHolder().get().returningFeval(method,1,para);
//			for(int i=0; i<objs.length;i++){
//				Object obj = objs[i];
//				System.out.println("##Object: " + obj);
//				double[] s = (double[])objs[i];
//				for(int j=0; j < s.length; j++){
//					System.out.println("##s: " + s[j]);
//				}
//			}
			double[] result = (double[])objs[0];
			
			return result;
		} catch (MatlabInvocationException e) {
            logger.error(e.getMessage());
		}
		
		return null;
	}

	public void excuteFunc(String method, String dir, String ontology, String[] attrs, int index, double[][] actual, double[][] pred, String policy, int numPos, int numNeg, String sampleDistribution) {
		try {
//			logger.info("Details: "+ index + ", " + dir + ", " + ontology + ", " + actual + ", " + pred + ", " + policy.toString()+ ", " + numPos+ ", " + numNeg+ ", " + sampleDistribution);
			geyProxyHolder().get().feval(method, dir, ontology, attrs, index, actual, pred, policy, numPos, numNeg, sampleDistribution);
		} catch (MatlabInvocationException e) {
            logger.error(e.getMessage());
		}
	}

	public void excuteFunc(String method, String ontology, String[] clsList, String timeStamp, double[][] actual, double[][] pred) {
		try {
			geyProxyHolder().get().feval(method, ontology, clsList, timeStamp, actual, pred);
		} catch (MatlabInvocationException e) {
            logger.error(e.getMessage());
		}
	}

	public void excuteFunc(String method, String dir, int nfold, int[] scoreLabels,
			double[] scoreList, double[] cList, String tutorPolicy, String learnerPolicy) {
		try {
			geyProxyHolder().get().feval(method, dir, nfold, scoreLabels, scoreList, cList, tutorPolicy, learnerPolicy);
		} catch (MatlabInvocationException e) {
            logger.error(e.getMessage());
		}
	}

	public void excuteFunc(String method, String dir, int nfold, int[] scoreLabels,
			double[] scoreList, double[] cList, String initiativeTyp, boolean isUncertainty, boolean isContentDependency, boolean hasCorretion, int numPos, int nemNeg, double posThreshold) {
		try {
			if(!hasCorretion)
				initiativeTyp += "noCorrect";
			
			geyProxyHolder().get().feval(method, dir, nfold, scoreLabels, scoreList, cList, initiativeTyp,
					String.valueOf(isUncertainty), String.valueOf(isContentDependency), numPos, nemNeg, posThreshold);
		} catch (MatlabInvocationException e) {
            logger.error(e.getMessage());
		}
	}

	public void excuteFunc(String method, String dir, int nfold, String ontology, int[] scoreLabels,
			double[] scoreList, String policy, int numPos, int numNeg, String requestClass) {

		try {
			geyProxyHolder().get().feval(method, dir, nfold, ontology, scoreLabels, scoreList, policy, numPos, numNeg, requestClass);
		} catch (MatlabInvocationException e) {
            logger.error(e.getMessage());
		}
	}
	
	public void excuteFunc(String method, String dir, int nfold, int[] scoreLabels, double[] scoreList, double[] cList) {
		try {
			geyProxyHolder().get().feval(method, dir, nfold, scoreLabels, scoreList, cList);
		} catch (MatlabInvocationException e) {
            logger.error(e.getMessage());
		}
	}
	
	public void excuteFunc(String method, String dir, int nfold, int[] scoreLabels, double[] scoreList,
			double[] cList, String tutorPolicy, String learnerPolicy, boolean hasDecamand, boolean hasCorretion,
			int numPos, int numNeg, double posThreshold) {
		try {
			geyProxyHolder().get().feval(method, dir, nfold, scoreLabels, scoreList, cList, tutorPolicy, learnerPolicy, String.valueOf(hasDecamand), String.valueOf(hasCorretion));
		} catch (MatlabInvocationException e) {
            logger.error(e.getMessage());
		}
	}
}
