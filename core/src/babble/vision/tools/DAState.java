package babble.vision.tools;

/**
 * Enum of System Dialog Status 
 * @author Yanchao Yu
 */
public enum DAState {
	listening, // listening for user talking, e.g. questions, statements, confirmation or others 
//	learning, // training the classifiers and modifying the probabilities
//	checking, // checking if semantic TTR subsumes the Visual Context
	bargining, // System bargin into the user utterace
	waitingforconfirm, // waiting for confirmation from users, i.e. yes or no 
	waitingforanswer, // waiting for confirmation from users, i.e. yes or no 
	requestingformore; // learning for more instance under specific attribute
}
