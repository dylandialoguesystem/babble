package babble.vision.tools;

import java.util.HashMap;
import java.util.Map;

public class Entropy {
	private static Entropy instance;
	public Entropy(){
	}
	
	public static Entropy getInstance(){
		if(instance == null)
			instance = new Entropy();
		
		return instance;
	}
	
	public Map<String, Double> relativeEntropy(String attr, double[] p, double[] q){
		
		double entropy = 0;
		for(int i=0; i<p.length; i++){
			entropy += p[i] * Math.log(p[i] / q[i]);
		}
		
		Map<String, Double> RE = new HashMap<String, Double>();
		RE.put(attr, entropy);
		
		return RE;
	}
	
	public double crossEntropy(double[] p, double[] q){
		double entropy = 0;
		for(int i=0; i<p.length; i++){
			double tmp = -1 * (p[i] * Math.log(q[i]));
			
			if(Double.isNaN(tmp))
				tmp = 0;
			else if(Double.isInfinite(tmp))
				tmp = 0;
			
			entropy += tmp;
		}
		
		return entropy;
	}
	
	public double relativeEntropy(double p, double q){
		double entropy = p * Math.log(p / q);
		
		if(Double.isNaN(entropy))
			entropy = 0;
		else if(Double.isInfinite(entropy))
			entropy = 0;
		
		return entropy;
	}

	public double entropy(double[] p){
		double entropy = 0.0;
		
		for(int i=0; i < p.length; i++){			
//			System.out.println("P["+i+"] = " + p[i]);

			double log2Val = Math.log10(p[i]) / Math.log10(2);
//			System.out.println("log2Val["+i+"] = " + log2Val);
			
			entropy -= p[i] * log2Val;
//			System.out.println("entropy = " + entropy);
		}
		
		return entropy;
	}
	
}
