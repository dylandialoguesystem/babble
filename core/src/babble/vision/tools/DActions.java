package babble.vision.tools;

/**
 * Enum of System Dialog Actions through TTRRecordType
 * @author Yanchao Yu
 */
public enum DActions {
	acknowledge,
	bargin, 
	subsume,
	learn;
}
