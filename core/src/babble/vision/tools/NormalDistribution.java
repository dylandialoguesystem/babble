package babble.vision.tools;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class NormalDistribution {
	private static NormalDistribution instance;
	
	public static NormalDistribution getInstance(){
		if(instance == null)
			instance = new NormalDistribution();
		
		return instance;
	}
	
	public Map<String, Double> getProbabilities(HashMap<String, Double> resultsMap){
		HashMap<String, Double> copy = resultsMap;
		Iterator<Entry<String, Double>> entries = resultsMap.entrySet().iterator();
		
		List<Double> probs= new ArrayList<Double>(copy.values());
		double sum = 0;
		for(double prob : probs){
			sum += prob;
		}
		
		//System.out.println("SUM = " + sum);
		while (entries.hasNext()) {
		  Entry<String, Double> pair = (Entry<String, Double>) entries.next();
		  String attr = (String)pair.getKey();
		  double value = (double)pair.getValue();
		  
		  double nomalizedValue =  value / sum;
		  copy.put(attr, nomalizedValue);
		}
		
	    Map<String, Double> newMap = MapToolKit.getInstance().sortByComparator(copy, true);
	    
		return newMap;
	}
	

	public double[][] getProbabilities(double[][] results){
		double[][] nomalized = results.clone();
		for(int i=0; i < results[0].length; i++){

			double sum = 0;
			for(int j=0; j < results.length; j++){
				sum += results[j][i];
			}

			for(int j=0; j < results.length; j++){
				 double nomalizedValue =  results[j][i] / sum;
				 nomalized[j][i] = nomalizedValue;
			}
		}
		
		return nomalized;
	}
}
