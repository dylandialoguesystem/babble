package babble.vision.tools;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import java.util.Set;

import burlap.mdp.core.action.Action;
import ngram.core.Condition;

public class MapToolKit {
	private static MapToolKit instance;	
	
	private MapToolKit(){}
	 
	public static MapToolKit getInstance() 
    {
  		if (instance == null)
  			instance = new MapToolKit();
      return instance;
    }	
	
	public Map<String, Double> sortByComparator(Map<String, Double> unsortMap, boolean isDescend) {

		// Convert Map to List
		List<Map.Entry<String, Double>> list = new LinkedList<Map.Entry<String, Double>>(unsortMap.entrySet());
	
		Comparator<Map.Entry<String, Double>> comparator = new Comparator<Map.Entry<String, Double>>() {
			public int compare(Map.Entry<String, Double> o1,
                    Map.Entry<String, Double> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		};
		
		if(isDescend)
			comparator = Collections.reverseOrder(comparator);
		
		Collections.sort(list,comparator);
		
		// Convert sorted map back to a Map
		Map<String, Double> sortedMap = new LinkedHashMap<String, Double>();
				
		for (Iterator<Map.Entry<String, Double>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Double> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
				
		return sortedMap;
	}
	
	public String getTopKey(Map<String, Double> map){
		int count = 0;
		Iterator<Entry<String, Double>> iterator = map.entrySet().iterator();
		while(iterator.hasNext()){
			if(count < 1){
				Entry<String, Double> pair = iterator.next();
				String str = pair.getKey();
				return str;
			}
		}
		
		return null;
	}

	/**
	 * @return the best predicted result cross all classifiers
	 */
	public Map<String, Double> normalize(HashMap<String, Double> map){
		if(map != null && !map.isEmpty()){
			Map<String, Double> normalizes = NormalDistribution.getInstance().getProbabilities(map);
			return normalizes;
		}
		return null;
	}

	public Map<String, double[]> gNormalize(Set<String> attrSet, double[][] predResults){
		Map<String, double[]> normalizeMap = new HashMap<String, double[]>();
		if(predResults != null){
			double[][] pred = NormalDistribution.getInstance().getProbabilities(predResults);
			
			int i=0;
			for(String str : attrSet){
				String attr = str;
				normalizeMap.put(attr, pred[i]);
				i++;
			}
		}
		return normalizeMap;
	}
	
	public Map<Condition, Double> sortObjMapByComparator(Map<Condition, Double> unsortMap, boolean isDescend) {

		// Convert Map to List
		List<Map.Entry<Condition, Double>> list = new LinkedList<Map.Entry<Condition, Double>>(unsortMap.entrySet());
	
		Comparator<Map.Entry<Condition, Double>> comparator = new Comparator<Map.Entry<Condition, Double>>() {
			public int compare(Map.Entry<Condition, Double> o1,
                    Map.Entry<Condition, Double> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		};
		
		if(isDescend)
			comparator = Collections.reverseOrder(comparator);
		
		Collections.sort(list,comparator);
		
		// Convert sorted map back to a Map
		Map<Condition, Double> sortedMap = new LinkedHashMap<Condition, Double>();
				
		for (Iterator<Map.Entry<Condition, Double>> it = list.iterator(); it.hasNext();) {
			Map.Entry<Condition, Double> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
				
		return sortedMap;
	}
	
	public Condition getTopKeyFromObjMap(Map<Condition, Double> map){
		int count = 0;
		Iterator<Entry<Condition, Double>> iterator = map.entrySet().iterator();
		while(iterator.hasNext()){
			if(count < 1){
				Entry<Condition, Double> pair = iterator.next();
				Condition obj = pair.getKey();
				return obj;
			}
		}
		
		return null;
	}
	
	public Map<Condition, Double> getNBest(Map<Condition, Double> map, int n_best){
		Map<Condition, Double> nBests = new HashMap<Condition, Double>();
		
		if(map != null && !map.isEmpty()){
			int index = 0;
			Iterator<Entry<Condition, Double>> iterator = map.entrySet().iterator();
			while(iterator.hasNext()){
				if(index < n_best){
					Entry<Condition, Double> pair = iterator.next();
					Condition condition = pair.getKey();
					Double value = pair.getValue();
					nBests.put(condition, value);
					index++;
				}
				else
					break;
			}
		}
		
		return this.sortObjMapByComparator(nBests, true);
	}
	

	
	public Map<Action, Double> sortActionMapByComparator(Map<Action, Double> unsortMap, boolean isDescend) {

		// Convert Map to List
		List<Map.Entry<Action, Double>> list = new LinkedList<Map.Entry<Action, Double>>(unsortMap.entrySet());
	
		Comparator<Map.Entry<Action, Double>> comparator = new Comparator<Map.Entry<Action, Double>>() {
			public int compare(Map.Entry<Action, Double> o1,
                    Map.Entry<Action, Double> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		};
		
		if(isDescend)
			comparator = Collections.reverseOrder(comparator);
		
		Collections.sort(list,comparator);
		
		// Convert sorted map back to a Map
		Map<Action, Double> sortedMap = new LinkedHashMap<Action, Double>();
				
		for (Iterator<Map.Entry<Action, Double>> it = list.iterator(); it.hasNext();) {
			Map.Entry<Action, Double> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
				
		return sortedMap;
	}
	
	public Action getTopKeyFromActionMap(Map<Action, Double> map){
		int count = 0;
		Iterator<Entry<Action, Double>> iterator = map.entrySet().iterator();
		while(iterator.hasNext()){
			if(count < 1){
				Entry<Action, Double> pair = iterator.next();
				Action obj = pair.getKey();
				return obj;
			}
		}
		
		return null;
	}
}
