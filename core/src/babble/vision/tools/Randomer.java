package babble.vision.tools;

import java.util.Random;

public class Randomer {
	private static Randomer instance;	
	
	private Randomer(){}
	 
	public static Randomer getInstance() 
    {
  		if (instance == null)
  			instance = new Randomer();
      return instance;
    }	
	
	public int randomInt(int min, int max){
		Random random = new Random();
		int num = random.nextInt(max - min + 1) + min;
		return num;
	}

	public double randomDouble(){
		Random random = new Random();
		double num = random.nextDouble();
		return num;
	}

	public int randomInt(int max) {
		Random random = new Random();
		int num = random.nextInt(max);
		return num;
	}
}
