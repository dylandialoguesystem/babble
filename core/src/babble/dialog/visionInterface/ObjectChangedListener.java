package babble.dialog.visionInterface;

import org.json.simple.JSONObject;

public interface ObjectChangedListener {
	
	public void objectHasChanged(JSONObject json);

}
