package babble.dialog.visionInterface;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.util.Map;

import javax.swing.JPanel;

/**
 * 
 * @author Yanchao Yu
 *
 */

public class JDrawerPanel extends JPanel{
	private static final long serialVersionUID = 1L;
	
	private Color paintColor;
	private Map<String, Integer> valueMap;
	private int[] values;
	private String basicType;
	
	public JDrawerPanel(int width, int height){
		this.setPreferredSize(new Dimension(width,height));
		this.setMinimumSize(new Dimension(width,height));
		this.setMaximumSize(new Dimension(width,height));
	}

	public JDrawerPanel(){
	}
	
	public void setWhiteBackground(){
		setBackground(Color.WHITE);
		setForeground(Color.WHITE);
	}
	
	public void setColor(Color color){
		this.paintColor = color;
	}
	
	public void setValueMap(Map<String, Integer> valueMap){
		this.valueMap = valueMap;
	}
	
	public void setValues(int[] values){
		this.values = values;
	}
	
	public void setBasicType(String basicType){
		this.basicType = basicType;
	}
	
	private boolean clear = false;
	public void paint(Graphics g){
		super.paint(g);
	    if(clear){
	        return;
	    }
	
	    Graphics2D g2 = (Graphics2D)g;
	    g2.setPaint(this.paintColor);
		
		if(this.basicType != null && !this.basicType.isEmpty()){
			if(this.basicType.equals("Rectangle")){
				if(valueMap != null && !valueMap.isEmpty()){
					int x = valueMap.get("pointX");
					int y = valueMap.get("pointY");
					int width = valueMap.get("width");
					int height = valueMap.get("height");
					
					g2.fill(new Rectangle2D.Double(x, y, width, height));
				}
				else 
					System.err.println("cannot load values for rectangle...");
			}
			else if(this.basicType.equals("Triangle")){
				if(values != null && values.length != 0){
					
					Path2D path = new Path2D.Double();
					for(int i = 0; i < values.length; i+=2){
						if(i == 0)
							path.moveTo(values[i],values[i+1]);
						else
							path.lineTo(values[i],values[i+1]);
					}
				      
					path.closePath();
					g2.fill(path); 
				}
				else 
					System.err.println("cannot load values for triangle...");
			}
			else if(this.basicType.equals("Ellipse")){
				if(valueMap != null && !valueMap.isEmpty()){
					int x = valueMap.get("centerX");
					int y = valueMap.get("centerY");
					int radius1 = valueMap.get("radius1");
					int radius2 = valueMap.get("radius2");
				    
				    g2.fill(new Ellipse2D.Double(x, y, radius1, radius2));
				}
				else 
					System.err.println("cannot load values for rectangle...");
			}
			else
				System.err.println("unable to build this shape type...");
		}
	}
}
