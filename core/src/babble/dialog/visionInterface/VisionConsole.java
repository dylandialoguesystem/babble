package babble.dialog.visionInterface;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import babble.simulation.ConsoleClickListener;

public class VisionConsole extends JFrame{
	private static final long serialVersionUID = 1L;
	
	JDrawerPanel jdp;
    Vector<JButton> jbuttons = new Vector<JButton>();
    
    private static int IMG_WIDTH;
    private static int IMG_HEIGHT;
	
	public VisionConsole(int width, int height, String[] buttonnames, ConsoleClickListener listener){
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		
		this.getContentPane().setLayout(new BorderLayout());
		jdp = new JDrawerPanel(width,height*3/4);
		jdp.setWhiteBackground();
		IMG_WIDTH = width;
		
		this.getContentPane().add(jdp, BorderLayout.CENTER);
		
		JPanel southPanel = new JPanel();
	    southPanel.setLayout(new BorderLayout());
	    JPanel southPanelNorth = new JPanel();
	    JPanel southPanelSouth = new JPanel();
	        
	    if(buttonnames != null && buttonnames.length != 0){
	    	for(int i=0;i<buttonnames.length;i++){
	    		JButton jb = new JButton(buttonnames[i]);
		        jb.setFocusable(false);
		           
		        southPanelNorth.add(jb);    
		        this.jbuttons.add(jb);
		        jb.addActionListener(listener);
		     }
	    }

	    southPanel.add(southPanelNorth,BorderLayout.NORTH);
	    southPanel.add(southPanelSouth,BorderLayout.SOUTH);
	        
	    this.getContentPane().add(southPanel,BorderLayout.SOUTH);
	      
	    this.setResizable(false);
	    this.setVisible(false);
	    this.pack(); 
	}
	
	public static void main(String[] args){
		String[] buttonnames = {"Next Object"};
		VisionConsole visionConsole = new VisionConsole(400,600, buttonnames, null);
		visionConsole.setVisible(true);
	}
	
	public void buildObject(String json) {
		JSONParser parser = new JSONParser();
		try {
			if(json != null && !json.isEmpty()){
				JSONObject data = (JSONObject) parser.parse(json);

				// deal with color description
				JSONObject colorJSON = (JSONObject) data.get("color");
				JSONObject valJSON = (JSONObject) colorJSON.get("values");
				int r_value = ((Long) valJSON.get("R")).intValue();
				int g_value = ((Long) valJSON.get("G")).intValue();
				int b_value = ((Long) valJSON.get("B")).intValue();
				
				Color newColor = new Color(r_value, g_value, b_value);
				this.jdp.setColor(newColor);
				
				// deal with shape description
				JSONObject shapeJSON = (JSONObject) data.get("shape");
				String basicType = (String) shapeJSON.get("basicType");
				
				if(basicType.equals("Rectangle")){
					Map<String, Integer> values = new HashMap<String, Integer>();
					JSONObject valueJSON = (JSONObject) shapeJSON.get("values");
					values.put("pointX", ((Long)valueJSON.get("pointX")).intValue());
					values.put("pointY", ((Long)valueJSON.get("pointY")).intValue());
					values.put("width", ((Long)valueJSON.get("width")).intValue());
					values.put("height", ((Long)valueJSON.get("height")).intValue());

					this.jdp.setBasicType(basicType);
					this.jdp.setValueMap(values);
				}
				else if(basicType.equals("Triangle")){
					JSONArray valueArray = (JSONArray) shapeJSON.get("values");
					int[] values = new int[valueArray.size() * 2];
					for(int i = 0; i < valueArray.size(); i++){
						JSONObject pointJSON = (JSONObject) valueArray.get(i);
						values[i*2] = ((Long) pointJSON.get("pointX")).intValue();
						values[i*2+1] = ((Long) pointJSON.get("pointY")).intValue();
					}
					this.jdp.setBasicType(basicType);
					this.jdp.setValues(values);
				}
				else if(basicType.equals("Ellipse")){
					Map<String, Integer> values = new HashMap<String, Integer>();
					JSONObject valueJSON = (JSONObject) shapeJSON.get("values");
					values.put("centerX", ((Long)valueJSON.get("centerX")).intValue());
					values.put("centerY", ((Long)valueJSON.get("centerY")).intValue());
					values.put("radius1", ((Long)valueJSON.get("radius1")).intValue());
					values.put("radius2", ((Long)valueJSON.get("radius2")).intValue());

					this.jdp.setBasicType(basicType);
					this.jdp.setValueMap(values);
				}
				this.jdp.validate();
				this.jdp.repaint();
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public void buildObject(String objCombine, long duration) {
		buildObject(objCombine);
		
		final VisionConsole jvcl = this;
	       SwingUtilities.invokeLater(new Runnable(){
	           public void run(){
//	               jdp.changeImage(objCombine, "", duration);
	           }	            
        });
	}
}
