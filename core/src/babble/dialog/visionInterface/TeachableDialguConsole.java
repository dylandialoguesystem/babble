package babble.dialog.visionInterface;

import java.awt.BorderLayout;
import java.awt.Color;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import babble.dialog.chatinterface.DialogueConsole;
import babble.dialog.chatinterface.JTextPaneRightJustified;
import babble.dialogue.DSDialogueAgent;
import qmul.ds.dag.UtteredWord;

public class TeachableDialguConsole extends DialogueConsole implements ObjectChangedListener{

	Logger logger = Logger.getLogger(TeachableDialguConsole.class);
	Queue<UtteredWord> uttered = new LinkedList<UtteredWord>();
	final static Color other_colour = Color.blue;

	JDrawerPanel jdPanel;
	
	private static final int WIDTH = 700;
	private static final int HEIGHT = 600;
	
	public TeachableDialguConsole(String name) {
		super(name);
	}
	
	public void init(){
		console.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		console.setLayout(new BorderLayout());

		textPane = new JTextPaneRightJustified(this);
		textPane.setSize(WIDTH, HEIGHT/6);
		// textPane.jtprjfd.addDocumentListener(this);
		// textPane.jtprjfd.insertStringFromOther("HELLO", Color.green);
		JScrollPane scrollPane = new JScrollPane(textPane);
		console.add(scrollPane, BorderLayout.SOUTH);

		jdPanel = new JDrawerPanel(WIDTH,HEIGHT*5/6);
		jdPanel.setWhiteBackground();
		console.add(jdPanel, BorderLayout.CENTER);
		
		console.setSize(WIDTH, HEIGHT);
		console.setVisible(true);
		textPane.startFade();
	}


	public static void main(String[] a) {
		TeachableDialguConsole console = new TeachableDialguConsole("arash");
		
	}

	@Override
	public synchronized UtteredWord nextWord() throws InterruptedException {
		if (uttered.isEmpty()) {
			wait(2000);
			if (uttered.isEmpty()&&dialogue_env.hasFloor(this))
				return new UtteredWord(DSDialogueAgent.RELEASE_TURN, this.name);
			else if (uttered.isEmpty())
				return new UtteredWord(DSDialogueAgent.WAIT, this.name);

		}
		return uttered.poll();
	}

	public boolean hasFloor() {
		return this.dialogue_env.hasFloor(this);
	}

	public boolean floorIsOpen() {
		return dialogue_env.floorIsOpen();
	}

	@Override
	public void run() {
		InputStreamReader reader = new InputStreamReader(System.in);
		//not running on its own thread yet..... will prob never do.

	}

	UtteredWord last = null;

	@Override
	public void wordUttered(UtteredWord w) {
		if (w.speaker().equals(name))
			return;
		
		if (w.word().equals(DSDialogueAgent.RELEASE_TURN))
			return;

		String toInsert = null;
		try {
			String text = this.textPane.jtprjfd.getText(0, this.textPane.jtprjfd.getLength());
			if (text.endsWith(" "))
				toInsert = w.word() + " ";
			else
				toInsert = " " + w.word() + " ";
		} catch (Exception e) {
			e.printStackTrace();
		}

		textPane.insertTextByOther(toInsert, other_colour, w.speaker());

	}

	public synchronized void addWord(String word) {
		logger.debug("Adding word:" + word);
		uttered.offer(new UtteredWord(word, this.name));
		notifyAll();
	}

	@Override
	public void reset() {
	}

	@Override
	public void objectHasChanged(JSONObject json) {
		if(json != null)
			this.buildObject(json.toJSONString());
	}
	
	public void buildObject(String json) {
		JSONParser parser = new JSONParser();
		try {
			if(json != null && !json.isEmpty()){
				JSONObject data = (JSONObject) parser.parse(json);

				// deal with color description
				JSONObject colorJSON = (JSONObject) data.get("color");
				JSONObject valJSON = (JSONObject) colorJSON.get("values");
				int r_value = ((Long) valJSON.get("R")).intValue();
				int g_value = ((Long) valJSON.get("G")).intValue();
				int b_value = ((Long) valJSON.get("B")).intValue();
				
				Color newColor = new Color(r_value, g_value, b_value);
				this.jdPanel.setColor(newColor);
				
				// deal with shape description
				JSONObject shapeJSON = (JSONObject) data.get("shape");
				String basicType = (String) shapeJSON.get("basicType");
				
				if(basicType.equals("Rectangle")){
					Map<String, Integer> values = new HashMap<String, Integer>();
					JSONObject valueJSON = (JSONObject) shapeJSON.get("values");
					values.put("pointX", ((Long)valueJSON.get("pointX")).intValue());
					values.put("pointY", ((Long)valueJSON.get("pointY")).intValue());
					values.put("width", ((Long)valueJSON.get("width")).intValue());
					values.put("height", ((Long)valueJSON.get("height")).intValue());

					this.jdPanel.setBasicType(basicType);
					this.jdPanel.setValueMap(values);
				}
				else if(basicType.equals("Triangle")){
					JSONArray valueArray = (JSONArray) shapeJSON.get("values");
					int[] values = new int[valueArray.size() * 2];
					for(int i = 0; i < valueArray.size(); i++){
						JSONObject pointJSON = (JSONObject) valueArray.get(i);
						values[i*2] = ((Long) pointJSON.get("pointX")).intValue();
						values[i*2+1] = ((Long) pointJSON.get("pointY")).intValue();
					}
					this.jdPanel.setBasicType(basicType);
					this.jdPanel.setValues(values);
				}
				else if(basicType.equals("Ellipse")){
					Map<String, Integer> values = new HashMap<String, Integer>();
					JSONObject valueJSON = (JSONObject) shapeJSON.get("values");
					values.put("centerX", ((Long)valueJSON.get("centerX")).intValue());
					values.put("centerY", ((Long)valueJSON.get("centerY")).intValue());
					values.put("radius1", ((Long)valueJSON.get("radius1")).intValue());
					values.put("radius2", ((Long)valueJSON.get("radius2")).intValue());

					this.jdPanel.setBasicType(basicType);
					this.jdPanel.setValueMap(values);
				}
				this.jdPanel.validate();
				this.jdPanel.repaint();
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public void buildObject(String objCombine, long duration) {
		buildObject(objCombine);
		
		final TeachableDialguConsole jvcl = this;
	       SwingUtilities.invokeLater(new Runnable(){
	           public void run(){
//	               jdPanel.changeImage(objCombine, "", duration);
	           }	            
        });
	}
}
