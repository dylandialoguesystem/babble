package babble.dialog.rl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import babble.dialogue.DSDialogueAgent;
import burlap.mdp.auxiliary.DomainGenerator;
import burlap.mdp.core.action.UniversalActionType;
import burlap.mdp.singleagent.SADomain;
import qmul.ds.Context;
import qmul.ds.Dialogue;
import qmul.ds.InteractiveContextParser;
import qmul.ds.Utterance;
import qmul.ds.dag.DAGEdge;
import qmul.ds.dag.DAGTuple;
import qmul.ds.dag.GroundableEdge;
import qmul.ds.dag.UtteredWord;
import qmul.ds.formula.AtomicFormula;
import qmul.ds.formula.DisjunctiveType;
import qmul.ds.formula.PredicateArgumentFormula;
import qmul.ds.formula.TTRField;
import qmul.ds.formula.TTRFormula;
import qmul.ds.formula.TTRRecordType;
import qmul.ds.formula.Variable;
import qmul.ds.tree.Node;
import qmul.ds.tree.label.Label;
import qmul.ds.tree.label.LabelFactory;

/**
 * This class is for the construction of an MDP state encoding from a set of
 * successful dialogues in a domain. Currently works by parsing the successful
 * dialogues, getting their final contexts, decomposing them, and constructing
 * the space of all super-types of those contexts, constituting the feature
 * space.
 * 
 * Much more on this later.
 * 
 * Now the first TWO positions in state vector are reserved, for pointed node
 * encoding, and floor status (0 if open, 1 if self has floor, 2 if the other).
 * 
 * The size of the state vector is now: 2 + goal_ttr_features.size() +
 * cur_turn_ttr_features.size();
 * 
 * @author Arash
 *
 */
public class TTRMDPStateEncoding implements DomainGenerator {

	protected static Logger logger = Logger.getLogger(TTRMDPStateEncoding.class);

	public static final String ENCODING_FILE_NAME = "encoding.enc";
	public static final String SLOT_VALUES_FILE_NAME = "slot-values";
	public static final String DOMAIN_FOLDER = "data/Domains/";
	public static final String GRAMMAR_FOLDER = "../dsttr/resource/";
	public static final String TRAINING_DIALOGUE_FILE_NAME = "training_dialogues";
	public static final String TESTING_DIALOGUE_FILE_NAME = "testing_dialogues";

	public boolean optimisticGrounding = true;

	/**
	 * Integer encodings to use in the state encoding for various ds types and
	 * associated requirements.
	 * 
	 */
	public static final Map<Label, Integer> DS_TYPE_ENCODINGS;
	static {
		Map<Label, Integer> map = new HashMap<Label, Integer>();

		map.put(LabelFactory.create("?Ty(t)"), 0);
		map.put(LabelFactory.create("Ty(t)"), 1);

		map.put(LabelFactory.create("?Ty(e)"), 2);
		map.put(LabelFactory.create("Ty(e)"), 3);

		map.put(LabelFactory.create("?Ty(e>t)"), 4);
		map.put(LabelFactory.create("Ty(e>t)"), 5);

		map.put(LabelFactory.create("?Ty(cn)"), 6);
		map.put(LabelFactory.create("Ty(cn)"), 7);

		map.put(LabelFactory.create("?Ty(cn>e)"), 8);
		map.put(LabelFactory.create("Ty(cn>e)"), 9);

		map.put(LabelFactory.create("?Ty(e>cn)"), 10);
		map.put(LabelFactory.create("Ty(e>cn)"), 11);

		DS_TYPE_ENCODINGS = Collections.unmodifiableMap(map);
	}

	public static final int INDEX_OF_FLOOR_ENCODING = 1;// TODO: not yet being
														// used properly.
	public static final int INDEX_OF_POINTED_TYPE_ENCODING = 0;// TODO: not yet
																// being used
																// properly.

	InteractiveContextParser parser;

	protected String domainName = null;

	
	/**
	 * Semantic features of the goal, i.e. the grounded content, that are to be
	 * tracked by the mdp
	 */
	protected List<TTRRecordType> goal_features = null;
	/**
	 * Semantic features of the ongoing, current turn that are to be tracked by
	 * the mdp
	 */

	protected List<TTRRecordType> pending_utt_features = null;

	protected List<String> slot_values = null;

	protected Lexicon lexicon;

	/**
	 * a set of lists of integers. Each list encodes a goal state, by
	 * enumerating the indeces of the features which should be on (i.e. = 1) in
	 * that goal state.
	 * 
	 */
	protected Set<SortedSet<Integer>> goal_states = null;

	static List<TTRRecordType> priorityTemplates = new ArrayList<TTRRecordType>();

	public TTRMDPStateEncoding(String domain, String grammarResource) throws IOException {
		initPriorityTemplates();
		loadDomainEncoding(domain);
		if (grammarResource != null)
			this.lexicon = new Lexicon(new qmul.ds.action.Lexicon(GRAMMAR_FOLDER + grammarResource));
		else
			this.lexicon = null;

	}

	
	public UnmodifiableEncoding unmodifiableEncoding()
	{
		return new UnmodifiableEncoding(this.pending_utt_features, this.goal_features, this.domainName, this.lexicon, this.slot_values, this.optimisticGrounding);
	
	}
	public void initPriorityTemplates() {
		priorityTemplates.add(TTRRecordType
				.parse("[e1:es|e2:es|x1:e|x2:e|p1==subj(e1,x1):t|p2==obj(e1,x2):t|p3==ind_obj(e1, e2):t]"));
		priorityTemplates.add(TTRRecordType.parse("[e1:es|x1:e|x2:e|p1==subj(e1,x1):t|p2==obj(e1,x2):t]"));
		priorityTemplates.add(TTRRecordType.parse("[e1:es|x1:e|p1==subj(e1,x1):t]"));

	}

	/**
	 * Decomposes this record type into its constituent record types, R_1..R_N
	 * such that the constinuents have minimal commonality, and, that, R1 ^ R2 ^
	 * .. ^ R_N = @this
	 * 
	 * 
	 * @return
	 */
	public List<TTRRecordType> decompose(TTRRecordType r) {

		List<TTRRecordType> decomposedList = new ArrayList<TTRRecordType>();

		TTRRecordType sorted = r.sortFieldsBySpecificity();

		for (int i = sorted.getFields().size() - 1; i >= 0; i--) {
			TTRField f = sorted.getFields().get(i);
			if (f.getType() != null)
				decomposedList.add(r.getMinimalSuperTypeWith(f));
		}

		return decomposedList;

	}

	/**
	 * @deprecated
	 * @param r
	 * @return
	 */
	public List<TTRRecordType> priorityDecomposition(TTRRecordType r) {

		List<TTRRecordType> decomposedList = new ArrayList<TTRRecordType>();
		TTRRecordType copy = new TTRRecordType(r);
		for (TTRRecordType p : this.priorityTemplates) {
			HashMap<Variable, Variable> map = new HashMap<Variable, Variable>();

			if (p.subsumesMapped(copy, map)) {
				TTRRecordType superType = new TTRRecordType();
				for (Variable v : map.keySet()) {
					TTRField corresponding = copy.getField(map.get(v));

					TTRField newF = new TTRField(corresponding);
					if (corresponding.getVariables().isEmpty())
						newF.setType(null);
					else
						copy.remove(map.get(v));

					superType.add(newF);
				}
				decomposedList.add(superType);
			}
			// else
			// System.out.println("Didn't get subsumption for priority:"+p);
		}
		List<TTRRecordType> rest = copy.decompose();
		decomposedList.addAll(rest);

		return decomposedList;

	}

	/**
	 * Loads the encoding for 'domain' from disk.
	 * 
	 * @param path
	 *            to the domain folder.
	 * @throws IOException
	 *             if the encoding.enc file doesn't exist.
	 */
	public TTRMDPStateEncoding(String domain) throws IOException {
		loadDomainEncoding(domain);
		this.lexicon = null;
		initPriorityTemplates();
	}

	/**
	 * Loads the encoding for 'domain' from disk. And initilises the lexicon to
	 * lex.
	 * 
	 * @param domain
	 *            - path to domain folder
	 * @param lex
	 *            - the lexicon
	 * @throws IOException
	 */
	public TTRMDPStateEncoding(String domain, qmul.ds.action.Lexicon lex) throws IOException {
		initPriorityTemplates();
		loadDomainEncoding(domain);
		if (lex != null)
			this.lexicon = new Lexicon(lex);
		else
			this.lexicon = null;

	}

	public String prettyPrintState(List<Integer> s)
	{
		String result = "[T ,F|";
		result+="0";
		for (int i = 1; i < this.goal_features.size(); i++) {

			result += "," + i;

		}
		result += "|"+"0";
		for (int i = 2 + this.goal_features.size()+1; i < this.goal_features.size() + this.pending_utt_features.size()
				+ 2; i++) {
			result += ","+(i-2-this.goal_features.size());
		}

		result += "]\n[" + s.get(0) + " ," + s.get(1) + "|"+s.get(2);

		for (int i = 3; i < this.goal_features.size() + 2; i++) {
			result += ",";
			String spaces="";
			String si=""+(i-2);
			for(int j=1;j<si.length();j++)
				spaces+=" ";
			
			result+=s.get(i);
			result+=spaces;
		}
		
		
		result += "|"+s.get(2 + this.goal_features.size());
		
		for (int i = 3 + this.goal_features.size(); i < this.goal_features.size() + this.pending_utt_features.size()
				+ 2; i++) {
			result += ",";
			String spaces="";
			String si=""+(i-2-this.goal_features.size());
			for(int j=1;j<si.length();j++)
				spaces+=" ";
			
			result+=s.get(i);
			result+=spaces;
		}

		return result+ "]";
		
	}
	/**
	 * 
	 * @return a readable string representing the encoded BURLAP state
	 */
	public String prettyPrintState(ContextState s) {
		String result = "[T ,F|";
		result+="0";
		for (int i = 1; i < this.goal_features.size(); i++) {

			result += "," + i;

		}
		result += "|"+"0";
		for (int i = 2 + this.goal_features.size()+1; i < this.goal_features.size() + this.pending_utt_features.size()
				+ 2; i++) {
			result += ","+(i-2-this.goal_features.size());
		}

		result += "]\n[" + s.get(0) + " ," + s.get(1) + "|"+s.get(2);

		for (int i = 3; i < this.goal_features.size() + 2; i++) {
			result += ",";
			String spaces="";
			String si=""+(i-2);
			for(int j=1;j<si.length();j++)
				spaces+=" ";
			
			result+=s.get(i);
			result+=spaces;
		}
		
		
		result += "|"+s.get(2 + this.goal_features.size());
		
		for (int i = 3 + this.goal_features.size(); i < this.goal_features.size() + this.pending_utt_features.size()
				+ 2; i++) {
			result += ",";
			String spaces="";
			String si=""+(i-2-this.goal_features.size());
			for(int j=1;j<si.length();j++)
				spaces+=" ";
			
			result+=s.get(i);
			result+=spaces;
		}

		return result+ "]";

	}

	/**
	 * 
	 * @param s the state to print
	 * @return string representation of the state vector, separating its three components.
	 */
	public String printState(ContextState s) {
		// List<Integer> stateVector=s.getStateVector();
		String result = "[" + s.get(0) + "," + s.get(1) + "|";

		for (int i = 2; i < this.goal_features.size() + 2; i++) {
			result += s.get(i) + ",";
		}
		result = result.substring(0, result.length() - 1);
		result += "|";
		for (int i = 2 + this.goal_features.size(); i < this.goal_features.size() + this.pending_utt_features.size()
				+ 2; i++) {
			result += s.get(i) + ",";
		}

		return result.substring(0, result.length() - 1) + "]";

	}

	/**
	 * An empty encoding. Use constructFromExampleDialogues(domainName,
	 * grammarResource) to construct an encoding.
	 */
	public TTRMDPStateEncoding() {
		initPriorityTemplates();
	}

	public String getDomainName() {
		return this.domainName;
	}

	/**
	 * loads domain from file. expects slot-values file, and encoding.enc files.
	 * 
	 * @param domain
	 */
	public void loadDomainEncoding(String domain) throws IOException {
		domainName = domain;
		goal_features = new ArrayList<TTRRecordType>();
		goal_states = new HashSet<SortedSet<Integer>>();
		slot_values = new ArrayList<String>();
		loadSlotValues();
		loadEncoding();
		ContextState.initKeys(this.pending_utt_features.size() + goal_features.size() + 2);
	}

	/**
	 * Constructs encoding from example dialogues. expects 'slot-values' file,
	 * and 'dialogues' file. use writeEncodingToFile method to write to file.
	 */
	public void constructFromExampleDialogues(String domainName, String grammarResource) throws IOException {
		this.domainName = domainName;
		goal_features = new ArrayList<TTRRecordType>();
		pending_utt_features = new ArrayList<TTRRecordType>();
		goal_states = new HashSet<SortedSet<Integer>>();
		slot_values = new ArrayList<String>();
		loadSlotValues();
		parseAndEncodeDomainDialogues(domainName, grammarResource);

	}

	public void constructFromLoadedDialogues(List<Dialogue> inDialogues, String domainName, String grammarResource)
			throws IOException {
		this.domainName = domainName;
		goal_features = new ArrayList<TTRRecordType>();
		pending_utt_features = new ArrayList<TTRRecordType>();
		goal_states = new HashSet<SortedSet<Integer>>();
		slot_values = new ArrayList<String>();
		loadSlotValues();
		parseAndEncodeLoadedDialogues(inDialogues, domainName, grammarResource);

	}

	public static Map<String, List<String>> loadSlotValuesOfDomain(String domain) throws IOException {
		logger.info("loading slot values for domain " + domain);
		BufferedReader reader = new BufferedReader(new FileReader(
				TTRMDPStateEncoding.DOMAIN_FOLDER + domain + "/" + TTRMDPStateEncoding.SLOT_VALUES_FILE_NAME));

		Map<String, List<String>> result = new HashMap<String, List<String>>();

		String line;
		while ((line = reader.readLine()) != null) {
			if (line.trim().isEmpty())
				continue;
			else if (line.trim().startsWith("//"))
				continue;

			String[] varValue = line.split(":");
			if (varValue.length != 2 || varValue[1].isEmpty()) {
				reader.close();
				throw new IllegalStateException("Bad line in slots file: " + line);
			}

			String[] values = varValue[1].split(",");
			List<String> valuesList = new ArrayList<String>();
			for (String v : values)
				valuesList.add(v.trim());

			result.put(varValue[0].trim(), valuesList);
		}

		reader.close();
		logger.info("loaded slot values:" + result);
		return result;
	}

	protected void loadSlotValues() throws IOException {
		// values should be separated by commas
		slot_values = new ArrayList<String>();
		Map<String, List<String>> values = loadSlotValuesOfDomain(domainName);
		for (List<String> sValues : values.values()) {
			slot_values.addAll(sValues);
		}

	}

	/**
	 * Abstracts out the slot values according to slot_values - replaces them by
	 * the right kinds of meta-variable. (PredicateMetaVariable or
	 * FormulaMetavariable)
	 * 
	 * @param rec
	 * @return new record type with slot values abstracted
	 */
	public TTRRecordType abstractOutSlotValues(TTRRecordType rec) {

		TTRRecordType result = new TTRRecordType();

		for (TTRField f : rec.getFields()) {
			TTRField newF = new TTRField(f);
			if (f.getType() != null && f.getType() instanceof AtomicFormula) {
				AtomicFormula af = (AtomicFormula) f.getType();

				if (slot_values.contains(af.getName())) {

					newF.setType(result.getFreshAtomicMetaVariable());
				}

			} else if (f.getType() != null && f.getType() instanceof PredicateArgumentFormula) {
				PredicateArgumentFormula paf = (PredicateArgumentFormula) f.getType();
				if (slot_values.contains(paf.getPredicate().getName()))
					newF.setType(
							new PredicateArgumentFormula(result.getFreshPredicateMetaVariable(), paf.getArguments()));

			}

			result.add(newF);
		}

		return result;

	}

	private static final String GOAL_BLOCK = "<begin-goals>";
	private static final String GOAL_FEATURES_BLOCK = "<begin-goal-features>";
	private static final String GOAL_DOMAIN_BLOCK = "<begin-goal-domain>";
	private static final String CUR_TURN_FEATURES_BLOCK = "<begin-cur-turn-features>";
	private static final String CUR_TURN_DOMAIN_BLOCK = "<begin-cur-turn-domain>";
	
	private static final String FEATURE_NUMBER_SEP="-->";

	/**
	 * load encoding from file. File will contain all the final context record
	 * types, one on each line.
	 * 
	 * 
	 * TODO
	 * 
	 * @param fileName
	 */
	private void loadEncoding() throws IOException {
		logger.info("loading domain encoding:" + this.domainName);
		this.goal_features = new ArrayList<TTRRecordType>();
		this.pending_utt_features = new ArrayList<TTRRecordType>();
		this.goal_states = new HashSet<SortedSet<Integer>>();
		String line;
		BufferedReader reader = new BufferedReader(
				new FileReader(DOMAIN_FOLDER + this.domainName + "/" + ENCODING_FILE_NAME));
		String block = null;
		while ((line = reader.readLine()) != null) {
			if (line.trim().isEmpty())
				continue;
			else if (line.trim().startsWith("//"))
				continue;

			if (line.contains(GOAL_FEATURES_BLOCK)) {
				block = GOAL_FEATURES_BLOCK;
				continue;
			} else if (line.contains(GOAL_DOMAIN_BLOCK)) {
				block = GOAL_DOMAIN_BLOCK;
				continue;
			} else if (line.contains(GOAL_BLOCK)) {
				block = GOAL_BLOCK;
				continue;

			} else if (line.contains(CUR_TURN_DOMAIN_BLOCK)) {
				block = CUR_TURN_DOMAIN_BLOCK;
				continue;
			} else if (line.contains(CUR_TURN_FEATURES_BLOCK)) {
				block = CUR_TURN_FEATURES_BLOCK;
				continue;

			}
			
			
			if (block.equals(GOAL_FEATURES_BLOCK)) {
				line=line.trim();
				line=line.split(FEATURE_NUMBER_SEP)[1];
				
				TTRRecordType cur = TTRRecordType.parse(line.trim());
				goal_features.add(cur);
				logger.info("Adding feature:" + cur);

			} else if (block.equals(GOAL_BLOCK)) {
				SortedSet<Integer> goal = this.getIntegerList(line.trim());
				goal_states.add(goal);
				logger.info("Adding goal:" + goal);

			} else if (block.equals(GOAL_DOMAIN_BLOCK)) {
				// TTRRecordType cur = TTRRecordType.parse(line.trim());
				// this.goal_encoding_domain = cur;
				// logger.info("Read Goal Domain:"+cur);

			} else if (block.equals(CUR_TURN_DOMAIN_BLOCK)) {
				// TTRRecordType cur = TTRRecordType.parse(line.trim());
				// this.cur_turn_encoding_domain = cur;
				// logger.info("Read Current Turn Domain:"+cur);

			} else if (block.equals(CUR_TURN_FEATURES_BLOCK)) {
				line=line.trim();
				line=line.split(FEATURE_NUMBER_SEP)[1];
				TTRRecordType cur = TTRRecordType.parse(line.trim());
				pending_utt_features.add(cur);
				logger.info("Adding feature:" + cur);

			} else
				throw new IllegalStateException();

		}

		reader.close();

	}

	public void loadParser(String resourceDir) {
		parser = new InteractiveContextParser(resourceDir);

	}

	/**
	 * no mcs here. adds features independently. Just decomposes and adds the
	 * rec types. This is a looser notion of goal..... very unprincipled at this
	 * minute!
	 * 
	 * @param context
	 */
	protected void addAsGoal(TTRRecordType context) {

		logger.trace("Adding " + context);
		TTRRecordType successContext = abstractOutSlotValues(context);
		logger.trace("Abstracted " + successContext);
		List<TTRRecordType> decomposition = successContext.decompose();

		SortedSet<Integer> goalIndeces = new TreeSet<Integer>();
		// int initSize=this.goal_ttr_features.size();
		int newCount = 0;
		List<TTRRecordType> newFeatures = new ArrayList<TTRRecordType>();
		decompLoop: for (int i = 0; i < decomposition.size(); i++) {
			TTRRecordType newFeature = decomposition.get(i);

			for (int j = 0; j < goal_features.size(); j++) {
				TTRRecordType existingFeature = goal_features.get(j);
				newFeature.resetMetas();
				existingFeature.resetMetas();
				if ((!newFeature.getMetas().isEmpty() && existingFeature.getMetas().isEmpty())
						|| (newFeature.getMetas().isEmpty() && !existingFeature.getMetas().isEmpty()))
					continue;
				else if (existingFeature.subsumes(newFeature)) {
					logger.trace("Matched existing feature:" + existingFeature);
					// if (j>=initSize)
					goalIndeces.add(j + 2);// first and second indeces reserved
											// for pointed type and floor status
					continue decompLoop;
				} else
					logger.trace(existingFeature + " didn't subsume " + newFeature);

			}
			logger.trace("NO MATCH FOR:" + newFeature);
			logger.trace("ADDING AT END");
			newCount++;
			newFeatures.add(newFeature);
			goal_features.add(newFeature);
			goalIndeces.add(goal_features.size() + 1);// first and second
														// indeces
														// reserved for pointed
														// type and floor status

		}

		this.goal_states.add(goalIndeces);
		logger.trace("Added Goal:" + goalIndeces);
		logger.info("There were " + newCount + " new goal features in this dialogue.");
		for (TTRRecordType f : newFeatures) {
			logger.info(f);
		}

	}

	protected <T extends DAGTuple, E extends DAGEdge> void addSuccessContext(Context<T, E> context) {
		// TreeSet<AustinianProp> asserted = context.getAllAssertions();

		TTRRecordType grounded = optimisticGrounding ? (TTRRecordType) context.getCautiouslyOptimisticGroundedContent()
				: (TTRRecordType) context.getGroundedContent();

		List<TTRRecordType> allClauses = context.getDAG().getAllClauseContents();
		addPendingUttFeatures(allClauses);

		addAsGoal(grounded);
	}

	private class RecTypeSpecificityComparator implements Comparator<TTRRecordType> {

		TTRRecordType subj = TTRRecordType.parse("[e1:es|x1:e|p1==subj(e1,x1):t]");
		TTRRecordType obj = TTRRecordType.parse("[e1:es|x1:e|p1==obj(e1,x1):t]");
		TTRRecordType ev_obj = TTRRecordType.parse("[e1:es|e2:es|p1==obj(e1,e2):t]");

		TTRRecordType ind_obj = TTRRecordType.parse("[e1:es|e2:es|p1==ind_obj(e1,e2):t]");

		@Override
		public int compare(TTRRecordType o1, TTRRecordType o2) {
			if (o1.getFields().size() < o2.getFields().size())
				return +1;
			else if (o1.getFields().size() > o2.getFields().size())
				return -1;
			else {
				if (subj.subsumes(o1))
					return -1;
				else if (subj.subsumes(o2))
					return +1;

				if (obj.subsumes(o1))
					return -1;
				else if (obj.subsumes(o2))
					return +1;

				if (ev_obj.subsumes(o1))
					return -1;
				else if (ev_obj.subsumes(o2))
					return +1;

				if (ind_obj.subsumes(o1))
					return -1;
				else if (ind_obj.subsumes(o2))
					return +1;

				return 0;
			}
		}

	}

	private void resetMetas(List<TTRRecordType> list) {
		for (TTRRecordType r : list)
			r.resetMetas();
	}

	private List<TTRRecordType> abstractOutSlotValues(List<TTRRecordType> list) {
		List<TTRRecordType> result = new ArrayList<TTRRecordType>();
		for (TTRRecordType r : list)
			result.add(this.abstractOutSlotValues(r));

		return result;
	}

	protected void addPendingUttFeatures(List<TTRRecordType> clauses) {

		// List<TTRRecordType> abstracted=abstractOutSlotValues(clauses);
		Iterator<TTRRecordType> iter = clauses.iterator();

//		List<TTRRecordType> initial = iter.next().decompose();
//
//		if (this.pending_utt_features.isEmpty()) 
//		{
//			this.pending_utt_features=initial;
//		
//			Collections.sort(pending_utt_features, new RecTypeSpecificityComparator());
//		}

		while (iter.hasNext()) {
			
			TTRRecordType next = iter.next();
			next=this.abstractOutSlotValues(next);
			decomposeAndConjoinMinimallyInto(next, pending_utt_features);
			Collections.sort(pending_utt_features, new RecTypeSpecificityComparator());

		}
//		List<TTRRecordType> abstracted = new ArrayList<TTRRecordType>();
//		for (TTRRecordType ttr : pending_utt_features) {
//			abstracted.add(this.abstractOutSlotValues(ttr));
//		}
//		pending_utt_features = abstracted;

	}
	
	protected void decomposeAndConjoinMinimallyInto(TTRRecordType r, List<TTRRecordType> recs) {
		logger.trace("Conjoining rectype:" + r);
		List<TTRRecordType> decomposition = r.decompose();
		HashMap<Variable, Variable> map = new HashMap<Variable, Variable>();

		List<TTRRecordType> newFeatures = new ArrayList<TTRRecordType>();
		component: for (TTRRecordType component : decomposition) {
			logger.trace("testing component:" + component);
			for (TTRRecordType rec : recs) {
				// HashMap<Variable, Variable> copy=new HashMap<Variable,
				// Variable>(map);
				logger.trace("against existing:" + rec);
				component.resetMetas();
				rec.resetMetas();
				if (component.subsumesMapped(rec, map)) {
					logger.trace("success");
					continue component;
				} else {
					logger.trace("failed");
					logger.trace("with map:" + map);
				}

			}
			// if we are here, component didn't subsume any of the existing rec
			// types in recs
			// add it to new features, later to be relabelled and added
			newFeatures.add(component);

		}

		for (TTRRecordType newFeature : newFeatures) {
			TTRRecordType relabelled = newFeature.relabel(map);
			recs.add(relabelled);
			logger.info("added new feature:"+relabelled);
		}
		
		//logger.info("There were "+newFeatures.size()+" new pending utt features in this dialogue");

	}

	public void printCurTurnFeatures() {
		for (TTRRecordType rt : this.pending_utt_features) {
			System.out.println(rt);
		}
	}
	

	public boolean addSuccessfulDialgoue(Dialogue d) {

		if (parser == null)
			throw new IllegalStateException("Parser not initialised");

		Context<DAGTuple, GroundableEdge> context = parser.parseDialogue(d);
		if (context == null)
			return false;

		addSuccessContext(context);

		return true;

	}

	public <T extends DAGTuple, E extends DAGEdge> ContextState encodeToBurlapState(Context<T, E> c) {
		Node pointed = c.getCurrentTuple().getTree().getPointedNode();
		Label typeLabel = pointed.getTypeLabel();

		Label type = typeLabel != null ? typeLabel : pointed.getTypeRequirement();

		// String address=pointed.getAddress().getAddress();
		return new ContextState(encode(c), type.toString());
	}

	public void testEncodeDialogues(String grammar) throws IOException {

		List<Dialogue> dialogues = Dialogue.loadDialoguesFromFile(DOMAIN_FOLDER + domainName + "/"+TESTING_DIALOGUE_FILE_NAME);
		this.parser = new InteractiveContextParser(GRAMMAR_FOLDER + grammar);
		for (Dialogue d : dialogues) {
			System.out.println("ParseEndocing Dialogue:"+d);
			parser.init(d.getParticiapnts());
			for (Utterance next : d) {
				System.out.println("Parse encoding: " + next);
				for (UtteredWord w : next.getWords()) {
					if (parser.parseWord(w) == null) {
						System.out.println("Parsing Error on: " + next);
						System.out.println("On word: " + w);
						System.out.println("Ignoring rest of dialogue");
						return;
					}
					System.out.println("After " + w);
					System.out.println("State=\n" + this.prettyPrintState(this.encodeToBurlapState(parser.getContext())));

				}

			}
			System.out.println("done.--------------------------------");

		}

	}

	/**
	 * This maps the current DS context of the dialogue to a state
	 * representation according to @this state encoding.
	 * 
	 * 
	 * The return type will be dependent on whether the state is continuous or
	 * not. For now, just a list of integers.... later we could incorporate
	 * feature probabilities for when there is ASR/syntactic/semantic
	 * uncertainty in the input.
	 * 
	 * The returned vector will be of this size: 2 + goal_ttr_features.size() +
	 * cur_turn_ttr_features.size() , where the first vector position (index 0)
	 * encodes the DS type of the pointed node on the right most tree in the
	 * context (according to @DS_TYPE_MAP), and the second (index 1) encodes the
	 * floor state
	 */
	public <T extends DAGTuple, E extends DAGEdge> List<Integer> encode(Context<T, E> c) {
		TTRRecordType g = c.getCautiouslyOptimisticGroundedContent();
		TTRFormula curFormula = c.getPendingContent();
		logger.trace("Pending Content: " + curFormula);
		logger.trace("Grounded Content: " + g);
		TTRRecordType cur;
		if (curFormula instanceof DisjunctiveType) {
			cur = ((DisjunctiveType) curFormula).evaluate();
		} else
			cur = (TTRRecordType) curFormula;

		List<Integer> state = new ArrayList<Integer>();

		// first encode the pointed node (use hash to encode both the type label
		// and the address of the pointed node.
		// currently only encoding pointed node type. including address leads to
		// bad generalisation properties.

		state.add(hash(c.getCurrentTuple().getTree().getPointedNode()));

		// then encode the floor status

		state.add(c.floorStatus());

		// then encode the grounded content
		// resetAllMetas();

		// TTRRecordType MCS =
		// goal_encoding_domain.mostSpecificCommonSuperType(grounded, new
		// HashMap<Variable, Variable>());

		logger.trace("Encoding Goal Features...");
		for (int i = 0; i < goal_features.size(); i++) {
			goal_features.get(i).resetMetas();
			if (goal_features.get(i).subsumes(g)) {

				logger.trace(goal_features.get(i) + "->1");
				state.add(1);
			} else {
				logger.trace(goal_features.get(i) + "->0");
				state.add(0);
			}
			goal_features.get(i).resetMetas();

		}

		// resetAllMetas();

		logger.trace("Encoding Cur Clause Features...");

		HashMap<Variable, Variable> map = new HashMap<Variable, Variable>();
		logger.trace(cur);
		for (TTRRecordType feature : this.pending_utt_features) {
			feature.resetMetas();
			logger.trace(feature + "->");
			if (feature.subsumesMapped(cur, map)) {
				logger.trace(feature + "->1");
				state.add(1);
			} else {
				logger.trace(feature + "->0");
				state.add(0);
			}
			feature.resetMetas();
			logger.trace("map is:" + map);
		}

		return state;
	}

	public void resetAllMetas() {
		for (TTRRecordType rt : this.goal_features) {
			rt.resetMetas();
		}
		// this.goal_encoding_domain.resetMetas();

		for (TTRRecordType rt : this.pending_utt_features) {
			rt.resetMetas();
		}
		// this.cur_turn_encoding_domain.resetMetas();

	}

	/**
	 * This will write the encoding domain (conjunction of all success contexts)
	 * and the goal states to file.
	 * 
	 * @param fileName
	 */
	public void writeEncodingToFile() throws IOException {
		BufferedWriter writer = new BufferedWriter(
				new FileWriter(DOMAIN_FOLDER + domainName + "/" + ENCODING_FILE_NAME));

		// writer.write(GOAL_DOMAIN_BLOCK);
		// writer.newLine();
		// writer.write(goal_encoding_domain.toString());

		// writer.newLine();
		// writer.newLine();
		// -------

		writer.write(GOAL_FEATURES_BLOCK);
		writer.newLine();

		for (int i = 0; i < this.goal_features.size(); i++) {
			goal_features.get(i).resetMetas();
			writer.write(i+FEATURE_NUMBER_SEP);
			writer.write(goal_features.get(i).toString());
			writer.newLine();
		}

		// writer.newLine();
		// writer.write(CUR_TURN_DOMAIN_BLOCK);
		// writer.newLine();
		// writer.write(cur_turn_encoding_domain.toString());
		// writer.newLine();

		writer.newLine();
		writer.write(CUR_TURN_FEATURES_BLOCK);
		writer.newLine();
		for (int i = 0; i < this.pending_utt_features.size(); i++) {
			pending_utt_features.get(i).resetMetas();
			writer.write(i+FEATURE_NUMBER_SEP+pending_utt_features.get(i).toString());
			writer.newLine();
		}

		writer.newLine();
		writer.write(GOAL_BLOCK);
		writer.newLine();
		for (Set<Integer> goalIndeces : goal_states) {
			writer.write(goalIndeces.toString());
			writer.newLine();
		}

		writer.close();

	}

	public String toString() {
		// String result = "encoding domain:" +
		// this.goal_encoding_domain.toString() + "\n";
		String result = "RT features:\n";
		for (int i = 0; i < goal_features.size(); i++) {
			TTRRecordType feature = goal_features.get(i);
			result += feature;
			result += ",\n";

		}
		result += "Goal states:\n";
		for (Set<Integer> goal : goal_states)
			result += goal + "\n";
		return result;
	}

	protected void parseAndEncodeDomainDialogues(String domainName, String grammarFolder) throws IOException {
		List<Dialogue> dialogues = Dialogue.loadDialoguesFromFile(DOMAIN_FOLDER + domainName + "/"+TRAINING_DIALOGUE_FILE_NAME);
		parseAndEncodeLoadedDialogues(dialogues, domainName, grammarFolder);
	}

	protected void parseAndEncodeLoadedDialogues(List<Dialogue> inDialogues, String domainName, String grammarFolder)
			throws IOException {
		// this.goal_encoding_domain = new TTRRecordType();
		this.parser = new InteractiveContextParser(GRAMMAR_FOLDER + grammarFolder);
		for (Dialogue d : inDialogues) {
			logger.info("Parsing and Adding:");
			System.out.println(d);
			if (!addSuccessfulDialgoue(d)) {

				logger.error("Could not parse dialogue:");
				logger.error(d);
			}

			logger.info("------------------------- done.");
		}

	}

	/**
	 * creates a list of integers from [1, 2, 34, 46]
	 * 
	 * @param s
	 * @return
	 */
	private SortedSet<Integer> getIntegerList(String s) {
		SortedSet<Integer> result = new TreeSet<Integer>();
		String sub = s.trim();
		sub = sub.substring(1, sub.length() - 1);
		String[] split = sub.split(",");
		for (String integer : split) {
			result.add(Integer.parseInt(integer.trim()));
		}
		return result;

	}

	public Set<SortedSet<Integer>> getGoalStates() {
		return this.goal_states;
	}

	public List<String> getSlotValues() {
		return this.slot_values;
	}

	public int getPointedTypeIndex() {

		return 0;
	}

	/**
	 * Checks to see if c is a goal state according to this encoding. Not only
	 * should it have the required grounded content according to the
	 * goal_states, but, we also require that the floor be open at the end of
	 * the dialogue.
	 * 
	 * @param c
	 * @return
	 */
	public boolean isGoalState(ContextState c) {

		for (Set<Integer> goal : goal_states) {
			if (matches(c, goal) && c.getStateVector().get(INDEX_OF_FLOOR_ENCODING) == 0)
				return true;

		}
		return false;
	}

	/**
	 * 
	 * @param c
	 * @param goal
	 * @return
	 */

	private boolean matches(ContextState c, Set<Integer> goal) {

		List<Integer> state = c.getStateVector();
		for (Integer index : goal) {
			if (state.get(index) != 1)
				return false;
		}

		return true;
	}

	public ContextState getInitialState() {
		List<Integer> state = new ArrayList<Integer>();
		for (int i = 0; i < this.goal_features.size() + this.pending_utt_features.size() + 2; i++) {
			state.add(0);

		}

		return new ContextState(state, "0", "?Ty(t)");

	}

	public ContextState getUnsuccessfulTerminalState() {
		List<Integer> state = new ArrayList<Integer>();
		state.add(-1);
		for (int i = 1; i < this.goal_features.size() + this.pending_utt_features.size() + 2; i++) {
			state.add(0);

		}

		return new ContextState(state, null, null);

	}

	public boolean isUnsuccessfulTerminal(ContextState c) {
		return c.get(this.getPointedTypeIndex()) == -1;
	}

	@Override
	public SADomain generateDomain() {
		SADomain dialogueDomain = new SADomain();
		for (String word : lexicon)
			dialogueDomain.addActionType(new UniversalActionType(word));

		dialogueDomain.addActionType(new UniversalActionType(DSDialogueAgent.RELEASE_TURN));
		dialogueDomain.addActionType(new UniversalActionType(DSDialogueAgent.WAIT));

		return dialogueDomain;
	}

	/**
	 * hashes a DS Tree node - for now just the type of the pointed node other
	 * versions could also hash the address of the pointed node. (now commented
	 * out) DS_TYPE_ENCODINGS. The result is used as the first element in the
	 * state vector encoded according to this encoding.
	 * 
	 * @param node
	 * @return An integer hash for the node.
	 */
	public static int hash(Node node) {
		Label typeLabel = node.getTypeLabel();

		Label type = typeLabel != null ? typeLabel : node.getTypeRequirement();

		String address = node.getAddress().getAddress();

		// first hash the address.
		// NOT HASHING THE NODE ADDRESS. It's not right. NO GENERALISATION
		// int hash=hashDSNodeAddress(address);
		int hash = 0;

		// now hash the type according to above, DS_TYPE_ENCODINGS, just adding
		// that value.

		if (DS_TYPE_ENCODINGS.containsKey(type)) {
			hash += DS_TYPE_ENCODINGS.get(type);

		} else {
			logger.error("DS type not in map. Adding integer corresponding to 'other' type");
			hash += DS_TYPE_ENCODINGS.size();
		}

		return hash;
	}

	private static int hashDSNodeAddress(String address) {

		int hash = 0;
		// first hash the address.
		for (int i = 0; i < address.length(); i++)
			hash += i * address.charAt(i);

		return hash;

	}

	public void setDomain(String domain) {
		this.domainName = domain;
	}

	

	public static BitSet stateToBitSet(List<Integer> state) {
		BitSet result = new BitSet(state.size());
		for (int index = 0; index != state.size(); ++index) {
			int value = state.get(index);
			assert 0 <= value && value <= 1 : "Called stateToBitSet with non-binary state vector";
			if (state.get(index) != 0) {
				result.set(index);
			}
		}
		return result;
	}

	public List<Integer> getGoalSubstate(ContextState state) {
		assert 2 + goal_features.size() <= state.getStateVector().size();
		return state.getStateVector().subList(2, 2 + goal_features.size());
	}

	public List<Integer> getGoalSubstate(List<Integer> state) {
		assert 2 + goal_features.size() <= state.size();
		return state.subList(2, 2 + goal_features.size());
	}

	public List<Integer> getPendingSubstate(ContextState state) {
		assert 2 + goal_features.size() + pending_utt_features.size() <= state.getStateVector().size();
		return state.getStateVector().subList(2 + goal_features.size(),
				2 + goal_features.size() + pending_utt_features.size());
	}

	public List<Integer> getPendingSubstate(List<Integer> state) {
		assert 2 + goal_features.size() + pending_utt_features.size() <= state.size();
		return state.subList(2 + goal_features.size(), 2 + goal_features.size() + pending_utt_features.size());
	}

	public static void main(String s[]) {
		try {

			// -----------------------------------

			TTRMDPStateEncoding encoding = new TTRMDPStateEncoding();// "restaurant-search"
			//encoding.testEncodeDialogues("2017-english-ttr-restaurant-search-ca");

			encoding.constructFromExampleDialogues("restaurant-search-ca", "2017-english-ttr-restaurant-search-ca");
			 encoding.writeEncodingToFile();
			// encoding.testEncodeDialogues();

			// System.out.println(parser.getContext().getPendingContent());
			// List<Dialogue> dialogues =
			// Dialogue.loadDialoguesFromFile("data/Domains/restaurant-search/dialogues");
			// System.out.println(dialogues.get(0));
			// parser.parseDialogue(dialogues.get(0));
			//
			// TTRRecordType
			// allAssertions=(TTRRecordType)parser.getContext().getCautiouslyOptimisticGroundedContent();
			//
			// for(TTRField f: allAssertions.getFields())
			// {
			// System.out.println(f);
			//
			// }
			// System.out.println(allAssertions.getFields().size());

			// List<TTRRecordType> decomposed1=r1.decompose();
			// parser.parseUtterance(new Utterance("sys:can arash book a
			// table?"));
			// TTRRecordType
			// r2=(TTRRecordType)parser.getContext().getCurrentTuple().getSemantics(parser.getContext()).removeHead();
			//
			// List<TTRRecordType> decomposed2 = r2.decompose();
			//
			// System.out.println("Decomposed 1:"+decomposed1);
			// System.out.println("Decomposed 2:"+decomposed2);
			//
			// List<TTRRecordType> conjoinedInto=new
			// ArrayList<TTRRecordType>(decomposed1);
			// encoding.decomposeAndConjoinMinimallyInto(r2, conjoinedInto);
			// System.out.println("\nresult:");
			// for(TTRRecordType rec:conjoinedInto)
			// {
			// System.out.println(rec);
			//
			// }
			// TTRRecordType
			// coinjoined=encoding.conjoinAndCollapseIsomorphicSuperTypes(all_contents);

			// TTRRecordType content=TTRRecordType.parse("[x6==arash : e|e6 :
			// es|x9==dylan : e|x1 : e|p2==modal(e6) : t|"
			// + "p32==question(x1) : t|p27==today(e6) : t|p9==obj(e6, x9) :
			// t|p10==subj(e6, x6) : t|p21==with(e6, x1) : t]");
			// HashMap<Variable, Variable> map=new HashMap<Variable,
			// Variable>();
			// long before=new Date().getTime();
			// TTRRecordType
			// mcs=content.mostSpecificCommonSuperType(all_content, map);
			//
			// long after=new Date().getTime();
			//
			//
			// System.out.println("Content:"+content);
			// System.out.println("subsumed:"+content.subsumesMapped(all_content,
			// new HashMap<Variable, Variable>()));
			// System.out.println("MCS:"+mcs);
			// System.out.println(map);
			// System.out.println("MCS took:"+(after-before));
			//

			// TTRRecordType
			// abstracted=encoding.abstractOutSlotValues(all_content);
			//
			// List<TTRRecordType> all_features=abstracted.decompose();
			//
			// for(TTRRecordType ttr:all_features)
			// {
			// System.out.println(ttr);
			// }

			// TTRRecordType grounded = (TTRRecordType)
			// parser.getContext().getCautiouslyOptimisticGroundedContent();
			// HashMap<Variable, Variable> map = new HashMap<Variable,
			// Variable>();
			//
			// //grounded.collapseIsomorphicSuperTypes(map);
			//
			// TTRRecordType abstracted =
			// encoding.abstractOutSlotValues(grounded);
			//
			// for (TTRField field : ((TTRRecordType) abstracted).getFields()) {
			// System.out.println(field);
			// }

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
