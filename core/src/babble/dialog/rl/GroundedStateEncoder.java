package babble.dialog.rl;

import java.io.IOException;
import java.util.List;

import qmul.ds.Context;
import qmul.ds.action.Lexicon;
import qmul.ds.dag.DAGEdge;
import qmul.ds.dag.DAGTuple;
import qmul.ds.formula.TTRRecordType;

public class GroundedStateEncoder extends TTRMDPStateEncoding {

	GroundingFunction grounding_function;
	public GroundedStateEncoder(GroundingFunction f) {
		super();
		this.grounding_function=f;
	}

	

	public GroundedStateEncoder(String domain) throws IOException {
		super(domain);
		// TODO Auto-generated constructor stub
	}
	
	
	protected void addAsGoal(TTRRecordType context) {
		
		
	}
	
	protected <T extends DAGTuple, E extends DAGEdge> void addSuccessContext(Context<T, E> context) {
		// TreeSet<AustinianProp> asserted = context.getAllAssertions();

		TTRRecordType grounded = optimisticGrounding ? (TTRRecordType) context.getCautiouslyOptimisticGroundedContent()
				: (TTRRecordType) context.getGroundedContent();

		List<TTRRecordType> allClauses = context.getDAG().getAllClauseContents();
		addPendingUttFeatures(allClauses);

		//addAsGoal(grounded);//goal is known here. and Goal features are added by the constructor
	}
	
	
	public static void main(String[] s)
	{
		TTRRecordType test=TTRRecordType.parse("[cuisine==italien:e]");
		System.out.println(test);
	}
	

}
