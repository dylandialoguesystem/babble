package babble.dialog.rl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import burlap.behavior.policy.EpsilonGreedy;
import burlap.behavior.singleagent.learning.tdmethods.QLearning;
import burlap.behavior.singleagent.learning.tdmethods.QLearningStateNode;
import burlap.behavior.valuefunction.QValue;
import burlap.mdp.core.state.State;
import burlap.mdp.singleagent.SADomain;
import burlap.statehashing.HashableState;
import burlap.statehashing.HashableStateFactory;
/**
 * just added get method for Q Table to the BURLAP QLearning class. 
 * @author Arash
 *
 */
public class QLearner extends QLearning {

	public QLearner(SADomain domain, double gamma, HashableStateFactory hashingFactory, double qInit,
			double learningRate) {
		super(domain, gamma, hashingFactory, qInit, learningRate);
		
	}
	
	public QLearner(SADomain domain, double gamma, HashableStateFactory hashingFactory, double qInit,
			double learningRate, double explorationRate) {
		super(domain, gamma, hashingFactory, qInit, learningRate);
		setLearningPolicy(new EpsilonGreedy(this, explorationRate));
		
	}
	
	
	
	public Map<HashableState, QLearningStateNode> getQTable()
	{
		return this.qFunction;
	}
	
	public String printQTable(TTRMDPStateEncoding encoding)
	{
		String result="Q-Table -------------------------\n";
		
		for(HashableState state: getQTable().keySet())
		{
			ContextState c=(ContextState)state;
			QLearningStateNode node=getQTable().get(state);
			result+=encoding.printState(c)+"->{";
			List<QValue> sorted=new ArrayList<QValue>(node.qEntry);
			Collections.sort(sorted, new QValueComparator());
			for(QValue qv: sorted)
			{
				result+=qv.a+"->"+qv.q+", ";
			}
			result=result.substring(0, result.length()-2)+"}\n\n";
			
		}
		return result;
		
	}
	
	
	
	class QValueComparator implements Comparator<QValue>
	{

		@Override
		public int compare(QValue o1, QValue o2) {
			if (o1.q <o2.q)
				return +1;
			else if (o1.q>o2.q)
				return -1;
			else
				return 0;
		}
		
	}


	public boolean hasQValueFor(State s) {
		HashableState hashS=this.hashingFactory.hashState(s);
		return super.qFunction.containsKey(hashS);
		
	}

	public void setQTable(Map<HashableState, QLearningStateNode> qTable) {
		this.qFunction=qTable;
		
	}
	

}
