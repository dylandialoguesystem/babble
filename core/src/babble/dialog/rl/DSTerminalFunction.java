package babble.dialog.rl;

import burlap.mdp.core.TerminalFunction;
import burlap.mdp.core.state.State;

public class DSTerminalFunction implements TerminalFunction {

	
	TTRMDPStateEncoding encoding;
	public DSTerminalFunction(TTRMDPStateEncoding encoding)
	{
		
		this.encoding=encoding;
	}
	
	/**
	 * a {@link ContextState} is terminal if its pointed type encoding is -1, e.g. when a word cannot be parsed, or when a goal state
	 * is reached according to {@link TTRMDPStateEncoding}
	 */
	@Override
	public boolean isTerminal(State s) {
		if (!(s instanceof ContextState))
		{
			throw new IllegalArgumentException("can only accept instances of ContextState");
		}
		ContextState c=(ContextState)s;
		if (encoding.isUnsuccessfulTerminal(c))
			return true;
		
		
		return encoding.isGoalState(c);
		
		
		
	}
	
	

}
