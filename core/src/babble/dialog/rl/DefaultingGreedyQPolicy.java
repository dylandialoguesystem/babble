package babble.dialog.rl;

import burlap.behavior.policy.GreedyQPolicy;
import burlap.mdp.core.action.Action;
import burlap.mdp.core.state.State;

public class DefaultingGreedyQPolicy extends GreedyQPolicy {
	
	protected QLearner qlearner;
	protected Action defaultAction;
	
	public DefaultingGreedyQPolicy(QLearner qlearner, Action defaultAction)
	{
		super(qlearner);
		this.qlearner=qlearner;
		this.defaultAction=defaultAction;
	}
	
	public Action action(State s)
	{
		
		if (qlearner.hasQValueFor(s))
			return super.action(s);
		
		return defaultAction;
	}
	
	public double actionProb(State s, Action a)
	{
		if (qlearner.hasQValueFor(s))
			return super.actionProb(s, a);
		
		if (a.equals(defaultAction))
			return 1.0;
		else return 0.0;
	}

}
