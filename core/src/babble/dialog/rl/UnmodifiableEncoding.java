package babble.dialog.rl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import qmul.ds.Context;
import qmul.ds.dag.DAGEdge;
import qmul.ds.dag.DAGTuple;
import qmul.ds.formula.DisjunctiveType;
import qmul.ds.formula.TTRFormula;
import qmul.ds.formula.TTRRecordType;
import qmul.ds.formula.Variable;

public class UnmodifiableEncoding extends TTRMDPStateEncoding {
	
	final List<TTRRecordType> pending_utt_features;
	final List<TTRRecordType> goal_features;
	final List<String> slot_values;
	public UnmodifiableEncoding(List<TTRRecordType> pending_utt_features, List<TTRRecordType> goal_features,
			String domainName, Lexicon lexicon, List<String> slot_values, boolean optimisticGrounding) {
		this.pending_utt_features=Collections.unmodifiableList(pending_utt_features);
		this.goal_features=Collections.unmodifiableList(goal_features);
		super.domainName=domainName;
		super.lexicon=lexicon;
		this.slot_values=Collections.unmodifiableList(slot_values);
		super.optimisticGrounding=optimisticGrounding;
		
	}
	
	
	public <T extends DAGTuple, E extends DAGEdge> List<Integer> encode(Context<T, E> c) {
		TTRRecordType g = c.getCautiouslyOptimisticGroundedContent();
		TTRFormula curFormula = c.getPendingContent();
		logger.trace("Pending Content: " + curFormula);
		logger.trace("Grounded Content: " + g);
		TTRRecordType cur;
		if (curFormula instanceof DisjunctiveType) {
			cur = ((DisjunctiveType) curFormula).evaluate();
		} else
			cur = (TTRRecordType) curFormula;

		List<Integer> state = new ArrayList<Integer>();

		// first encode the pointed node (use hash to encode both the type label
		// and the address of the pointed node.
		// currently only encoding pointed node type. including address leads to
		// bad generalisation properties.

		state.add(hash(c.getCurrentTuple().getTree().getPointedNode()));

		// then encode the floor status

		state.add(c.floorStatus());

		// then encode the grounded content
		// resetAllMetas();

		// TTRRecordType MCS =
		// goal_encoding_domain.mostSpecificCommonSuperType(grounded, new
		// HashMap<Variable, Variable>());

		logger.trace("Encoding Goal Features...");
		for (int i = 0; i < goal_features.size(); i++) {
			goal_features.get(i).resetMetas();
			if (goal_features.get(i).subsumes(g)) {

				logger.trace(goal_features.get(i) + "->1");
				state.add(1);
			} else {
				logger.trace(goal_features.get(i) + "->0");
				state.add(0);
			}
			goal_features.get(i).resetMetas();

		}

		// resetAllMetas();

		logger.trace("Encoding Cur Clause Features...");

		HashMap<Variable, Variable> map = new HashMap<Variable, Variable>();
		logger.trace(cur);
		for (TTRRecordType feature : this.pending_utt_features) {
			feature.resetMetas();
			logger.trace(feature + "->");
			if (feature.subsumesMapped(cur, map)) {
				logger.trace(feature + "->1");
				state.add(1);
			} else {
				logger.trace(feature + "->0");
				state.add(0);
			}
			feature.resetMetas();
			logger.trace("map is:" + map);
		}

		return state;
	}

}
