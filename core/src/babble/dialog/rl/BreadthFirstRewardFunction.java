package babble.dialog.rl;

import java.util.List;

import burlap.mdp.core.action.Action;
import burlap.mdp.core.state.State;
import burlap.mdp.singleagent.model.RewardFunction;

public class BreadthFirstRewardFunction implements RewardFunction {

	TTRMDPStateEncoding encoding;
	public BreadthFirstRewardFunction(TTRMDPStateEncoding encoding)
	{
		this.encoding=encoding;
	}
	@Override
	public double reward(State s, Action a, State sprime) {

		if (!(s instanceof ContextState&&sprime instanceof ContextState))
			throw new IllegalArgumentException("Expected ContextState, got:"+s.getClass());
		
		ContextState before=(ContextState)s;
		ContextState after=(ContextState)sprime;
		if (encoding.isUnsuccessfulTerminal(after))
			return -100.00;
		
		
		List<Integer> goalBefore=encoding.getGoalSubstate(before);
		List<Integer> goalAfter=encoding.getGoalSubstate(after);
		
		int diff=getDiff(goalBefore, goalAfter);
		
		//only goal features we have turned on are rewarded.
		//reward is -1 if goal didn't change. This will lead to dialogue length minimisation.
		//or if we managed to turn off goal features (e.g. through erroneous repair??)
				
		return diff-1;
	}

	protected int getDiff(List<Integer> goalBefore, List<Integer> goalAfter)
	{
		assert goalBefore.size()==goalAfter.size();
		int diff=0;
		
		for(int i=0;i<goalBefore.size();i++)
		{
			diff+=goalAfter.get(i)-goalBefore.get(i);
		}
		return diff;
	}

}
