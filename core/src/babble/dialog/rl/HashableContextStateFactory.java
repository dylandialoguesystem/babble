package babble.dialog.rl;

import burlap.mdp.core.state.State;
import burlap.statehashing.HashableState;
import burlap.statehashing.HashableStateFactory;

public class HashableContextStateFactory implements HashableStateFactory {

	@Override
	public HashableState hashState(State s) {
		if (!(s instanceof ContextState))
			throw new IllegalArgumentException("Can only hash instanes of ContextState. Got:"+s.getClass());
		
		ContextState state=(ContextState)s;
		
		return state;
	}

}
