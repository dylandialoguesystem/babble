package babble.dialog.rl;

import java.util.List;

import burlap.mdp.core.action.Action;
import burlap.mdp.core.state.State;
import burlap.mdp.singleagent.model.RewardFunction;

public class GoalBasedRewardFunction implements RewardFunction {

	TTRMDPStateEncoding encoding;
	public GoalBasedRewardFunction(TTRMDPStateEncoding encoding)
	{
		this.encoding=encoding;
	}
	@Override
	public double reward(State s, Action a, State sprime) {

		if (!(s instanceof ContextState&&sprime instanceof ContextState))
			throw new IllegalArgumentException("Expected ContextState, got:"+s.getClass());
		
		//ContextState before=(ContextState)s;
		ContextState after=(ContextState)sprime;
		if (encoding.isUnsuccessfulTerminal(after))
			return -100.00;
		
		
		if (encoding.isGoalState(after))
			return +100;
				
		return +1;
	}

	protected int getDiff(List<Integer> goalBefore, List<Integer> goalAfter)
	{
		assert goalBefore.size()==goalAfter.size();
		int diff=0;
		
		for(int i=0;i<goalBefore.size();i++)
		{
			diff+=goalAfter.get(i)-goalBefore.get(i);
		}
		return diff;
	}

}
