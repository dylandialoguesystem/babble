package babble.dialog.rl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.yaml.snakeyaml.Yaml;

import burlap.mdp.core.state.State;
import burlap.mdp.core.state.UnknownKeyException;
import burlap.statehashing.HashableState;

/**
 * 
 * Burlap State corresponding to a DS context, as encoded by a
 * TTRMDPStateEncoding instance. The first position encodes the DS type of the
 * pointed node on right most tree in context, combined with the address of that node. These are hashed together
 * to give an integer - see {@link TTRMDPStateEncoding}. Then the first half of the rest
 * encodes the grounded content and the next half the current turn content All
 * according to a TTRMDPStatEncoding instance.
 * 
 * The two strings, typeLabel, and pointedNodeAddress are only kept here for pretty printing. Otherwise, the state
 * is fully specified by the stateVector field alone.
 * 
 * 
 * @author Arash
 *
 */

public class ContextState implements State, Serializable, HashableState {

	private static final long serialVersionUID = -7697931320684399550L;

	// this is the DS context encoded as a vector using record type features.
	
	private List<Integer> stateVector = new ArrayList<Integer>();

	private String pointedNodeAddress=null;
	private String typeLabel=null;
	public static List<Object> keys=new ArrayList<Object>();

	public ContextState(List<Integer> vector) {
		this.stateVector = vector;

	}

	public ContextState(List<Integer> vector, String nodeAddress, String typeLabel)
	{
		this(vector);
		this.typeLabel=typeLabel;
		this.pointedNodeAddress=nodeAddress;
		
	}
	public ContextState()
	{}

	public ContextState(List<Integer> encode, String type) {
		this(encode, null, type);
	}

	public String getPointedNodeAddress()
	{
		return pointedNodeAddress;
	}
	
	/**
	 * 0=open; 1=self; 2=other
	 * @param f
	 */
	public void setFloor(int f)
	{
		this.stateVector.set(1, f);
	}
	
	
	public String getTypeLabel()
	{
		return this.typeLabel;
	}
	
	public void setPointedNodeAddress(String address)
	{
		this.pointedNodeAddress=address;
	}
	
	public void setTypeLabel(String l)
	{
		this.typeLabel=l;
	}
	@Override
	public ContextState copy() {
		return new ContextState(new ArrayList<Integer>(stateVector));
	}

	@Override
	public Integer get(Object arg0) {

		if (!(arg0 instanceof Integer))
			throw new UnknownKeyException(arg0);

		Integer index = (Integer) arg0;
		if (index >= stateVector.size())
			throw new UnknownKeyException(arg0);

		return stateVector.get(index);
	}

	@Override
	public List<Object> variableKeys() {
		return keys;
	}

	/**
	 * The state space is constructed automatically depending on any specific
	 * instanceo of TTRMDPStateEncoding
	 * 
	 * @param length
	 */
	public static void initKeys(int length) {
		
		System.out.println("initialising "+length+ " keys in context state");
		for (int i = 0; i <= length; i++)
			keys.add(i);
	}

	public String toString() {
		return this.stateVector.toString();
	}

	public int hashCode() {
		return stateVector.hashCode();
	}

	public boolean equals(Object o) {
		if (!(o instanceof ContextState))
			return false;

		ContextState other = (ContextState) o;

		return stateVector.equals(other.stateVector);
	}

	



	@Override
	public State s() {
		return this;
	}



	public List<Integer> getStateVector() {
		return stateVector;
	}
	
	public void setStateVector(List<Integer> vector)
	{
		this.stateVector=vector;
	}
	
	public static void main(String a[])
	{
		List<Integer> list=Arrays.asList(1,2,3,4);
		ContextState c=new ContextState(list, "001", "?Ty(e>t)");
		Yaml y=new Yaml();
		System.out.println(y.dump(c));
	}

	public String toPrettyString() {
		
		String result="["+((this.pointedNodeAddress!=null)?this.pointedNodeAddress+":":"")+this.typeLabel+", ";
		for(int i=1;i<stateVector.size();i++)
			result+=stateVector.get(i)+", ";
			
		return result.substring(0, result.length()-2)+"]";
	}



	

}