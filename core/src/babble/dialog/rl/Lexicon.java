package babble.dialog.rl;

import java.util.ArrayList;

public class Lexicon extends ArrayList<String> {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3486254278289185428L;

	public Lexicon(qmul.ds.action.Lexicon l)
	{
		addAll(l.keySet());
	}
}
