/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package babble.dialog.chatinterface;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import org.apache.log4j.Logger;

/**
 *
 * @author Greg Mills and Arash Eshghi
 */
public class JTextPaneRightJustifiedDocument extends DefaultStyledDocument {

	protected Logger logger = Logger.getLogger(JTextPaneRightJustifiedDocument.class);

	public static final Toolkit kit=java.awt.Toolkit.getDefaultToolkit();
	
	public static final String[] delimitersArray = { ".", "?", "!", " "};
	public static final String[] sentDelimitersArray = { ".", "?", "!"};
	public static final List<String> delimiters = Arrays.asList(delimitersArray);
	
	public static final List<String> sentenceDelimiters=Arrays.asList(sentDelimitersArray);
	int maximumLength = 24;
	JTextPaneRightJustified jtprj;
	Object stateHolding = new Object();

	int fontSize = 40;

	public JTextPaneRightJustifiedDocument(JTextPaneRightJustified jtprj) {
		this.jtprj = jtprj;

		// jcfrms = new JChatFrameRightMostState(this);
		StyleContext context = new StyleContext();
		StyledDocument document = new DefaultStyledDocument(context);
		Style style = context.getStyle(StyleContext.DEFAULT_STYLE);
		StyleConstants.setAlignment(style, StyleConstants.ALIGN_RIGHT);
		StyleConstants.setFontSize(style, fontSize);
		style.addAttribute("created", new Date().getTime());
		style.addAttribute("originalforeground", StyleConstants.getForeground(style));
		this.setLogicalStyle(0, style);

	}

	public boolean hasFloor() {
		return this.jtprj.agent.hasFloor();
	}

	public boolean floorIsOpen() {
		return this.jtprj.agent.floorIsOpen();
	}

	

	public void insertStringFromOther(final String str, final Color otherColor, final String usernameOther) {
		final JTextPaneRightJustifiedDocument thisjtdoc = this;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					thisjtdoc.insertString_PresupposesOnSwingThread(str, otherColor);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}
	
	

	public void insertString_PresupposesOnSwingThread(final String str, final Color otherColor) {
		SimpleAttributeSet newAttrs = new SimpleAttributeSet();
		StyleConstants.setForeground(newAttrs, otherColor);
		StyleConstants.setBackground(newAttrs, Color.WHITE);
		StyleConstants.setFontSize(newAttrs, fontSize);
		StyleConstants.setFontFamily(newAttrs, Font.MONOSPACED);
		StyleConstants.setAlignment(newAttrs, StyleConstants.ALIGN_RIGHT);
		newAttrs.addAttribute("created", new Date().getTime());
		newAttrs.addAttribute("originalforeground", StyleConstants.getForeground(newAttrs));

		int characterstodelete = 0;
		if (super.getLength() > maximumLength) {
			characterstodelete = super.getLength() - maximumLength;
			try {
				synchronized (this) {
					super.remove(0, characterstodelete);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		try {
			synchronized (this) {
				super.insertString(super.getLength(), str, newAttrs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public boolean canType() {
		return hasFloor() || floorIsOpen();
	}

	public int lastIndexOfDelimiter(String s)
	{
		
		for(int i=s.length();i>0;i--)
		{
			String sub=s.substring(i-1,i);
			if(delimiters.contains(sub))
				return i-1;
		}
		return -1;
	}
	
	public String getLastWord()
	{
		String text=null;
		try{
			text=getText(0,this.getLength());
		}catch(BadLocationException e)
		{
			e.printStackTrace();
			return null;
		}
		
		
		int last=lastIndexOfDelimiter(text);
		if (last==-1)
		{
			logger.debug("no last delimiter");
			return text.trim().toLowerCase();
			
		}
		else if (last==text.length()-1)
			return null;
		else
		{
			String word=text.substring(last+1, text.length());
			if (!word.isEmpty())
				return word;
			
			return null;
		}
		
		
	}
	
	public void insertString(int offs, String str, AttributeSet as) throws BadLocationException {

		
		if (!canType())
		{
			kit.beep();
			return;
		}
		
		if (str.equalsIgnoreCase("\n"))
			return;
		
		
		if (delimiters.contains(str))
		{
			logger.debug("Detected end of word:"+str);
			String text=getText(0,this.getLength());
			int last=lastIndexOfDelimiter(text);
			if (last==-1)
			{
				logger.trace("no last delimiter");
				this.jtprj.agent.addWord(text.trim().toLowerCase());
				
			}
			else
			{
				String word=text.substring(last+1, text.length());
				if (!word.isEmpty())
					this.jtprj.agent.addWord(word.trim().toLowerCase());
			}
			
			if (sentenceDelimiters.contains(str))
				this.jtprj.agent.addWord(str);
				
			
			
		}
		

		
		synchronized (stateHolding) {

			this.insertString_PresupposesOnSwingThread(str, Color.BLACK);
			stateHolding.notify();
			

		}
		this.jtprj.agent.setLastCharTime(new Date().getTime());

	}

	@Override
	public void remove(int i, int i1) throws BadLocationException {
		// super.remove(i, i1); //To change body of generated methods, choose
		// Tools | Templates.

		// System.exit(-56);
	}

	@Override
	public void replace(int i, int i1, String string, AttributeSet as) throws BadLocationException {

		if (i != this.getLength())
			return;

		SimpleAttributeSet newAttrs = new SimpleAttributeSet();
		StyleConstants.setForeground(newAttrs, StyleConstants.getForeground(as));

		StyleConstants.setBackground(newAttrs, Color.WHITE);
		StyleConstants.setForeground(newAttrs, Color.BLACK);
		StyleConstants.setFontSize(newAttrs, fontSize);
		StyleConstants.setFontFamily(newAttrs, StyleConstants.getFontFamily(as));

		StyleConstants.setAlignment(newAttrs, StyleConstants.ALIGN_RIGHT);
		newAttrs.addAttribute("created", new Date().getTime());
		newAttrs.addAttribute("originalforeground", StyleConstants.getForeground(newAttrs));

		super.replace(i, i1, string, newAttrs); // To change body of generated
												// methods, choose Tools |
												// Templates.

		// super.replace(i, i1, string, as); //To change body of generated
		// methods, choose Tools | Templates.
	}

	
}
