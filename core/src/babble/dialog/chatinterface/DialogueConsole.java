package babble.dialog.chatinterface;

import java.awt.BorderLayout;
import java.awt.Color;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import org.apache.log4j.Logger;

import babble.dialog.env.DyadicDialogueEnvironment;
import babble.dialogue.DSDialogueAgent;
import babble.dialogue.IncrementalDialogueAgent;
import babble.dialogue.SemanticDialogueAgent;
import qmul.ds.dag.UtteredWord;

public class DialogueConsole extends IncrementalDialogueAgent implements Runnable {

	Logger logger = Logger.getLogger(DialogueConsole.class);
	protected Queue<UtteredWord> uttered = new LinkedList<UtteredWord>();

	protected JTextPaneRightJustified textPane;
	protected JFrame console = new JFrame("Console");
	final static Color other_colour = Color.blue;
	int typingTimeout = 2000; //number of milliseconds to wait before timeout (end of turn generation)

	public DialogueConsole(String name) {
		super(name);
//		init();
	}
	
	public void init(){
		console.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		textPane = new JTextPaneRightJustified(this);
		// textPane.jtprjfd.addDocumentListener(this);

		// textPane.jtprjfd.insertStringFromOther("HELLO", Color.green);

		JScrollPane scrollPane = new JScrollPane(textPane);
		console.add(scrollPane, BorderLayout.CENTER);
		console.setSize(800, 100);
		console.setVisible(true);
		textPane.startFade();
	}

	@Override
	public synchronized UtteredWord nextWord() throws InterruptedException {
		
//		if (dialogue_env.hasFloor(this)&&!uttered.isEmpty())
//			throw new IllegalStateException();
		
		if (!(hasFloor()||floorIsOpen()))
		{
			logger.debug("I don't have the floor, and floor is not open. Waiting.");
			return new UtteredWord(DSDialogueAgent.WAIT, this.name);
		}
		
		while(uttered.isEmpty())
			wait();
		
		logger.debug("Uttering word:"+uttered.peek().word());
		return uttered.poll();
	}

	public boolean hasFloor() {
		return this.dialogue_env.hasFloor(this);
	}

	public boolean floorIsOpen() {
		return dialogue_env.floorIsOpen();
	}

	
	long lastCharTime=-1;
	protected synchronized void checkTypingTimeout()
	{
		if (lastCharTime==-1)
			return;
		
		long time=new Date().getTime();
		if (time-lastCharTime>this.typingTimeout)
		{
			String lastWord=textPane.jtprjfd.getLastWord();
			if (lastWord!=null)
				addWord(lastWord);
			
			addWord(DSDialogueAgent.RELEASE_TURN);
			//block typing.
			lastCharTime=-1;
		}
		
	}
	
	public void setTimeout(int timeout){
		this.typingTimeout = timeout;
	}
	
	public synchronized void setLastCharTime(long l)
	{
		this.lastCharTime=l;
	}
	
	@Override
	public void run() {
		
		while(true)
		{
			try
			{
				Thread.sleep(100);
				checkTypingTimeout();
			}
			catch(InterruptedException e)
			{
				e.printStackTrace();
			}
		}

	}

	UtteredWord last = null;

	@Override
	public void wordUttered(UtteredWord w) {
		logger.debug("UtteredWord from the other agent[" + w.speaker() + "]: " + w.word());
		
		if (w.speaker().equals(name))
			return;
		
		if (w.word().equals(DSDialogueAgent.RELEASE_TURN))
			return;

		String toInsert = null;
		try {
			String text = this.textPane.jtprjfd.getText(0, this.textPane.jtprjfd.getLength());
			if (text.endsWith(" "))
				toInsert = w.word() + " ";
			else
				toInsert = " " + w.word() + " ";
		} catch (Exception e) {
			e.printStackTrace();
		}

		textPane.insertTextByOther(toInsert, other_colour, w.speaker());

	}

	public synchronized void addWord(String word) {
		logger.debug("Adding word:" + word);
		uttered.offer(new UtteredWord(word, this.name));
		notifyAll();
	}

	public static void main(String[] a) throws Exception {
		DialogueConsole console = new DialogueConsole("usr");
		
		
		SemanticDialogueAgent semanticSim = new SemanticDialogueAgent("sys", "restaurant-search-ca", "2017-english-ttr-restaurant-search-ca");
		
		DyadicDialogueEnvironment env = new DyadicDialogueEnvironment(semanticSim, console);
	
		
	
		env.start();
		new Thread(console).start();
		
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub

	}
}
