package babble.dialog.remote;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.LinkedList;
import org.apache.log4j.Logger;

import babble.dialogue.DSDialogueAgent;
import babble.dialogue.IncrementalDialogueAgent;
import qmul.ds.dag.UtteredWord;

/** An IncrementalDialogueAgent that reads and writes utterances over a network.
 *
 * It is constructed with a socket as well as an agent name, this socket will be connected to a Client instance (Server
 *  manages setting this up) over a network.
 */
public class RemoteIncrementalDialogueAgent extends IncrementalDialogueAgent {
	// Both of these are about 5 mins
	private static final int TIMEOUT = 1000 * 60 * 5;
	private static final int COUNTDOWN_DEADLINE = 10 * 60 * 5;
	
	private Socket socket;
	private BufferedReader reader;
	private BufferedWriter writer;
	private int countdown = 0;
	Logger logger = Logger.getLogger(RemoteIncrementalDialogueAgent.class);
	LinkedList<String> inWords;
	private boolean alive;
	
	public RemoteIncrementalDialogueAgent(String name, Socket socket) {
		super(name);
		logger.debug("Running RemoteDialogueAgent connected to "+socket);
		this.socket = socket;
		
		try {
			socket.setSoTimeout(TIMEOUT);
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
		}catch (IOException e) {
			logger.error("Error handling connection: " + e.getMessage());
		}
		
		inWords = new LinkedList<String>();
		alive = true;
	}
	
	private void readWords() {
		try {
			countdown ++;
			while(reader.ready()) {
				String line = reader.readLine();
				String words[] = line.split(" ");
				for(String w : words) {
					inWords.addFirst(w);
				}
				countdown = 0;
			}
		} catch (IOException e) {
			alive = false;
		}
	}
	
	@Override
	public UtteredWord nextWord() throws InterruptedException {
		if(!this.dialogue_env.hasFloor(this) && !this.dialogue_env.floorIsOpen()) {
			return new UtteredWord(DSDialogueAgent.WAIT, name);
		}
		
		while(inWords.isEmpty()) {
			if(!this.alive || countdown >= COUNTDOWN_DEADLINE) {
				logger.debug("Remote connection has broken, saying bye.");
				try {
					socket.close();
				} catch (IOException e) {
					// Do nothing
				}
				return new UtteredWord(DSDialogueAgent.BYE, name);
			}
			readWords();
			Thread.sleep(100);
		}
		
		return new UtteredWord(inWords.removeLast(), name);
	}
	
	@Override
	public void wordUttered(UtteredWord w) {
		try {
			writer.append(w.speaker() + ": " + w.word());
			writer.newLine();
			writer.flush();
		} catch (IOException e) {
			alive = false;
		}
	}
	
	@Override
	public void reset() {
		inWords = new LinkedList<String>();
		try {
			reader.reset();
		} catch (IOException e) {
			alive = false;
		}
	}
}
