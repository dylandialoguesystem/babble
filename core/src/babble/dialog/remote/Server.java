package babble.dialog.remote;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocketFactory;

import org.apache.log4j.Logger;

import babble.dialog.env.DialogueEnvironment;
import babble.dialog.env.DyadicDialogueEnvironment;
import babble.dialogue.IncrementalDialogueAgent;
import babble.dialogue.SemanticDialogueAgent;
import babble.simulation.SemanticUserSimulation;
import babble.simulation.SimpleReplaySimulatedDialogueAgent;
import qmul.ds.babi.BabiDialogue;

/** This is a server that listens for connections from Client instances, and creates dialogue systems to handle them
 *
 * It is ran using `java Server [port]`, where port defaults to RemoteSettings.DEFAULT_PORT. It will then listen for
 *  connections from Client instances, and when it gets one it does the following:
 * * @TODO Stuff
 *
 * @TODO Document the tomfoolery you need to do to use SSLSockets
 * 
 * @author Ross Brunton
 */
public class Server extends Thread {
	private Logger logger = Logger.getLogger(Server.class);
	private int port;
	
	public static void main(String[] args) {
		int port = RemoteSettings.DEFAULT_PORT;
		
		if(args.length > 0) {
			port = Integer.parseInt(args[0]);
		}
		
		Server s = new Server(port);
		s.start();
		try {
			s.join();
		} catch (InterruptedException e) {
			// Pass
		}
	}
	
	public Server(int port) {
		this.port = port;
	}
	
	protected IncrementalDialogueAgent makePartner() {
		try {
			return new SemanticDialogueAgent("sys", "restaurant-search-ca", "2017-english-ttr-restaurant-search-ca");
		} catch (IOException e) {
			// TODO handle this
			return null;
		}
	}
	
	protected DialogueEnvironment makeEnvironment(IncrementalDialogueAgent me, IncrementalDialogueAgent partner) {
		return new DyadicDialogueEnvironment(me, partner);
	}
	
	public void run() {
		logger.debug("Starting server on port " + port);
		
		try {
			ServerSocketFactory factory;
			if(RemoteSettings.SECURE) {
				factory = SSLServerSocketFactory.getDefault();
			}else{
				factory = ServerSocketFactory.getDefault();
			}
			ServerSocket serverSocket = factory.createServerSocket(port);

			while(true) {
				try {
					logger.debug("Waiting for connection!");
					Socket socket = serverSocket.accept();
					logger.debug("Accepted connection from " + socket.getRemoteSocketAddress());
					
					RemoteIncrementalDialogueAgent usr = new RemoteIncrementalDialogueAgent("usr", socket);
					IncrementalDialogueAgent sys = makePartner();
					DialogueEnvironment env = makeEnvironment(sys, usr);
					env.start();
				} catch(IOException e) {
					logger.error("Error accepting socket: " + e.getMessage());
				}
			}
		} catch(IOException e){
			logger.fatal("Error setting up socket factory: " + e.getMessage());
			System.exit(1);
		}
	}
}
