package babble.dialog.remote;

import java.io.IOException;
import java.util.Scanner;

public class ConsoleClient {
	private static String USR = "usr";
	private static String OTHER = "sys";
	
	private static String BYE = "bye";
	private static String RT = "<rt>";
	
	public static void main(String[] args) {
		Scanner stdin = new Scanner(System.in);
		
		try {
			Client c = null;
			
			switch(args.length) {
				case 2:
					c = new Client(args[0], Integer.parseInt(args[1]));
					break;
				case 1:
					c = new Client(args[0]);
					
					break;
				default:
					System.err.println("Usage: Client hostname [port]");
					System.exit(2);
			}
			
			System.out.println("== Welcome to Babble ==");
			
			while(true) {
				// Read and send user utterance
				System.out.println("");
				System.out.print(USR);
				System.out.print(": ");
				try {
					while(System.in.available() == 0) {
						// Pass
					}
					if(System.in.available() > 0) {
						String in = stdin.nextLine();
						c.sendUtterance(in);
					}
				} catch (IOException e) {
					// TODO If stdin is broken, we have bigger problems
				}
				
				c.sendUtterance(RT);
				
				// Write system utterance
				boolean bye = false;
				System.out.print(OTHER);
				System.out.print(":");
				ClientResponse r = null;
				while(true) {
					r = c.getResponse();
					
					if(r == null) {
						continue;
					}
					if(r.getWord().equals(RT) && r.getSpeaker().equals(OTHER)) {
						break;
					}
					if(r.getSpeaker().equals(OTHER)) {
						System.out.print(" ");
						System.out.print(r.getWord());
					}
					if(r.getWord().equals(BYE)) {
						bye = true;
						break;
					}
				}
				
				if(bye) {
					System.out.println("");
					System.out.println("Dialogue Finished");
					break;
				}
			}
		} catch(ClientDisconnectedException e) {
			System.err.println("Failed to connect Client: " + e.getMessage());
			System.exit(1);
		}
	}
}
