package babble.dialog.env;

import babble.dialogue.IncrementalDialogueAgent;
import qmul.ds.dag.UtteredWord;

public class DyadicDialogueEnvironment extends DialogueEnvironment {
	
	public DyadicDialogueEnvironment(IncrementalDialogueAgent a1, IncrementalDialogueAgent a2)
	{
		super();
		join(a1);
		join(a2);
	}
	
	
	
	public IncrementalDialogueAgent getInterlocutor(IncrementalDialogueAgent a)
	{
		if (a==this.participants.get(0))
			return this.participants.get(1);
		else if(a==this.participants.get(1))
			return this.participants.get(0);
		else throw new IllegalArgumentException();
		
	}
	
	@Override
	public boolean wordUttered(UtteredWord w)
	{
		if (w.speaker().equals(participants.get(0).name()))
			w.setAddressee(participants.get(1).name());
		else if (w.speaker().equals(participants.get(1).name()))
			w.setAddressee(participants.get(0).name());
		else
		{
			
			throw new IllegalArgumentException("speaker of "+w.word()+" is "+w.speaker()+": not part of conversation");
		}
		
		return super.wordUttered(w);
	}

}
