package babble.dialog.env;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;

import babble.dialog.chatinterface.DialogueConsole;
import babble.dialogue.DSDialogueAgent;
import babble.dialogue.DSQLearningAgent;
import babble.dialogue.IncrementalDialogueAgent;
import babble.simulation.SemanticUserSimulation;
import babble.simulation.SimpleReplaySimulatedDialogueAgent;
import qmul.ds.Dialogue;
import qmul.ds.Utterance;
import qmul.ds.babi.BabiDialogue;
import qmul.ds.dag.UtteredWord;

public class DialogueEnvironment extends Thread {

	protected Logger logger = Logger.getLogger(DialogueEnvironment.class);

	protected List<IncrementalDialogueAgent> participants = new ArrayList<IncrementalDialogueAgent>();
	protected Dialogue dialogue = new Dialogue();

	protected String whoHasFloor = null;// name of agent who has the floor.

	public DialogueEnvironment() {

	}
	
	public DialogueEnvironment(IncrementalDialogueAgent... agents)
	{
		for(IncrementalDialogueAgent a:agents)
			join(a);
		
	}

	protected void announceParticipantJoined(IncrementalDialogueAgent a) {
		for (IncrementalDialogueAgent agent : this.participants) {
			if (!agent.name().equals(a.name()))
				agent.joinedConversation(a);
		}
	}

	public synchronized void join(IncrementalDialogueAgent agent) {
		participants.add(agent);
		agent.join(this);
		announceParticipantJoined(agent);
	}

	public synchronized boolean hasFloor(IncrementalDialogueAgent a) {
		if (!this.participants.contains(a))
			throw new IllegalArgumentException("Agent " + a + " is not part of the conversation");

		return a.name().equals(this.whoHasFloor);
	}

	public synchronized boolean hasFloor(String name) {
		return name.equals(this.whoHasFloor);

	}

	public synchronized void removeParticipant(IncrementalDialogueAgent incrementalDialogueAgent) {
		this.participants.remove(incrementalDialogueAgent);

	}

	/**
	 * called whenever somebody says something.
	 * 
	 * @param w
	 * @return true if the word is successfully uttered. False if the agent is
	 *         speaking out of turn.
	 * 
	 */
	protected synchronized boolean wordUttered(UtteredWord w) {
		logger.debug("Notifying all of word uttered: " + w);

		// first check to see if speaker of w is speaking out of turn. If so, it
		// will fail, and not update anything.
		// one case here is if the agent generates a <wait> action when it has
		// the floor.

		if (w.word().equals(DSDialogueAgent.WAIT)) {
			if (hasFloor(w.speaker())) {
				logger.warn("Illegal <wait>: " + w.speaker() + " has the turn. Turn must be released first.");
				return false;
			} else
				return true;
		} else if (w.word().equals(DSDialogueAgent.RELEASE_TURN)) {
			if (hasFloor(w.speaker())) {
				openFloor();
			} else {
				logger.warn(w.speaker() + " attempted to release turn, but she didn't have the floor");
				return false;
			}
		} else if (w.word().equals(DSDialogueAgent.BYE)) {
			// Bye can be generated out of turn.
			giveFloorTo(w.speaker());
		} else if (floorIsOpen()) {
			giveFloorTo(w.speaker());
		} else if (!hasFloor(w.speaker())) {
			logger.warn("Word is out of turn/overlapping: " + w);
			logger.warn(this.whoHasFloor + " has the floor.");
			return false;
		}

		// if (!w.word().equals(DSDialogueAgent.RELEASE_TURN))
		dialogue.append(w);

		// notify everyone that word was uttered - they will each have their own
		// way of processing the word.
		for (IncrementalDialogueAgent agent : participants) {
			agent.wordUttered(w);
		}

		return true;

	}

	protected void openFloor() {
		logger.debug("opening floor");
		this.whoHasFloor = null;
	}

	private synchronized void giveFloorTo(String name) {
		logger.debug("Giving floor to:" + name);
		this.whoHasFloor = name;
	}

	public synchronized void giveFloorTo(IncrementalDialogueAgent agent) {
		giveFloorTo(agent.name());
	}

	public synchronized boolean floorIsOpen() {
		return this.whoHasFloor == null;
	}

	public boolean timeStep() throws InterruptedException {
		boolean dialogueInProgress = true;
		for (int i = 0; i < participants.size(); i++) {
			IncrementalDialogueAgent speaker = participants.get(i);
			UtteredWord polledWord = speaker.nextWord();
			logger.debug("Polled Word:" + polledWord + " from: " + speaker);

			if (!wordUttered(polledWord)) {
				logger.error("Something went wrong. One of the agents did something illegal.");
				//Deal with overlapping words here. 
				//what do you do when people speak at the same time?
				//or when the agent speaks at the same time as the user.
				System.exit(-1);
			}
			dialogueInProgress &= !(polledWord.word().equals(DSDialogueAgent.BYE));
		}
		return dialogueInProgress;
	}

	public void run() {
		boolean dialogueInProgress = true;
		logger.debug("start to run...");
		int i=0; 
		while (dialogueInProgress) {
			try {
				logger.debug("i: " + i++);
				dialogueInProgress &= timeStep();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}

		logger.debug("dialogueInProgress has been stoped for now... because of the dialogueInProgress: " + dialogueInProgress);
	}

	public synchronized Utterance getLastUtterance() {
		return this.dialogue.lastUtterance();
	}

	public void resetEnvironment() {

		dialogue.clear();
		openFloor();
		for (IncrementalDialogueAgent agent : participants) {
			agent.reset();
		}
	}

	public Dialogue getDialogue() {
		return dialogue;
	}

	public static void main(String a[]) throws IOException, InterruptedException {


//		// DialogueConsole arash = new DialogueConsole("arash");
//		// arash.initalization();
//
//		SemanticUserSimulation sim = new SemanticUserSimulation("usr", "restaurant-search",
//				"2016-english-ttr-restaurant-search");
//
//		// sim=new SemanticsBasedSimulatedDialogueAgent(
//		// "usr",
//		// domain,
//		// inGrammarResource
//		// );
//
//		// sim=new TemplateBasedSimulatedDialogueAgent("sim", "shopping-mall");
//		DSQLearningAgent dylan = new DSQLearningAgent("sys", "restaurant-search", "2016-english-ttr-restaurant-search");
//		// dylan.loadPolicyFromFile("policy2");
//		SimulatedDialogueEnvironment env = new NonIncrementalSimulatedDialogueEnvironment(dylan, sim);
//		// dylan.learn(10000, 1.0, 0.1);
//		dylan.learn(1000, 1.0, 0.1);
//		dylan.writePolicyToFile("initial_policy");
		
		
		
		SimpleReplaySimulatedDialogueAgent userSimul = new SimpleReplaySimulatedDialogueAgent(
				"usr",
				BabiDialogue.loadFromBabbleFile("data/Domains/restaurant-search-ca/testing_dialogues").get(0)
		);
		
		DialogueConsole arash = new DialogueConsole("arash");
		arash.init();
		
		DialogueEnvironment evn = new DialogueEnvironment(arash, userSimul);
		evn.start();

	}

}
