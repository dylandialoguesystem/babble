package babble.dialog.env;

import babble.dialog.rl.ContextState;
import babble.dialog.rl.DSTerminalFunction;
import babble.dialog.rl.BreadthFirstRewardFunction;
import babble.dialogue.DSDialogueAgent;
import babble.dialogue.IncrementalDialogueAgent;
import burlap.mdp.core.TerminalFunction;
import burlap.mdp.core.action.Action;
import burlap.mdp.singleagent.environment.Environment;
import burlap.mdp.singleagent.environment.EnvironmentOutcome;
import burlap.mdp.singleagent.model.RewardFunction;
import qmul.ds.dag.UtteredWord;

public class SimulatedDialogueEnvironment extends DyadicDialogueEnvironment implements Environment {

	RewardFunction rf;
	TerminalFunction tf;
	DSDialogueAgent self;
	IncrementalDialogueAgent other;

	public SimulatedDialogueEnvironment(DSDialogueAgent self, IncrementalDialogueAgent other, RewardFunction rf,
			TerminalFunction tf) {
		super(self, other);
		this.self = self;
		this.other = other;
		this.rf = rf;
		this.tf = tf;

	}

	public SimulatedDialogueEnvironment(DSDialogueAgent self, IncrementalDialogueAgent other) {
		this(self, other, new BreadthFirstRewardFunction(self.getEncoding()), new DSTerminalFunction(self.getEncoding()));
	}

	public SimulatedDialogueEnvironment(DSDialogueAgent self2, IncrementalDialogueAgent other2, RewardFunction rf2) {
		this(self2, other2, rf2, new DSTerminalFunction(self2.getEncoding()));
	}

	public burlap.mdp.core.state.State currentObservation() {
		return self.getCurrrentState();
	}

	protected double lastReward = 0;

	@Override
	public EnvironmentOutcome executeAction(Action a) {

		UtteredWord word = new UtteredWord(a.actionName(), self.name());
		ContextState cur = self.getCurrrentState();
		ContextState unsuccessfulTerminal = self.getUnsuccessfulTerminalState();

		if (!wordUttered(word))// utters word to all including self (which
								// updates its state).
		{
			// if we are here, self spoke out of turn, or generated an illegal <wait>. Terminate episode and
			// penalise.
			logger.debug("Out of turn word: "+word.word()+". Terminating.");
			lastReward = rf.reward(cur, a, unsuccessfulTerminal);
			self.setStateToUnsuccessfulTerminal();//all states are read from self. So DONT FORGET to SET IT in this case.
			return new EnvironmentOutcome(cur, a, unsuccessfulTerminal, lastReward, true);
		}

		ContextState next = self.getCurrrentState();
		//if self generated release turn, return immediately with update without asking the simulator for word
		//this is so that the agent experiences the state with open floor, and subsequently learns that it needs
		//to <wait> for the user input.
		//the following if clauses determine in general when execution returns without input from the user simulator.
		
		if (tf.isTerminal(next)) {
			//we'd be here if either we have reached the goal, or the word was not parsable.
			logger.debug(
					"Terminal after " +word.word()+" from self.");
			lastReward = rf.reward(cur, a, next);
			return new EnvironmentOutcome(cur, a, next, lastReward, true);
		}
		

		

		// if we are here, self managed to utter a word successfully, and the
		// state is not terminal.
		// it could have been a <wait> action, but not a release-turn
		// now poll word from the simulator

		UtteredWord otherWord = null;
		try {
			otherWord = other.nextWord();
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.exit(-1);

		}

		//if self generated a successful release-turn, then return resulting state without update from 
		//simulator. 
		if (word.word().equals(DSDialogueAgent.RELEASE_TURN)&&!otherWord.word().equals(DSDialogueAgent.BYE))
		{
			logger.debug("self successfully released turn. Not ingegrating/parsing simulator's output word just yet.");
			lastReward = rf.reward(cur, a, next);
			return new EnvironmentOutcome(cur, a, next, lastReward, false);
			
		}

		
		logger.debug("Simulator uttered: " + otherWord.word());
		if (!wordUttered(otherWord))
		{
			throw new IllegalStateException("Simulator uttered word out of turn");
		}
		
		
		next = self.getCurrrentState();
		
		lastReward = rf.reward(cur, a, next);
		return new EnvironmentOutcome(cur, a, next, lastReward, tf.isTerminal(next));

	}

	@Override
	public double lastReward() {
		return lastReward;
	}

	@Override
	public boolean isInTerminalState() {
		return tf.isTerminal(currentObservation());
	}

	@Override
	public void resetEnvironment() {
		super.resetEnvironment();
	}
}
