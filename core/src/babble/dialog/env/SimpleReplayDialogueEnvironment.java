package babble.dialog.env;

import java.io.IOException;

import babble.dialogue.IncrementalDialogueAgent;
import babble.dialogue.SemanticDialogueAgent;
import babble.simulation.SimpleReplaySimulatedDialogueAgent;
import qmul.ds.Utterance;
import qmul.ds.babi.BabiDialogue;

// just for the development purposes
public class SimpleReplayDialogueEnvironment extends DialogueEnvironment {
	public static void main(String a[]) throws IOException, InterruptedException {
//		System.out.println("Simulating system:");
//		simulateSystem(
//			"data/Domains/restaurant-search/dialogues",
//			"2016-english-ttr-restaurant-search",
//			"restaurant-search"
//		);


		System.out.println("Simulating system:");
		simulateSystem(
			"data/Domains/restaurant-search-ca/testing_dialogues",
			"2017-english-ttr-restaurant-search-ca",
			"restaurant-search-ca"
		);
//		System.out.println("\n\n\n\nSimulating user:");
//		simulateUser(
//			"data/Domains/restaurant-search/dialogues",
//			"2016-english-ttr-restaurant-search",
//			"restaurant-search"
//		);
	}

	public static void simulateSystem(
		String inDialogue,
		String inGrammarResource,
		String domain
	) throws IOException, InterruptedException {
		IncrementalDialogueAgent
			replaySim = new SimpleReplaySimulatedDialogueAgent(
				"usr",
				BabiDialogue.loadFromBabbleFile(inDialogue).get(0)
			),
			semanticSim = new SemanticDialogueAgent(
				"sys",
				domain,
				inGrammarResource
			);
		DyadicDialogueEnvironment env = new DyadicDialogueEnvironment(replaySim, semanticSim);
		env.start();
		env.join();
	
		System.out.println("\n\n\nGot the following dialogue:");
		for (Utterance utterance: env.dialogue) {
			System.out.println(utterance);
		}
	}

	public static void simulateUser(
		String inDialogue,
		String inGrammarResource,
		String domain
	) throws IOException, InterruptedException {
		
		
		IncrementalDialogueAgent
			replaySim = new SimpleReplaySimulatedDialogueAgent(
				"sys",
				BabiDialogue.loadFromBabbleFile(inDialogue).get(0)
			),
				semanticSim = new SemanticDialogueAgent(
				"usr",
				domain,
				inGrammarResource
			);
		
		
		DyadicDialogueEnvironment env = new DyadicDialogueEnvironment(semanticSim, replaySim);
		env.start();
		env.join();
	
		System.out.println("\n\n\nGot the following dialogue:");
		for (Utterance utterance: env.dialogue) {
			System.out.println(utterance);
		}
	}
}
