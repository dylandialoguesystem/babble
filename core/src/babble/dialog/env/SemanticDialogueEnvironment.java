package babble.dialog.env;

import java.io.IOException;

import babble.dialog.chatinterface.DialogueConsole;
import babble.dialog.rl.TTRMDPStateEncoding;
import babble.dialogue.IncrementalDialogueAgent;
import babble.simulation.SemanticsBasedSimulatedDialogueAgent;

// just for the development purposes
public class SemanticDialogueEnvironment extends DialogueEnvironment {
	public static void main(String a[]) throws IOException {

		DialogueConsole console = new DialogueConsole("usr"); // = new ScriptedDialogueAgent("USR");
		

		IncrementalDialogueAgent
			sys = new SemanticsBasedSimulatedDialogueAgent(
				"sys",
				"restaurant-search",
				"2016-english-ttr-restaurant-search",
				new TTRMDPStateEncoding("restaurant-search")
			),
			usr = console;
		DyadicDialogueEnvironment env = new DyadicDialogueEnvironment(sys, usr);
		env.start();
	}
}
