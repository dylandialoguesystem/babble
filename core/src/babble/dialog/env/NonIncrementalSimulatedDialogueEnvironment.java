package babble.dialog.env;

import babble.dialog.rl.ContextState;
import babble.dialogue.DSDialogueAgent;
import babble.dialogue.IncrementalDialogueAgent;
import burlap.mdp.core.TerminalFunction;
import burlap.mdp.core.action.Action;
import burlap.mdp.singleagent.environment.Environment;
import burlap.mdp.singleagent.environment.EnvironmentOutcome;
import burlap.mdp.singleagent.model.RewardFunction;
import qmul.ds.dag.UtteredWord;

public class NonIncrementalSimulatedDialogueEnvironment extends SimulatedDialogueEnvironment implements Environment {

	

	public NonIncrementalSimulatedDialogueEnvironment(
		DSDialogueAgent self,
		IncrementalDialogueAgent other,
		RewardFunction rf,
		TerminalFunction tf
	) {
		super(self, other, rf, tf);
		

	}
	
	public NonIncrementalSimulatedDialogueEnvironment(
			DSDialogueAgent self,
			IncrementalDialogueAgent other,
			RewardFunction rf
		) {
			super(self, other, rf);
			

		}

	public NonIncrementalSimulatedDialogueEnvironment(
		DSDialogueAgent self,
		IncrementalDialogueAgent other
	) {
		super(self,other);
	}
	
	

	

	@Override
	public EnvironmentOutcome executeAction(Action a) {

		UtteredWord word = new UtteredWord(a.actionName(), self.name());
		ContextState cur = self.getCurrrentState();
		ContextState unsuccessfulTerminal = self.getUnsuccessfulTerminalState();

		if (!wordUttered(word))// utters word to all including self (which updates its state).
		{
			// if we are here, self spoke out of turn, or generated an illegal <wait>. Terminate episode and penalise.
			logger.debug("Out of turn word: "+word.word()+". Terminating.");
			lastReward = rf.reward(cur, a, unsuccessfulTerminal);
			//all states are read from self. So DONT FORGET to SET IT in this case.
			self.setStateToUnsuccessfulTerminal();
			return new EnvironmentOutcome(cur, a, unsuccessfulTerminal, lastReward, true);
		}

		ContextState next = self.getCurrrentState();
		
		
		

		if (tf.isTerminal(next)) {
			//we'd be here if either we have reached the goal, or the word was not parsable.
			logger.debug("Terminal after " +word.word()+" from self.");
			lastReward = rf.reward(cur, a, next);
			return new EnvironmentOutcome(cur, a, next, lastReward, true);
		}

		// if we are here, self managed to utter a word successfully, and the
		// state is not terminal.
		// it could have been a <wait> action, but not a release-turn
		// now poll word from the simulator

		UtteredWord otherWord = null;
		boolean otherReleasedTurn = false;
		while (!otherReleasedTurn) {
			try {
				otherWord = other.nextWord();
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(-1);
			}

			if (otherWord.word().equals(DSDialogueAgent.WAIT)) {
				break;
			}

			//if self generated release turn, return immediately with update without asking the simulator for word
			//this is so that the agent experiences the state with open floor, and subsequently learns that it needs
			//to <wait> for the user input.
			//the following if clauses determine in general when execution returns without input from the user simulator.
			//if self generated a successful release-turn, then return resulting state without update from 
			//simulator. 
			
			/*
			if (word.word().equals(DSDialogueAgent.RELEASE_TURN)&&!otherWord.word().equals(DSDialogueAgent.BYE))
			{
				logger.debug(
					"self successfully released turn. Not ingegrating/parsing simulator's output word just yet."
				);
				lastReward = rf.reward(cur, a, next);
				return new EnvironmentOutcome(cur, a, next, lastReward, false);
				
			}*/

			logger.debug("Simulator uttered: " + otherWord.word());
			if (!wordUttered(otherWord))
			{
				throw new IllegalStateException("Simulator uttered word out of turn");
			}
			
			if (otherWord.word().equals(DSDialogueAgent.BYE))
				break;
			
			otherReleasedTurn = otherWord.word().equals(DSDialogueAgent.RELEASE_TURN);
		}
		next = self.getCurrrentState();

		lastReward = rf.reward(cur, a, next);
		logger.debug("State is terminal:"+tf.isTerminal(next));
		return new EnvironmentOutcome(cur, a, next, lastReward, tf.isTerminal(next));
	}


}

