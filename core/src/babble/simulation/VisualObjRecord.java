package babble.simulation;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import babble.vision.object.BasicObject;
import babble.vision.tools.Constants;

public class VisualObjRecord extends BasicObject{
	Logger logger = Logger.getLogger(VisualObjRecord.class);
	
	private String descrip;
	private String colorCpt;
	private String shapeCpt;
	private String path;
	
	public VisualObjRecord(int idx, String filename, String descrip) {
		super(idx);
		this.setFileName(filename);
		this.path = Constants.trnSetDir + filename;

		this.setLabels(descrip);
	}
	
	private double[] feat;
	
	public String getDescription(){
		return this.descrip;
	}
	
	public String getPath(){
		return this.path;
	}
	
	public String getColorCpt(){
		return this.colorCpt;
	}
	
	public String getShapeCpt(){
		return this.shapeCpt;
	}
	
	public void setLabels(String descrip){
		this.descrip = descrip;
		if(!descrip.equals("")){
			String[] subs = descrip.split(" ");
			labels = null;
			labels = Arrays.asList(subs);
			
			this.colorCpt = subs[0];
			this.shapeCpt = subs[1];
		}
		else{
			labels = null;
		}
		setLabels(labels);
	}
}
