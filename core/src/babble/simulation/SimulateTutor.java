package babble.simulation;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import babble.vision.gui.AbstractActivityListener;
import babble.vision.tools.Constants;
import hw.system.SimulateListener;
import hw.system.dm.SimulateIM;
import hw.system.dm.UttTyps;

public class SimulateTutor {
	Logger logger = Logger.getLogger(SimulateTutor.class);
	private TutorManager tutorManager;
	private SimulateIM im;
	private AbstractActivityListener actListener;
	private SimulateListener simListener;
	private int maxNumofInstances; // decide how many instances in training set...
	
	public SimulateTutor(AbstractActivityListener actListener, SimulateListener simListener, SimulateIM im, Map<String, List<VisualObjRecord>> datMap){
		this.actListener = actListener;
		this.simListener = simListener;
		this.im = im;
		this.im.setTstSet(datMap.get("tstList"));
		this.tutorManager = new TutorManager();
		this.classifyChecker = new ClassifyChecker();
		if(initialImageSet(datMap.get("trnList"))){
			historyRecords = new ArrayList<VisualObjRecord>(); 
		}
		currentState = UserState.normal;
	}
	
	private List<VisualObjRecord> visualRecords;
	private List<VisualObjRecord> historyRecords;
	private boolean initialImageSet(List<VisualObjRecord> datList){
		if(datList != null){
			this.visualRecords = datList;
			this.maxNumofInstances = this.visualRecords.size();
			logger.info("maxNumofInstances: " + maxNumofInstances);
			return true;
		}
		else{
			if(this.visualRecords == null)
				this.visualRecords = new ArrayList<VisualObjRecord>();
			else
				this.visualRecords.clear();
			
			BufferedReader br = null;
			try{
				File annotFile = new File(Constants.annotTrnSet);
				
				if(annotFile.exists()){
					logger.info("Read New File!!!");
					FileInputStream fstream = new FileInputStream(annotFile);
					br = new BufferedReader(new InputStreamReader(fstream));
					
					int count = 0;
					String strLine;
					while ((strLine = br.readLine()) != null){
						if(!strLine.trim().equals("")){
							String[] items = strLine.split(",");
							String filename = items[0];
							String description = items[1];
		
							double[] feat = new double[1280];
							String featVal = items[2].trim().toString();
							String[] vals = featVal.split(" ");
							for(int i=0;i<vals.length;i++){
								  if(vals[i].equals("NaN"))
									  feat[i] = 0;
								  else
									  feat[i] = Double.valueOf(vals[i].trim());
							 }
							
							VisualObjRecord record = new VisualObjRecord(count++, filename, description);
							record.setFeat(feat);
							visualRecords.add(record);
						}
					}
					return true;
				}
				else
					return false;
			}
			catch(Exception e){
				this.logger.error(e.getMessage());
			}
		}
		return false;
	}
	
	public List<VisualObjRecord> getAllVisualRecords(){
		return this.visualRecords;
	}
	
	public List<VisualObjRecord> getHistoryRecords(){
		return this.historyRecords;
	}
	
	public VisualObjRecord findVisualRecordByCpt(String cocept){
		if(this.visualRecords != null && !this.visualRecords.isEmpty()){
			List<VisualObjRecord> flush = flush(this.visualRecords);
			for(VisualObjRecord vRecord: flush){
				if(vRecord.getDescription().contains(cocept)){
					historyRecords.add(vRecord);
					visualRecords.remove(vRecord);
					return vRecord;
				}
			}
		}
		return null;
	}
	
	public List<VisualObjRecord> flush(List<VisualObjRecord> collection){
		if(collection != null && !collection.isEmpty()){
			List<VisualObjRecord> clone = new ArrayList<VisualObjRecord>(collection);
		    Collections.shuffle(clone);
		    
		    return clone;
		}
		return null;
	}
	
	public VisualObjRecord findVisualRecordRandomly(List<VisualObjRecord> vRecordList){
		if(vRecordList != null && !vRecordList.isEmpty()){
			List<VisualObjRecord> flush = flush(vRecordList);
			VisualObjRecord record = flush.get(0);
			historyRecords.add(record);
			flush.remove(0);
			return record;
		}
		return null;
	}
	
	public void reset(){
		this.historyRecords.clear();
		this.currentUtt = null;
//		initialImageSet();
	}
	
	public void importImageRandomly(List<VisualObjRecord> vRecordList){
		int actionIndex = this.randomInt(1, 5);
		logger.debug("Index of Action: " + actionIndex);
		actionIndex = 1;
		
		if(actionIndex == 4 && tutorManager.getNumberOfObjects() > 10)
			logger.info("ignore the new object request...");
		else{
			VisualObjRecord record = findVisualRecordRandomly(vRecordList);
			if(record != null){
				File tmp = new File(record.getPath());
				if(tmp.exists()){
					this.visualRecords.remove(record);
					this.historyRecords.add(record);
					importImage(record);
				}
				else{
					this.historyRecords.remove(record);
					importImageRandomly(vRecordList);
				}
			}
		}
	}

	public void importImageByCpt(String cpt){
		VisualObjRecord record = findVisualRecordByCpt(cpt);
		if(record != null){
			File tmp = new File(record.getPath());
			if(tmp.exists()){
				this.visualRecords.remove(record);
				this.historyRecords.add(record);
				importImage(record);

				logger.debug("REQUEST Record: " + record.getDescription());
			}
			else{
				this.historyRecords.remove(record);
				importImageByCpt(cpt);
			}
		}
	}
	
	private void importImage(VisualObjRecord record){
//		Mat mat = Highgui.imread();
//		BufferedImage img = ImageMat.getInstance().mat2Img(mat);
		BufferedImage img;
		try {
			img = ImageIO.read(new File(record.getPath()));
		
			im.captueNewObject(record.getFeat());
	
			int index = tutorManager.formNewDialog(record);
			if(index <= maxNumofInstances){
				this.actListener.updateImage(img);
				this.simListener.updateObjDescript(index);
				if(this.classifyChecker != null)
					this.classifyChecker.startTimer();
			}
			else{
	//			this.reportHistoryRecords();
				this.simListener.updateSimulationStatus("Simulation Completed..");
	//			this.im.switchOffExecution(this.visualRecords);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	private void reportHistoryRecords() {
//		if(this.historyRecords != null & !this.historyRecords.isEmpty()){
//			for(VisualObjRecord record: this.historyRecords){
//				String rec = record.getFilename() + ", " + record.getDescription();
//				writeLinbyLin("/data/experiment/simulate/history.txt", rec);
//			}
//		}
//	}
//	
//	private void writeLinbyLin(String path, String msg){
//		try {
//			BufferedWriter output = new BufferedWriter(new FileWriter(path, true));
//			output.write(msg + System.getProperty("line.separator"));
//			output.flush();
//			output.close();
//		} catch (IOException e) {
//			logger.error(e.getMessage());
//		}
//	}

	public List<VisualObjRecord> getVisualRecordList(){
		return this.visualRecords;
	}
	
	
	private boolean isDialogGoing = true;
	private ClassifyChecker classifyChecker;
	private void startDialog() {
		while(!isDialogGoing){
			logger.info("in pausing...");
		}
		
		String utt = null;
		switch(this.currentState){
		case normal:
			
			int numOfDialog = tutorManager.getCountOfObject();
			if(numOfDialog < 3){
				utt = tutorManager.pickStatement(null);
			}else{
				utt = tutorManager.pickQuestionRandomly();
			}
			break;
		case requestMore:
			if(requestAttr != null)
				utt = tutorManager.pickStatement(requestAttr);
			
			if(countOfRequest > maxOfRequest){
				this.currentState = UserState.normal;
				this.requestAttr = null;
				countOfRequest = 1;
			}
			break;
		}
		
		logger.info("User Utterance: " + utt);
		actListener.updateDialog(utt, UttTyps.user, true);
	
		im.switchCheckers(true);
		if(this.classifyChecker != null)
			this.classifyChecker.stopTimer();
	}
	
	
	private boolean isStart = false;
	public void switchUserModel(boolean flag){
		this.isStart = flag;
		if(flag){
			List<VisualObjRecord> fullList = getVisualRecordList();
			importImageRandomly(fullList);
		}
		else{
			reset();
		}
	}
	
	public boolean isAlive(){
		return this.isStart;
	}

	private String currentUtt = null;
	public void getUserRespond(String utt) throws InterruptedException{

		while(!isDialogGoing){
			logger.info("in pausing...");
		}
		
		logger.info("System Utterance: " + utt);
		
		currentUtt = utt;
		utt = utt.substring(2);
		
		Thread.sleep(Constants.UNIT * 2);
		
		if(utt.trim().startsWith("okay")){
			return;
		}
		else if(utt.trim().startsWith("sorry")){
			String question = tutorManager.getPreviousQestion();
			getUserRespond(question);
		}
		else if(utt.trim().startsWith("can you show me")){
			String[] words = utt.trim().split(" ");
			if(words[4].equals("a"))
				switch(currentState){
				case normal:
					showObjects(SINGLE, null);
					break;
				case requestMore:
					showObjects(MULIPLE, this.requestAttr);
					break;
				}
			else if(words[4].equals("more")){
				logger.debug("REQUEST FOR MORE ISTANCS...");
				String attr = words[words.length-1].replace("?", "");
				this.currentState = UserState.requestMore;
				this.requestAttr = attr;
				showObjects(MULIPLE, this.requestAttr);
			}
			else
				logger.info("System word: " + words[4]);
		}
		else{
			String usrRespond = tutorManager.generateRespond(utt.trim());
			System.out.println("User Respond: " + usrRespond);
			this.actListener.updateDialog(usrRespond, UttTyps.user, true);
		}
	}
	
	private UserState currentState = null;
	private static final int SINGLE = 0; 
	private static final int MULIPLE = 1; 
	private int countOfRequest = 1;
	private final int maxOfRequest = 2;
	private String requestAttr = null;
 	private void showObjects(int index, String attr){
		switch(index){
			case SINGLE:
					List<VisualObjRecord> fullList = getVisualRecordList();
					this.importImageRandomly(fullList);
				break;
			case MULIPLE:
					logger.debug("REQUEST ATTRIBUTE: " + attr);
				
					this.importImageByCpt(attr);
					countOfRequest++;
				break;
		}
	}
	
	private int randomInt(int min, int max){
		Random random = new Random();
		int num = random.nextInt(max - min + 1) + min;
		return num;
	}
	
	/**
	 * Waiting for system completing the classification and then send the first utterance, i.e. question or statement. 
	 * @author Yanchao Yu
	 */
	class ClassifyChecker{
		private Timer timer;
		public ClassifyChecker(){}
		
		/**
		 * Start a timer task for processing the existing words in 10s.
		 */
		public void startTimer(){
			timer = new Timer("TurnCheck", true);
			timer.schedule(new TimerTask(){
				@Override
				public void run() {
					startDialog();
				}
			}, Constants.UNIT * 10);
		}
		
		/**
		 * Stop the timer for next word!
		 */
		public void stopTimer(){
			if(timer != null){
				timer.cancel();
				timer.purge();
			}
		}
	}
	
	enum UserState{
		normal,
		requestMore;
	}

	public void switchDialogue(boolean isDialogGoing) {
		this.isDialogGoing = isDialogGoing;
		
		if(this.isDialogGoing == true){
			
		}
	}
}
