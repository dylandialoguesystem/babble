package babble.simulation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import babble.simulation.TutorManager.RequestType;
import babble.simulation.TutorManager.VisualOntology;

public class TemplateNLG {
	Logger logger = Logger.getLogger(TemplateNLG.class);
	private Map<VisualOntology, String> visualCpt;
	
	public TemplateNLG(){}
	
	public void updateVisualMap(Map<VisualOntology, String> visualCpt){
		logger.info("Map<VisualOntology, String>: " + visualCpt);
		this.visualCpt = visualCpt;
		logger.info("Map<VisualOntology, String> Global: " + this.visualCpt);
		initialQuestion();
		inisitalStatement();
	}

	private static final String[] stateBegins = new String[]{"this is"};
	public String generateUtt(JSONObject request){
		logger.info("Map<VisualOntology, String>: " + visualCpt);
		String sysUtt = null;
		if(visualCpt != null && request != null){
			if(request.containsKey("request")){
				RequestType reType = (RequestType)request.get("request");
				switch(reType){
				case Ask:
					if(request.containsKey("ontology")){
						VisualOntology onlotoly = (VisualOntology)request.get("ontology");
						String cpt = visualCpt.get(onlotoly);
						int num1 = randInt(0, stateBegins.length-1);
						sysUtt = stateBegins[num1];
						
						if(onlotoly.equals(VisualOntology.full) || onlotoly.equals(VisualOntology.shape))
							sysUtt += " a";
						sysUtt += " " + cpt +".";
					}
					break;
				case Check:
					if(request.containsKey("ontology")){
						
						logger.info("request: " + request);
						
						
						VisualOntology onlotoly = (VisualOntology)request.get("ontology");
						logger.info("----------------VisualOntology: " + onlotoly);
						if(request.containsKey("concept")){
							String cpt = (String)request.get("concept");
							String targetCpt = visualCpt.get(onlotoly);
							if(targetCpt.equals(cpt)){
								sysUtt = "yes.";
							}
							else{
								int num1 = randInt(0, stateBegins.length-1);
								sysUtt = "no ";
								sysUtt += stateBegins[num1];
								
								if(onlotoly.equals(VisualOntology.full) || onlotoly.equals(VisualOntology.shape))
									sysUtt += " a";
								sysUtt += " " + targetCpt +".";
							}
						}
					}
					else
						logger.info("-------- no ontology!! --------");
					break;
				case Clarify:
					break;
				}
			}
		}
		else
			logger.error("Map<VisualOntology, String> is null!!");
		
		return sysUtt;
	}
	
	public String generateQuestion(){
		String question = null;
		if(questionMap != null && !questionMap.isEmpty()){
			int index = randInt(0,ontologyList.length-1);
			VisualOntology ontology = ontologyList[index];
			if(questionMap.containsKey(ontology)){
				Set<String> questions = questionMap.get(ontology);
				
				List<String> flusList = flush(questions);
				question = flusList.get(0);
				
				if(ontology.equals(VisualOntology.full))
					questionMap.clear();
				else
					questionMap.remove(ontology);
					
			}
			else
				question = generateQuestion();
		}
		return question;
	}
	
	public String generateStatement(String requestAttr) {
		String statement = null;
		
		if(requestAttr != null){
			if(stateSet != null && !stateSet.isEmpty()){
				int index = randInt(0,stateSet.size()-1);
				String[] states = new String[stateSet.size()];
				states = stateSet.toArray(states);
				statement = states[index];
				
				if(!statement.contains(requestAttr))
					return generateStatement(requestAttr);
			}
			
			logger.debug("Statement: " + statement);
		}
		else{
			int num1 = randInt(0, stateBegins.length-1);
			return stateBegins[num1] + " a " + visualCpt.get(VisualOntology.full) + ".";
		}
		return statement;
	}
	
	/************************************** Initial NLP Templates ***********************************/
	private VisualOntology[] ontologyList = new VisualOntology[]{VisualOntology.color, VisualOntology.shape};
	Map<VisualOntology, Set<String>> questionMap;
	private void initialQuestion(){
		if(questionMap == null)
			questionMap = new HashMap<VisualOntology, Set<String>>();
		else
			questionMap.clear();
		
		for(int i = 0; i < ontologyList.length; i++){
			VisualOntology ontology = ontologyList[i];
			
			Set<String> questionSet = null; 
			if(questionMap.containsKey(ontology))
				questionSet = questionMap.get(ontology);
			else{
				questionSet = new HashSet<String>();
				questionMap.put(ontology, questionSet);
			}
			
			switch(ontology){
				case color:
//					questionSet.add("is it " + visualCpt.get(VisualOntology.color) + "?");
//					questionSet.add("is this " + visualCpt.get(VisualOntology.color) + "?");
					questionSet.add("what color is this?");
//					questionSet.add("what color is it?");
					break;
				case full:
//					questionSet.add("is this " + "a " + visualCpt.get(VisualOntology.full) + "?");
//					questionSet.add("is it " + "a " + visualCpt.get(VisualOntology.full) + "?");
					break;
				case shape:
//					questionSet.add("is it " + "a " + visualCpt.get(VisualOntology.shape) + "?");
//					questionSet.add("is this " + "a " + visualCpt.get(VisualOntology.shape) + "?");
					questionSet.add("what shape is this?");
//					questionSet.add("what shape is it?");
					questionSet.add("what is this?");
//					questionSet.add("what is it?");
					break;
			}
		}
	}
	
	private List<String> stateSet = null;
	private void inisitalStatement() {
		if(visualCpt != null){
			
			if(stateSet == null)
				stateSet = new ArrayList<String>();
			else
				stateSet.clear();
			
			int num1 = randInt(0, stateBegins.length-1);
			stateSet.add(stateBegins[num1] + " " + visualCpt.get(VisualOntology.color) + ".");
			stateSet.add(stateBegins[num1] + " a " + visualCpt.get(VisualOntology.full) + ".");
			stateSet.add(stateBegins[num1] + " a " + visualCpt.get(VisualOntology.shape) + ".");
		}
		
	}
	
	private int randInt(int min, int max) {
	    Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;
	    return randomNum;
	}
	
	private List<String> flush(Set<String> collection){
		if(collection != null && !collection.isEmpty()){
			List<String> clone = new ArrayList<String>();
			clone.addAll(collection);
		    Collections.shuffle(clone);
		    return clone;
		}
		return null;
	}

}
