package babble.simulation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import babble.dialog.rl.TTRMDPStateEncoding;
import babble.dialogue.DSDialogueAgent;
import babble.dialogue.IncrementalDialogueAgent;
import csli.util.Pair;
import qmul.ds.Dialogue;
import qmul.ds.InteractiveContextParser;
import qmul.ds.Utterance;
import qmul.ds.babi.BabiDialogue;
import qmul.ds.dag.UtteredWord;


public class SemanticsBasedSimulatedDialogueAgent extends IncrementalDialogueAgent {
	public static final String DOMAIN_FOLDER = TTRMDPStateEncoding.DOMAIN_FOLDER;
	public static final String GRAMMAR_FOLDER = TTRMDPStateEncoding.GRAMMAR_FOLDER;
	public static final String TEMPLATES_FILE_PREFIX = "dialogue_semantics";

	protected static Logger logger = Logger.getLogger(SemanticsBasedSimulatedDialogueAgent.class);

	// not too good to have Lists as keys in general, but they are Collection.unmodifiableList's here
	protected Map<List<Integer>, List<UtteranceTemplate>> groundedSemanticTemplates;
	protected Map<List<Integer>, List<UtteranceTemplate>> pendingSemanticTemplates;
	protected List<List<Integer>> usedSemanticTemplates = new ArrayList<>();
	protected Map<String, List<String>> slotValues;

	protected boolean allowReuse = false;
	protected Queue<UtteredWord> toUtter = new LinkedList<>();
	//the current Template being used that has given rise to the current toUtter sequences of words
	protected List<Integer> lastSemanticTemplateUsed = null;
	protected Random rand = new Random(new Date().getTime());
	protected InteractiveContextParser parser;

	private TTRMDPStateEncoding stateEncoder;

	public SemanticsBasedSimulatedDialogueAgent(String name) {
		super(name);
	}

	public SemanticsBasedSimulatedDialogueAgent(
		String name,
		String domain,
		String grammarResourceName,
		TTRMDPStateEncoding inEncoder
	) throws IOException {
		this(name);
		slotValues = TTRMDPStateEncoding.loadSlotValuesOfDomain(domain);
		parser = new InteractiveContextParser(
			String.join(File.separator, new String[]{GRAMMAR_FOLDER, grammarResourceName}),
			"usr",
			"sys"
		);
		parser.init();
		stateEncoder = inEncoder;
		Pair<Map<List<Integer>, List<UtteranceTemplate>>, Map<List<Integer>, List<UtteranceTemplate>>> allTemplates =
			loadTemplates(domain);
		groundedSemanticTemplates = allTemplates.a;
		pendingSemanticTemplates = allTemplates.b;
	}

	public static void main(String[] args) throws IOException {
		String
			domain = "restaurant-search",
			grammarResource = "2016-english-ttr-restaurant-search";
		List<Dialogue> dialogues = BabiDialogue.loadDialoguesFromFile(DOMAIN_FOLDER + "/dialogues");
		TTRMDPStateEncoding stateEncoder = new TTRMDPStateEncoding(domain, grammarResource);
		generateTemplatesForBothSides(dialogues, domain,  grammarResource, stateEncoder);
	}

	public static void generateTemplatesForBothSides(
		List<Dialogue> inDialogues,
		String inDomain,
		String inGrammarResource,
		TTRMDPStateEncoding inEncoder
	) throws IOException {
		generateTemplates(
			inDialogues,
			String.join(File.separator, new String[]{DOMAIN_FOLDER, inDomain, "dialogue_semantics.sys.grounded"}),
			inDomain,
			inGrammarResource,
			inEncoder,
			"sys",
			true
		);
		generateTemplates(
			inDialogues,
			String.join(File.separator, new String[]{DOMAIN_FOLDER, inDomain, "dialogue_semantics.sys.pending"}),
			inDomain,
			inGrammarResource,
			inEncoder,
			"sys",
			false
		);
		generateTemplates(
			inDialogues,
			String.join(File.separator, new String[]{DOMAIN_FOLDER, inDomain, "dialogue_semantics.usr.grounded"}),
			inDomain,
			inGrammarResource,
			inEncoder,
			"usr",
			true
		);
		generateTemplates(
			inDialogues,
			String.join(File.separator, new String[]{DOMAIN_FOLDER, inDomain, "dialogue_semantics.usr.pending"}),
			inDomain,
			inGrammarResource,
			inEncoder,
			"usr",
			false
		);
		logger.info(String.format("succesfully loaded templates"));
	}

	public static Map<List<Integer>, List<String>> extractTemplatesFromDialogue(
		Dialogue inDialogue,
		InteractiveContextParser inParser,
		TTRMDPStateEncoding inEncoder,
		String inAgentName,
		boolean grounded
	) {
		if (!inDialogue.get(inDialogue.size() - 1).getSpeaker().equals(inAgentName)) {
			inDialogue.add(new Utterance(inAgentName, "bye"));
		}

		inParser.init();
		Map<List<Integer>, List<String>> result = new HashMap<>();
		List<Integer> lastState = inEncoder.encode(inParser.getContext());
		for (Utterance turn: inDialogue) {
			if (turn.getSpeaker().equals(inAgentName)) {
				if (!result.containsKey(lastState)) {
					result.put(lastState, new ArrayList<>());
				}
				result.get(Collections.unmodifiableList(new ArrayList<>(lastState))).add(turn.getText());
			}
			for (UtteredWord word: turn.getWords()) {
				if (word.word().equals(Utterance.RELEASE_TURN_TOKEN) && !grounded) {
					lastState = inEncoder.encode(inParser.getContext());
				}
				inParser.parseWord(word);
				if (word.word().equals(Utterance.RELEASE_TURN_TOKEN) && grounded) {
					lastState = inEncoder.encode(inParser.getContext());
				}
			}
		}
		return result;
	}

	public static void generateTemplates(
		List<Dialogue> inDialogues,
		String dstFile,
		String domain,
		String grammarResource,
		TTRMDPStateEncoding inStateEncoder,
		String simulatedAgentName,
		boolean grounded
	) throws IOException {
		Map<List<Integer>, Set<String>> templates = new HashMap<>();
		Map<String, List<String>> slotValues = TTRMDPStateEncoding.loadSlotValuesOfDomain(domain);
		InteractiveContextParser parser = new InteractiveContextParser(
			String.join(File.separator, new String[]{GRAMMAR_FOLDER, grammarResource}),
			"sys",
			"usr"
		);
		for (Dialogue dialogue: inDialogues) {
			Map<List<Integer>, List<String>> currentTemplates = extractTemplatesFromDialogue(
				dialogue,
				parser,
				inStateEncoder,
				simulatedAgentName,
				grounded
			);
			for (Map.Entry<List<Integer>, List<String>> entry: currentTemplates.entrySet()) {
				if (!templates.containsKey(entry.getKey())) {
					templates.put(entry.getKey(), new HashSet<>());
				}
				templates.get(entry.getKey()).addAll(entry.getValue());
			}
		}
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(dstFile)));

		for (Map.Entry<List<Integer>, Set<String>> entry: templates.entrySet()) {
			out.write(entry.getKey().toString());
			out.write("\t");
			out.write(String.join("\t", entry.getValue()));
			out.newLine();
		}
		out.close();
	}

	public Pair<Map<List<Integer>, List<UtteranceTemplate>>, Map<List<Integer>, List<UtteranceTemplate>>> loadTemplates(String domain) throws IOException {
		String groundedTemplatesFileName = String.join(File.separator, new String[]{
			DOMAIN_FOLDER,
			domain,
			TEMPLATES_FILE_PREFIX + "." + name.toLowerCase() + "." + "grounded"
		});
		String pendingTemplatesFileName = String.join(File.separator, new String[]{
			DOMAIN_FOLDER,
			domain,
			TEMPLATES_FILE_PREFIX + "." + name.toLowerCase() + "." + "pending"
		});
		Map<List<Integer>, List<UtteranceTemplate>>
			groundedTemplates = loadTemplatesFile(groundedTemplatesFileName),
			pendingTemplates = loadTemplatesFile(pendingTemplatesFileName);
		return new Pair<>(groundedTemplates, pendingTemplates);
	}

	public Map<List<Integer>, List<UtteranceTemplate>> loadTemplatesFile(String inFileName) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(new File(inFileName)));
		String line;

		Map<List<Integer>, List<UtteranceTemplate>> result = new HashMap<>();
		while ((line = in.readLine()) != null) {
			if (line.isEmpty() || line.trim().startsWith("//"))
				continue;

			String[] lineParts = line.split("\t");
			assert 1 < lineParts.length: "Invalid template line: \"" + line.trim() + "\"";

			List<Integer> state = parseStateFromString(lineParts[0].trim());
			if (!result.containsKey(state)) {
				result.put(
					Collections.unmodifiableList(new ArrayList<>(state)),
					new ArrayList<UtteranceTemplate>()
				);
			}
			for (int index = 1; index != lineParts.length; ++index) {
				String answer = lineParts[index].trim().toLowerCase();
				result.get(state).add(new UtteranceTemplate(answer, slotValues));
			}
		}
		in.close();
		return result;
	}

	private static List<Integer> parseStateFromString(String inString) {
		List<Integer> result = new ArrayList<>();
		String listString = inString.substring(1, inString.length() - 1); // chop off brackets
		StringTokenizer tokenizer = new StringTokenizer(listString, ",");
		while (tokenizer.hasMoreTokens()) {
			result.add(Integer.parseInt(tokenizer.nextToken().trim()));
		}
		return Collections.unmodifiableList(result);
	}

	private boolean containsContext(List<Integer> target, Collection<List<Integer>> contexts, boolean exact) {
		boolean result = false;
		for (List<Integer> candidate: contexts) {
			// for partial match in the middle of a turn, only check that the clause part makes some sense
			result = isSubsumed(stateEncoder.getPendingSubstate(target), stateEncoder.getPendingSubstate(candidate));
			result =
				result && (
				!exact || (
					isSubsumed(stateEncoder.getPendingSubstate(candidate), stateEncoder.getPendingSubstate(target)) &&
					isSubsumed(stateEncoder.getGoalSubstate(target), stateEncoder.getGoalSubstate(candidate)) &&
					isSubsumed(stateEncoder.getGoalSubstate(candidate), stateEncoder.getGoalSubstate(target))
				)
			);
			if (result) {
				break;
			}
		}
		if (!result) {
			return result;
		}
		return result;
	}

	private boolean isSubsumed(List<Integer> subsuming, List<Integer> subsumed) {
		assert subsuming.size() == subsumed.size() : "Incompatible arguments for the 'isSubsumed' operation";
		BitSet
			subsumingBits = TTRMDPStateEncoding.stateToBitSet(subsuming),
			subsumedBits = TTRMDPStateEncoding.stateToBitSet(subsumed);
		BitSet intersection = (BitSet) subsumingBits.clone();
		intersection.and(subsumedBits);
		return intersection.equals(subsumingBits);
	}

	private List<List<Integer>> findContexts(
		List<Integer> target,
		Collection<List<Integer>> contexts,
		boolean exactMatch
	) {
		List<List<Integer>> result = new ArrayList<>();
		for (List<Integer> candidate: contexts) {
			if (isSubsumed(target, candidate)) {
				if (!exactMatch || isSubsumed(candidate, target)) {
					result.add(candidate);
				}
			}
		}
		return result;
	}

	public UtteredWord nextWord() {
		Utterance lastUtterance = this.dialogue_env.getLastUtterance();

		if (this.dialogue_env.hasFloor(this)) {
			// I have the floor, am I finished?
			logger.debug("Simulator has the floor");
			if (this.toUtter.isEmpty()) {
				logger.debug("Nothing more to say");
				if (lastSemanticTemplateUsed != null) {
					this.usedSemanticTemplates.add(lastSemanticTemplateUsed);
				}

				lastSemanticTemplateUsed = null;
				return new UtteredWord(DSDialogueAgent.RELEASE_TURN, this.name);
			} else {
				return toUtter.peek();
			}
		} else if (this.dialogue_env.floorIsOpen()) {
			// the floor is open.
			logger.debug("Floor is open");
			// I'm the last speaker
			if (lastUtterance != null && lastUtterance.getSpeaker().equals(name)) {
				// floor is open and I'm the last speaker. So I must have opened it.
				// The other only generated <wait> action.
				logger.debug("I'm the last speaker and floor is open");

				if (fullyMatches()) {
					List<List<Integer>> matches = findMatchesToCurrentState(true);
					lastSemanticTemplateUsed = matches.isEmpty() ? null : matches.get(0);
					List<UtteranceTemplate> responseTemps = groundedSemanticTemplates.get(lastSemanticTemplateUsed);
					Utterance response = responseTemps.get(rand.nextInt(responseTemps.size())).getRandomUtterance(name);
					this.toUtter.clear();// TODO: this is dangerous! Having to do this is bad.
					this.toUtter.addAll(response.getWords());
					return toUtter.peek();
				} else{
					return new UtteredWord(DSDialogueAgent.BYE, this.name);
				}
			}

			// if we are here, last utt is from the other, and floor is open.
			if (fullyMatches()) {
				logger.debug("Floor is open with full recognisable utterance from the other");
				List<List<Integer>> matches = findMatchesToCurrentState(true);
				lastSemanticTemplateUsed = matches.isEmpty() ? null : matches.get(0);
				List<UtteranceTemplate> responseTemps = groundedSemanticTemplates.get(lastSemanticTemplateUsed);
				Utterance response = responseTemps.get(rand.nextInt(responseTemps.size())).getRandomUtterance(name);
				this.toUtter.clear();// TODO: this is dangerous! Having to do this is bad.
				this.toUtter.addAll(response.getWords());
				return toUtter.peek();
			} else {
				logger.debug("Other has released floor, but his utterance not recognisable");

				// invalid utterence by other. signal leaving of dialogue
				return new UtteredWord(DSDialogueAgent.BYE, this.name);
			}

		} else {
			if (lastUtterance.getText().endsWith("let")) {
				System.out.println("1");
			}
			logger.debug("Other has the floor. Is his utterance so far recognisable?");
			if (partiallyMatches()) {
				logger.debug("yes");
				return new UtteredWord(DSDialogueAgent.WAIT, this.name);
			} else {
				logger.debug("no! Leaving dialogue");
				return new UtteredWord(DSDialogueAgent.BYE, this.name);
			}
		}
	}

	@Override
	public void wordUttered(UtteredWord w) {
		parser.parseWord(w);

		if (!w.speaker().equals(name)) {
			return;
		}

		if (w.word().equals(DSDialogueAgent.BYE) || w.word().equals(DSDialogueAgent.RELEASE_TURN)) {
			return;
		}

		logger.debug("now removing word from queue:" + w);
		if (!toUtter.peek().equals(w)) {
			throw new IllegalStateException("top of queue:" + toUtter.peek() + " but got:" + w);
		}
		toUtter.remove();
	}

	public boolean fullyMatches() {
		List<Integer> state = stateEncoder.encode(parser.getContext());
		return containsContext(state, groundedSemanticTemplates.keySet(), true);
	}

	public boolean partiallyMatches() {
		boolean result = false;
		do {
			List<Integer> state = stateEncoder.encode(parser.getContext());
			result = containsContext(state, pendingSemanticTemplates.keySet(), false);

			if (result) {
				break;
			}
		} while (parser.parse());
		parser.getState().resetToFirstTupleAfterLastWord();
		return result;
	}

	public List<List<Integer>> findMatchesToCurrentState(boolean exactMatch) {
		List<Integer> state = stateEncoder.encode(parser.getContext());
		Map<List<Integer>, List<UtteranceTemplate>> templatesMap = 
			exactMatch
			? groundedSemanticTemplates
			: pendingSemanticTemplates;
		List<List<Integer>> candidates = findContexts(state, templatesMap.keySet(), exactMatch);

		if (!this.allowReuse) {
			candidates.removeIf(p -> usedSemanticTemplates.contains(p));
		}

		return candidates.isEmpty() ? null : candidates;
	}

	@Override
	public void reset() {
		usedSemanticTemplates.clear();
	}
}