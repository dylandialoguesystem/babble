package babble.simulation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;

import babble.dialog.rl.TTRMDPStateEncoding;
import qmul.ds.Utterance;
import qmul.ds.dag.UtteredWord;
/**
 * An utterance template, with (slot) variables, to be preceded with '$' e.g. "I want a $brand $product" 
 * @author Arash
 *
 */
public class UtteranceTemplate{
	
	public static final String VAR_PREFIX="$";
	protected Random rand=new Random(new Date().getTime());
	protected Logger logger=Logger.getLogger(UtteranceTemplate.class); 
	
	protected Map<String, List<String>> slot_values=new HashMap<String, List<String>>();
	
	/**
	 * the words
	 */
	protected List<String> template=new ArrayList<String>();
	
	
	public UtteranceTemplate(String template, String domain)
	{
		
	
		this.template=tokenize(template);
		
		if (domain==null)
			return;
		try{
			this.slot_values=TTRMDPStateEncoding.loadSlotValuesOfDomain(domain);
		} catch(IOException e)
		{
			e.printStackTrace();
		}
		
	}
	
	public UtteranceTemplate(String template, Map<String, List<String>> slot_values)
	{
		
	
		this.template=tokenize(template);
		
		this.slot_values=slot_values;
		
	}
	
	
	
	
	private List<String> tokenize(String template)
	{
		List<String> templateWords=new ArrayList<String>();
		String[] words=template.split(" ");
		for(String wordd:words)
		{
			String word=wordd.trim();
			
			if (word.length()>1&&(word.charAt(word.length()-1)=='?'||word.charAt(word.length()-1)=='!'||word.charAt(word.length()-1)=='.'))
			{
				templateWords.add(word.substring(0, word.length()-1));
				templateWords.add(word.substring(word.length()-1, word.length()));
			}
			else
				templateWords.add(word);
		}
		
		return templateWords;
		
	}
	
	
	
	
	
	
	public boolean equals(Object o)
	{
		if (!(o instanceof UtteranceTemplate))
			return false;
		
		UtteranceTemplate other=(UtteranceTemplate)o;
		
		return this.template.equals(other.template)&&this.slot_values.equals(other.slot_values);
	}
	
	public Utterance getRandomUtterance(String speaker)
	{
		
		
		Utterance utt=new Utterance();
		utt.setSpeaker(speaker);
		for(String word:this.template)
		{
			if (word.startsWith(VAR_PREFIX))
			{
				String key=word.substring(VAR_PREFIX.length(), word.length());
				
				List<String> values=this.slot_values.get(key);
				
				
				utt.append(new UtteredWord(values.get(rand.nextInt(values.size())), speaker));
			}
			else
				utt.append(new UtteredWord(word, speaker));
		}
		
		return utt;
		
	}
	
	public static void main(String[] args)
	{
		
		
	}
	
	public String toString()
	{
		String s="";
		for(String word: template)
		{
			s+=word+" ";
		}
		
		return s.substring(0, s.length()-1);
	}
	

}
