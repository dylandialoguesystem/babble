package babble.simulation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;

import org.apache.log4j.Logger;

import babble.dialog.rl.TTRMDPStateEncoding;
import babble.dialogue.DSDialogueAgent;
import babble.dialogue.IncrementalDialogueAgent;
import qmul.ds.Utterance;
import qmul.ds.dag.UtteredWord;

public class TemplateBasedSimulatedDialogueAgent extends IncrementalDialogueAgent {

	public static final String DOMAIN_FOLDER = TTRMDPStateEncoding.DOMAIN_FOLDER;
	public static final String TEMPLATES_FILE = "simulator-templates";

	protected Logger logger = Logger.getLogger(TemplateBasedSimulatedDialogueAgent.class);

	protected Map<String, List<UtteranceTemplate>> templates;
	protected List<String> usedTemplates;
	protected Map<String, List<String>> slot_values;

	protected boolean allow_reuse = false;
	protected Queue<UtteredWord> toUtter = new LinkedList<UtteredWord>();
	protected String curTemplate;//the current Template being used that has given rise to the current toUtter sequences of words.
	protected Random rand = new Random(new Date().getTime());

	public TemplateBasedSimulatedDialogueAgent(String name) {
		super(name);

	}

	public TemplateBasedSimulatedDialogueAgent(String name, String domain) throws IOException {
		this(name);
		this.slot_values = TTRMDPStateEncoding.loadSlotValuesOfDomain(domain);
		logger.info("loaded slot values:" + slot_values);
		templates = new HashMap<String, List<UtteranceTemplate>>();
		usedTemplates = new ArrayList<String>();
		loadTemplates(domain);

	}

	private void loadTemplates(String domain) throws IOException {

		BufferedReader in = new BufferedReader(new FileReader(new File(DOMAIN_FOLDER + domain + "/" + TEMPLATES_FILE)));

		String line;

		while ((line = in.readLine()) != null) {
			if (line.isEmpty())
				continue;
			else if (line.trim().startsWith("//"))
				continue;
				
			// splitting: previosu utterance -> template
			String[] uttTemplate = line.split("->");

			if (uttTemplate.length != 2) {
				in.close();
				throw new IllegalStateException("Bad line in templates file:" + line);
			}
			String utt = uttTemplate[0].trim().toLowerCase();
			String[] templates = uttTemplate[1].split(";");// assumes templates
															// separated by ';'.
															// See the templates
															// file.
			List<UtteranceTemplate> temps = new ArrayList<UtteranceTemplate>();
			for (String temp : templates) {
				UtteranceTemplate template = new UtteranceTemplate(temp.trim(), slot_values);
				temps.add(template);
				logger.debug("found template: " + template);

			}

			this.templates.put(utt, temps);
			logger.info("loaded response templates to: " + utt);

		}

		in.close();
	}

	/*
	 * public UtteredWord nextWordTestTime() { Utterance lastUtterance =
	 * this.dialogue_env.getLastUtterance(); logger.debug("last utterance: "
	 * +lastUtterance); if (lastUtterance==null) return new
	 * UtteredWord(DSDialogueAgent.RELEASE_TURN, this.name);
	 * 
	 * 
	 * String lastUtt = lastUtterance.getText().toLowerCase(); logger.debug(
	 * "last utt as text: "+lastUtt); if (this.dialogue_env.hasFloor(this)) { //
	 * I have the floor, am I finished? logger.debug("I have the floor"); if
	 * (this.toUtter.isEmpty()) { logger.debug("Nothing more to say"); return
	 * new UtteredWord(DSDialogueAgent.RELEASE_TURN, this.name); } else {
	 * 
	 * return toUtter.poll(); } } else if (this.dialogue_env.floorIsOpen()) {
	 * //the floor is open. logger.debug("Floor is open"); if
	 * (lastUtterance.getSpeaker().equals(this.name))//I'm the last speaker {
	 * //floor is open and I'm the last speaker. So I must have opened it. //why
	 * did the other not say anything? logger.debug(
	 * "I'm the last speaker, I'm not going to do anything");
	 * 
	 * return new UtteredWord(DSDialogueAgent.RELEASE_TURN, this.name); } //if
	 * we are here, last utt is from the other. if
	 * (this.templates.containsKey(lastUtt)) { logger.debug(
	 * "Recognisable utterance from the other"); List<UtteranceTemplate>
	 * responseTemps=this.templates.get(lastUtt); Utterance
	 * response=responseTemps.get(rand.nextInt(responseTemps.size())).
	 * getRandomUtterance(name); this.toUtter.addAll(response.getWords());
	 * return toUtter.poll(); } else { for (String temp :
	 * this.templates.keySet()) { if (temp.startsWith(lastUtt)) return new
	 * UtteredWord(DSDialogueAgent.RELEASE_TURN, this.name); } // invalid
	 * utterence by other. signal leaving of dialogue return new
	 * UtteredWord(DSDialogueAgent.BYE, this.name); }
	 * 
	 * } else { logger.debug("Other has the floor. I'll just wait."); return new
	 * UtteredWord(DSDialogueAgent.RELEASE_TURN, this.name); }
	 * 
	 * 
	 * 
	 * 
	 * }
	 */
	
	@Override
	public UtteredWord nextWord() {
		Utterance lastUtterance = this.dialogue_env.getLastUtterance();
		// logger.debug("last utterance: "+lastUtterance);

		String lastUtt = lastUtterance == null ? "" : lastUtterance.getText().toLowerCase();
		// logger.debug("last utt as text: "+lastUtt);
		if (this.dialogue_env.hasFloor(this)) {
			// I have the floor, am I finished?
			logger.debug("Simulator has the floor");
			if (this.toUtter.isEmpty()) {
				logger.debug("Nothing more to say");
				if (curTemplate!=null)
					this.usedTemplates.add(curTemplate);
				
				curTemplate=null;
				return new UtteredWord(DSDialogueAgent.RELEASE_TURN, this.name);
			} else {

				return toUtter.peek();
			}
		} else if (this.dialogue_env.floorIsOpen()) {
			// the floor is open.
			logger.debug("Floor is open");
			if (lastUtterance != null && lastUtterance.getSpeaker().equals(name))// I'm
																					// the
																					// last
																					// speaker
			{
				// floor is open and I'm the last speaker. So I must have opened
				// it.
				// why did the other not say anything?
				// The other has generated erroneous <wait> action.
				logger.debug("I'm the last speaker and floor is open. Other should have said something");

				return new UtteredWord(DSDialogueAgent.BYE, this.name);
			}

			// if we are here, last utt is from the other, and floor is open.
			if (fullyMatches(lastUtt)) {
				logger.debug("Floor is open with full recognisable utterance from the other");
				curTemplate=lastUtt;
				List<UtteranceTemplate> responseTemps = this.templates.get(lastUtt);
				Utterance response = responseTemps.get(rand.nextInt(responseTemps.size())).getRandomUtterance(name);
				this.toUtter.clear();// TODO: this is dangerous! Having to do
										// this is bad.
				this.toUtter.addAll(response.getWords());
				return toUtter.peek();
			} else {
				logger.debug("Other has released floor, but his utterance not recognisable");

				// invalid utterence by other. signal leaving of dialogue
				return new UtteredWord(DSDialogueAgent.BYE, this.name);
			}

		} else {
			logger.debug("Other has the floor. Is his utterance so far recognisable?");
			if (partiallyMatches(lastUtt))
			{
				logger.debug("yes");
				return new UtteredWord(DSDialogueAgent.WAIT, this.name);
			}
			else
			{
				logger.debug("no! Leaving dialogue");
				return new UtteredWord(DSDialogueAgent.BYE, this.name);		
			}
				
			
		}

	}

	@Override
	public void wordUttered(UtteredWord w) {
		if (!w.speaker().equals(name))
			return;

		if (w.word().equals(DSDialogueAgent.BYE) || w.word().equals(DSDialogueAgent.RELEASE_TURN))
			return;

		logger.debug("now removing word from queue:" + w);
		if (!toUtter.peek().equals(w))
			throw new IllegalStateException("top of queue:" + toUtter.peek() + " but got:" + w);

		toUtter.remove();
		
		

	}

	public boolean fullyMatches(String lastUtt)
	{
		if (this.allow_reuse)
			return this.templates.containsKey(lastUtt);
		
		return this.templates.containsKey(lastUtt)&&!this.usedTemplates.contains(lastUtt);
	}
	
	public boolean partiallyMatches(String lastUtt)
	{
		for (String temp : this.templates.keySet()) {
			if (!this.allow_reuse&&usedTemplates.contains(temp))
				continue;
			
			if (temp.startsWith(lastUtt)) {
				return true;
			}
		}
		
		return false;
		
	}
	
	@Override
	public void reset() {
		toUtter.clear();
		this.usedTemplates.clear();
		this.curTemplate=null;

	}

}
