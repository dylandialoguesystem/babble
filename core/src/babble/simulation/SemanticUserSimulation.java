package babble.simulation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import babble.dialog.rl.ContextState;
import babble.dialog.rl.TTRMDPStateEncoding;
import babble.dialogue.DSDialogueAgent;
import babble.dialogue.IncrementalDialogueAgent;
import qmul.ds.InteractiveContextParser;
import qmul.ds.Utterance;
import qmul.ds.babi.BabiDialogue;
import qmul.ds.dag.UtteredWord;
import qmul.ds.formula.TTRRecordType;


public class SemanticUserSimulation extends IncrementalDialogueAgent {
	public static final String DOMAIN_FOLDER = TTRMDPStateEncoding.DOMAIN_FOLDER;
	public static final String GRAMMAR_FOLDER = TTRMDPStateEncoding.GRAMMAR_FOLDER;
	public static final String TEMPLATES_FILE_PREFIX = "dialogue_semantics";

	public static final int MAX_UTTERANCE_LENGTH=12;
	protected static Logger logger = Logger.getLogger(SemanticUserSimulation.class);

	// not too good to have Lists as keys in general, but they are Collection.unmodifiableList's here
	protected Map<List<Integer>, List<UtteranceTemplate>> triggerTemplates;
	protected Map<List<Integer>, List<TTRRecordType>> monitorTemplates;
	protected List<List<Integer>> usedSemanticTemplates = new ArrayList<>();
	protected Map<String, List<String>> slotValues;

	protected boolean allowReuse = false;
	protected Queue<UtteredWord> toUtter = new LinkedList<>();
	//the current Template being used that has given rise to the current toUtter sequences of words
	protected List<Integer> lastSemanticTemplateUsed = null;
	protected Random rand = new Random(new Date().getTime());
	protected InteractiveContextParser parser;

	protected List<Integer> previousGoalState;
	protected TTRMDPStateEncoding stateEncoder;

	public SemanticUserSimulation(String name) {
		super(name);
	}

	public SemanticUserSimulation(
		String name,
		String domain,
		String grammarResourceName
	) throws IOException {
		this(name);
		slotValues = TTRMDPStateEncoding.loadSlotValuesOfDomain(domain);
		parser = new InteractiveContextParser(
			String.join(File.separator, new String[]{GRAMMAR_FOLDER, grammarResourceName}),
			"usr",
			"sys"
		);
		
		
		stateEncoder = new TTRMDPStateEncoding(domain);
		reset();
		loadTemplates(domain);
	}

	public static void main(String[] args) throws IOException {
		String domain = "restaurant-search-ca";
		String grammar = "2017-english-ttr-restaurant-search-ca";
		generateTemplates(
			String.join(File.separator, new String[]{DOMAIN_FOLDER, domain, TTRMDPStateEncoding.TRAINING_DIALOGUE_FILE_NAME}),
			String.join(File.separator, new String[]{DOMAIN_FOLDER, domain, "dialogue_semantics.sys.trigger"}),
			domain,
			grammar,
			"sys",
			"trigger"
		);
		/*
		generateTemplates(
			String.join(File.separator, new String[]{DOMAIN_FOLDER, domain, TTRMDPStateEncoding.TRAINING_DIALOGUE_FILE_NAME}),
			String.join(File.separator, new String[]{DOMAIN_FOLDER, domain, "dialogue_semantics.sys.monitor"}),
			domain,
			grammar,
			"sys",
			"monitor"
		);
		generateTemplates(
			String.join(File.separator, new String[]{DOMAIN_FOLDER, domain, TTRMDPStateEncoding.TRAINING_DIALOGUE_FILE_NAME}),
			String.join(File.separator, new String[]{DOMAIN_FOLDER, domain, "dialogue_semantics.usr.trigger"}),
			domain,
			grammar,
			"usr",
			"trigger"
		);
		generateTemplates(
			String.join(File.separator, new String[]{DOMAIN_FOLDER, domain, TTRMDPStateEncoding.TRAINING_DIALOGUE_FILE_NAME}),
			String.join(File.separator, new String[]{DOMAIN_FOLDER, domain, "dialogue_semantics.usr.monitor"}),
			domain,
			grammar,
			"usr",
			"monitor"
		);*/
		logger.info(String.format("Succesfully generated templates"));
	}

	public static Map<List<Integer>, List<UtteranceTemplate>> extractTriggerTemplatesFromDialogue(
		BabiDialogue inDialogue,
		InteractiveContextParser inParser,
		TTRMDPStateEncoding inEncoder,
		Map<String, List<String>> inSlotValues,
		String inAgentName
	) {
		inParser.init();
		// this is a hack for the dialogues ending with someone else's turn
		if (!inDialogue.get(inDialogue.size() - 1).getSpeaker().equals(inAgentName)) {
			inDialogue.add(new Utterance(inAgentName, "bye"));
		}

		Map<List<Integer>, List<UtteranceTemplate>> result = new HashMap<>();
		String previousSpeaker = null;
		
		List<Integer> lastGoalState = inEncoder.getGoalSubstate(inEncoder.encode(inParser.getContext()));
		List<Integer> lastFullState = inEncoder.encode(inParser.getContext());
		logger.debug("Last Full state:"+inEncoder.prettyPrintState(lastFullState));
		lastFullState = lastFullState.subList(2, lastFullState.size());
		List<UtteredWord> toUtterBuffer = new ArrayList<>();

		for (Utterance utterance: inDialogue) {
			for (UtteredWord word: utterance.getWords()) {
				
				if (previousSpeaker == null) {
					previousSpeaker = word.speaker();
				}

				List<Integer>
					fullState = inEncoder.encode(inParser.getContext()),
					goalSubstate = inEncoder.getGoalSubstate(fullState);
					//clauseSubstate = inEncoder.getPendingSubstate(fullState);
				if (!lastGoalState.equals(goalSubstate) || !previousSpeaker.equals(word.speaker())) {
					// whether the collected words are ours
					logger.debug("either goal state changed or speaker changed (end of turn)");
					if (!toUtterBuffer.isEmpty() && toUtterBuffer.get(0).speaker().equals(inAgentName)) {
						logger.debug("Adding Entry:");
						logger.debug("Trigger State:"+lastFullState);
						if (!result.containsKey(lastFullState)) {
							result.put(Collections.unmodifiableList(new ArrayList<>(lastFullState)), new ArrayList<>());
						}
						String template = String.join(
							" ",
							toUtterBuffer.stream().map(x -> x.word()).collect(Collectors.toList())
						);
						logger.debug("To utterance:"+template);
						result.get(lastFullState).add(new UtteranceTemplate(template, inSlotValues));
					}
					toUtterBuffer.clear();
					lastFullState = fullState;
					lastFullState = lastFullState.subList(2, lastFullState.size());
					lastGoalState = goalSubstate;
				}
				logger.debug("processing word: "+word);
				inParser.parseWord(word);
				toUtterBuffer.add(word);
				previousSpeaker = word.speaker();
			}
		}
		List<Integer>
			fullState = inEncoder.encode(inParser.getContext()),
			goalSubstate = inEncoder.getGoalSubstate(fullState);
			//clauseSubstate = inEncoder.getPendingSubstate(fullState);
		if (!lastGoalState.equals(goalSubstate)) {
			// whether the collected words are ours
			if (!toUtterBuffer.isEmpty() && toUtterBuffer.get(0).speaker().equals(inAgentName)) {
				if (!result.containsKey(lastFullState)) {
					result.put(Collections.unmodifiableList(new ArrayList<>(lastFullState)), new ArrayList<>());
				}
				String template = String.join(
					" ",
					toUtterBuffer.stream().map(x -> x.word()).collect(Collectors.toList())
				);
				result.get(lastFullState).add(new UtteranceTemplate(template, inSlotValues));
			}
			toUtterBuffer.clear();
			lastFullState = fullState;
			lastFullState = lastFullState.subList(2, lastFullState.size());
			lastGoalState = goalSubstate;
		}
		return result;
	}

	public static Map<List<Integer>, List<TTRRecordType>> extractMonitorTemplatesFromDialogue(
		BabiDialogue inDialogue,
		InteractiveContextParser inParser,
		TTRMDPStateEncoding inEncoder,
		Map<String, List<String>> inSlotValues,
		String inAgentName
	) {
		inParser.init();
		if (!inDialogue.get(inDialogue.size() - 1).getSpeaker().equals(inAgentName)) {
			inDialogue.add(new Utterance(inAgentName, "bye"));
		}

		Map<List<Integer>, List<TTRRecordType>> result = new HashMap<>();
		List<Integer> lastGoalState = inEncoder.getGoalSubstate(inEncoder.encode(inParser.getContext()));
		List<Integer> EMPTY_PENDING_STATE = inEncoder.getPendingSubstate(inEncoder.encode(inParser.getContext()));
		List<Integer> previousPendingState = new ArrayList<>(EMPTY_PENDING_STATE);
		List<UtteredWord> toUtterBuffer = new ArrayList<>();
		String previousSpeaker = null;
		for (Utterance utterance: inDialogue) {
			for (UtteredWord word: utterance.getWords()) {
				if (previousSpeaker == null) {
					previousSpeaker = word.speaker();
				}
				List<Integer>
					fullState = inEncoder.encode(inParser.getContext()),
					goalSubstate = inEncoder.getGoalSubstate(fullState),
					clauseSubstate = inEncoder.getPendingSubstate(fullState);
				// we're at the end of a 'pending' segment or at the release of turn - time to generate a new template
				// boolean segmentEnd = (clauseSubstate.equals(EMPTY_PENDING_STATE) && !previousPendingState.equals(EMPTY_PENDING_STATE)) || !word.speaker().equals(previousSpeaker);
				boolean segmentEnd = (clauseSubstate.equals(EMPTY_PENDING_STATE)) || !word.speaker().equals(previousSpeaker);
				if (segmentEnd) {
					if (!toUtterBuffer.isEmpty() && toUtterBuffer.get(0).speaker().equals(inAgentName)) {
					}
					else {
						toUtterBuffer.clear();
						toUtterBuffer.add(new UtteredWord(inAgentName, Utterance.WAIT));
					}
					if (!toUtterBuffer.get(0).speaker().equals(inAgentName)) {
						if (!result.containsKey(lastGoalState)) {
							result.put(Collections.unmodifiableList(new ArrayList<>(lastGoalState)), new ArrayList<>());
						}
						result.get(lastGoalState).add(inParser.getFinalSemantics());
					}
					toUtterBuffer.clear();
				}
				lastGoalState = goalSubstate;
				previousPendingState = clauseSubstate;
				previousSpeaker = word.speaker();

				inParser.parseWord(word);
				// with this check, we can assume that toUtterBuffer is all by the same speaker
				if (!toUtterBuffer.isEmpty()) {
					assert toUtterBuffer.get(toUtterBuffer.size() - 1).speaker().equals(inAgentName);
				}
				toUtterBuffer.add(word);
			}
		}
		return result;
	}

	public static void generateTemplates(
		String srcFile,
		String dstFile,
		String domain,
		String grammarResource,
		String simulatedAgentName,
		String templateType
	) throws IOException {
		List<BabiDialogue> dialogues = BabiDialogue.loadFromBabbleFile(srcFile);
		Map<String, List<String>> slotValues = TTRMDPStateEncoding.loadSlotValuesOfDomain(domain);
		// +1 hack only for dialogues ending with a simulated side's turn
		InteractiveContextParser parser = new InteractiveContextParser(
			String.join(File.separator, new String[]{GRAMMAR_FOLDER, grammarResource}),
			"sys",
			"usr"
		);
		TTRMDPStateEncoding stateEncoder = new TTRMDPStateEncoding(domain, grammarResource);

		if (templateType.equals("trigger")) {
			Map<List<Integer>, List<UtteranceTemplate>> result = new HashMap<>();
			for (BabiDialogue dialogue: dialogues) {
				System.out.println("Processing dialogue:\n");
				System.out.println(dialogue);
				Map<List<Integer>, List<UtteranceTemplate>> currentTemplates = extractTriggerTemplatesFromDialogue(
					dialogue,
					parser,
					stateEncoder,
					slotValues,
					simulatedAgentName
				);
				for (Map.Entry<List<Integer>, List<UtteranceTemplate>> entry: currentTemplates.entrySet()) {
					if (!result.containsKey(entry.getKey())) {
						result.put(entry.getKey(), new ArrayList<>());
					}
					List<UtteranceTemplate> currentStateRules = result.get(entry.getKey());
					for (UtteranceTemplate rule: entry.getValue()) {
						if (!currentStateRules.contains(rule)) {
							currentStateRules.add(rule);
						}
					}
				}
			}
			saveTriggerTemplates(result, dstFile);
		} else if (templateType.equals("monitor")) {
			Map<List<Integer>, List<TTRRecordType>> result = new HashMap<>();
			for (BabiDialogue dialogue: dialogues) {
				Map<List<Integer>, List<TTRRecordType>> currentTemplates = extractMonitorTemplatesFromDialogue(
					dialogue,
					parser,
					stateEncoder,
					slotValues,
					simulatedAgentName
				);
				for (Map.Entry<List<Integer>, List<TTRRecordType>> entry: currentTemplates.entrySet()) {
					if (!result.containsKey(entry.getKey())) {
						result.put(entry.getKey(), new ArrayList<>());
					}
					List<TTRRecordType> currentStateRules = result.get(entry.getKey());
					for (TTRRecordType rule: entry.getValue()) {
						if (!currentStateRules.contains(rule)) {
							currentStateRules.add(rule);
						}
					}
				}
			}
			saveMonitorTemplates(result, dstFile);
		}
	}

	public static void saveTriggerTemplates(
		Map<List<Integer>, List<UtteranceTemplate>> inTemplates,
		String dstFile
	) throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(dstFile)));

		for (Map.Entry<List<Integer>, List<UtteranceTemplate>> entry: inTemplates.entrySet()) {
			out.write(entry.getKey().toString());
			out.write("\t");
			List<String> templates = entry.getValue().stream().map(x -> x.toString()).collect(Collectors.toList());
			out.write(String.join(
				"\t",
				String.join(" ", templates)
			));
			out.newLine();
		}
		out.close();
	}

	public static void saveMonitorTemplates(
		Map<List<Integer>, List<TTRRecordType>> inTemplates,
		String dstFile
	) throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(dstFile)));

		for (Map.Entry<List<Integer>, List<TTRRecordType>> entry: inTemplates.entrySet()) {
			out.write(entry.getKey().toString());
			out.write("\t");
			List<String> ttrs = entry.getValue().stream().map(x -> x.toString()).collect(Collectors.toList());
			out.write(String.join("\t", ttrs));
			out.newLine();
		}
		out.close();
	}


	private void loadTemplates(String domain) throws IOException {
		String triggerTemplatesFileName = String.join(File.separator, new String[]{
			DOMAIN_FOLDER,
			domain,
			TEMPLATES_FILE_PREFIX + "." + name.toLowerCase() + ".trigger"
		});
		String monitorTemplatesFileName = String.join(File.separator, new String[]{
			DOMAIN_FOLDER,
			domain,
			TEMPLATES_FILE_PREFIX + "." + name.toLowerCase() + ".monitor"
		});
		triggerTemplates = loadTriggerTemplatesFromFile(triggerTemplatesFileName);
		monitorTemplates = loadMonitorTemplatesFromFile(monitorTemplatesFileName);
	}


	private Map<List<Integer>, List<UtteranceTemplate>> loadTriggerTemplatesFromFile(
		String inFileName
	) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(new File(inFileName)));
		String line;

		Map<List<Integer>, List<UtteranceTemplate>> result = new HashMap<>();
		while ((line = in.readLine()) != null) {
			if (line.isEmpty() || line.trim().startsWith("//"))
				continue;

			String[] lineParts = line.split("\t");
			assert 1 < lineParts.length: "Invalid template line: \"" + line.trim() + "\"";

			List<Integer> state = parseStateFromString(lineParts[0].trim());
			if (!result.containsKey(state)) {
				result.put(
					Collections.unmodifiableList(new ArrayList<>(state)),
					new ArrayList<>()
				);
			}
			for (int index = 1; index != lineParts.length; ++index) {
				result.get(state).add(new UtteranceTemplate(lineParts[index], slotValues));
			}
		}
		in.close();
		return result;
	}

	private Map<List<Integer>, List<TTRRecordType>> loadMonitorTemplatesFromFile(
			String inFileName
		) throws IOException {
			BufferedReader in = new BufferedReader(new FileReader(new File(inFileName)));
			String line;

			Map<List<Integer>, List<TTRRecordType>> result = new HashMap<>();
			while ((line = in.readLine()) != null) {
				if (line.isEmpty() || line.trim().startsWith("//"))
					continue;

				String[] lineParts = line.split("\t");
				assert 1 < lineParts.length: "Invalid template line: \"" + line.trim() + "\"";

				List<Integer> state = parseStateFromString(lineParts[0].trim());
				if (!result.containsKey(state)) {
					result.put(
						Collections.unmodifiableList(new ArrayList<>(state)),
						new ArrayList<>()
					);
				}
				for (int index = 1; index != lineParts.length; ++index) {
					result.get(state).add(TTRRecordType.parse(lineParts[index]));
				}
			}
			in.close();
			return result;
		}


	private static List<Integer> parseStateFromString(String inString) {
		List<Integer> result = new ArrayList<>();
		String listString = inString.substring(1, inString.length() - 1); // chop off brackets
		StringTokenizer tokenizer = new StringTokenizer(listString, ",");
		while (tokenizer.hasMoreTokens()) {
			result.add(Integer.parseInt(tokenizer.nextToken().trim()));
		}
		return Collections.unmodifiableList(result);
	}


	public UtteredWord nextWord() {
		
		if (this.exceptionCaught)
		{
			return new UtteredWord(DSDialogueAgent.BYE, this.name);
		}
		
		Utterance lastUtterance = this.dialogue_env.getLastUtterance();
		
		
		if (lastUtterance!=null)
		{	
			
			
			if (lastUtterance.getWords().size()>1 && lastUtterance.getWords().get(lastUtterance.getWords().size()-1).equals(lastUtterance.getWords().get(lastUtterance.getWords().size()-2)))
			{
				logger.debug("Repeated word. Leaving Dialogue");
				return new UtteredWord(DSDialogueAgent.BYE, this.name);
			}
		
			else if (lastUtterance.getLength()>MAX_UTTERANCE_LENGTH)
			{
				logger.debug("utterance too long. Leaving Dialogue");
				return new UtteredWord(DSDialogueAgent.BYE, this.name);
			}
			
		
		}
		

		if (this.dialogue_env.hasFloor(this)) {
			// I have the floor, am I finished?
			logger.debug("Simulator has the floor");
			if (this.toUtter.isEmpty()) {
				logger.debug("End of the current segment. Trying loading another one");
				List<List<Integer>> matches = findMatchesToCurrentState();
				assert !matches.isEmpty();
				lastSemanticTemplateUsed = matches.isEmpty() ? null : matches.get(0);
				List<UtteranceTemplate> responseTemps = triggerTemplates.get(lastSemanticTemplateUsed);
				Utterance response = responseTemps.get(rand.nextInt(responseTemps.size())).getRandomUtterance(name);
				this.toUtter.addAll(response.getWords());
			}
			return toUtter.peek();
		} else if (this.dialogue_env.floorIsOpen()) {
			// the floor is open.
			logger.debug("Floor is open");
			// I'm the last speaker
			if (lastUtterance != null && lastUtterance.getSpeaker().equals(name)) {
				// floor is open and I'm the last speaker. So I must have opened it.
				// The other only generated <wait> action.
				logger.debug("I'm the last speaker and floor is open");

				if (fullyMatches()) {
					List<List<Integer>> matches = findMatchesToCurrentState();
					lastSemanticTemplateUsed = matches.isEmpty() ? null : matches.get(0);
					List<UtteranceTemplate> responseTemps = triggerTemplates.get(lastSemanticTemplateUsed);
					Utterance response = responseTemps.get(rand.nextInt(responseTemps.size())).getRandomUtterance(name);
					this.toUtter.clear();// TODO: this is dangerous! Having to do this is bad.
					this.toUtter.addAll(response.getWords());
					return toUtter.peek();
				} else{
					logger.debug("no full match");
					return new UtteredWord(DSDialogueAgent.BYE, this.name);
				}
			}

			// if we are here, last utt is from the other, and floor is open.
			if (fullyMatches()) {
				logger.debug("Floor is open with full recognisable utterance from the other");
				List<List<Integer>> matches = findMatchesToCurrentState();
				lastSemanticTemplateUsed = matches.isEmpty() ? null : matches.get(0);
				List<UtteranceTemplate> responseTemps = triggerTemplates.get(lastSemanticTemplateUsed);
				Utterance response = responseTemps.get(rand.nextInt(responseTemps.size())).getRandomUtterance(name);
				this.toUtter.clear();// TODO: this is dangerous! Having to do this is bad.
				this.toUtter.addAll(response.getWords());
				return toUtter.peek();
			} else {
				logger.debug("Other has released floor, but his utterance not recognisable");

				// invalid utterence by other. signal leaving of dialogue
				
				return new UtteredWord(DSDialogueAgent.BYE, this.name);
			}

		} else {
			if (lastUtterance!=null && lastUtterance.getText().contains("where")) {
				System.out.println(1);
			}
			logger.debug("Other has the floor. Is his utterance so far recognisable?");
			if (partiallyMatches()) {
				logger.debug("yes");
				return new UtteredWord(DSDialogueAgent.WAIT, this.name);
			} else {
				logger.debug("no! Leaving dialogue");
				return new UtteredWord(DSDialogueAgent.BYE, this.name);
			}
		}
	}
	protected boolean exceptionCaught=false;

	@Override
	public void wordUttered(UtteredWord w) {
		
		
		previousGoalState = stateEncoder.getGoalSubstate(stateEncoder.encode(parser.getContext()));
		try{
			parser.parseWord(w);
		}
		catch(Exception e)
		{
			exceptionCaught=true;
			logger.error("Exception from parser caught. bying.");
			e.printStackTrace();
		}

		if (!w.speaker().equals(name)) {
			return;
		}

		if (w.word().equals(DSDialogueAgent.BYE)) {
			return;
		}

		logger.debug("now removing word from queue:" + w);
		if (!toUtter.peek().equals(w)) {
			throw new IllegalStateException("top of queue:" + toUtter.peek() + " but got:" + w);
		}
		
		
		toUtter.remove();
	}

	protected List<UtteranceTemplate> getNearestNeighbor(List<Integer> inState) {
		List<UtteranceTemplate> result = null;
		int minDistance = 999999;
		for (List<Integer>stateCandidate: triggerTemplates.keySet()) {
			int hammingDistance = 0;
			for (int index = 0; index != inState.size(); ++index) {
				hammingDistance += Math.abs(stateCandidate.get(index) - inState.get(index));
			}
			if (hammingDistance < minDistance) {
				minDistance = hammingDistance;
				result = triggerTemplates.get(stateCandidate);
			}
		}
		return result;
	}
	
	public boolean fullyMatches() {
		List<Integer> state = stateEncoder.encode(parser.getContext());
		
		logger.debug("Checking current state against triggers:\n"+stateEncoder.prettyPrintState(state));
		state = state.subList(2, state.size());
		for (List<Integer> goalState: triggerTemplates.keySet()) {
			if (goalState.equals(state)) {
				logger.debug("Matched");
				return true;
			}
		}
		logger.debug("No match");
		return false;
	}


	public boolean partiallyMatches() {
		List<TTRRecordType> candidates = monitorTemplates.getOrDefault(previousGoalState, new ArrayList<>()); 
		List<TTRRecordType> interpretations=parser.getTopNPending(3);
		for(TTRRecordType parse: interpretations) {
			for (TTRRecordType candidate: candidates) {
				//System.out.println(parse);
				//System.out.println(candidate);
				if (parse.subsumes(candidate)) {
					return true;
				}
			}
		}
		return false;
	}


	public List<List<Integer>> findMatchesToCurrentState() {
		List<Integer> state = stateEncoder.encode(parser.getContext());
		state = state.subList(2, state.size());
		final List<Integer> stateForMapping = new ArrayList<>(state);

		List<List<Integer>> candidates = triggerTemplates.keySet()
			.stream()
			.filter(x -> x.equals(stateForMapping))
			.collect(Collectors.toList());
		if (!this.allowReuse) {
			candidates.removeIf(p -> usedSemanticTemplates.contains(p));
		}

		return candidates.isEmpty() ? null : candidates;
	}

	@Override
	public void reset() {
		toUtter.clear();
		exceptionCaught=false;
		usedSemanticTemplates.clear();
		parser.init();
		previousGoalState = stateEncoder.getGoalSubstate(stateEncoder.encode(parser.getContext()));
	}
}
