package babble.simulation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.apache.log4j.Logger;

import babble.dialogue.DSDialogueAgent;
import babble.dialogue.IncrementalDialogueAgent;
import qmul.ds.Utterance;
import qmul.ds.babi.BabiDialogue;
import qmul.ds.dag.UtteredWord;

public class SimpleReplaySimulatedDialogueAgent extends IncrementalDialogueAgent {
	protected static Logger logger = Logger.getLogger(SimpleReplaySimulatedDialogueAgent.class);

	private List<Utterance> turns = new ArrayList<>();
	private int turnCounter = 0;
	private Queue<UtteredWord> toUtter = new LinkedList<UtteredWord>();

	public SimpleReplaySimulatedDialogueAgent(String name, BabiDialogue inDialogue) {
		super(name);
		loadDialogue(inDialogue);
		populateToUtterBuffer();
	}

	public UtteredWord nextWord() {
		if (!dialogue_env.floorIsOpen() && !dialogue_env.hasFloor(name)) {
			return new UtteredWord(DSDialogueAgent.WAIT, this.name);
		}
		if (this.toUtter.isEmpty()) {
			throw new IllegalStateException("No words left to say");
		}
		return toUtter.peek();
	}

	@Override
	public void wordUttered(UtteredWord w) {
		if (!w.speaker().equals(name)) {
			return;
		}

		if (!toUtter.peek().equals(w)) {
			throw new IllegalStateException("top of queue:" + toUtter.peek() + " but got:" + w);
		}

		logger.debug("now removing word from queue:" + w);
		toUtter.remove();

		if (w.word().equals(DSDialogueAgent.BYE) || w.word().equals(DSDialogueAgent.RELEASE_TURN)) {
			++turnCounter;
			populateToUtterBuffer();
		}
	}

	@Override
	public void reset() {
		turnCounter = 0;
		toUtter.clear();
	}

	private void loadDialogue(BabiDialogue inDialogue) {
		turns.clear();
		for (Utterance turn: inDialogue) {
			if (turn.getSpeaker().equals(name)) {
				turns.add(turn);
				turns.get(turns.size() - 1).append(new UtteredWord(Utterance.RELEASE_TURN_TOKEN, name));
			}
		}
	}

	private void populateToUtterBuffer() {
		toUtter.clear();
		if (turnCounter < turns.size()) {
			toUtter.addAll(turns.get(turnCounter).getWords());
		}
		else {
			toUtter.add(new UtteredWord(DSDialogueAgent.BYE, this.name));
		}
	}
}
