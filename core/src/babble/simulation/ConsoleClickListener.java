package babble.simulation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public interface ConsoleClickListener extends ActionListener{
	public abstract void actionPerformed(ActionEvent e);
}
