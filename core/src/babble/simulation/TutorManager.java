package babble.simulation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import babble.vision.tools.Constants;
import babble.vision.tools.VSTimer;

public class TutorManager {
	Logger logger = Logger.getLogger(TutorManager.class);
	
	private int countOfObject = 0;
	private TemplateNLG nlg;
	private String prevQuestion = null;
	
	public TutorManager(){
		this.countOfObject = 1;
		this.visualCpt = new HashMap<VisualOntology, String>();
		try {
			initialOntologyMap();
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		nlg = new TemplateNLG();
	}
	
	Map<VisualOntology, String> visualCpt;
	public int formNewDialog(VisualObjRecord record) {
		visualCpt.clear();
		visualCpt.put(VisualOntology.full, record.getDescription());
		visualCpt.put(VisualOntology.color, record.getColorCpt());
		visualCpt.put(VisualOntology.shape, record.getShapeCpt());
		nlg.updateVisualMap(visualCpt);
		
		return countOfObject++;
	}
	
	public int getNumberOfObjects(){
		return this.countOfObject;
	}
	
	public String pickQuestionRandomly(){
		String question = nlg.generateQuestion();
		prevQuestion = question;
		return question;
	}
	
	public String getPreviousQestion(){
		return this.prevQuestion;
	}

	public String pickStatement(String requestAttr) {
		String statement = nlg.generateStatement(requestAttr);
		logger.debug("Statement: " + statement);
		return statement;
	}
	
	public String generateRespond(String utt) {
		JSONObject requestJson = null;
		if(utt != null && utt.length() != 0){
			if(utt.endsWith("?")){
				logger.info("question from system: " + utt);
				requestJson = formRequestFormulaFromQuestion(utt);
			}
			else if(utt.endsWith(".")){
				logger.info("statement from system: " + utt);
				requestJson = formRequestFormulaFromStatement(utt);
			}
			else
				logger.error("cannot recognize ...");
		}
		else
			logger.error("no Utterance is available..");
			
		logger.info("request Json from system: " + requestJson);
		
		String respond = nlg.generateUtt(requestJson);
		logger.info("respond from system: " + respond);
		
		return respond;
	}
	
	private JSONObject formRequestFormulaFromQuestion(String utt){
		int length = 0;
		if(utt.startsWith("what")){
			if(utt.contains("color")){
				return buildRequestJSON(RequestType.Ask, VisualOntology.color, null);
			}
			else if(utt.contains("shape")){
				return buildRequestJSON(RequestType.Ask, VisualOntology.shape, null);
			}
			else{
				return buildRequestJSON(RequestType.Ask, VisualOntology.shape, null);
			}
		}
		else if(utt.startsWith("is")){
			String tmp = "";
			if(utt.contains("it")){
				length = "it".length();
				tmp = utt.substring(utt.indexOf("it")+length, utt.indexOf("?"));
			}
			else if(utt.contains("this")){
				length = "this".length();
				tmp = utt.substring(utt.indexOf("this")+length, utt.indexOf("?"));
			}

			tmp = tmp.replace("a ", "").trim();
			String[] split = tmp.split(" ");
			VisualOntology ontology = null;
			if(split.length == 2)
				ontology = VisualOntology.full;
			else
				ontology = this.findOntologyByCpt(tmp);
			
			return buildRequestJSON(RequestType.Check, ontology, tmp);
		}
		else{
			String tmp = utt.substring(0, utt.indexOf("?"));
			tmp = tmp.replace("a ", "").trim();
			String[] split = tmp.split(" ");
			VisualOntology ontology = null;
			if(split.length == 2)
				ontology = VisualOntology.full;
			else
				ontology = this.findOntologyByCpt(tmp);
			
			return buildRequestJSON(RequestType.Check, ontology, tmp);
		}
	}
	
	private JSONObject formRequestFormulaFromStatement(String utt){
		int length = 0;
		String tmp = "";
		if(utt.startsWith("it")){
			length = "it is".length();
			tmp = utt.trim().substring(utt.indexOf("it is")+length, utt.indexOf("."));
		}
		else if(utt.startsWith("this")){
			length = "this is".length();
			tmp = utt.trim().substring(utt.indexOf("this is")+length, utt.indexOf("."));
		}
		else if(utt.startsWith("yes")){
			tmp = utt.trim().replace("yes. ", "");
			return formRequestFormulaFromStatement(tmp);
		}
		else if(utt.startsWith("no")){
			tmp = utt.trim().replace("no. ", "");
			return  formRequestFormulaFromStatement(tmp);
		}
		else
			tmp = utt.substring(0, utt.indexOf("."));

		tmp = tmp.replace("a ", "").trim();
		String[] split = tmp.split(" ");
		VisualOntology ontology = null;
		if(split.length == 2)
			ontology = VisualOntology.full;
		else
			ontology = this.findOntologyByCpt(tmp);
		
		return buildRequestJSON(RequestType.Check, ontology, tmp);
	} 
	
	@SuppressWarnings("unchecked")
	private JSONObject buildRequestJSON(RequestType reType,VisualOntology ontology, String cpt){
		JSONObject json = new JSONObject();
		json.put("request", reType);
		switch(reType){
			case Ask:
				json.put("ontology", ontology);
				break;
			case Check:
				json.put("ontology", ontology);
				json.put("concept", cpt);
				break;
			case Clarify:
				break;
		}
		json.put("timestamp", VSTimer.getInstance().getTimeStamp());
		
		return json;
	}
	
	
	
	/***************************************************************************/
	
	public int getCountOfObject(){
		return this.countOfObject;
	}
	
	enum VisualOntology{
		full, color, shape;
	}
	
	enum RequestType{
		Ask, Check, Clarify;
	}
	
	private Map<VisualOntology, Set<String>> ontologyMap;
	private void initialOntologyMap() throws IOException{
		
		if(ontologyMap == null)
			ontologyMap = new HashMap<VisualOntology, Set<String>>();
		
		File attrFile = new File(Constants.classifierType);
		if(attrFile.exists()){
			FileInputStream fis = new FileInputStream(attrFile);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
	 
			String line = null;
			while ((line = br.readLine()) != null) {
				if(line.length() != 0){
					String[] tmpt = line.split(":");
					VisualOntology ontology = null;
					if(tmpt[0].equals(VisualOntology.color.toString()))
						ontology = VisualOntology.color;
					else if(tmpt[0].equals(VisualOntology.shape.toString()))
						ontology = VisualOntology.shape;
					
					Set<String> cptList = null;
					if(ontologyMap.containsKey(ontology))
						cptList = ontologyMap.get(ontology);
					else{
						cptList = new HashSet<String>();
						ontologyMap.put(ontology, cptList);
					}
					cptList.add(tmpt[1]);
				}
			}
			br.close();
		}
	}
	
	private VisualOntology findOntologyByCpt(String cpt){
		if(ontologyMap != null && !ontologyMap.isEmpty()){
			Iterator<Entry<VisualOntology, Set<String>>> iterator = ontologyMap.entrySet().iterator();
			while(iterator.hasNext()){
				Entry<VisualOntology, Set<String>> pair = (Entry<VisualOntology, Set<String>>)iterator.next();
				VisualOntology ontology = pair.getKey();
				Set<String> cptSet = pair.getValue();
				
				for(String concept : cptSet){
					if(concept.equals(cpt))
						return ontology;
				}
			}
		}
		return null;
	}
}
