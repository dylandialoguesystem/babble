package babble.system;

import hw.system.dm.UttTyps;
import qmul.ds.Utterance;
import qmul.ds.formula.TTRFormula;
import qmul.ds.tree.Tree;

/**
 * Interface for DyLan module updating
 * @author Yanchao Yu
 */
public interface DyLanlistener {
	public abstract void getSemantic(String utt, TTRFormula ttr,Tree tree, String action, boolean isGrounded);
	public abstract void getNLG(String utt, UttTyps type);
	public abstract void throwException(Utterance utt, Exception e);
}
