package babble.system;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import babble.evaluation.TestClassifierCollection;
import babble.evaluation.eval.ActiveEval;
import babble.vision.VisualPAFSet;
import babble.vision.VisualPredicateArgumentFormula;
import babble.vision.VisualRecords;
import babble.vision.VisualTTRUpdate;
import babble.vision.classification.Concept;
import babble.vision.classification.VisualClassifier;
import babble.vision.context.VisualContext;
import babble.vision.gui.AbstractActivityListener;
import babble.vision.object.BasicObject;
import babble.vision.object.VisualObject;
import babble.vision.tools.Constants;
import babble.vision.tools.MatlabLink;
import babble.vision.tools.Randomer;
import qmul.ds.formula.Formula;
import qmul.ds.formula.Predicate;
import qmul.ds.formula.TTRRecordType;

/**
 * Vision module for generating visual context
 * 
 * TODO: 1) update the real-time video, edges
 * 		 2) extract features and view visual histogram
 * 		 3) TTR Update: timetask to generate the visual TTR
 * 
 * @author Yanchao Yu
 */
public abstract class AbstractVisionModule {
	static Logger logger = Logger.getLogger(AbstractVisionModule.class);
	
	protected AbstractActivityListener actListener;
	
	public TTRRecordType finalRecord;
	protected VisualRecords<Formula, VisualObject> vsRecords;
	protected VisualPAFSet<VisualPredicateArgumentFormula> vpafSet;
	
	protected ConceptMap clAttrs;
	
	protected VisualContext nLC;
	protected VisualTTRUpdate visualTTRUpdater;
	protected int objNo = 0;
	protected ActiveEval evaluator;
	
	protected int split = 0;
	
	protected boolean hasEvaluated = false;
	
	public AbstractVisionModule(){
		vsRecords = new VisualRecords<Formula, VisualObject>();
		vpafSet = new VisualPAFSet<VisualPredicateArgumentFormula>();
	}
	
	public AbstractVisionModule(VisualContext nLC, double threshold) {
		this.nLC = nLC;
		this.threshold = threshold;
	}

	public void setSplit(int split){
		this.split = split;
	}
	
	public void setVisualContext(VisualContext nLC){
		this.nLC = nLC;
	}
	
	public void registerListener(AbstractActivityListener actListener, boolean savable){
		this.actListener = actListener;
		initalComponent(savable);
	}
	
	public boolean isEvaluatable(){
		return this.hasEvaluated;
	}
	
	public void setEvaluatable(boolean hasEvaluated){
		this.hasEvaluated = hasEvaluated;
	}
	
	/**
	 * Initial components in the vision module
	 */
	protected void initalComponent(boolean savable){
		
		if(clAttrs == null){
			clAttrs = new ConceptMap();
		
			if(!clAttrs.isEmpty()){
				HashMap<String, List<String>> cptMap =  clAttrs.getOntologies();
				Iterator<Entry<String, List<String>>> iterator = cptMap.entrySet().iterator();
				while(iterator.hasNext()){
					 Entry<String, List<String>> pair = iterator.next();
					 String ontology = (String)pair.getKey();
					 List<String> cptList = (List<String>)pair.getValue();
					 this.addNewVpaf(ontology, cptList, savable);
				}
			}
			else{
				logger.error("clAttrs is empty!");
			}
		}
		
		visualTTRUpdater = new VisualTTRUpdate(this, nLC);
		
	}
	
	/****************************** Initial Visual Predicate Argument Formula (IVPAF) **********************************/
	/**
	 * Add new Visual Predicate Argument Formula (VPAF) for specific attribute category
	 * @param ontology the name of attribute category (e.g. color, shape or class)
	 * @param cptList a list of attributes
	 */
	protected void addNewVpaf(String ontology, List<String> cptList, boolean savable) {
		 logger.debug("Updated Attributs : "+ontology +" = "+ cptList);
		VisualPredicateArgumentFormula vpaf = new VisualPredicateArgumentFormula(new Predicate(ontology),
			 new ArrayList<Formula>() ,this, cptList, this.actListener, savable);
		addNewVpaf(vpaf);
	}
	
	/**
	 * Add new Visual Predicate Argument Formula (VPAF)
	 * @param vpaf Visual Predicate Argument Formula (VPAF)
	 */
	protected void addNewVpaf(VisualPredicateArgumentFormula vpaf){
		if(this.vpafSet != null){
			logger.debug("add new VPAF: "+ vpaf.getPredicate().toString());
			vpafSet.add(vpaf);
		}
	}

	/**
	 * Get List of Visual Predicate Argument Formula (VPAF)
	 */
	public VisualPAFSet<VisualPredicateArgumentFormula> getPredicateArguments(){
		return this.vpafSet;
	}

	/*************************************************** END OF IVPAF **************************************************/

	
	/**
	 * Tell the vision module that there is a new object provided in the scene and start classification process.
	 * @param flag
	 * @throws IOException
	 */
	protected boolean isNewObjectRelease = false;
	protected Thread vsTTRThread;
	public void switchVisionCapture(boolean flag) throws IOException{
		isNewObjectRelease = flag;
		if(flag){
//			captureObject();
			
			vsTTRThread = new Thread(this.visualTTRUpdater);
			this.visualTTRUpdater.runnable = true;
			vsTTRThread.setDaemon(true);
			vsTTRThread.start();
		}
		else{
			File picFile = new File(Constants.picFilename);
			if(picFile.exists())
				picFile.delete();

			if(visualTTRUpdater != null){
				visualTTRUpdater.runnable = false;
			}
			
			clearObject();
		}
	}
	
	/**
	 * @return flag if a new object has been released
	 */
	public boolean isNewObjectRelease(){
		return this.isNewObjectRelease;
	}
	
	/**
	 * only work for simulation tutor for importing new object
	 * @param feat visual features of new object
	 */
	public void importObject(double[] feat){
		if(this.vsTTRThread != null){
			this.visualTTRUpdater.runnable = false;
			this.visualTTRUpdater.clearVSRecords();
		}
		else{
			vsTTRThread = new Thread(this.visualTTRUpdater);
			vsTTRThread.setDaemon(true);
			vsTTRThread.start();
		}

		clearObject();
		this.visualTTRUpdater.runnable = true;
		
		this.actListener.updateHist(feat);
		
		currentObj = new VisualObject(this.objNo++);
		currentObj.setFeat(feat);
		Formula f = Formula.create("this");
		
		if(vsRecords.containsKey(f))
			vsRecords.put(f, currentObj);
		else
			vsRecords.put(f, currentObj);
		
		this.nLC.addnewObj(f);
		
		logger.info("New Visual Records Added");
		
		if(hasEvaluated){
			if(this.objNo % this.split == 0){
				logger.info("objNo: " + objNo);
				HashMap<String, VisualClassifier> classifierMap = new HashMap<String, VisualClassifier>();
				for(VisualPredicateArgumentFormula vpaf: vpafSet){
					classifierMap.putAll(vpaf.getClassifiers());
					logger.info("classifierMap Size: " + classifierMap.size());
				}
				
				TestClassifierCollection collection = new TestClassifierCollection("all");
				collection.addAllClassifiers(classifierMap);
				evaluator.evalClassifiers("all", this.tstList,collection, ActiveEval.ASKMORE, null);
			}
		}
	}
	

	protected VisualObject currentObj;
	public abstract void extractFeature(BufferedImage buff);
	
//	/**
//	 * Extract visual features from the specific image
//	 * @param buff bufferimage of the captured object
//	 */
//	protected void extractFeature(BufferedImage buff) {
//		// extract visual features
//		double[] feat = extractor.extFeature(Constants.picFilename);
//		this.actListener.updateHist(feat);
//		
//		// create a new visual object with extracted features
////		Mat img = ImageMat.getInstance().bufferedImageToMat(buff);
//		currentObj = new VisualObject(this.objNo++);
//		currentObj.setFeat(feat);
////		currentObj.setImgRawData(img);
//		Formula f = Formula.create("this");
//		
//		if(vsRecords.containsKey(f))
//			vsRecords.replace(f, currentObj);
//		else
//			vsRecords.put(f, currentObj);
//		
//		this.nLC.addnewObj(f);
//		
//		logger.info("New Visual Records Added");
//		
//		if(this.objNo % 10 == 0){
//			logger.info("objNo: " + objNo);
//			evaluator.evalClassifiers();
//		}
//	}

	/**
	 * Get a list of visual objects in the scene
	 */
	public VisualRecords<Formula, VisualObject> getRecords(){
		return vsRecords;
	}
	
	/**
	 * Clear all visual objects in the list
	 */
	protected void clearObject(){
		if(vsRecords != null && !vsRecords.isEmpty())
			vsRecords.clear();
		
		if(this.nLC != null && !nLC.isEmpty())
			nLC.clearContext();

		currentObj = null;
	}
	
	/**
	 * Incrementally learn object with new labels
	 * @param annotation labels
	 * @return flag if successful
	 */
	public boolean learnObject(TTRRecordType annotation){
		if(currentObj != null){
			currentObj.setLabels(annotation);
			
			List<String> labels = currentObj.getLabels();
			for(String s: labels){
				logger.debug("Object Labels: " + s);
			}
			
			for(VisualPredicateArgumentFormula vpaf : vpafSet){
				if(!vpaf.trainModle(currentObj))
					return false;
			}
			return true;
		}
		
		return false;
	}
	
	List<? extends BasicObject> trnList = null;
	public void setTrainSet(List<? extends BasicObject> objList) {
		this.trnList = objList;
	}
	
	public List<? extends BasicObject> getTrainSet(){
		return this.trnList;
	}
	
	protected List<VisualObject>  tstList = null;
	public void setTestSet(List<? extends BasicObject> objList) {
		this.tstList = null;
		this.tstList = (List<VisualObject>) objList;
	}
	

	protected double threshold = 1; // default
	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}

	protected Map<String, Queue<Concept>> predictMap = new HashMap<String, Queue<Concept>>();
	public Queue<Concept> getPrecition(String ontology) {
		return predictMap.get(ontology);
	}
	
	public Map<String, Queue<Concept>> getConceptMap() {
		return this.predictMap;
	}

	public abstract Map<String, Integer> getRLStates(BasicObject object);
	public abstract Map<String, Queue<Concept>> getCLassifictionResults(BasicObject object);
	public abstract boolean updateClassifier(String ontology, String conceptName);

	public VisualObject getObjectRandomly() {
		if(trnList != null && !trnList.isEmpty()){
			int select = Randomer.getInstance().randomInt(0, trnList.size()-1);
			VisualObject vboj = (VisualObject)trnList.get(select);
			this.currentObj = vboj;
			trnList.remove(select);
			
			return vboj;
		}
		return null;
	}

	public abstract void reset();

	public int getSplit() {
		return this.split;
	}

	public abstract void eval(int numOfObject, int nfold, double cost);

	public void viewNewObject(VisualObject vObj) {
		this.currentObj = vObj.clone();
	}
}
