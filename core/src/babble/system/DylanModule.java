package babble.system;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.text.SimpleAttributeSet;

import org.apache.log4j.Logger;

import edu.stanford.nlp.ling.BasicDocument;
import edu.stanford.nlp.ling.Document;
import edu.stanford.nlp.process.DocumentProcessor;
import edu.stanford.nlp.process.StripTagsProcessor;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import hw.system.dm.UttTyps;
import qmul.ds.ContextParser;
import qmul.ds.DAGGenerator;
import qmul.ds.DSParser;
import qmul.ds.InteractiveContextParser;
import qmul.ds.ParserTuple;
import qmul.ds.Utterance;
import qmul.ds.dag.DAGTuple;
import qmul.ds.formula.TTRFormula;
import qmul.ds.formula.TTRRecordType;
import qmul.ds.tree.Tree;

/**
 * Interactive DyLan module from Arash works
 * @author Yanchao Yu
 */
public class DylanModule {
	static Logger logger = Logger.getLogger(DylanModule.class);
	
	private TTRFormula groundedContext = null;
	public boolean isGrounded = false;
	
	// constants for language specification
	public static final int ENGLISH = 0;
	public static final int JAPANESE = 1;
	// default locations for pre-packaged grammars
	public static final String BASEURL = "http://www.dcs.qmul.ac.uk/~mpurver/ds/";
	public static final String ENGLISHTTRURL = "resource/2017-english-ttr-copula-simple";
	public static final String[] PARSERTYPES = {
			LoadParserandGeneratorThread.breadthFirst, LoadParserandGeneratorThread.interactive };
	private static TreebankLanguagePack tlp;
	private String encoding = "UTF-8";

	private String defaultParser = "";

	//private FormulaPanel semPanel;
//	private DAGViewer<DAGTuple, GroundableEdge> conPanel;
	public DSParser parser;
	
//	private Generator<?> generator;
	
	// worker threads to handle long operations
	private LoadParserandGeneratorThread lpThread;
	private ParseThread parseThread;
//	private GenerateThread generateThread;
	
	private DyLanlistener listener;
	
	private ArrayList<ParserTuple> tuples = new ArrayList<ParserTuple>();
	public int beamWidth = 10;

	private boolean isInitiated = false;
	
	public DylanModule(){
		setLanguage(ENGLISH);
	}
	
	private void setLanguage(int lanIndex) {
		switch (lanIndex) {
		case ENGLISH:
			tlp = new PennTreebankLanguagePack();
			encoding = tlp.getEncoding();
			break;
		}
	}
	
	public void registerListener(DyLanlistener listener){
		this.listener = listener;
	}
	
	/**
	 * Loads a parser grammar/lexicon specified by given path
	 */
	public void loadParser(String filename) {
		if (filename == null) {
			return;
		}

		// set default for next time
		defaultParser = filename;

		// check if file exists before we start the worker thread and progress
		// monitor
		// File file = new File(filename);
		// if (file.exists()) {
		String pType = PARSERTYPES[1].toString();
		lpThread = new LoadParserandGeneratorThread(filename, pType);
		lpThread.start();
		// } else {
		// JOptionPane.showMessageDialog(this, "Could not find file " +
		// filename, null, JOptionPane.ERRORMESSAGE);
		// setStatus("Error loading parser");
		// }
	}
	
	/**
	 * Loads a text or html file from a file path or URL. Treats anything
	 * beginning with <tt>http:\\</tt>,<tt>.htm</tt>, or <tt>.html</tt> as an
	 * html file, and strips all tags from the document
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void loadFile(String filename) {
		if (filename == null) {
			return;
		}

		File file = new File(filename);

		String urlOrFile = filename;
		// if file can't be found locally, try prepending http:// and looking on
		// web
		if (!file.exists() && filename.indexOf("://") == -1) {
			urlOrFile = "http://" + filename;
		}
		// else prepend file:// to handle local html file urls
		else if (filename.indexOf("://") == -1) {
			urlOrFile = "file://" + filename;
		}

		// load the document
		Document doc;
		try {
			if (urlOrFile.startsWith("http://") || urlOrFile.endsWith(".htm")
					|| urlOrFile.endsWith(".html")) {
				// strip tags from html documents
				Document docPre = new BasicDocument().init(new URL(urlOrFile));
				DocumentProcessor noTags = new StripTagsProcessor();
				doc = noTags.processDocument(docPre);
			} else {
				doc = new BasicDocument(tlp.getTokenizerFactory())
						.init(new InputStreamReader(new FileInputStream(
								filename), encoding));
			}
		} catch (Exception e) {
			logger.error("Error loading document");
			return;
		}

		// load the document into the text pane
		StringBuilder docStr = new StringBuilder();
		for (Iterator it = doc.iterator(); it.hasNext();) {
			if (docStr.length() > 0) {
				docStr.append(' ');
			}
			docStr.append(it.next().toString());
		}
		logger.info("Done");
	}
	
	/**
	 * Tokenizes the highlighted text (using a tokenizer appropriate for the
	 * selected language, and initiates the ParseThread to parse the tokenized
	 * text.
	 * @param act 
	 * @return 
	 */
	public void parse(String text, String act) {
		
		if (text == null || text.length() == 0) {
			logger.error("nothing to parse");
		}
	
		logger.info("got text " + text);

		if (parser != null && text.length() > 0) {
			
			Utterance utt=new Utterance(text+" -- " + act);
			
			if (prevUtterance!=null&&utt.getSpeaker().equals(Utterance.defaultSpeaker)){
				utt.setSpeaker(prevUtterance.getSpeaker());
			}
			parseThread = new ParseThread(utt, act, listener);
			parseThread.start();
			
		}else{
			logger.error("parser is null");
		}
	}
	
	
	public ParseForm parse2(String text, String act){
		if (parser != null) {
			boolean successful = false;
			Utterance utt = new Utterance(text + " -- " + act);
			logger.info(utt);
			
			try {
				if (utt == null || utt.isEmpty()) {
					logger.error("Nothing to parse!");
					successful = true;
				} else{
					successful = parser.parseUtterance(utt);
				}
			} catch (Exception e) {
				return new ParseForm(utt, e);
			}

			if (successful) {
				if (parser instanceof InteractiveContextParser) {
					InteractiveContextParser p=(InteractiveContextParser)parser;
					tuples.clear();

					DAGTuple current = p.getState().getCurrentTuple();
					tuples.add(current);
					
					TTRFormula ttr = current.getSemantics();
					Tree tree = current.getTree();
					String sem = ttr.toString();
					
					String tupleLabel = p.getState().getTupleLabel(current);
					logger.debug("DAG TurpleLabels: " + tupleLabel);
					logger.debug("DAG AcceptedPointers: " + p.getState().getAcceptancePointers(current));
					
					if(tupleLabel.equals("T-S") || tupleLabel.equals("S-T")){
						isGrounded = true;
						logger.debug("groundedContext: " + groundedContext + ";   TTR: " + ttr);
					}
					else{
						isGrounded = false;
						groundedContext = null;
					}
						
					logger.debug("is Grounded: " + isGrounded);
					
					return new ParseForm(utt, ttr, tree, isGrounded);
				}
			} else {
//				logger.error("Error parsing");
//				this.listener.getSemantic(utt.toString(), null, null, successful);
				return new ParseForm(utt, new Exception("Cannot parse the utterance."));
			}
		}else{
			logger.error("parser is null");
		}
		
		return null;
	}
	
	/**
	 * Display the best parse if available
	 */
	@SuppressWarnings("unused")
	private void displayBestParse() {
		// tuples = new ArrayList<ParserTuple>(parser.getState());
		// ArrayList<ParserTuple> ttrtuples
		// TODO this is TTR specific, it shouldn't be in the gui
		// tuples = new ArrayList<ParserTuple>(parser.getTTRState()); //just
		// displaying the ttr representations...
		if (parser instanceof qmul.ds.Parser)
			tuples = new ArrayList<ParserTuple>(
					((qmul.ds.Parser) parser).getState());
	}
	
	public Boolean fullyLoaded(){
		return this.isInitiated;
	}
	
	public TTRRecordType getGroundedContext(){
		return (TTRRecordType)this.groundedContext;
	}

	/************************************Custom Threads****************************************************/
	/**
	 * Worker thread for loading the parser. Loading a parser usually takes ~2min
	 */
	private class LoadParserandGeneratorThread extends Thread {
		String filename;
		String parserType = interactive;// either 'depth-first' or
		// 'breadth-first'
		static final String breadthFirst = "Breadth First";
		static final String interactive = "Interactive (Best-First)";

		LoadParserandGeneratorThread(String filename, String parserType) {
			this.filename = filename;
			this.parserType = parserType;
		}

		@Override
		public void run() {
			try {
				if (this.parserType.equalsIgnoreCase(breadthFirst)) {
					parser = new ContextParser(filename);
				} else if (this.parserType.equalsIgnoreCase(interactive)) {
					parser = new InteractiveContextParser(filename);
//					generator = new InteractiveContextGenerator((InteractiveContextParser)parser, null);
				} else{
					logger.error("Error loading parser");
					parser = null;
//					generator = null;
				}
				logger.info("loaded parser");
				initParser();
				logger.info("Initialised Parser");
				
				isInitiated = true;
			} catch (Exception ex) {
				ex.printStackTrace();
				logger.error("Error loading parser");
				parser = null;
//				generator = null;

			} catch (OutOfMemoryError e) {
				logger.error("Error loading parser");
				parser = null;
//				generator = null;
			}
		}
	}

	Utterance prevUtterance=null;
	/**
	 * Re-initialises the current parser
	 */
	public void initParser() {
		if (parser != null) {
			tuples.clear();
			parser.init();
			prevUtterance=null;
			
			if (parser instanceof InteractiveContextParser) {
				InteractiveContextParser p = (InteractiveContextParser) parser;
				tuples.add(p.getState().getCurrentTuple());
			}
		}
	}

	/**
	 * Worker thread for parsing.
	 */
	private class ParseThread extends Thread {
		DyLanlistener listener;
		Utterance utt;
		String sem = null;
		TTRFormula ttr = null;
		String act;

		public ParseThread(Utterance sentence,String act, DyLanlistener listener) {
			this.utt = sentence;
			this.act = act;
			this.listener = listener;
		}

		@Override
		public void run() {
			synchronized(this){
				boolean successful = false;
				try {
					if (utt == null || utt.isEmpty()) {
						logger.error("Nothing to parse!");
						successful = true;
					} else{
						successful = parser.parseUtterance(utt);
					}
				} catch (Exception e) {
					listener.throwException(utt, e);
					return;
				}
				
				if (successful) {
					if (parser instanceof InteractiveContextParser) {
						InteractiveContextParser p=(InteractiveContextParser)parser;
						tuples.clear();
	
						DAGTuple current = p.getState().getCurrentTuple();
						tuples.add(current);
						
						ttr = current.getSemantics();
						Tree tree = current.getTree();
						sem = ttr.toString();
						
						String tupleLabel = p.getState().getTupleLabel(current);
						logger.debug("DAG TurpleLabels: " + tupleLabel);
						logger.debug("DAG AcceptedPointers: " + p.getState().getAcceptancePointers(current));
						
						if(tupleLabel.equals("T-S") || tupleLabel.equals("S-T")){
							isGrounded = true;
							logger.debug("groundedContext: " + groundedContext + ";   TTR: " + ttr);

							groundedContext = ttr;
//							if(groundedContext == null)
//								groundedContext = ttr;
//							else
//								groundedContext = groundedContext.conjoin(ttr);
//	
//							logger.debug("groundedContext: " + groundedContext);
						}
						else{
							isGrounded = false;
							groundedContext = null;
						}
							
						logger.debug("is Grounded: " + isGrounded);
						
						if(listener != null){
							listener.getSemantic(utt.toString(), ttr, tree, act, isGrounded);
						}
						else
							logger.error("No Listener...");
					}
				} else {
					logger.error("Error parsing");
//					this.listener.getSemantic(utt.toString(), null, null, successful);
					listener.throwException(utt, new Exception("Cannot parse the utterance."));
				}
			}
		}
	}

	public void displayParsedUtterance(Utterance utterance) {
		SimpleAttributeSet simple = new SimpleAttributeSet();
		if (prevUtterance==null)
			
			logger.info(utterance.toString());
		else if (utterance.getSpeaker().equals(prevUtterance.getSpeaker()))
			logger.info(utterance.getText());
		else
			logger.info(utterance+" ");
		
		prevUtterance=utterance;
	}

	/**************************************Main Method*************************************************/
	public static void main(String[] args){
		edu.stanford.nlp.util.DisabledPreferencesFactory.install();
		String dataFilename = null;
		String parserFilename = DylanModule.ENGLISHTTRURL;
//		if (args.length > 0) {
//			if (args[0].equals("-h")) {
//				logger.error("Usage: java " + DylanModule.class + " [parserfilename] [textFilename]");
//			} else {
//				parserFilename = args[0];
//				if (args.length > 1) {
//					dataFilename = args[1];
//				}
//			}
//		}
		
		System.out.println("dataFilename: "+dataFilename);
		System.out.println("parserFilename: "+parserFilename);
		
		DylanModule dlPasers = new DylanModule();
		logger.info("ParserFile: " + parserFilename);
		dlPasers.loadParser(parserFilename);
		dlPasers.loadFile(dataFilename);
		dlPasers.registerListener(new DyLanlistener(){
			
			@Override
			public void getNLG(String sem, UttTyps type) {
				System.out.println("NLG: " + sem);
			}

			@Override
			public void getSemantic(String utt, TTRFormula ttr, Tree tree, String action, boolean isGrounded) {
				System.out.println(" Utt: " + utt + "\r\n TTRFormula: " + ttr + "\r\n Tree: " + tree);
//				dlPasers.generate(ttr);
			}

			@Override
			public void throwException(Utterance u, Exception e) {
				
			}
			
		});
		while(true){
			System.out.println("Please say a sentence:");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			try {
				String s = br.readLine();
				dlPasers.parse(s, null);
				if(!s.equals("okay"))
					continue;
				else
					break;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
