package babble.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import edu.stanford.nlp.trees.Tree;
import qmul.ds.Utterance;
import qmul.ds.formula.Formula;
import qmul.ds.formula.TTRField;
import qmul.ds.formula.TTRFormula;
import qmul.ds.formula.TTRLabel;
import qmul.ds.formula.TTRRecordType;

public class ParseForm {
	static Logger logger = Logger.getLogger(ParseForm.class);
	
	private Utterance utt;
	private TTRFormula ttr = null;
	private Map<OntologyType, String> groundMap = new HashMap<OntologyType, String>();
	private boolean isGrounded = false;
	private boolean hasException = false;
	private Tree tree;
	private Exception exception;
	
	public ParseForm(){}	

	public ParseForm(Utterance utt, TTRFormula ttr, qmul.ds.tree.Tree tree, boolean isGrounded){
		this.utt = utt;
		this.ttr = ttr;
		this.isGrounded = isGrounded;
		
		if(ttr != null && ttr instanceof TTRRecordType){
			TTRRecordType context = (TTRRecordType)ttr.clone();
			
			List<TTRField> fieldList = context.getFields();
			for(TTRField field : fieldList){
				Formula type = field.getType();
				if(type != null){
					String typeStr = type.toString();
					String requestTTRlabel = null;
					OntologyType focusOntology = null;
					
					if(typeStr.contains("color")){
						focusOntology = OntologyType.COLOR;
						requestTTRlabel = typeStr.replace("color"+ "(", "");
						requestTTRlabel = requestTTRlabel.replace(")", "");
					}
					else if(typeStr.contains("shape") ){
						focusOntology = OntologyType.SHAPE;
						requestTTRlabel = typeStr.replace("shape"+ "(", "");
						requestTTRlabel = requestTTRlabel.replace(")", "");
					}
					else if(typeStr.contains("class") ){
						focusOntology = OntologyType.SHAPE;
						requestTTRlabel = typeStr.replace("class"+ "(", "");
						requestTTRlabel = requestTTRlabel.replace(")", "");
					}
					
					if(requestTTRlabel != null){
						logger.debug("requestTTRlabel at "+focusOntology+": " + requestTTRlabel);
						
						TTRField f = context.getField(new TTRLabel(requestTTRlabel));
						if(f != null && f.getType() != null){
							groundMap.put(focusOntology, f.getType().toString());
						}
					}
				}
			}
		}
	}
	
	public ParseForm(Utterance utt, TTRFormula ttr, Tree tree){
		this.utt = utt;
		this.ttr = ttr;
		this.tree = tree;
	}
	
	public ParseForm(Utterance utt, Exception e){
		this.utt = utt;
		this.exception = e;
		this.hasException = true;
	}
	
	public Utterance getUtterance(){
		return this.utt;
	}
	
	public TTRFormula getSemanticTTR(){
		return this.ttr;
	}
	
	public boolean checkGround(){
		return this.isGrounded;
	}
	
	public Tree getContxtalTree(){
		return this.tree;
	}
	
	public String getException(){
		return this.exception.getMessage() + ": " + this.utt;
	}
	
	public boolean hasException(){
		return this.hasException;
	}

	public void getUtterance(Utterance utt){
		this.utt = utt;
	}
	
	public void getSemanticTTR(TTRFormula ttr){
		this.ttr = ttr;
	}
	
	public void setGround(boolean isGrounded){
		this.isGrounded = isGrounded;
	}
	
	public void setTree(Tree tree){
		this.tree = tree;
	}
	
	public Map<OntologyType, String> getGroundMap(){
		return this.groundMap;
	}
	
	public enum OntologyType{
		COLOR,
		SHAPE;
	}
}
