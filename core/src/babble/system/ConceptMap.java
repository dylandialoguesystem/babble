package babble.system;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import babble.vision.tools.Constants;

/**
 * Initialize all attribute categories and related attributes
 * from the specific text file
 * 
 * @author Yanchao Yu
 */
public class ConceptMap{
	static Logger logger = Logger.getLogger(ConceptMap.class);
	
	private HashMap<String,List<String>> conceptMap; 
	private static final String attrPath = "resource/classifier-types.txt";
	
	public ConceptMap(){
		conceptMap = new HashMap<String,List<String>>();
		if(!loadAttributes()){
			File attrFile = new File(attrPath);
			try {
				attrFile.createNewFile();
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		}
	}
	
	/**
	 * Load all attributes and their categories into System 
	 * @return flag if loading successfully
	 */
	private boolean loadAttributes(){
		try {
			File attrFile = new File(attrPath);
			if(attrFile.exists()){
				FileInputStream fis = new FileInputStream(attrFile);
				
				BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		 
				String line = null;
				while ((line = br.readLine()) != null) {
					if(line.length() != 0){
						String[] tmpt = line.split(":");
						addNewConcept(tmpt[0].trim(),tmpt[1].trim());
					}
				}
				br.close();
				
				return true;
			}
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		
		return false;
	}
	
	/**
	 * Check if required category is existed
	 * @param ontology the name of the attribute category
	 * @return flag if existed
	 */
	public boolean checkOntologyExist(String ontology){
		return this.conceptMap.containsKey(ontology);
	}
	
	/**
	 * Check if the attribute has been defined on the specific category
	 * @param ontology the name of the category
	 * @param concept the name of attribute required 
	 */
	public boolean checkConceptExist(String ontology, String concept){
		if(this.conceptMap.containsKey(ontology)){
			List<String> itemList = conceptMap.get(ontology);
			return itemList.contains(concept);
		}
		return false;
	}
	
	/**
	 * Add new attribute concept on the particular category
	 * @param newOntology the name of category
	 * @param newCpt the name of new attribute concept
	 */
	public void addNewConcept(String newOntology, String newCpt){
		if(!checkOntologyExist(newOntology)){
			List<String> itemList = new ArrayList<String>();
			itemList.add(newCpt);
			conceptMap.put(newOntology,itemList);
		}
		else{
			if(!checkConceptExist(newOntology,newCpt)){
				List<String> itemList = conceptMap.get(newOntology);
				itemList.add(newCpt);
			}
		}
	}
	
	/**
	 * Get map of all categories and corresponding attributes
	 */
	public HashMap<String,List<String>> getOntologies(){
		return this.conceptMap;
	}
	
	/**
	 * Get the list of attributes on the same category
	 * @param ontology the name of the attribute category
	 * @return the list of attribute names
	 */
	public List<String> getConceptListByOntology(String ontology){
		return this.conceptMap.get(ontology);
	}
	
	/**
	 * Check if there are attributes in the list
	 * @return flag if there are any attributes
	 */
	public boolean isEmpty(){
		return this.conceptMap.isEmpty();
	}
	
	/**
	 * Check if there are attribute defined on particular category
	 * @param ontology the name of category
	 * @return flag if there are any attributes
	 */
	public boolean isEmpty(String ontology){
		List<String> itemList = conceptMap.get(ontology);
		return itemList.isEmpty();
	}
	/**
	 * Count the number of attribute categories
	 */
	public int getSizeofOntologyList(){
		return conceptMap.size();
	}
	
	/**
	 * Check how many attributes defined under particular category
	 * @param ontology the name of attribute category
	 * @return the number of attributes
	 */
	public int getSizeofCptList(String ontology){
		List<String> itemList = conceptMap.get(ontology);
		return itemList.size();
	}
	
	/**
	 * Write new attribute and corresponding category into text file
	 * @param newAtt the name of new attribute
	 * @return flag if written successfully
	 */
	private boolean writeNewAttribute(String newAtt){
		try {
			if(this.conceptMap != null){
				File myFoo = new File(attrPath);
				FileWriter fooWriter = new FileWriter(myFoo, true);
				
				fooWriter.write(newAtt+"\n");
				fooWriter.close();
				
				return loadAttributes();
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return false;
	}
		
	/**************************** Main Method *************************/
	public static void main(String[] args){
		ConceptMap att = new ConceptMap();
		logger.debug("Updated Attributs :" + att.getOntologies());
//		att.writeNewAttribute("col:yellow");
//		att.writeNewAttribute("cla:car");
//		att.writeNewAttribute("shap:enlogated");
//			
//		logger.info("Updated Attributs -- color:" + att.getConceptListByOntology("col"));
//		logger.info("Updated Attributs -- class:" + att.getConceptListByOntology("cla"));
//		logger.info("Updated Attributs -- shape:" + att.getConceptListByOntology("shap"));
	}
}
