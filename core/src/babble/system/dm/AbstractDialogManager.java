package babble.system.dm;

import java.util.List;

import org.apache.log4j.Logger;

import babble.system.AbstractVisionModule;
import babble.system.DyLanlistener;
import babble.system.DylanModule;
import babble.vision.context.VisualContext;
import babble.vision.gui.AbstractActivityListener;
import babble.vision.object.BasicObject;
import babble.vision.tools.Constants;

public abstract class AbstractDialogManager{
	static Logger logger = Logger.getLogger(AbstractDialogManager.class);

	protected AbstractBabbleDialog babbleDialog;
	
	public AbstractDialogManager(AbstractBabbleDialog babbleDialog){
		this.babbleDialog = babbleDialog;
	}
	
	protected AbstractActivityListener actlistener;
	public void registerActivityListener(AbstractActivityListener actlistener){
		this.actlistener = actlistener;
		babbleDialog.registerListener(actlistener);
	}
	
	public AbstractActivityListener getActivityListener(){
		return this.actlistener;
	}

	
	/**
	 * @return flag if the Dylan module is ready for use.
	 */
	public abstract Boolean isdmReady();
	
	public abstract void initialDyLanModule(String path);
	
	protected void initialDyLanModule(String path, DyLanlistener listener) {
		if(path == null || path.isEmpty())
			path = Constants.parserFilename;
		String dataFilename = null;
		
		if(babbleDialog.dylanModule == null){
			babbleDialog.dylanModule = new DylanModule();
			babbleDialog.dylanModule.loadParser(path);
			babbleDialog.dylanModule.loadFile(null);
			babbleDialog.dylanModule.registerListener(listener);
		}
	}
	
	public abstract void initialVisionModule(AbstractVisionModule vsmodule);
	
	public AbstractBabbleDialog getBabbleDialog(){
		return this.babbleDialog;
	}

	public abstract void configPolicy(String path);
	
	public AbstractPolicy getDlgPolicy(){
		return this.babbleDialog.dlgPolicy;
	}
	
	public void setTrnSet(List<? extends BasicObject> objList){
		this.babbleDialog.visionModule.setTrainSet(objList);
	}
	
	public void setTstSet(List<? extends BasicObject> objList){
		this.babbleDialog.visionModule.setTestSet(objList);
	}
	
	public abstract void captueNewObject(double[] feat);
	public abstract void captueNewObject(boolean flag);
}
