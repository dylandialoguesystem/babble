package babble.system.dm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import babble.system.AbstractVisionModule;
import babble.system.DylanModule;
import babble.vision.context.VisualContext;
import babble.vision.gui.AbstractActivityListener;
import babble.vision.gui.DSTypes;
import babble.vision.tools.Constants;
import babble.vision.tools.DAState;
import babble.vision.tools.DActions;
import hw.system.dm.UttTyps;
import qmul.ds.formula.TTRFormula;
import qmul.ds.formula.TTRRecordType;
import qmul.ds.tree.Tree;

public abstract class AbstractBabbleDialog {
	Logger logger = Logger.getLogger(AbstractBabbleDialog.class);
	public AbstractPolicy dlgPolicy;

	protected String userID = null;
	protected DAState sysCurrentDS = null;
	
	protected Hashtable<String, Boolean> dialogSettings;
	protected Hashtable<Long, JSONObject> history; // history of dialog and corresponding actions
	
	public VisualContext nLC;
	public DylanModule dylanModule;
	public AbstractVisionModule visionModule;
	
	protected AbstractActivityListener actListener;
	public Thread sgThread;
	
	public AbstractBabbleDialog(String userID){
		this.userID = userID;
		dialogSettings = new Hashtable<String, Boolean>();
		history = new Hashtable<Long, JSONObject>();
	}

	public AbstractBabbleDialog(AbstractActivityListener actListener,String userID){
		this.actListener = actListener;
		this.userID = userID;
		dialogSettings = new Hashtable<String, Boolean>();
		history = new Hashtable<Long, JSONObject>();
	}
	
	public void registerListener(AbstractActivityListener actListener){
		this.actListener = actListener;
	}
	
	
	/**
	 * Initiate all potential settings about the dialog turns.
	 */
	public void initialSettings(String path) {
		if(path == null || path.isEmpty()){
			dialogSettings.put("userTalking", false);
			dialogSettings.put("sysTalking", false);
			dialogSettings.put("userBargin", false);
			dialogSettings.put("sysBargin", false);
			dialogSettings.put("requestMore", false);
			dialogSettings.put("userUttParsed:null", false);
			dialogSettings.put("userUttQuestion", false);
			dialogSettings.put("userUttParsed:done", false);
			dialogSettings.put("openUserRequest", false);
			dialogSettings.put("isWHQuestion", false);
		}
		else{
			FileInputStream fis = null;
			BufferedReader br = null;
			try {
				fis = new FileInputStream(path);
				br = new BufferedReader(new InputStreamReader(fis));
			 
				String line = null;
				while ((line = br.readLine()) != null) {
					String[] setting = line.split(" = ");
					dialogSettings.put(setting[0].trim(), Boolean.valueOf(setting[1].trim()));
				}
			} catch (IOException e) {
				logger.error(e.getMessage());
			} finally{
				try {fis.close();br.close();} catch (Exception ex) {/*ignore*/}
			}
		}
		
		this.sysCurrentDS = DAState.listening;
	}
	
	/********************************** Dialog Operations **********************************/
	/**
	 * runRules based on Semantic TTR from DyLan Module
	 * @param ttr semantic TTR representation
	 * @param tree 
	 */
	protected TTRRecordType curSemanticTTR;
	public abstract void runWithTTR(TTRFormula ttr, Tree tree);
	
	/**
	 * Do next action for creating the generator goal TTR.
	 * @param action Dialog action 
	 * @param hypothesis TTR record type that need to be processed... 
	 */
	public abstract void doAction(DActions action, TTRRecordType hypothesis); 
	
	public void generate(TTRRecordType targetTTR, String utt){
		if(targetTTR != null && utt != null){
			this.actListener.updateTTRRecords(targetTTR, DSTypes.contextTTR);
			this.sysCurrentDS = DAState.listening;
			
			dylanModule.parse(Constants.SYS + utt, "");
			this.actListener.updateDialog(utt, UttTyps.system, true);
		}
	}
	
	/**
	 * run Rules when nothing parsed
	 * @param utt user utterance so far in the latest turn
	 */
	public abstract void runWithoutTTR(String utt);
	
	/********************************* Dialog History Methods ********************************/
	/**
	 * Update the dialog settings 
	 * @param setting specific setting item
	 * @param flag	flag of switch on or off
	 */
	public void updateSettings(String setting, boolean flag){
		dialogSettings.put(setting, flag);
	}
	
	/**
	 * Update current system dialog status, i.e. listening, checking, waitingforconfirm...
	 */
	public void updateCurrentSysDS(DAState status){
		this.sysCurrentDS = status;
	}
	
	/**
	 * @return current system dialog status
	 */
	public DAState getCurrentSysDS(){
		return this.sysCurrentDS;
	}
	
	/**
	 * Store all dialog states and actions into History
	 * @param timestamp current time stamp
	 * @param dialogState dialog state/action in the form of JSON
	 */
	private void storeDialogHistory(long timestamp, JSONObject dialogState){
		if(dialogState != null){
			this.history.put(timestamp, dialogState);
		}
	}
	
	/**
	 * Store all dialog states and actions into History
	 * @param dialogState dialog state/action in the form of JSON
	 */
	public void storeDialogHistory(JSONObject dialogState){
		if(dialogState != null)
			storeDialogHistory(getTimeStamp(), dialogState);
	}
	
	public void exportDialogHistory(String path){
		if(path == null && !path.isEmpty()){
			BufferedWriter writer = null;

			try {
				writer = new BufferedWriter(new FileWriter(path, true));
				Iterator<Entry<Long,JSONObject>> iterator = this.history.entrySet().iterator();
				while(iterator.hasNext()){
					Entry<Long,JSONObject> pair = iterator.next();
					String newline = pair.getKey() + " :: " + pair.getValue();
					writer.write(newline);
					writer.newLine();
				}
			} catch (IOException e) {
				logger.error(e.getMessage());
			} finally {
			   try {writer.close();} catch (Exception ex) {/*ignore*/}
			}
		}
		else 
			logger.error("the pathe is unavaliable...");
	}
	
	/**
	 * @return time stamp of now
	 */
	private long getTimeStamp(){
		long now = (new Date()).getTime();
		return now;
	}
	
	/************************ Timer Out for Request *****************************************/
	protected Timer timer;
	protected TimerTask silenceChecker;
}
