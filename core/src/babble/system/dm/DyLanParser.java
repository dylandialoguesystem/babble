package babble.system.dm;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import babble.system.ParseForm;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import qmul.ds.ContextParser;
import qmul.ds.DSParser;
import qmul.ds.InteractiveContextParser;
import qmul.ds.ParserTuple;
import qmul.ds.Utterance;
import qmul.ds.formula.TTRFormula;

public class DyLanParser {
	Logger logger = Logger.getLogger(DyLanParser.class);
	
	public static final String ENGLISHTTRURL = "resource/2017-english-ttr-copula-simple";
	public static final int ENGLISH = 0;
	private String encoding = "UTF-8";
	private DSParser parser;
	private String defaultParser = "";
	private static TreebankLanguagePack tlp;
	public static final String[] PARSER_TYPES = {LoadParserThread.breadthFirst,LoadParserThread.interactive};
	
	private LoadParserThread lpThread;

	private ArrayList<ParserTuple> tuples = new ArrayList<ParserTuple>();
	
	public DyLanParser(String parserFilename, String dataFilename) {
		this.setLanguage(ENGLISH);
		this.loadParser(parserFilename);
		this.loadParser(dataFilename);
	}
	
	
	public void loadParser(String filename) {
		if (filename == null)
			return;

		// set default for next time
		defaultParser = filename;

		// check if file exists before we start the worker thread and progress
		String pType = PARSER_TYPES[1].toString();
		lpThread = new LoadParserThread(filename, pType);
		lpThread.start();
	}
	
	public void setLanguage(int language) {
		switch (language) {
			case ENGLISH:
				tlp = new PennTreebankLanguagePack();
				encoding = tlp.getEncoding();
				break;
		}
	}


	Utterance prevUtterance=null;
	/**
	 * Re-initialises the current parser
	 */
	public void initParser() {
		if (parser != null) {
			tuples.clear();
			parser.init();
			prevUtterance=null;
			
			if (parser instanceof InteractiveContextParser) {
				InteractiveContextParser p = (InteractiveContextParser) parser;
				tuples.add(p.getState().getCurrentTuple());
			}
		}
	}
	
	public ParseForm parse(String text) {
		 logger.info("got text " + text);

		if (parser != null && text.length() > 0) {
			// Tokenizer<? extends HasWord> toke = tlp.getTokenizerFactory()
			// .getTokenizer(new CharArrayReader(text.toCharArray()));
			// List<? extends HasWord> wordList = toke.tokenize();

			Utterance utt = new Utterance(text);

			if (prevUtterance != null
					&& utt.getSpeaker().equals(Utterance.defaultSpeaker)) {
				utt.setSpeaker(prevUtterance.getSpeaker());
			}

			// parse the utterance
			boolean successful = false;
			try {
				if (utt == null || utt.isEmpty()) {
					successful = true;
				} else
					successful = parser.parseUtterance(utt);
			} catch (Exception e) {
				return new ParseForm(utt, e);
			}

			if (successful) {
				if (parser instanceof InteractiveContextParser) {
					InteractiveContextParser p = (InteractiveContextParser) parser;
					tuples.clear();

					tuples.add(p.getState().getCurrentTuple());
				}
				return this.displayBestParse(utt);
			} else {
				return new ParseForm(utt, new Exception("Cannot parse the utterance at "));
			}
		}
		return new ParseForm(null, new Exception("Parser is unavaliable."));
	}

	private int tupleNumber = 0;
	private ParseForm displayBestParse(Utterance utt) {
		if (parser instanceof qmul.ds.Parser)
			tuples = new ArrayList<ParserTuple>(
					((qmul.ds.Parser) parser).getState());
		
		// tuples.addAll(ttrtuples);
		tupleNumber = tuples.size() - 1;
		ParserTuple tuple = tuples.get(tupleNumber);
		Tree tree = (tuple == null ? null : tuple.getTree().toStanfordTree());
		TTRFormula sem = (tuple == null ? null : tuple.getSemantics());
		return new ParseForm(utt, sem, tree);
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		edu.stanford.nlp.util.DisabledPreferencesFactory.install();
		String dataFilename = null;
		String parserFilename = DyLanParser.ENGLISHTTRURL;
		DyLanParser parser = new DyLanParser(parserFilename, dataFilename);
	}
	
	
	
	/**
	 * Worker thread for loading the parser. Loading a parser usually takes ~2
	 * min
	 */
	private class LoadParserThread extends Thread {
		String filename;
		String parserType = interactive;// either 'depth-first' or
		
		// 'breadth-first'
		static final String breadthFirst = "Breadth First";
		static final String interactive = "Interactive (Best-First)";

		LoadParserThread(String filename, String parserType) {
			this.filename = filename;
			this.parserType = parserType;
			
		}

		@Override
		public void run() {
			try {

				if (this.parserType.equalsIgnoreCase(breadthFirst)) 
					parser = new ContextParser(filename);
				else if (this.parserType.equalsIgnoreCase(interactive))
					parser = new InteractiveContextParser(filename);
				else
					parser = null;

				logger.info("loaded parser");
				// parser = new SimpleParser(filename);
				initParser();
				logger.info("Initialised Parser");
			} catch (Exception ex) {
				logger.error(ex.getMessage());
				parser = null;

			} catch (OutOfMemoryError e) {
				logger.error(e.getMessage());
				parser = null;
			}
		}
	}

}
