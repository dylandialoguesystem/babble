
dir_root = fileparts(which('setup.m'));

% root dirs in path
matlab_root = fullfile(dir_root,'/matlab/');
tools_dir = fullfile(matlab_root,'/toolbox/');
cell2csv_dir = fullfile(tools_dir,'/cell2csv/');

featExrt_dir = fullfile(matlab_root,'/feature_extraction/');
eval_dir = fullfile(matlab_root,'/eval/');
addpath(matlab_root,tools_dir,featExrt_dir,eval_dir,cell2csv_dir);

% % model dirs in path
% sgdLR_model = fullfile(models_dir,'/sgdLR/');
% probab_tool = fullfile(sgdLR_model,'/probab/');
% estimate_tool = fullfile(probab_tool,'/estimation/');
% addpath(sgdLR_model,probab_tool,estimate_tool);

savepath;

if exist('vl_version') ~= 3,
 run(fullfile(dir_root,'/matlab/vlfeat/toolbox/vl_setup'));
end