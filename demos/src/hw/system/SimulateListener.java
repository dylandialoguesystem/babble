package hw.system;

public interface SimulateListener {
	public abstract void updateSimulationStatus(String status); 
	public abstract void updateObjDescript(int index);
}
