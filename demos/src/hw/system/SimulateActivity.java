package hw.system;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import org.apache.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import babble.simulation.SimulateTutor;
import babble.simulation.VisualObjRecord;
import babble.vision.gui.AbstractActivityListener;
import babble.vision.gui.ClassifierPanel;
import babble.vision.gui.DSTypes;
import babble.vision.gui.DrawPanel;
import babble.vision.gui.StatusBar;
import babble.vision.tools.Constants;
import babble.vision.tools.VSTimer;
import hw.system.dm.SimulateDialog;
import hw.system.dm.SimulateIM;
import hw.system.dm.UttTyps;
import qmul.ds.formula.TTRRecordType;
import qmul.ds.gui.FormulaPanel;

/**
 * Build the simulation activity GUI for managing all system components and create a 
 * text-based dialog system with simulated users, this system will simulate the whole 
 * dialog of teaching process between real human tutor and TeachBot system.
 * 
 * @author Yanchao Yu
 */
public class SimulateActivity extends JFrame implements AbstractActivityListener, SimulateListener{
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(SimulateActivity.class);
	private SimulateIM dm;
	
	private boolean isStart = false;
	
	private JTextPane digText;
	private FormulaPanel semPanel, visualPanel, nlgPanel;
	private ClassifierPanel clasPanel;
	private ChartPanel histPanel;
	private DrawPanel snapPanel;
	
	private List<JComponent> componentList = new ArrayList<JComponent>();

	private Border regularBorder = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
	private Border highligtBorder = BorderFactory.createLineBorder(Color.RED, 3);
	private boolean hasHightLight = true;
	private SimulateTutor simulUsr;
	public SimulateActivity(DialogInitiative initiative, boolean hasUncertainty, boolean hasContextDependency, double threshold){
		initialGUI();
		initialComponents();
	}

	/**
	 * pack all components together.
	 */
	private static final int width = 1100;
	private static final int height = 700;
	
	private StatusBar statusBar;
	private void initialGUI() {
		this.setTitle("TeachBot System");
		this.setSize(width, height);
		this.setPreferredSize(new Dimension(width + 10, height));
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setVisualBox();
		
		JPanel mainPanel = new JPanel();
		mainPanel.setPreferredSize(new Dimension(width*3/4 + 5 , height));
		mainPanel.setLayout(new BorderLayout());
		this.add(mainPanel, BorderLayout.CENTER);
		mainPanel.setBackground(Color.red);
		
		mainPanel.add(buildSemanticBox(), BorderLayout.CENTER);
		mainPanel.add(buildVsBox(), BorderLayout.SOUTH);
		
		this.setJMenuBar(getMainmenu());
		statusBar = new StatusBar();
		this.add(statusBar,BorderLayout.SOUTH);
		this.pack();
		
		this.setVisible(true);
		
		
		for(JComponent componenet: this.componentList){
			final JComponent temp = componenet;
    		temp.setBorder(regularBorder);
			temp.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseEntered(java.awt.event.MouseEvent evt) {
                	if(hasHightLight)
                		temp.setBorder(highligtBorder);
                }
                public void mouseExited(java.awt.event.MouseEvent evt) {
                	if(hasHightLight)
                		temp.setBorder(regularBorder);
                }
            });
		}
	}

	private final int numOfrows = 1;
	private final int numOfcols = 3;
	private final int gap = 5;
	/**
	 * Setup Visual Panel that contains: 
	 * 1) snapshot of the real object,
	 * 2) feature histogram 
	 */
	private JPanel buildVsBox(){
		JPanel vsBox = new JPanel();
		vsBox.setLayout(new GridLayout(numOfrows, 2, gap, gap));
		vsBox.setPreferredSize(new Dimension(width*2/3, height*3/8));
		
		// panel for showing snapshot of the real object
		JPanel snpGroupBox = new JPanel();
		snpGroupBox.setPreferredSize(new Dimension(width/3-10, height*3/8));
		snpGroupBox.setLayout(new BorderLayout());
		TitledBorder snpTitle = BorderFactory.createTitledBorder("Snapshot");
		snpGroupBox.setBorder(snpTitle);
		snapPanel = new DrawPanel();
		snapPanel.setPreferredSize(new Dimension(width/3-5, height*3/8));
		snapPanel.setBackground(Color.WHITE);
		snpGroupBox.add(snapPanel, BorderLayout.CENTER);
		vsBox.add(snpGroupBox);
		this.componentList.add(snapPanel);
		
		JPanel histGroupBox = new JPanel();
		histGroupBox.setPreferredSize(new Dimension(new Dimension(width/3-5, height*3/8)));
		histGroupBox.setLayout(new BorderLayout());
		TitledBorder histTitle = BorderFactory.createTitledBorder("Histogram");
		histGroupBox.setBorder(histTitle);
		histPanel = new ChartPanel(initialJFreeChart());
		histPanel.setPreferredSize(new Dimension(new Dimension(width/3-5, height*3/8)));
		histGroupBox.add(histPanel, BorderLayout.CENTER);
		vsBox.add(histGroupBox);
		this.componentList.add(histPanel);
		
		return vsBox;
	}
	
	/**
	 * Setup the Visual Box that contains all classification results and the corresponding visual context produced in TTR format
	 */
	private void setVisualBox(){
		JPanel visualBox = new JPanel();
		visualBox.setPreferredSize(new Dimension(new Dimension(width*1/4-5, height*3/8)));
		visualBox.setLayout(new BorderLayout());
		TitledBorder visualTitle = BorderFactory.createTitledBorder("Visual Context");
		visualBox.setBorder(visualTitle);
		
//		JTabbedPane ttrTab = new JTabbedPane();
//		ttrTab.setPreferredSize(new Dimension(width*1/4-5, height));
	    visualPanel =new FormulaPanel();
	    visualPanel.setBackground(Color.white);
	    visualPanel.setPreferredSize(new Dimension(width*1/4-5, height*3/8));
		JScrollPane visualScroll = new JScrollPane(visualPanel);
		visualScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		visualPanel.setContainer(visualScroll);
		this.componentList.add(visualPanel);
		
		clasPanel = new ClassifierPanel(this);
		clasPanel.setPreferredSize(new Dimension(width/3-5, height*5/8));
		try {
			clasPanel.collectClassifierPanels();
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		this.componentList.add(clasPanel);
		
//		
//	    ttrTab.addTab("Visual Context", visualPanel);
//	    ttrTab.addTab("Classification Results", clasPanel);
//	    visualBox.add(ttrTab, BorderLayout.CENTER);

	    visualBox.add(visualPanel, BorderLayout.NORTH);
	    visualBox.add(clasPanel, BorderLayout.CENTER);
		
	    this.add(visualBox, BorderLayout.EAST);
	}

	/**
	 * Build Semantic Box that contains all dialogue-related GUI components:
	 * 1) dialogue box that shows entire dialogue between the tutor and the learning agent; 
	 * 2) semantic box that displays the semantic representation in the TTR form parsed via the DyLan;
	 * 3) nlg box that shows the semantic representation produced via unification with semantic and visual context;
	 * @return JPanel Semantic Box
	 */
	private JScrollPane digScroll;
	private JPanel buildSemanticBox() {
		JPanel semanicBox = new JPanel();
		semanicBox.setLayout(new GridLayout(numOfrows, numOfcols, gap, gap));
		semanicBox.setPreferredSize(new Dimension(width*3/4 + 5, height*4/8));

		JPanel dlgGroupBox = new JPanel();
		dlgGroupBox.setPreferredSize(new Dimension(new Dimension(width/4-5, height*4/8)));
		dlgGroupBox.setLayout(new BorderLayout());
		TitledBorder dlgTitle = BorderFactory.createTitledBorder("Dialog");
		dlgGroupBox.setBorder(dlgTitle);
	    digText =new JTextPane();
	    digText.setEditable(false);
	    digText.setBackground(Color.white);
	    digText.setFont(new Font("Serif", Font.PLAIN , 20));
	    digText.setPreferredSize(new Dimension(width/4-5, height*4/8));
		digScroll = new JScrollPane(digText);
		digScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		dlgGroupBox.add(digScroll, BorderLayout.CENTER);
		
		this.componentList.add(digText);
		semanicBox.add(dlgGroupBox);
	    
		JPanel semGroupBox = new JPanel();
		semGroupBox.setPreferredSize(new Dimension(new Dimension(width/4-5, height*4/8)));
		semGroupBox.setLayout(new BorderLayout());
		TitledBorder semTitle = BorderFactory.createTitledBorder("Dialogue Context");
		semGroupBox.setBorder(semTitle);
		semPanel =new FormulaPanel();
	    semPanel.setBackground(Color.white);
	    semPanel.setPreferredSize(new Dimension(width/4-5, height*4/8));
		JScrollPane semScroll = new JScrollPane(semPanel);
		semScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
	    semPanel.setContainer(semScroll);
	    semGroupBox.add(semPanel, BorderLayout.CENTER);
		this.componentList.add(semPanel);
		semanicBox.add(semGroupBox);
		
		JPanel nlgGroupBox = new JPanel();
		nlgGroupBox.setPreferredSize(new Dimension(new Dimension(width/4-5, height*4/8)));
		nlgGroupBox.setLayout(new BorderLayout());
		TitledBorder nlgTitle = BorderFactory.createTitledBorder("Generation Goal");
		nlgGroupBox.setBorder(nlgTitle);
		nlgPanel =new FormulaPanel();
		nlgPanel.setBackground(Color.white);
		nlgPanel.setPreferredSize(new Dimension(width/4-5, height*4/8));
		JScrollPane contextScroll = new JScrollPane(nlgPanel);
		contextScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		nlgPanel.setContainer(contextScroll);
		nlgGroupBox.add(nlgPanel, BorderLayout.CENTER);
		this.componentList.add(nlgPanel);
		semanicBox.add(nlgGroupBox);
		
		return semanicBox;
	}
	
	/**
	 * Setup Histogram Chart panel for showing the extracted visual features 
	 * from the specific object
	 */
	private XYSeries series;
	private JFreeChart initialJFreeChart(){
		series = new XYSeries("Feature");
		final XYSeriesCollection dataset = new XYSeriesCollection(series);
		
		String plotTitle = "Feature Histogram";
    	String xaxis = "bin #";
    	String yaxis = "val";
    	PlotOrientation orientation = PlotOrientation.VERTICAL;
    	boolean showLegend = true;
    	boolean toolTips = true;
    	boolean urls = false;
    	JFreeChart jchart = ChartFactory.createXYBarChart(plotTitle,xaxis,false,yaxis,dataset,orientation,showLegend,toolTips,urls);
    	
    	return jchart;
	}
	
	/**
	 * Method to get new MenuBar
	 * @return new JMenuBar
	 */
	public JMenuBar getMainmenu(){
		JMenuBar mainMenuBar = new JMenuBar();		
		JMenu sysMenu = new JMenu("System"); 	
		final JMenuItem switchItem = new JMenuItem("Start");
		switchItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(switchItem.getText().equals("Start")){
					simulUsr.switchUserModel(true);
					switchItem.setText("Stop");
					isStart = true;
				}
				else{
					simulUsr.switchUserModel(false);
					resetGUI();
					switchItem.setText("Start");
					isStart = false;
				}
			}
			
		});
		sysMenu.add(switchItem);
		
		final JMenuItem pauseItem = new JMenuItem("Pause");
		pauseItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(isStart){
					if(pauseItem.getText().equals("Pause")){
						simulUsr.switchDialogue(false);
						pauseItem.setText("Continue");
					}
					else if(pauseItem.getText().equals("Continue")){ 
						simulUsr.switchDialogue(true);
						pauseItem.setText("Pause");
					}
				}
			}
			
		});
		sysMenu.add(pauseItem);
		
		final JMenuItem highlightItem = new JMenuItem("Regular");
		if(this.hasHightLight){
			highlightItem.setText("HighLight");
		}
		highlightItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(highlightItem.getText().equals("HighLight")){
					highlightItem.setText("Regular");
					hasHightLight = false;
				}
				else if(highlightItem.getText().equals("Regular")){ 
					highlightItem.setText("HighLight");
					hasHightLight = true;
				}
			}
			
		});
		sysMenu.add(highlightItem);		
		
		JMenuItem exportItem = new JMenuItem("Export");
		exportItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				 logger.debug("export all processes data");
			}
			
		});
		sysMenu.add(exportItem);
		
		JMenuItem exitItem = new JMenuItem("Exit");
		exitItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				int option = JOptionPane.showConfirmDialog(null, "Do you want to exit?", "Exit",JOptionPane.YES_NO_OPTION);
				 if(option == JOptionPane.YES_NO_OPTION){
					 logger.debug("exit the simulation activity.");
					 System.exit(0);
				 }
			}
			
		});
		sysMenu.add(exitItem);		
		mainMenuBar.add(sysMenu);
		
		return mainMenuBar;
	}
	
	private void resetGUI(){
		snapPanel.clear();
		histPanel.removeAll();
		histPanel.revalidate();
		histPanel.repaint();
		
		visualPanel.removeAll();
		visualPanel.revalidate();
		visualPanel.repaint();
		
		semPanel.removeAll();
		semPanel.revalidate();
		semPanel.repaint();
		
		nlgPanel.removeAll();
		nlgPanel.revalidate();
		nlgPanel.repaint();
		
		digText.setText("");
	}
	
	/************************** Initial Functional Components ****************************/
	private void initialComponents(){
		SimulateDialog ds = new SimulateDialog(Constants.simulateUserID);
		dm = new SimulateIM(ds);
		dm.configPolicy(Constants.configFile);
		dm.registerActivityListener(this);
		
		dm.initialDyLanModule(Constants.parserFilename);
		dm.initialVisionModule(new SimulateVisionModule());
		
		Map<String, List<VisualObjRecord>> datMap = buildupTrnTst(400);
		simulUsr = new SimulateTutor(this, this, dm, datMap);
		dm.setTstSet(datMap.get("tstList"));
	}
	
	/************************** Simulation Functionalities ****************************/
	
	private boolean isNewUtt = true;
	private void switchUserUttStatus(boolean switcher){
		isNewUtt = switcher;
	}
	
	private String prevUtt; 
	private int indexOfline = 0;
	@Override
	public void updateDialog(String utt, UttTyps type, boolean isStopped) {
		if(digText != null){
			String dialog = utt;
			indexOfline = this.digText.getText().length();
			
			switch(type){
				case user:
						dm.parse(utt);
						if(isNewUtt){
							dialog = "T: " + dialog;
							switchUserUttStatus(false);
						}
						
						if(!isStopped){
							dialog += Constants.SPACE;
						}
						else{
							dialog += Constants.NEWLINE;
							switchUserUttStatus(true);
						}
						
						try{	
							SimpleAttributeSet userSetting = new SimpleAttributeSet();
							StyleConstants.setForeground(userSetting, Color.GREEN);
							StyleConstants.setFontSize(userSetting, Constants.fontSize);
							String str = VSTimer.getInstance().getTimeStamp() +" -- "+dialog;
							this.digText.getDocument().insertString(indexOfline, str, userSetting);

							digScroll.getViewport().setViewPosition(new Point(0,digText.getDocument().getLength()));
						} catch (BadLocationException e) {
							logger.error(e.getMessage());
						}
					break;
				case system: 
					try {
						dialog += Constants.NEWLINE;
						dialog = "S: " + dialog;
						SimpleAttributeSet sysSetting = new SimpleAttributeSet();
						StyleConstants.setForeground(sysSetting, Color.BLUE);	
						StyleConstants.setFontSize(sysSetting, Constants.fontSize);
						String str = VSTimer.getInstance().getTimeStamp() +" -- "+dialog;
						digText.getDocument().insertString(indexOfline, str, sysSetting);
						
						digScroll.getViewport().setViewPosition(new Point(0,digText.getDocument().getLength()));
						
						Thread.sleep(1000);
						
						simulUsr.getUserRespond(dialog);
					} catch (BadLocationException e) {
						logger.error(e.getMessage());
					} catch (InterruptedException e) {
						logger.error(e.getMessage());
					}
			break;
			}
		}
	}

	@Override
	public void updateObjDescript(int index) {
		logger.info("Objects in total: " + index);
		try{
			SimpleAttributeSet userSetting = new SimpleAttributeSet();
			StyleConstants.setForeground(userSetting, Color.GRAY);
			StyleConstants.setFontSize(userSetting, Constants.fontSize);
			String str = "---- " + "NewObject :: " + index + " ----" + Constants.NEWLINE;
			
			this.digText.getDocument().insertString(this.digText.getText().length(), str, userSetting);
			
			digScroll.getViewport().setViewPosition(new Point(0,digText.getDocument().getLength()));
		} catch (BadLocationException e) {
			logger.error(e.getMessage());
		}
		
	}

	@Override
	public void updateSimulationStatus(String status) {
		status = status + Constants.NEWLINE;
		
		try {
			SimpleAttributeSet sysSetting = new SimpleAttributeSet();
			StyleConstants.setForeground(sysSetting, Color.red);	
			StyleConstants.setFontSize(sysSetting, Constants.fontSize);
			digText.getDocument().insertString(this.digText.getText().length(), status, sysSetting);

			digScroll.getViewport().setViewPosition(new Point(0,digText.getDocument().getLength()));
		} catch (BadLocationException e) {
			logger.error(e.getMessage());
		}
		
		this.updateStatus(status);
	}
	
	@Override
	public void updateTTRRecords(TTRRecordType f, DSTypes type) {
		if(f != null){
			logger.info(type.toString()+" :: " + f.toString());
			switch(type){
			case semanticTTR:
				if(semPanel != null)
					semPanel.setFormula(f);
				break;
			case visualTTR:
				if(visualPanel != null){
					visualPanel.setFormula(f);
				}
				break;
			case contextTTR:
				if(nlgPanel != null){
					nlgPanel.setFormula(f);
				}
				break;
			}
		}
	}

	@Override
	public void updateImage(BufferedImage img) {
		if(img != null && snapPanel != null){
			snapPanel.addImage(img, false);
			snapPanel.validate();
			snapPanel.repaint();
		}
	}

	@Override
	public void updateHist(double[] hist) {
		if(hist != null && hist.length != 0){
			
			if(series != null){
				if(series.isEmpty()){
					for(int i=0; i < hist.length; i++){
						series.add(i, hist[i]);
					}
				}
				else{
					for(int i=0; i < hist.length; i++){
						series.update(Double.valueOf(i), hist[i]);
					}
					series.fireSeriesChanged();
				}
				
				if(histPanel != null){
		    		histPanel.validate();
		    		histPanel.repaint();
		    	}
			}
		}
	}
	
	@Override
	public void updateClassifierResults(Map<String, Double> classifierMap) {
		if(this.clasPanel != null){
			this.clasPanel.updateResults(classifierMap);
		}
	}

	@Override
	public void updateStatus(String msg) {
		statusBar.setStatusTip(msg);
	}

	/**
	 * Temporary!! 
	 */
	public void sysBargin(){
		
	}
	
	/**
	 * Waiting for the next word input, set timeout is 10s. if word is added in time, 
	 * the action will be stopped, otherwise a timeout action will be setup for processing 
	 * the existing phrases in the message set!
	 * 
	 * @author Yanchao Yu
	 */
	class TurnChecker{
		private JFrame activity;
		private Timer timer;
		
		public TurnChecker(JFrame activity){
			this.activity = activity;
		}
		
		/**
		 * Start a timer task for processing the existing words in 10s.
		 */
		public void startTimer(){
			timer = new Timer("TurnCheck", true);
			timer.schedule(new TimerTask(){
				@Override
				public void run() {
					if(activity instanceof SimulateActivity){
						((SimulateActivity)activity).sysBargin();
					}
				}
			}, Constants.UNIT * 5);
			if(activity instanceof SimulateActivity){
				((SimulateActivity)activity).updateStatus("Start at" + new Date());
			}
		}
		
		/**
		 * Stop the timer for next word!
		 */
		public void stopTimer(){
			if(timer != null){
				timer.cancel();
				timer.purge();
				
				if(activity instanceof SimulateActivity){
					((SimulateActivity)activity).updateStatus("Stop at" + new Date());
				}
			}
		}
	}
	
	private Map<String, List<VisualObjRecord>> buildupTrnTst(int split){
		BufferedReader br = null;
		try{
			File annotFile = new File(Constants.annotTrnSet);
			if(annotFile.exists()){
				logger.info("Read New File!!!");
				FileInputStream fstream = new FileInputStream(annotFile);
				br = new BufferedReader(new InputStreamReader(fstream));

				List<VisualObjRecord> dataList = new ArrayList<VisualObjRecord>();
				
				int count = 0;
				String strLine;
				while ((strLine = br.readLine()) != null){
					if(!strLine.trim().equals("")){
						String[] items = strLine.split(",");
						String filename = items[0];
						String description = items[1];
	
						double[] feat = new double[1280];
						String featVal = items[2].trim().toString();
						String[] vals = featVal.split(" ");
						for(int i=0;i<vals.length;i++){
							  if(vals[i].equals("NaN"))
								  feat[i] = 0;
							  else
								  feat[i] = Double.valueOf(vals[i].trim());
						 }
						
						VisualObjRecord record = new VisualObjRecord(count++, filename, description);
						record.setFeat(feat);
						dataList.add(record);
					}
				}

				dataList = flush(dataList);
				
				List<VisualObjRecord> trnList = dataList.subList(0, split);
				logger.info("A. trnList Size: " + trnList.size());
				List<VisualObjRecord> tstList = dataList.subList(split, dataList.size());
				logger.info("B. tstList Size: " + tstList.size());
				
				Map<String, List<VisualObjRecord>> dataMap = new HashMap<String, List<VisualObjRecord>>();
				dataMap.put("trnList", trnList);
				dataMap.put("tstList", tstList);
				
//				Set<String> keySet = datMap.keySet();
//				for(String key: keySet){
//					logger.info(key + ": ");
//					List<VisualObjRecord> list = datMap.get(key);
//					int count = 1;
//					for(VisualObjRecord record: list){
//						logger.info("Record["+ count++ +"]: " + record.getFilename() + " ~ " + record.getDescription());
//					}
//				}
				
				return dataMap;
			}
			else
				logger.error("File isn't existed: " + annotFile.getPath());
		}
		catch(Exception e){
			logger.error(e.getMessage());
		}
		
		return null;
	}
	
	public List<VisualObjRecord> flush(List<VisualObjRecord> collection){
		if(collection != null && !collection.isEmpty()){
			List<VisualObjRecord> clone = new ArrayList<VisualObjRecord>(collection);
		    Collections.shuffle(clone);
		    
		    return clone;
		}
		return null;
	}
	
	/****************************** Main Method *******************************/
	public static void main(String[] args){
//		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		boolean hasUncertainty = false;
		boolean hasContextDependency = false;
		DialogInitiative initiative = DialogInitiative.Tutor;
		double threshold = 0.9;
		
		SimulateActivity va = new SimulateActivity(initiative, hasUncertainty, hasContextDependency, threshold);
	}
	
	public enum DialogInitiative{
		Tutor,
		Learner;
	}
}
