package hw.system.dm;

import java.util.Date;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import babble.vision.tools.DAState;
import babble.vision.tools.VSTimer;

public class NonTTRTaskManager {
	Logger logger = Logger.getLogger(NonTTRTaskManager.class);
	
	public NonTTRTaskManager(){
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject runSocialGreeting(SGState state){
		JSONObject sysJson = new JSONObject();
		String sysUtt = null;
		DAState sysDA = null;
		
		switch(state){
			case checkConnection:
				sysUtt = "hi, can you hear me?";
				sysDA = DAState.listening;
				break;
			case goodbye:
				sysUtt = "thanks for teaching, see you later.";
				break;
			case greeting:
				sysUtt = "hi, i'm teachbot. i'm learning knowledge about objects.";
				sysDA = DAState.listening;
				break;
			case help:
				sysUtt = "sorry, i don't know about this.";
				sysDA = DAState.waitingforanswer;
				break;
			case wakeup:
				sysUtt = "hi, i'm here. what can i do for you?";
				sysDA = DAState.listening;
				break;
		}

		sysJson.put("sysUtt", sysUtt);
		sysJson.put("sysDA", sysDA);
		sysJson.put("timestamp", VSTimer.getInstance().getTimeStamp());
		sysJson.put("dimension", "social");

		return sysJson;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject runFeedback(FBState state, String msg){
		JSONObject sysJson = new JSONObject();
		String sysUtt = null;
		DAState sysDA = null;
		
		switch(state){
		case donotKnow:
			sysUtt = "sorry, i don't know about this.";
			sysDA = DAState.waitingforanswer;
			break;
		case ignore:
			sysUtt = "sorry, i'm busy now. please wait for a second.";
			break;
		case learnFailed:
			sysUtt = "sorry, i can't learn this object.";
			break;
		case requestMore:
			sysUtt = "can you show me more instances about " + msg + "?";
			sysDA = DAState.listening;
			break;
		case requestNew:
			sysUtt = "can you show me a new object?";
			sysDA = DAState.listening;
			break;
		case unclearAnswer:
			sysUtt = "sorry, i can't understand what you saying.";
			break;
		case unclearUtt:
			sysUtt = "sorry, i can't understand what you saying.";
			break;
		case unacceptedQ:
			sysUtt = "sorry, question cannot be accepted here.";
			break;
		}

		sysJson.put("sysUtt", sysUtt);
		sysJson.put("sysDA", sysDA);
		sysJson.put("timestamp", VSTimer.getInstance().getTimeStamp());
		sysJson.put("dimension", "feedback");
	
		return sysJson;
	}

	
	public enum SGState{
		greeting, 
		goodbye, 
		checkConnection, 
		help, 
		wakeup;
	}
	
	public enum FBState{
		donotKnow, 
		unclearUtt, 
		unclearAnswer, 
		learnFailed, 
		requestMore, 
		requestNew,
		unacceptedQ,
		ignore;
	}
	
	public static void main(String[] args){
		NonTTRTaskManager fbManager = new NonTTRTaskManager();
		System.out.println("Respond JSON: " + fbManager.runFeedback(FBState.donotKnow, null));
		System.out.println("Respond JSON: " + fbManager.runFeedback(FBState.unclearUtt, null));
		System.out.println("Respond JSON: " + fbManager.runFeedback(FBState.learnFailed, null));
		System.out.println("Respond JSON: " + fbManager.runFeedback(FBState.unclearAnswer, null));
		System.out.println("Respond JSON: " + fbManager.runFeedback(FBState.requestMore, "red"));
		System.out.println("Respond JSON: " + fbManager.runFeedback(FBState.requestNew, null));
		System.out.println("Respond JSON: " + fbManager.runFeedback(FBState.unacceptedQ, null));
		System.out.println("Respond JSON: " + fbManager.runFeedback(FBState.ignore, null));
		

		System.out.println("Respond JSON: " + fbManager.runSocialGreeting(SGState.goodbye));
		System.out.println("Respond JSON: " + fbManager.runSocialGreeting(SGState.greeting));
		System.out.println("Respond JSON: " + fbManager.runSocialGreeting(SGState.checkConnection));
		System.out.println("Respond JSON: " + fbManager.runSocialGreeting(SGState.help));
		System.out.println("Respond JSON: " + fbManager.runSocialGreeting(SGState.wakeup));
	}
}
