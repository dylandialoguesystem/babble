package hw.system.dm;

import org.apache.log4j.Logger;

import babble.system.AbstractVisionModule;
import babble.system.DyLanlistener;
import babble.system.dm.AbstractDialogManager;
import babble.system.dm.AbstractBabbleDialog;
import babble.vision.context.VisualContext;
import babble.vision.gui.AbstractActivityListener;
import babble.vision.gui.DSTypes;
import babble.vision.tools.Constants;
import qmul.ds.Utterance;
import qmul.ds.formula.TTRFormula;
import qmul.ds.formula.TTRRecordType;
import qmul.ds.tree.Tree;

/**
 * Rule-based Dialog Manager (DM) for managing the whole learning process,
 * it implements the ds module and vision module
 *  
 * @author Yanchao Yu
 */
public class SimulateIM extends AbstractDialogManager implements DyLanlistener{
	static Logger logger = Logger.getLogger(SimulateIM.class);

	public SimulateIM(AbstractBabbleDialog babbleDialog) {
		super(babbleDialog);
		
//		if(super.babbleDialog instanceof SimulateDialog){
//			((SimulateDialog)super.babbleDialog).nonTTRManager = new NonTTRTaskManager();
//		}
	}
	
	/**
	 * @return flag if the Dylan module is ready for use.
	 */
	public Boolean isdmReady(){
		if (super.getBabbleDialog().dylanModule.fullyLoaded()){
			return true;
		}
		return false;
	}
	
	/*********************************** DyLan Module: Parser & Generator (DSModule) **************************/
	@Override
	public void initialDyLanModule(String path) {
		super.initialDyLanModule(path, this);
	}
	
	/**
	 * Parse the Utterance from user/system
	 * @param utt utterance will be parsed
	 * @param uttype utterance type, e.g. user and system 
	 */
	public void parse(String utt, UttTyps uttype){
		if(utt != null && utt.length() != 0){
			String paserUtt = "";
			switch(uttype){
			case user:
				paserUtt = Constants.TUTOR + utt;
				break;
			case system:
				paserUtt = Constants.SYS + utt;
				break;
			}
			super.getBabbleDialog().dylanModule.parse(paserUtt,"");
		}
	}
	
	/**
	 * Parse the Utterance from user, PS: the utterance is only parsed from user.
	 * @param utt utterance will be parsed
	 */
	public void parse(String utt){
		if(utt != null && utt.length() != 0){
			String paserUtt = Constants.TUTOR + utt;
			super.getBabbleDialog().dylanModule.parse(paserUtt,"");
		}
	}
		
	boolean isBargin = false;
	private TTRRecordType currentSemantic;
	@Override
	public void getSemantic(String utt, TTRFormula ttr, Tree tree, String action, boolean isGrounded) { 
		logger.debug(" Utt: " + utt + "\r\n TTRFormula: " + ttr + "\r\n Tree: " + tree);
		AbstractBabbleDialog dialog = super.getBabbleDialog();
		
		if(dialog instanceof SimulateDialog){
			((SimulateDialog)dialog).isGrounded = isGrounded;
			logger.debug(" is DC Grounded: " + isGrounded);
			
			if(utt.startsWith(Constants.TUTOR)){
				((SimulateDialog)dialog).cancelChecker();
				
				try {
					Thread.sleep(Constants.UNIT * 1);
				} catch (InterruptedException e) {
					logger.error(e.getMessage());
				}
				
				utt = utt.replace("T:", "");
				((SimulateDialog)dialog).updateSettings("isWHQuestion", utt.trim().startsWith("what"));
				logger.debug("isWHQuestion: " + utt.trim().startsWith("what") + " ~ /" + utt.trim()+"/");
				
				// update semantic TTR representations from Tutor
				super.actlistener.updateTTRRecords(((TTRRecordType)ttr), DSTypes.semanticTTR);
				
				// setup the flag if user keeps talking
				((SimulateDialog)dialog).updateSettings("userTalking", !tree.isComplete());
				((SimulateDialog)dialog).updateSettings("userUttParsed:done", tree.isComplete());
				
				if(ttr.toString().equals("[]")){ // if parse successfully
					((SimulateDialog)dialog).updateSettings("userUttParsed:null", true);
					logger.debug("Running runWithoutTTR");
					
					((SimulateDialog)dialog).runWithoutTTR(utt);
				}
				else{
					((SimulateDialog)dialog).updateSettings("userUttParsed:null", false);
					
					boolean isQuestion = ((TTRRecordType)ttr).hasFieldbyType("question");
		//			Node root = tree.getRootNode();
		//			boolean isQuestion = (root.first().toString().equals("+Q");
					
					if(isQuestion)
						((SimulateDialog)dialog).updateSettings("userUttQuestion", true);
					else
						((SimulateDialog)dialog).updateSettings("userUttQuestion", false);
					
					logger.debug("Running runWithTTR");
						
					currentSemantic = (TTRRecordType)ttr;
					((SimulateDialog)dialog).runWithTTR(ttr,tree);
				}
			}
		}
	}
	
	@Override
	public void getNLG(String utt, UttTyps type) {
		super.actlistener.updateDialog(utt, type, true);
	}
	
	
//	public void sysBargin(){
//		if(this.currentSemantic != null && !currentSemantic.isEmpty()){
//			
//			this.((DSVersion2)dialog).doAction(DActions.bargin, this.currentSemantic);
//			
//		}
//		else
//			logger.error("nothing to process in the dialog...");
//	}
//	
//	
//	public ActivityListener getLisnter(){
//		return this.actListener;
//	}
//	/********************************************** END OF DSModule ********************************************/
//	
//	/****************** Vision Module: Object Capture/Extractor/Classifier/Visual Context ***********************/
//	/**
//	 * Initial the Vision Module that will manage the camera, capture objects, 
//	 * extract features for classification as well as generating the visual 
//	 * context.
//	 */
//	private void initialVisionModule(double threshold){
//		((DSVersion2)dialog).nLC = new VisualContext(actListener);
//		
//		if(((DSVersion2)dialog).visionModule == null){
//			((DSVersion2)dialog).visionModule = new DemoVisionModule(((DSVersion2)dialog).nLC, threshold);
//			((DSVersion2)dialog).visionModule.registerListener(this.actListener);
//		}
//	}
//	
//	/**
//	 * Switch the camera for real-time video capture
//	 */
//	public void switchCamera(){
//		boolean switchFlag = !((DSVersion2)dialog).visionModule.isCamAlived();
//		((DSVersion2)dialog).visionModule.switchCamera(switchFlag);
//		((DSVersion2)dialog).switchSGChecker(switchFlag);
//	}
	
//	/**
//	 * Switch the function of processing new object
//	 * @param flag if switch on/off the "New Object" function
//	 */
//	public void captureNewObject(boolean flag){
//		try {
//			((DSVersion2)dialog).visionModule.switchVisionCapture(flag);
//			if(!flag)
//				((DSVersion2)dialog).goalContxt.initialGoalContext();
//			
//		} catch (IOException e) {
//			logger.error(e.getMessage());
//		}
//	}

	public void switchCheckers(boolean b) {
		AbstractBabbleDialog dialog = super.getBabbleDialog();
		if(dialog instanceof SimulateDialog)
			((SimulateDialog)dialog).switchSGChecker(b);
	}

	@Override
	public void configPolicy(String path) {
		
	}

	@Override
	public void captueNewObject(double[] feat) {
		AbstractBabbleDialog dialog = super.getBabbleDialog();
		if(dialog instanceof SimulateDialog){
			((SimulateDialog)dialog).visionModule.importObject(feat);
			((SimulateDialog)dialog).targetContext.initial();
		}
	}

	@Override
	public void captueNewObject(boolean flag) {
		
	}

	@Override
	public void initialVisionModule(AbstractVisionModule vsmodule) {
		if(this.actlistener != null){
			babbleDialog.nLC = new VisualContext(this.actlistener);
			
			if(babbleDialog.visionModule == null){
				babbleDialog.visionModule = vsmodule;
				babbleDialog.visionModule.setVisualContext(babbleDialog.nLC);
				babbleDialog.visionModule.registerListener(this.actlistener, false);
			}
		}
		else
			logger.error("Activity Listener is unavaliable...");
	}

	@Override
	public void throwException(Utterance utt, Exception e) {
		// TODO Auto-generated method stub
		
	}
}
