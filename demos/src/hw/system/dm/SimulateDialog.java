package hw.system.dm;

import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import babble.system.dm.AbstractBabbleDialog;
import babble.vision.context.RespondTTR;
import babble.vision.gui.AbstractActivityListener;
import babble.vision.gui.DSTypes;
import babble.vision.tools.Constants;
import babble.vision.tools.DAState;
import babble.vision.tools.DActions;
import babble.vision.tools.NLGstate;
import babble.vision.tools.TargetTTRType;
import hw.system.dm.NonTTRTaskManager.FBState;
import hw.system.dm.NonTTRTaskManager.SGState;
import qmul.ds.formula.Formula;
import qmul.ds.formula.TTRField;
import qmul.ds.formula.TTRFormula;
import qmul.ds.formula.TTRLabel;
import qmul.ds.formula.TTRRecordType;
import qmul.ds.formula.Variable;
import qmul.ds.tree.Tree;
import qmul.ds.type.DSType;

/**
 * Class is developed for the version 2 dialog manager rules. It contains all dialog actions 
 * and related rules for learning new objects. Instead of re-implement all components, developers
 * just need to create a new package of DSVersion with new rules for testing all dialog turns. 
 * 
 * TODO: 1) keep tracking all dialog histories in the form of JSON
 * 	     2) perform dialog actions based on pre-defined RULES
 * 		 3) store all possible settings related to dialog system
 * 
 * @author Yanchao Yu
 */
public class SimulateDialog extends AbstractBabbleDialog{
	Logger logger = Logger.getLogger(SimulateDialog.class);

	private NonTTRTaskManager nonTTRManager;
	private TTRBasedTemplateNLG nlg;
	
	private TTRRecordType preSysRespond;
	public SemanticGroundChecker sgChecker;
	public Thread sgThread;
	
	public TargetContext targetContext;
	
	public boolean isGrounded = false; 

	public SimulateDialog(String userID) {
		super(userID);
		nlg = new TTRBasedTemplateNLG();
		nonTTRManager = new NonTTRTaskManager();
		super.initialSettings(null);
		

		List<String> ontologyList = new ArrayList<String>();
		ontologyList.add("color");
		ontologyList.add("shape");
		targetContext = new TargetContext(this,ontologyList);
	}
	
	public void switchSGChecker(boolean runnable){
		logger.debug("Semantic Ground Checker is : " + runnable);
		
		this.isGrounded = false;
		sgChecker = new SemanticGroundChecker();
		sgThread = new Thread(sgChecker);
		sgChecker.runnable = true;
		sgThread.start();
		
	}
	/********************************** Dialog Operations **********************************/
	/**
	 * runRules based on Semantic TTR from DyLan Module
	 * @param ttr semantic TTR representation
	 * @param tree 
	 */
	private TTRRecordType curSemanticTTR;
	private TTRFormula prevhypothesis = null;
	@Override
	public void runWithTTR(TTRFormula ttr, Tree tree){
		curSemanticTTR = (TTRRecordType)ttr;
		boolean isQuestion = dialogSettings.get("userUttQuestion");
		preSysRespond = TTRRecordType.parse("[pred2==circle : cn|p5==class(pred2) : t|x1==this : e|p1==subj(pred2, x1) : t|head==x1 : e|pred1==red : cn|p3==color(pred1) : t|p4==subj(pred1, x1) : t]");
		if(this.dialogSettings.get("userUttParsed:done")){
			TTRRecordType hypothesis = buildHypothesis(ttr,tree);
			logger.debug("Hypothesis TTR is " + hypothesis);
			
			logger.debug("Current System Status: " + this.sysCurrentDS);
			
			// check current status
			switch(this.sysCurrentDS){
				case listening:
						if(isQuestion){
//							this.updateCurrentSysDS(DAState.checking);
							doAction(DActions.subsume, hypothesis);
						}
						else{
							// doing acknowledgement & learning new labels for current object in the scene.
							if(!this.isGrounded)
								doAction(DActions.acknowledge, hypothesis);

//							this.updateCurrentSysDS(DAState.learning);
							doAction(DActions.learn, hypothesis);
						}
						
					break;
				case waitingforconfirm:
					// check if a confirmation from tutor!
					// generating acknowledgement firstly, i.e. okay / yeah.
					// learning the new sample with labels from statements + updating the visual context
					// checking the idle situation and select next dialog action.
					if(!isQuestion){
						logger.debug("get confirmation : " + hypothesis);
						
						if(!this.isGrounded)
							doAction(DActions.acknowledge, hypothesis);

						doAction(DActions.learn, hypothesis);
					}
					else{ // questions won't be accepted while waiting for confirmation form TUTOR
						logger.error("FeedBack: can't accpt questions at waitingforconfirm");
						doFeedBack(FBState.unacceptedQ, null);
					}
					break;
				case waitingforanswer:
					if(!isQuestion){
						logger.debug("get confirmation");
						if(!this.isGrounded)
							doAction(DActions.acknowledge, hypothesis);
						
						doAction(DActions.learn, hypothesis);
					}
					else // questions won't be accepted while waiting for confirmation form TUTOR
						logger.error("FeedBack: can't accpt questions at waitingforconfirm");
					break;
				case bargining:
					logger.debug("user bargin at system bargining");
					logger.error("FeedBack: can't accept bargin while system bargining");
					doFeedBack(FBState.ignore, null);
					break;
				case requestingformore:
					throw new UnsupportedOperationException(); 
				default:
					break;
			}
		}
		else
			logger.debug("waiting for user complete firstly (no bargin) ...");
	}	
	
	/**
	 * Do next action for creating the generator goal TTR.
	 * @param action Dialog action 
	 * @param hypothesis TTR record type that need to be processed... 
	 */
	@Override
	public void doAction(DActions action, TTRRecordType hypothesis) {
		if(this.nLC != null && !this.nLC.isEmpty()){
			TTRRecordType targetTTR = null;
			NLGstate nlgState = null;
			switch(action){
				case subsume:
					TTRField qFeild = hypothesis.getFieldsbyType("question").get(0);
					TTRRecordType copy = hypothesis.removeField("question");
					logger.debug("Question Hypothesis is : " + copy);
					
					TTRRecordType currentVC = null;
					while(!this.nLC.isCompleted()){
						logger.debug(this.nLC.isCompleted());
					}
					currentVC = this.nLC.getCurrentVisualContext();
					
					HashMap<Variable, Variable> subsuMap = new HashMap<Variable, Variable>();
					if(copy.subsumesMapped(currentVC, subsuMap)){
						TTRRecordType answer = retrievalAnswer(copy,currentVC,subsuMap, qFeild);

						String attr =answer.findFormulaByStr("pred").toString();
						double score = this.nLC.getConfidenceScoreByAttribute(attr);
						double threshold = this.nLC.getThresholdByAttribute(attr);
						
						targetTTR = createTargetTTR(TargetTTRType.answer, answer, score, threshold);
						if(dialogSettings.get("isWHQuestion"))
							nlgState = NLGstate.answer;
						else
							nlgState = NLGstate.check;
					}
					else{
						TTRRecordType superType = copy.mostSpecificCommonSuperType(currentVC, new HashMap<Variable, Variable>());
						logger.debug("superType: " + superType);
						if(superType != null && !superType.toString().equals("[]")){
							HashMap<Variable, Variable> subsumMap = new HashMap<Variable, Variable>();
							
							if(superType.subsumesMapped(currentVC, subsumMap)){
								TTRRecordType answer = retrievalAnswer(superType, currentVC, subsumMap, qFeild);
								logger.debug("answerType: " + answer);

								String attr = answer.findFormulaByStr("pred").toString();
								double score = this.nLC.getConfidenceScoreByAttribute(attr);
								double threshold = this.nLC.getThresholdByAttribute(attr);
								
								targetTTR = createTargetTTR(TargetTTRType.answer, answer, score, threshold);
								nlgState = NLGstate.check;
							}
						}
						else{
							targetTTR = createTargetTTR(TargetTTRType.donotKnow, null, -1, 0);
						}
					}
					break;
				case bargin:
					 // only happens on the "listening" status
					if(this.sysCurrentDS.equals(DAState.listening)){
						logger.debug("bargin Utterance: " + hypothesis);
						
						// complete current semantic TTR as question
						TTRRecordType hypo = buildHypothesis(hypothesis,null);
						doAction(DActions.subsume, hypo);
					}
					break;
				case learn:
					if(this.visionModule != null){
						if(this.visionModule.learnObject(hypothesis))
							logger.info("Learning Successfully!!!!");
						else
							logger.error("Learning Failed!!!!");
					}
					break;
				case acknowledge:
					targetTTR = hypothesis;
					generate(targetTTR, "okay.");
//					this.isGrounded = true;
					return;
			}

			logger.debug("Target TTR: " + targetTTR);
			logger.debug("Current System Status: " + this.sysCurrentDS);
			logger.debug("Current nlgState: " + nlgState);
			
			generate(targetTTR, nlgState);
		}
		else
			logger.debug("nothing in the scene...");
	}

	public void requestMore(){
		// find the worst classifier and its attribute name
		if(this.nLC != null){
			String classifier = this.nLC.getWorstClassifier();
			logger.info("WORST CLASSIFIER: " + classifier);
			if(classifier != null)
				doFeedBack(FBState.requestMore, classifier);
			else
				doFeedBack(FBState.requestNew, null);
		}
		else 
			logger.error("VisualContext is unavailable...");
	}
	
	public void generate(TTRRecordType targetTTR, NLGstate nlgState){
		if(targetTTR != null){
			this.actListener.updateTTRRecords(targetTTR, DSTypes.contextTTR);
	//		this.dylanModule.generate(targetTTR);
			JSONObject json = this.nlg.generateSysUttFromTTR(curSemanticTTR, targetTTR, nlgState);
			logger.debug("JSON from Respond: " + json.toString());
			if(json.containsKey("sysDA"))
				this.sysCurrentDS = (DAState)json.get("sysDA");
			
			if(json.containsKey("sysUtt")){
				String sysUtt = json.get("sysUtt").toString();
				dylanModule.parse(Constants.SYS + sysUtt, "");
				this.actListener.updateDialog(sysUtt, UttTyps.system, true);
			}
		}
	}
	
	public void generate(TTRRecordType targetTTR, String utt){
		if(targetTTR != null && utt != null){
			this.actListener.updateTTRRecords(targetTTR, DSTypes.contextTTR);
			this.sysCurrentDS = DAState.listening;
			
			dylanModule.parse(Constants.SYS + utt, "");
			this.actListener.updateDialog(utt, UttTyps.system, true);
		}
	}
	
	public void doScocialGreeting(SGState state){
		JSONObject json = this.nonTTRManager.runSocialGreeting(state);
		if(json != null){
			if(json.containsKey("sysUtt")){
				DAState status = (DAState)json.get("sysDA");
				if(status != null)
					this.updateCurrentSysDS(status);
				
				String utt = (String)json.get("sysUtt");
				this.actListener.updateDialog(utt, UttTyps.system, true);
			}
		}
	}
	
	public void doFeedBack(FBState state, String msg){
		JSONObject json = this.nonTTRManager.runFeedback(state, msg);
		if(json != null){
			if(json.containsKey("sysUtt")){
				DAState status = (DAState)json.get("sysDA");
				if(status != null)
					this.updateCurrentSysDS(status);
				
				String utt = (String)json.get("sysUtt");
				this.actListener.updateDialog(utt, UttTyps.system, true);
				
				if(state.equals(FBState.requestNew)){
					this.startChecker(600);
				}
			}
		}
	}

	private TTRRecordType buildHypothesis(TTRFormula ttr, Tree tree) {
		return ((TTRRecordType)ttr).removeHead();
	}
	
	private TTRRecordType createTargetTTR(TargetTTRType type, TTRRecordType curRespondTTR, double score, double threshold){
		TTRRecordType copy = null;
		switch(type){
		case answer:
				copy = curRespondTTR.clone();
				logger.debug("createTargetTTR Result: Confidence Score = " + score + " : Threshold = " + threshold);
				if(score <= threshold){
					List<TTRField> fieldList = copy.getFields();
					
					TTRLabel label = null;
					for(TTRField f : fieldList){
						DSType dsType = f.getDSType();
						if(dsType.equals(DSType.e)){
							label = f.getLabel();
						}
					}
					
					TTRLabel qLabel = copy.getFreeLabel(new TTRLabel("p"));
					copy.add(new TTRField(qLabel, DSType.t, Formula.create("question("+label+")")));
				}
				preSysRespond = copy;
				this.updateCurrentSysDS(DAState.waitingforconfirm);
			break;
		case donotKnow:
			copy = TTRRecordType.parse("¬[x==learner:e|e1==know:es|x1==this:e|p==subj(e1,x):t|p1==obj(e1,x1):t]");
			this.updateCurrentSysDS(DAState.listening);
			break;
		case question:
			copy = curRespondTTR.clone();
			this.updateCurrentSysDS(DAState.waitingforanswer);
			break;
		}
		return copy;
	}
	
	/**
	 * Merge answer from context to question
	 * @param questionTTR question TTR record type
	 * @param mapedTTR context TTR record type
	 * @param subsummap subsumed map between question and context
	 * @param qField question Filed focused
	 */
	public Formula predValue = null;
	public TTRRecordType retrievalAnswer(TTRRecordType questionTTR, TTRRecordType mapedTTR, HashMap<Variable, Variable> subsummap, TTRField qField){
		TTRRecordType reuslt = questionTTR.clone();
		List<TTRField> fList = reuslt.getFields();
		for(TTRField f: fList){
			TTRLabel label = f.getLabel();
			logger.debug("TTRField : " +  f);
			 if(f.getType() == null && subsummap.containsKey(label)){
				 TTRLabel mappedLabel = (TTRLabel)subsummap.get(label);
				 
				 Formula predValue = mapedTTR.get(mappedLabel);
				 logger.debug("predValue : " +  predValue);
				 f.setType(predValue);
			 }
		}
		reuslt.updateParentLinks();
		return reuslt.removeField(qField);
	}
	
	/**
	 * run Rules when nothing parsed
	 * @param utt user utterance so far in the latest turn
	 */
	@Override
	public void runWithoutTTR(String utt){
		throw new UnsupportedOperationException(); 
	}
	
	/************************ Timer Out for Request *****************************************/	
	public void startChecker(int second){
		if(timer != null)
			timer = null;
		
		timer = new Timer("SilenceCheck", true);
		silenceChecker= new TimerTask(){
			@Override
			public void run() {
				synchronized(this){
					logger.debug("current State = " + sysCurrentDS);
					
					if(sysCurrentDS.equals(DAState.listening)){
						requestMore();
					}
					else
						doScocialGreeting(SGState.checkConnection);
						
				}
			}
		};
		timer.schedule(silenceChecker, Constants.UNIT * second);
	}
	
	public void cancelChecker(){
		if(timer != null){
			Toolkit.getDefaultToolkit().beep();
			timer.cancel();
			timer.purge();
		}
	}
	/************************** Grounded Semantic Checking ******************************/
	public class SemanticGroundChecker implements Runnable {
		public volatile boolean runnable = false;
		
		public SemanticGroundChecker(){
			targetContext.initial();
		}
		
		@Override
		public void run() {
			synchronized(this){
//				logger.debug("runnable: " + runnable);
				
				while(runnable){
//					logger.debug("while runnable: " + runnable);
					if(isGrounded){
						TTRRecordType groundedContxt = dylanModule.getGroundedContext();
						logger.debug("GroundedContext: " + groundedContxt);
						boolean isResolved = targetContext.answerQuestion(groundedContxt);
						
						if(!isResolved){
							RespondTTR respond = targetContext.buildRespond(nLC);
							TTRRecordType targetTTR = respond.getRespondTTR();
							if(targetTTR != null)
								generate(targetTTR, NLGstate.ask);
						}
						else{
							logger.debug("has achieved idle situation...");
							doFeedBack(FBState.requestNew,null);
							runnable = false;
						}
					}
					
					try {
						Thread.sleep(Constants.UNIT * 3);
					} catch (InterruptedException e) {
						logger.error(e.getMessage());
					}
				}
				
				if(!runnable){
					logger.info("Semantic Ground Checker is stoped..");
				}
			}
		}
	}
}
