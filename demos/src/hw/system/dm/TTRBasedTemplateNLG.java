package hw.system.dm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import babble.system.DyLanlistener;
import babble.system.DylanModule;
import babble.vision.tools.Constants;
import babble.vision.tools.DAState;
import babble.vision.tools.NLGstate;
import babble.vision.tools.VSTimer;
import qmul.ds.Utterance;
import qmul.ds.formula.Formula;
import qmul.ds.formula.TTRField;
import qmul.ds.formula.TTRFormula;
import qmul.ds.formula.TTRLabel;
import qmul.ds.formula.TTRRecordType;
import qmul.ds.tree.Tree;

public class TTRBasedTemplateNLG {
	Logger logger = Logger.getLogger(TTRBasedTemplateNLG.class);
	
	public TTRBasedTemplateNLG(){}
	
	public JSONObject generateSysUttFromTTR(TTRRecordType semantic, TTRRecordType recordType, NLGstate state){
		if(recordType != null){
			JSONObject sysResponse = new JSONObject();
			String sysUtt = null;
			DAState sysDA = DAState.listening;
			
			System.out.println("TTR: " + recordType);
			if(recordType.toString().trim().equals("¬[x==learner:e|e1==know:es|x1==this:e|p==subj(e1,x):t|p1==obj(e1,x1):t]")){
				sysUtt = "sorry, i don't know about this.";
				sysDA = DAState.listening;
			}
			else{
				if(!recordType.getFieldsbyType("question").isEmpty()){
					TTRField qField = recordType.getFieldsbyType("question").get(0);
					TTRRecordType copy = recordType.removeField(qField);
					
					List<TTRLabel> labelXs = copy.getLabelsByStr("x");
					Formula xf = copy.getType(labelXs.get(0));
					
					String xStr = "it";
					if(xf != null)
						xStr = xf.toString();
					
					List<TTRLabel> predLabelList = copy.getLabelsByStr("pred");
					if(!predLabelList.isEmpty()){
						if(predLabelList.size() == 1){
							Formula f = copy.getType(predLabelList.get(0));
							
							List<TTRField> pField = copy.getFieldsbyType("("+predLabelList.get(0)+")");
							String category = pField.get(0).getType().toString();
							category = category.substring(0,category.indexOf("("));
							
							if(f == null){
								if(category.equals("class"))
									sysUtt = "what is " + xStr.trim() + "?";
								else
									sysUtt = "what " + category + " is " + xStr.trim() + "?";
							}
							else{
								String prep = "";
								if(category.equals("class") || category.equals("shape"))
									prep = " a"; 
								sysUtt = "is " + xStr.trim() + prep + " " +f.toString() + "?";
	
								sysDA = DAState.waitingforconfirm;
							}
						}
						else{
							
							Map<String, String> predMap = new HashMap<String,String>();
							for(TTRLabel label : predLabelList){
								Formula f = copy.getType(label);
								List<TTRField> pField = copy.getFieldsbyType("("+label+")");
								String category = pField.get(0).getType().toString();
								category = category.substring(0,category.indexOf("("));
								predMap.put(category, f.toString());
							}
							
							System.out.println("Map: " + predMap);
							
							if(predMap.containsKey("class"))
								sysUtt = "is " + xStr.trim() + " a " + predMap.get("color") + " " + predMap.get("class") + "?";
							else if(predMap.containsKey("shape"))
								sysUtt = "is " + xStr.trim() + " a " + predMap.get("color") + " " + predMap.get("shape") + "?";
							sysDA = DAState.waitingforconfirm;
						}
					}
				}
				else{
					List<TTRLabel> labelXs = recordType.getLabelsByStr("x");
					Formula xf = recordType.getType(labelXs.get(0));
					
					String xStr = "it";
					if(xf != null)
						xStr = xf.toString();
							
					Map<String, String> predMap = getValueMap(recordType);
					
					if(!predMap.isEmpty()){
						String color = predMap.containsKey("color") ? predMap.get("color") + " " : "";
							
						String desrip = "";
						if(predMap.containsKey("class"))
							desrip = "a " + color + predMap.get("class");
						else if(predMap.containsKey("shape"))
							desrip = "a " + color + predMap.get("shape");
						else
							desrip = color.trim();
								
						logger.debug("desrip: " + desrip);
						
						sysUtt = xStr.trim() + " is " + desrip + ".";
						sysDA = DAState.waitingforconfirm;
					}
					
					
					if(state.equals(NLGstate.check)){
						Map<String, String> semMap = getValueMap(semantic);
						boolean isSame = checkSameValues(semMap,predMap);	
						
						if(isSame)
							sysUtt = "yes. " + sysUtt;
						else
							sysUtt = "no. " + sysUtt;
					}
				}
			}
			if(sysUtt != null)
				sysResponse.put("sysUtt", sysUtt);
			sysResponse.put("timestamp", VSTimer.getInstance().getTimeStamp());
			sysResponse.put("sysDA", sysDA);

			return sysResponse;
		}
		return null;
	}
	
	private boolean checkSameValues(Map<String, String> semMap, Map<String, String> predMap) {
		return semMap.equals(predMap);
	}

	private Map<String, String> getValueMap(TTRRecordType recordType){
		Map<String, String> predMap = new HashMap<String,String>();
		
		List<TTRLabel> predLabelList = recordType.getLabelsByStr("pred");
		if(!predLabelList.isEmpty()){
			for(TTRLabel label : predLabelList){
				Formula f = recordType.getType(label);
				List<TTRField> pField = recordType.getFieldsbyType("("+label+")");
				String category = pField.get(0).getType().toString();
				category = category.substring(0,category.indexOf("("));
				String value = f != null ? f.toString() : "";
				predMap.put(category, value);
			}
		}
		
		return predMap;
	}
	
	
	public static void main(String[] args){
		final TTRBasedTemplateNLG nlg = new TTRBasedTemplateNLG();
		DylanModule dylan = new DylanModule();
		dylan.loadParser(Constants.parserFilename);
		dylan.loadFile(null);
		dylan.registerListener(new DyLanlistener(){

			@Override
			public void getNLG(String utt, UttTyps type) {
				
			}

			@Override
			public void getSemantic(String utt, TTRFormula ttr, Tree tree, String action, boolean isGrounded) {
				
				TTRFormula s = TTRRecordType.parse("¬[x==learner:e|e1==know:es|x1==this:e|p==subj(e1,x):t|p1==obj(e1,x1):t]");
				
				System.out.println("TTRFormula: " + s);
				
				JSONObject json = nlg.generateSysUttFromTTR((TTRRecordType)s, (TTRRecordType)s, NLGstate.answer);
				System.out.println("JSON: " + json.toString());
			}

			@Override
			public void throwException(Utterance utt, Exception e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		while(true){
			System.out.println("Please say a sentence:");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			try {
				String s = br.readLine();
				dylan.parse(s, "");
				if(!s.equals("okay"))
					continue;
				else
					break;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
