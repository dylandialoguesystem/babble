package hw.system.dm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import babble.system.dm.AbstractBabbleDialog;
import babble.vision.classification.Concept;
import babble.vision.context.AbstractTargetContext;
import babble.vision.context.RespondTTR;
import babble.vision.context.VisualContext;
import babble.vision.tools.Randomer;
import qmul.ds.formula.Formula;
import qmul.ds.formula.TTRField;
import qmul.ds.formula.TTRLabel;
import qmul.ds.formula.TTRRecordType;
import qmul.ds.formula.Variable;

public class TargetContext extends AbstractTargetContext{
	Logger logger = Logger.getLogger(TargetContext.class);
	
	public TargetContext(AbstractBabbleDialog babbleDialog, List<String> ontologyList) {
		super(babbleDialog, ontologyList);
		this.initial();
	}

	@Override
	public boolean answerQuestion(TTRRecordType context) {
		TTRRecordType copy = context.clone();
		copy = copy.removeHead();
		
		logger.debug("Tutor Dialog Context: " + context);
		
		Map<String, TTRRecordType> map = new HashMap<String, TTRRecordType>();
		map.putAll(qContextMap);
		if(!this.isIdleSituationByQuestion()){
			Iterator<Entry<String, TTRRecordType>> iterator = map.entrySet().iterator();
			while(iterator.hasNext()){
				Entry<String, TTRRecordType> temp = iterator.next();
				String ontology = temp.getKey();
				logger.debug("-------- For Question of " + ontology + " -----------");
				TTRRecordType qContext = temp.getValue();
				qContext = qContext.removeField(qContext.getFieldsbyType("question").get(0));
				
				logger.debug("qContext without Question Field: " + qContext);
				
				HashMap<Variable, Variable> subsummap = new HashMap<Variable, Variable>();
				boolean isSummed = qContext.subsumesMapped(copy, subsummap);
				logger.debug("isSummed: " + isSummed + " ~ " + subsummap);
				

				HashMap<Variable, Variable> commap = new HashMap<Variable, Variable>();
				TTRRecordType superType = qContext.mostSpecificCommonSuperType(copy, commap);
				if(superType != null){
					logger.debug("mostSpecificCommonSuperType: " + superType);
						
					boolean flag = superType.toString().contains(ontology);
					logger.debug("Has? " + flag + " :: " + superType + " ~ " + ontology);
					
					if(superType.toString().contains(ontology))
						qContextMap.remove(ontology);
					
					if(ontology.equals("shape")){
						if(superType.toString().contains("class"))
							qContextMap.remove(ontology);
					}
				}
				else
					logger.error("Error: " + qContext + " ~ " + copy);
			}
		}
		return this.isIdleSituationByQuestion();
	}

	@Override
	public RespondTTR buildRespond(VisualContext visualContext) {
		
		int qIndex = Randomer.getInstance().randomInt(0, this.qContextMap.size()-1);
		
		String[] qList = new String[this.qContextMap.size()];
		
		qList = this.qContextMap.keySet().toArray(qList);
		TTRRecordType question = this.qContextMap.get(qList[qIndex]);
		boolean isWHQ = true; 
		Concept attribute = visualContext.getAttributByOntology(qList[qIndex]);
		
		logger.debug("question Context : " + question);
		
		if(attribute.getConfidenceScore() > 0.5){
			TTRField field = question.getField(new TTRLabel("pred"));
			logger.debug("original Field : " + field);
			question = question.removeField(field);
			TTRField clone = field.clone();
			clone.setType(Formula.create(attribute.getConceptName()));
			logger.debug("Attribute Field : " + clone);
			question.add(clone);
			isWHQ = false;
		}
		
		return new RespondTTR(question,isWHQ);
	}

	@Override
	public RespondTTR buildRespond(TTRRecordType qTTR, VisualContext visualContext, TTRField selQ) {
		return null;
	}

	public static void main(String[] args){
		List<String> ontologyList = new ArrayList<String>();
		ontologyList.add("color");
		ontologyList.add("shape");
		TargetContext tContext = new TargetContext(null, ontologyList);
		
		VisualContext visualContxt = new VisualContext("[x0==this:e|pred5==red:cn|p8==color(pred5):t|p9==subj(pred5,x0):t|pred4==square:cn|p10==shape(pred4):t|p11==class(pred4):t|p12==subj(pred4,x0):t]");
		
		TTRRecordType groundedContext1 = (TTRRecordType)TTRRecordType.create("[x0==this:e|pred5==square:cn|p8==class(pred5):t|p9==subj(pred5,x0):t]");
//		TTRRecordType groundedContext2 = (TTRRecordType)TTRRecordType.create("[x0==this:e|pred4==square:cn|p10==shape(pred4):t|p12==subj(pred4,x0):t]");
		TTRRecordType groundedContext3 = (TTRRecordType)TTRRecordType.create("[x0==this:e|pred4==red:cn|p10==color(pred4):t|p12==subj(pred4,x0):t]");
		
		boolean isIdle = tContext.answerQuestion(groundedContext1);
		System.out.println("********************" + isIdle + "***************************");
		System.out.println("********************" + tContext.sizeOfQContext() + "***************************");
		
//		tContext.answerQuestion(groundedContext2);
//		System.out.println("************************************************");
		
		isIdle = tContext.answerQuestion(groundedContext3);
		System.out.println("********************" + isIdle + "***************************");
		System.out.println("********************" + tContext.sizeOfQContext()  + "***************************");
	}
}
