function evalExcelReport(dir, ontology, attrs, id, acutual, predict, policy, numPos, numNeg, other)
%EVALREPORT Summary of this function goes here
    setup;
    dir_root = fileparts(which('setup.m'));
    filename = sprintf('%s/expReport-%s-%s.csv',dir, policy, datestr(now,'mm-dd-yyyy'));
    path = fullfile(dir_root,filename);
    disp(path);
    
    if exist(path, 'file') == 0
         head = {'/********************************************************************/';
                 '  Classifier Summary Version -- Logistic Regression with SGD';
        	     '  @ Author:  Yanchao Yu';
                 '  @ Version: 0.1';
                 '  @ Date:    March 2016';
         sprintf('  @ Policy Type:  %s ',policy);
         sprintf('  @ TrainSet Size:  %d     TestSet Size:  %d', numPos, numNeg);
                 '/********************************************************************/'};
         writeToRow(path,head);
    end
    
    if strcmp(ontology,'color')
         writeToRow(path,{'','','','',''});
        writeToRow(path,{sprintf('/*********************** Test %d fold ***********************/', (id+1))});
    end
    acutual = cell2mat(acutual');
    [row,cols] = size(acutual);
    acutual = acutual *2 -1;

    predict = cell2mat(predict');
    [row,cols] = size(predict);
    predict = predict *2 -1;

    title = {'','Precision', 'Recall', 'Macro-F1', 'Accuracy'};
    writeToRow(path, title);
    
    precision = {};
    recall = {};
    microF1 = {};
    accuracy = {};
    for i = 1 : cols
        X = acutual(:,i);
        Y = predict(:,i);
        [ precision{i}, recall{i}, microF1{i}, accuracy{i}, ~ ] = im2evaluation( X',Y' );

        temp = {attrs{i}, num2str(precision{i}), num2str(recall{i}), num2str(microF1{i}), num2str(accuracy{i})};
        writeToRow(path, temp);
    end
%   [ averPrecision, averRecall, averF1, averAccuracy, infor ] = im2evaluation( acutual',predict' );
    averPrecision = averEval(precision);
    averRecall= averEval(recall);
    averF1= averEval(microF1);
    averAccuracy = averEval(accuracy);
    data = {'Average', num2str(averPrecision), num2str(averRecall), num2str(averF1), num2str(averAccuracy)};
    writeToRow(path, data);
    other = strrep(other, ',', ' ');
    writeToRow(path,{sprintf('Sample Distribution across %s:   %s', ontology, other)});
end

function value = averEval(data)
    num = length(data)
    value = 0;

    for i = 1 : num
        if(isnan(data{i}))
            data{i} = 0;
        end
        
        value = value + data{i};
    end
    
    value = value / num;
end

function writeToRow(path, data)
    disp(data);
    fid = fopen(path,'a');
    numColumns = size(data,2);
    fmt = repmat('%s,',1,numColumns-1);
    fprintf(fid,[fmt,'%s\n'],data{1:end,:})
    fclose(fid);
end

  
  

