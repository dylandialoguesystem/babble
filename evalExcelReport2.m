function evalExcelReport2(dir, ontology, attrs, id, acutual, predict, policy, numPos, numNeg, other)
%EVALREPORT Summary of this function goes here
    setup;
    dir_root = fileparts(which('setup.m'));
    filename = sprintf('%s/expReport-%s-%s.csv',dir, policy, datestr(now,'mm-dd-yyyy'));
    path = fullfile(dir_root,filename);
    disp(path);
    
    if exist(path, 'file') == 0
         head = {'/********************************************************************/';
                 '  Classifier Summary Version -- Logistic Regression with SGD';
        	     '  @ Author:  Yanchao Yu';
                 '  @ Version: 0.1';
                 '  @ Date:    March 2016';
         sprintf('  @ Policy Type:  %s ',policy);
         sprintf('  @ TrainSet Size:  %d     TestSet Size:  %d', numPos, numNeg);
                 '/********************************************************************/'};
         writeToRow(path,head);
    end
    
    if strcmp(ontology,'color')
         writeToRow(path,{'','','','',''});
        writeToRow(path,{sprintf('/*********************** Test %d fold ***********************/', (id+1))});
    end
    acutual = cell2mat(acutual');
    [row,cols] = size(acutual);
    acutual = acutual *2 -1;

    predict = cell2mat(predict');
    [row,cols] = size(predict);
    predict = predict *2 -1;
    
    
    
    labels = {'train size'};
    scores = {'recog score'};
    
    for i = 1 : cols
        X = acutual(:,i);
        Y = predict(:,i);
        [ ~, ~, microF1, ~, ~ ] = im2evaluation( X',Y' );

        microF1s{i+1} = num2str(microF1);
        labels = 
    end
    if strcmp(ontology,'color')
         writeToRow(path,{'','','','',''});
        writeToRow(path,{sprintf('/*********************** Test %d fold ***********************/', (id+1))});
    end
    writeToRow(path, microF1s);
end

function value = averEval(data)
    num = length(data)
    value = 0;

    for i = 1 : num
        if(isnan(data{i}))
            data{i} = 0;
        end
        
        value = value + data{i};
    end
    
    value = value / num;
end

function writeToRow(path, data)
    disp(data);
    fid = fopen(path,'a');
    numColumns = size(data,2);
    fmt = repmat('%s,',1,numColumns-1);
    fprintf(fid,[fmt,'%s\n'],data{1:end,:})
    fclose(fid);
end

  
  

