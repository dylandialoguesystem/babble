function  [ preds, probs, RESULT ] = predictModel( tstData, attr, label)
%PREDICTMODEL 
classifier_path = sprintf('data/_classifier/%s_cl.mat',attr);
if exist(classifier_path,'file')
    load(classifier_path);
    [ preds, probs ] = lrClassify( tstData, lRmodel );
    
    if nargin < 3
       disp('no evaluation');
       RESULT = [];
    else
        RESULT = evaluate( label, label, probs );
    end
end

