{"Attribute":"%colorValue","name":"Red","values":{"R":164,"G":56,"B":52}}
{"Attribute":"%colorValue","name":"Blue","values":{"R":1,"G":160,"B":184}}
{"Attribute":"%colorValue","name":"Green","values":{"R":52,"G":138,"B":93}}
{"Attribute":"%shapeValue","name":"Square","basicType":"Rectangle","values":{"pointX":50,"pointY":50,"width":200,"height":200}}
{"Attribute":"%shapeValue","name":"Triangle","basicType":"Triangle","values":[{"pointX":10,"pointY":10},{"pointX":300,"pointY":300},{"pointX":10,"pointY":300}]}
{"Attribute":"%shapeValue","name":"Circle","basicType":"Ellipse","values":{"centerX":150,"centerY":150,"radius1":200,"radius2":200}}