function evalRecogReport2(dir, id, scoreLabels, scoreList, costList, initiativeType, hasUncertainty, hasContentDependency, numPos, numNeg, posThreshold)
%EVALREPORT Summary of this function goes here
    setup;
    
    dir_root = fileparts(which('setup.m'));
    filename = sprintf('%s/expRecogReport-%s-%s-%s-%.2f.csv',dir, initiativeType,hasUncertainty,hasContentDependency, posThreshold);
    path = fullfile(dir_root,filename);
    disp(path);
    
    if exist(path, 'file') == 0
         head = {'/********************************************************************/';
                 '  TeachBot Dialog Policy Experiment';
        	     '  @ Author:  Yanchao Yu';
                 '  @ Date:    April 2016';
         sprintf('  @ Initiative Type:  %s',initiativeType);
         sprintf('  @ has Uncertainty:  %s',hasUncertainty);
         sprintf('  @ has Content-dependency:  %s',hasContentDependency);
         sprintf('  @ Positive Threshold:  %.2f',posThreshold);
         sprintf('  @ num of Positive:  %d     number of Negative:  %d', numPos, numNeg);
                 '/********************************************************************/'};
         writeToRow(path,head);
    end
    
    writeToRow(path,{'',''});
    writeToRow(path,{sprintf('/*********************** Test on %d-fold ************************/', id)});
    
    [~, coulum] = size(scoreLabels);
    
    labels = {'train size'};
    scores = {'recog score'};
    costs = {'tutor cost'};
%     labels = {};
%     scores = {};
    for i = 1: coulum
        label = scoreLabels(1,i);
        score = scoreList(1,i);
        cost = costList(1,i);
        
        labels{i+1} = num2str(label);
        scores{i+1} = num2str(score);
        costs{i+1} = num2str(cost);
    end
    writeToRow(path, labels);
    writeToRow(path, scores);
    writeToRow(path, costs);
    writeToRow(path,{'',''});
    writeToRow(path,{'',''});
end

function writeToRow(path, data)
    disp(data);
    fid = fopen(path,'a');
    numColumns = size(data,2);
    fmt = repmat('%s,',1,numColumns-1);
    fprintf(fid,[fmt,'%s\n'],data{1:end,:})
    fclose(fid);
end

  
  

