function AUC = attribute_AUC( scores, test_label )

[num_att, num_feature] = size(test_label);
for i = 1: num_att;
    y = test_label(i,:);
    y_score = scores(i,:);
    if(length(y(y==1))<2)
        auc = 0;
    else
        [X, Y, T, auc] = perfcurve(y,y_score,1);
    end;
    
    AUC{i} = auc;
end
AUC = cell2mat(AUC);

% % x = 1:1:length(AUC);
% % plot(x,AUC);
% bar(AUC);
% % set(gca,'XTickLabel',{'1', '2', '3', '4'})
% xlabel('Attributes');
% ylabel('AUC');
% title('AUC for Classification on Each Attribute');