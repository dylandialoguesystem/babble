function MC_ac =  mean_accuracy(Pre_Labels,test_target)
%Pre_Labels: the predicted labels of the classifier, if the ith instance belong to the jth class, Pre_Labels(j,i)=1, otherwise Pre_Labels(j,i)=-1
%test_target: the actual labels of the test instances, if the ith instance belong to the jth class, test_target(j,i)=1, otherwise test_target(j,i)=-1

%Pre_Labels: the predicted labels of the classifier, if the ith instance belong to the jth class, Pre_Labels(j,i)=1, otherwise Pre_Labels(j,i)=-1
%test_target: the actual labels of the test instances, if the ith instance belong to the jth class, test_target(j,i)=1, otherwise test_target(j,i)=-1
 
    [num_class,num_instance]=size(Pre_Labels);
    num_correct = 0;
     
    for i = 1: num_class
        if Pre_Labels(i,1) == test_target(i,1)
            num_correct = num_correct +1;
        end
    end
    MC_ac  = num_correct / num_class;
    out = sprintf('number correct is %d in total %d',num_correct,num_class);
    disp(out);
end