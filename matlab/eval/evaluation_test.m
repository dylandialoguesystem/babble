
SETUP;

mlknn = load('data/MLkNN_results_full.mat');
tram = load('data/TRAM_results_full.mat');
farhadi = load('data/farhadi_result_full.mat');
dap = load('data/DAP_results_full.mat');

y = mlknn.test_label';
y = y(:);
y_score = mlknn.Outputs(:);
[XkNN, YkNN, TkNN, AUC.kNN] = perfcurve(y,y_score,1);
MC_ac.mlknn =  mean_accuracy(mlknn.Pre_Labels(:),y);
[Recall.mlknn, precision.mlknn] = vl_pr(y,y_score);
[TPR.mlknn,TNR.mlknn, INFO.mlknn] = vl_roc(y,y_score);
clear y y_score;


y = tram.test_label';
y = y(:);
y_score = tram.Confidence(:);
[Xtram, Ytram, Ttram, AUC.tram] = perfcurve(y,y_score,1);
MC_ac.tram =  mean_accuracy(tram.Pre_Labels(:),y);
[Recall.tram, precision.tram] = vl_pr(y,y_score);
[TPR.tram,TNR.tram, INFO.tram] = vl_roc(y,y_score);
clear y y_score;

y = tram.test_label';
y = y(:);
y_score = farhadi.pred_labels';
y_score = y_score(:);
[Xfarhadi, Yfarhadi, Tfarhadi, AUC.farhadi] = perfcurve(y,y_score,1);
ss = farhadi.att_labels';
ss = ss(:);
[num_class,num_instance]=size(ss);
MC_ac.farhadi =  mean_accuracy(ss,y);
[Recall.farhadi, precision.farhadi] = vl_pr(y,y_score);
[TPR.farhadi,TNR.farhadi, INFO.farhadi] = vl_roc(y,y_score);
clear y y_score ss;

y = dap.Ltsts';
y = y(:);
prob = [];
for i = 1:64
    prob = [prob;dap.probs(i*2,:)]
end
y_score = prob(:);
[Xdap, Ydap, Tdap, AUC.dap] = perfcurve(y,y_score,1);
ss = dap.preds';
ss = ss(:);
MC_ac.dap =  mean_accuracy(ss,y);
[Recall.dap, precision.dap] = vl_pr(y,y_score);
[TPR.dap,TNR.dap,INFO.dap] = vl_roc(y,y_score);
clear y y_score ss;

figure(1);
plot(XkNN, YkNN);
hold on;
plot(Xtram, Ytram);
plot(Xdap, Ydap);
plot(Xfarhadi, Yfarhadi);

legend('MLkNN', 'TRAM', 'DAP', 'Linear SVM');
xlabel('False Positive Rate');
ylabel('True Positive Rate');
title('ROC for Attribute-based Classification on Full Dataset');

figure(2)
plot(precision.mlknn,Recall.mlknn);
hold on;
plot(precision.tram,Recall.tram);
plot(precision.dap,Recall.dap);
plot(precision.farhadi,Recall.farhadi);

legend('MLkNN', 'TRAM', 'DAP', 'Linear SVM');
xlabel('Recall');
ylabel('Precision');
title('ROC for Attribute-based Classification on Full Dataset');

figure(3)
plot(TNR.mlknn,TPR.mlknn);
hold on;
plot(TNR.tram,TPR.tram);
plot(TNR.dap,TPR.dap);
plot(TNR.farhadi,TPR.farhadi);

legend('MLkNN', 'TRAM', 'DAP', 'Linear SVM');
xlabel('True Negative Rate');
ylabel('True Positive Rate');
title('ROC for Attribute-based Classification on Full Dataset');

