SETUP;

dataset = 'full';
mlknn_file = sprintf('data/MLkNN_results_%s(new).mat',dataset);
tram_file = sprintf('data/TRAM_results_%s.mat',dataset);
dap_file = sprintf('data/DAP_results_%s.mat',dataset);
farhadi_file = sprintf('data/farhadi_result_%s_new.mat',dataset);
% farhadi_file = sprintf('data/farhadi_result_%s(new)2.mat',dataset);

load('data/attr_list.mat');
indexs = [8,23,24,33,36,37,39:43,47:51,63];
attribute(indexs) = [];

knn = load(mlknn_file);
tram = load(tram_file);
dap = load(dap_file);

%% Evaluation on MLkNN 
scores = knn.Outputs;
preds = knn.Pre_Labels;
targets = knn.test_label';

scores(indexs,:) = [];
preds(indexs,:) = [];
targets(indexs,:) = [];

knn.precision = [];
knn.recall = [];
knn.microf1 = [];
knn.accuracy = [];
knn.tp = [];
knn.fp = [];
knn.tn = [];
knn.fn = [];
knn.auc = [];

for i = 1 : size(targets,1)
    x = preds(i,:);
    y = targets(i,:);
    z = scores(i,:);
    
%     [rc, pr, info] = vl_pr(y, z);
%     precision{i} = info.ap;
    
%      [p,r,f] = microf1(x,y);
%      precision{i} = p;
%      recall{i} = r;
%      microf1{i} = f;

    [pr,rc,mf, acc, infor] = im2evaluation( x,y );
    knn.precision = [knn.precision;pr];
    knn.recall = [knn.recall;rc];
    knn.microf1 = [knn.microf1;mf];
    knn.accuracy = [knn.accuracy;acc];
    knn.tp = [knn.tp;infor.num_tp];
    knn.tn = [knn.tn;infor.num_tn];
    knn.fp = [knn.fp;infor.num_fp];
    knn.fn = [knn.fn;infor.num_fn];
    
    [~, ~,in{i}] = vl_roc(y,z);
end

for j = 1:length(in)
    knn.auc =  [knn.auc;in{j}.auc(1,1)];
end

% knn.precision(isnan(knn.precision)) = 0;
% knn.recall(isnan(knn.recall)) = 0;
% knn.microf1(isnan(knn.microf1)) = 0;
% knn.accuracy(isnan(knn.accuracy)) = 0;

knn.precision_aver = mean(knn.precision);
knn.recall_aver = mean(knn.recall);
knn.macrof1_aver = 2 * knn.precision_aver * knn.recall_aver / (knn.precision_aver + knn.recall_aver);

[knn.micro_precision_aver,knn.micro_recall_aver,knn.microf1_aver,~]   = im2evaluation2( preds,targets );

% knn.microf1_aver = mean(knn.microf1);
knn.accuracy_aver = mean(knn.accuracy);
knn.auc(knn.auc==0) = [];
knn.auc_aver = mean(knn.auc);

% figure(5)
[knn.TPR,knn.FPR,~] = vl_roc(targets,scores)%,'plot','fptp');

% %% Evaluation on TRAM
% scores = tram.Confidence;
% preds = tram.Pre_Labels;
% targets = tram.test_label';
% 
% scores(indexs,:) = [];
% preds(indexs,:) = [];
% targets(indexs,:) = [];
% 
% tram.precision = [];
% tram.recall = [];
% tram.microf1 = [];
% tram.accuracy = [];
% tram.tp = [];
% tram.fp = [];
% tram.tn = [];
% tram.fn = [];
% tram.auc = [];
% 
% for i = 1 : size(targets,1)
%     x = preds(i,:);
%     y = targets(i,:);
%     z = scores(i,:);
%     
% %     [rc, pr, info] = vl_pr(y, z);
% %     precision{i} = info.ap;
%     
% %      [p,r,f] = microf1(x,y);
% %      precision{i} = p;
% %      recall{i} = r;
% %      microf1{i} = f;
% 
%     [pr,rc,mf, acc, infor] = im2evaluation( x,y );
%     tram.precision = [tram.precision;pr];
%     tram.recall = [tram.recall;rc];
%     tram.microf1 = [tram.microf1;mf];
%     tram.accuracy = [tram.accuracy;acc];
%     tram.tp = [tram.tp;infor.num_tp];
%     tram.tn = [tram.tn;infor.num_tn];
%     tram.fp = [tram.fp;infor.num_fp];
%     tram.fn = [tram.fn;infor.num_fn];
%     
% %     [~, ~,trin{i}] = vl_roc(y,z);
% end
% 
% % for j = 1:length(trin)
% %     tram.auc =  [tram.auc;trin{j}.auc(1,1)];
% % end
% 
% % tram.precision(isnan(tram.precision)) = 0;
% % tram.recall(isnan(tram.recall)) = 0;
% % tram.microf1(isnan(tram.microf1)) = 0;
% % tram.accuracy(isnan(tram.accuracy)) = 0;
% 
% tram.precision_aver = mean(tram.precision);
% tram.recall_aver = mean(tram.recall);
% tram.macrof1_aver = 2 * tram.precision_aver * tram.recall_aver / (tram.precision_aver + tram.recall_aver);
% 
% [tram.micro_precision_aver,tram.micro_recall_aver,tram.microf1_aver,~]   = im2evaluation2( preds,targets );
% 
% % tram.microf1_aver = mean(tram.microf1);
% tram.accuracy_aver = mean(tram.accuracy);
% % tram.auc_aver = mean(tram.auc);
% 
% % [tram.TPR,tram.FPR,~] = vl_roc(targets,scores,'plot','fptp');

%% Evaluation on DAP 
scores = [];
for i = 1:64
    scores = [scores;dap.probs(i*2,:)]
end
preds = dap.preds';
targets = dap.Ltsts';

scores(indexs,:) = [];
preds(indexs,:) = [];
targets(indexs,:) = [];


dap.precision = [];
dap.recall = [];
dap.microf1 = [];
dap.accuracy = [];
dap.tp = [];
dap.fp = [];
dap.tn = [];
dap.fn = [];
dap.auc = [];

for i = 1 : size(targets,1)
    x = preds(i,:);
    y = targets(i,:);
    z = scores(i,:);
    
%     [rc, pr, info] = vl_pr(y, z);
%     precision{i} = info.ap;
    
%      [p,r,f] = microf1(x,y);
%      precision{i} = p;
%      recall{i} = r;
%      microf1{i} = f;

    [pr,rc,mf, acc, infor] = im2evaluation( x,y );
    dap.precision = [dap.precision;pr];
    dap.recall = [dap.recall;rc];
    dap.microf1 = [dap.microf1;mf];
    dap.accuracy = [dap.accuracy;acc];
    dap.tp = [dap.tp;infor.num_tp];
    dap.tn = [dap.tn;infor.num_tn];
    dap.fp = [dap.fp;infor.num_fp];
    dap.fn = [dap.fn;infor.num_fn];
    
    [~, ~,dapin{i}] = vl_roc(y,z);
end

for j = 1:length(dapin)
    dap.auc =  [dap.auc;dapin{j}.auc(1,1)];
end

% dap.precision(isnan(dap.precision)) = 0;
% dap.recall(isnan(dap.recall)) = 0;
% dap.microf1(isnan(dap.microf1)) = 0;
% dap.accuracy(isnan(dap.accuracy)) = 0;

dap.precision_aver = mean(dap.precision);
dap.recall_aver = mean(dap.recall);
dap.macrof1_aver = 2 * dap.precision_aver * dap.recall_aver / (dap.precision_aver + dap.recall_aver);
[dap.micro_precision_aver,dap.micro_recall_aver,dap.microf1_aver,~]   = im2evaluation2( preds,targets );

dap.auc(dap.auc==0) = [];
% dap.microf1_aver = mean(dap.microf1);
dap.accuracy_aver = mean(dap.accuracy);
dap.auc_aver = mean(dap.auc);

% figure(6)
[dap.TPR,dap.FPR,~] = vl_roc(targets,scores);%,'plot','fptp');

%% Evaluation on Farhadi's 

farhadi = load(farhadi_file);



scores = farhadi.pred_labels';
preds = farhadi.att_labels';
targets = farhadi.test_target';
% clear farhadi;
scores(indexs,:) = [];
preds(indexs,:) = [];
targets(indexs,:) = [];

farhadi.precision = [];
farhadi.recall = [];
farhadi.microf1 = [];
farhadi.accuracy = [];
farhadi.tp = [];
farhadi.fp = [];
farhadi.tn = [];
farhadi.fn = [];
farhadi.auc = [];

for i = 1 : size(targets,1)
    x = preds(i,:);
    y = targets(i,:);
    z = scores(i,:);
    
%     [rc, pr, info] = vl_pr(y, z);
%     precision{i} = info.ap;
    
%      [p,r,f] = microf1(x,y);
%      precision{i} = p;
%      recall{i} = r;
%      microf1{i} = f;

    [pr,rc,mf, acc, infor] = im2evaluation( x,y );
    farhadi.precision = [farhadi.precision;pr];
    farhadi.recall = [farhadi.recall;rc];
    farhadi.microf1 = [farhadi.microf1;mf];
    farhadi.accuracy = [farhadi.accuracy;acc];
    farhadi.tp = [farhadi.tp;infor.num_tp];
    farhadi.tn = [farhadi.tn;infor.num_tn];
    farhadi.fp = [farhadi.fp;infor.num_fp];
    farhadi.fn = [farhadi.fn;infor.num_fn];
    
[~, ~,farhadiin{i}] = vl_roc(y,z);
end

for j = 1:length(farhadiin)
    farhadi.auc =  [farhadi.auc;farhadiin{j}.auc(1,1)];
end

% farhadi.precision(isnan(farhadi.precision)) = 0;
% farhadi.recall(isnan(farhadi.recall)) = 0;
% farhadi.microf1(isnan(farhadi.microf1)) = 0;
% farhadi.accuracy(isnan(farhadi.accuracy)) = 0;

farhadi.precision_aver = mean(farhadi.precision);
farhadi.recall_aver = mean(farhadi.recall);
farhadi.macrof1_aver = 2 * farhadi.precision_aver * farhadi.recall_aver / (farhadi.precision_aver + farhadi.recall_aver);
[farhadi.micro_precision_aver,farhadi.micro_recall_aver,farhadi.microf1_aver,~]   = im2evaluation2( preds,targets );

farhadi.auc(farhadi.auc==0) = [];
% farhadi.microf1_aver = mean(farhadi.microf1);
farhadi.accuracy_aver = mean(farhadi.accuracy);
farhadi.auc_aver = mean(farhadi.auc);

% figure(7)
[farhadi.TPR,farhadi.FPR,~] = vl_roc(targets,scores)%,'plot','fptp');


%% Plot Visual Results
% Precision Plot
figure(1)
X = 1:1:length(attribute);
plot(X,knn.precision,'-.g*','LineWidth',1.3);
hold on;
% plot(X,tram.precision,'--gx','LineWidth',1.3);
plot(X,dap.precision,':bs','LineWidth',1.3,...
                'MarkerEdgeColor','b',...
                'MarkerFaceColor','g',...
                'MarkerSize',5);
plot(X,farhadi.precision,'--mo','LineWidth',1.3,...
                'MarkerEdgeColor','m',...
                'MarkerFaceColor','m',...
                'MarkerSize',5);

% x=[0,attribute(auc)+2];
% y=[0.5,0.5];
% plot(x,y,'--','MarkerEdgeColor','r','MarkerFaceColor','r');

% Set the tick locations and remove the labels 
set(gca,'XTick',1:numel(attribute),'XTickLabel','') 
% Define the labels 
% Estimate the location of the labels based on the position 
% of the xlabel 
hx = get(gca,'XLabel');  % Handle to xlabel 
set(hx,'Units','data'); 
pos = get(hx,'Position'); 
y = pos(2); 
% Place the new labels 
for i = 1:length(attribute) 
    t(i) = text(X(i),y,attribute{i}); 
end 
set(t,'Rotation',90,'HorizontalAlignment','right') 

legend('MLkNN', 'DAP', 'Linear SVM');
% xlabel('False Positive Rate');
ylabel('Precision');
% title('Precision of Classficiation Models on Each Attribute (Full Training Set)');


% Recall Plot
figure(2)
X = 1:1:length(attribute);
plot(X,knn.recall,'-.g*','LineWidth',1.3);
hold on;
% plot(X,tram.recall,'--gx','LineWidth',1.3);
plot(X,dap.recall,':bs','LineWidth',1.3,...
                'MarkerEdgeColor','b',...
                'MarkerFaceColor','g',...
                'MarkerSize',5);
plot(X,farhadi.recall,'--mo','LineWidth',1.3,...
                'MarkerEdgeColor','m',...
                'MarkerFaceColor','m',...
                'MarkerSize',5);

% x=[0,attribute(auc)+2];
% y=[0.5,0.5];
% plot(x,y,'--','MarkerEdgeColor','r','MarkerFaceColor','r');

% Set the tick locations and remove the labels 
set(gca,'XTick',1:numel(attribute),'XTickLabel','') 
% Define the labels 
% Estimate the location of the labels based on the position 
% of the xlabel 
hx = get(gca,'XLabel');  % Handle to xlabel 
set(hx,'Units','data'); 
pos = get(hx,'Position'); 
y = pos(2); 
% Place the new labels 
for i = 1:length(attribute) 
    t(i) = text(X(i),y,attribute{i}); 
end 
set(t,'Rotation',90,'HorizontalAlignment','right') 

legend('MLkNN', 'DAP', 'Linear SVM');
% xlabel('False Positive Rate');
ylabel('Recall');
% title('Recall of Classficiation Models on Each Attribute (Full Training Set)');

% AUC Plot
figure(3)
X = 1:1:length(attribute);
plot(X,knn.auc,'-.g*','LineWidth',1.3);
hold on;
% plot(X,tram.auc,'--gx');
plot(X,dap.auc,':bs','LineWidth',1.3,...
                'MarkerEdgeColor','b',...
                'MarkerFaceColor','g',...
                'MarkerSize',5);
plot(X,farhadi.auc,'--mo','LineWidth',1.3,...
                'MarkerEdgeColor','m',...
                'MarkerFaceColor','m',...
                'MarkerSize',5);

% x=[0,attribute(auc)+2];
% y=[0.5,0.5];
% plot(x,y,'--','MarkerEdgeColor','r','MarkerFaceColor','r');

% Set the tick locations and remove the labels 
set(gca,'XTick',1:numel(attribute),'XTickLabel','') 
% Define the labels 
% Estimate the location of the labels based on the position 
% of the xlabel 
hx = get(gca,'XLabel');  % Handle to xlabel 
set(hx,'Units','data'); 
pos = get(hx,'Position'); 
y = pos(2); 
% Place the new labels 
for i = 1:length(attribute) 
    t(i) = text(X(i),y,attribute{i}); 
end 
set(t,'Rotation',90,'HorizontalAlignment','right') 

legend('MLkNN', 'DAP', 'Linear SVM');
% xlabel('False Positive Rate');
ylabel('Area Under Curve (AUC)');
% title('AUC of Classficiation Models on Each Attribute (Full Training Set)');

% MicroFi Plot
figure(4)
X = 1:1:length(attribute);
plot(X,knn.microf1,'-.g*','LineWidth',1.3);
hold on;
% plot(X,tram.microf1,'--gx','LineWidth',1.3);
plot(X,dap.microf1,':bs','LineWidth',1.3,...
                'MarkerEdgeColor','b',...
                'MarkerFaceColor','g',...
                'MarkerSize',5);
plot(X,farhadi.microf1,'--mo','LineWidth',1.3,...
                'MarkerEdgeColor','m',...
                'MarkerFaceColor','m',...
                'MarkerSize',5);

% x=[0,attribute(auc)+2];
% y=[0.5,0.5];
% plot(x,y,'--','MarkerEdgeColor','r','MarkerFaceColor','r');

% Set the tick locations and remove the labels 
set(gca,'XTick',1:numel(attribute),'XTickLabel','') 
% Define the labels 
% Estimate the location of the labels based on the position 
% of the xlabel 
hx = get(gca,'XLabel');  % Handle to xlabel 
set(hx,'Units','data'); 
pos = get(hx,'Position'); 
y = pos(2); 
% Place the new labels 
for i = 1:length(attribute) 
    t(i) = text(X(i),y,attribute{i}); 
end 
set(t,'Rotation',90,'HorizontalAlignment','right') 

legend('MLkNN', 'DAP', 'Linear SVM');
% xlabel('False Positive Rate');
ylabel('Macro-F1');
% title('MicroF1 of Classficiation Models on Each Attribute (Full Training Set)');


% AUC Plots
figure(5)
plot(fliplr(knn.FPR),knn.TPR,'g','LineWidth',1.3);
hold on;
plot(fliplr(dap.FPR),dap.TPR,'m','LineWidth',1.3);
plot(fliplr(farhadi.FPR),farhadi.TPR,'b','LineWidth',1.3);

plot([0,1],[0,1],'-.k','LineWidth',1);

legend('MLkNN', 'DAP', 'Linear SVM');
xlabel('False Positive Rate');
ylabel('True Positive Rate');
% title('AUC on Classficiation Models (Full Training Set)');
