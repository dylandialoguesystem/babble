SETUP;

mlknn = load('MLkNN_results_full.mat');
tram = load('TRAM_results_full.mat');
farhadi = load('farhadi_result_full.mat');
dap = load('DAP_results_full.mat');

pred_results = dap.preds';
test_labels = dap.Ltsts';
scores = farhadi.pred_labels';

[num_att, num_feature] = size(test_labels);
for i = 1: 1;
    y = test_labels(i,:);
%     y_score = scores(i,:);

    prob = [];
    for j = 1:64
      prob = [prob;dap.probs(j*2,:)]
    end
    y_score = prob(i,:);
    
    [Recall{i}, Precision{i}] = vl_pr(y,y_score);
end

figure(2)
hold on;
for i = 1:1
    plot(Precision{i},Recall{i});
end

xlabel('Recall');
ylabel('Precision');
title('ROC for Attribute-based Classification on Full Dataset');

[average_Recall, average_Precision] = vl_pr(test_labels,scores);





% X = pred_results;
% Y = test_labels;
% 
% X(X>0) = 1;X(X<=0) = 0;
% Y(Y>0) = 1;Y(Y<=0) = 0;
% XandY = X&Y;
% Precision=sum(XandY(:))/sum(X(:));
% Recall=sum(XandY(:))/sum(Y(:));
% F1=2*Precision*Recall/(Precision+Recall);




