function infor = im2RecgScore( X,Y, ontology)
%PRE_RECALL Summary of this function goes here
%   Detailed explanation goes here

% for high confidence
num_tp = 0;
num_tn = 0;
num_fp = 0;
num_fn = 0;

% for low confidence
num_ptp = 0;
num_ptn = 0;
num_pfp = 0;
num_pfn = 0;

X = X(:)'; Y = Y(:)';

for i = 1: length(X)
    if strcmp(ontology,'color')
        if  X(1,i) == 1
            if Y(1,i) >= 0.7
                num_tp = num_tp + 1;
            elseif 0.1667 <= Y(1,i) < 0.7
                num_ptp = num_ptp + 1;
            elseif 0.1 < Y(1,i) < 0.1667
                num_pfp = num_pfp + 1;
            elseif Y(1,i) <= 0.1
                num_fp = num_fp + 1;
            end
        elseif  X(1,i) == -1
            if Y(1,i) >= 0.7
                num_fn = num_fn + 1;
            elseif 0.1667 <= Y(1,i) < 0.7
                num_pfn = num_pfn + 1;
            elseif 0.1 < Y(1,i) < 0.1667
                num_ptn = num_ptn + 1;
            elseif Y(1,i) <= 0.1
                num_tn = num_tn + 1;
            end
        end
    elseif strcmp(ontology,'shape')
        if  X(1,i) == 1
            if Y(1,i) >= 0.7
                num_tp = num_tp + 1;
            elseif 0.33 <= Y(1,i) < 0.7
                num_ptp = num_ptp + 1;
            elseif 0.15 < Y(1,i) < 0.33
                num_pfp = num_pfp + 1;
            elseif Y(1,i) <= 0.15
                num_fp = num_fp + 1;
            end
        elseif  X(1,i) == -1
            if Y(1,i) >= 0.7
                num_fn = num_fn + 1;
            elseif 0.33 <=Y(1,i) < 0.7
                num_pfn = num_pfn + 1;
            elseif 0.15 < Y(1,i) < 0.33
                num_ptn = num_ptn + 1;
            elseif Y(1,i) <= 0.15
                num_tn = num_tn + 1;
            end
        end
    end
end


disp(num_tp);
disp(num_fp);
disp(num_tn);
disp(num_fn);
disp(num_ptp);
disp(num_pfp);
disp(num_ptn);
disp(num_pfn);

infor.num_tp = num_tp;
infor.num_fp = num_fp;
infor.num_tn = num_tn;
infor.num_fn = num_fn;
infor.num_ptp = num_ptp;
infor.num_pfp = num_pfp;
infor.num_ptn = num_ptn;
infor.num_pfn = num_pfn;
infor.recg_score = (num_tp + num_tn + num_ptp * 0.5 + num_ptn * 0.5) - (num_fp + num_fn + num_pfp * 0.5 + num_pfn * 0.5);

end







