% function AUC = evaluate_AUC( scores, test_label )
%EVALUATE Summary of this function goes here
%   Detailed explanation goes here
mlknn = load('MLkNN_results_full.mat');
tram = load('TRAM_results_full.mat');
farhadi = load('farhadi_result_full.mat');
dap = load('DAP_results_full.mat');

test_label = tram.test_label';
scores = mlknn.Outputs;
% scores = [];
% for j = 1:64
%     scores = [scores;dap.probs(j*2,:)]
% end

[num_att, num_feature] = size(test_label);
for i = 1: num_att;
    y = test_label(i,:);
    y_score = scores(i,:);
    if(length(y(y==1))<2)
        auc = 0;
    else
        [X, Y, T, auc] = perfcurve(y,y_score,1);
    end;
    
    AUC{i} = auc;
end
AUC = cell2mat(AUC);

% x = 1:1:length(AUC);
% plot(x,AUC);
bar(AUC);
% set(gca,'XTickLabel',{'1', '2', '3', '4'})
xlabel('Attributes');
ylabel('AUC');
title('AUC for Classification on Each Attribute');


average_AUC = mean(AUC);

