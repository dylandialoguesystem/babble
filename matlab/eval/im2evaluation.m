function [ precision, recall, microF1, accuracy, infor ] = im2evaluation( X,Y )
%PRE_RECALL Summary of this function goes here
%   Detailed explanation goes here

num_tp = 0;
num_tn = 0;

num_fp = 0;
num_fn = 0;
X = X(:)'; Y = Y(:)';
for i = 1: length(X)
    if Y(1,i) == -1 && X(1,i) == 1
        num_fp = num_fp + 1;
    elseif Y(1,i) == 1 && X(1,i) == -1
        num_fn = num_fn + 1;
    elseif Y(1,i) == 1 && X(1,i) == 1
        num_tp = num_tp + 1;
    elseif Y(1,i) == -1 && X(1,i) == -1
        num_tn = num_tn + 1;
    end
end

%clear X Y XorY XandY; 

precision = num_tp / (num_tp + num_fp);
recall = num_tp / (num_tp + num_fn);
accuracy = (num_tp + num_tn) / (num_tp + num_fp + num_tn + num_fn);

if isnan(precision)
    precision = 1;
end

microF1 = 2*precision*recall/(precision+recall);
if isnan(microF1)
    microF1 = 0;
end

infor.num_tp = num_tp;
infor.num_fp = num_fp;
infor.num_tn = num_tn;
infor.num_fn = num_fn;


disp(infor);










