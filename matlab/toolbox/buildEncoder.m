setup;

%% Settings
opts.dataset = 'Simples';
opts.prefix = 'fv';
opts.encoderParams = {'type', 'fv'};
opts.seed = 1;
opts.lite = true;
opts.C = 1;
opts.kernel = 'kchi2';
opts.dataDir = 'data';

for pass = 1:2
  opts.datasetDir = fullfile(opts.dataDir, opts.dataset) ;
  opts.annotationPath = fullfile(opts.datasetDir,'lableOfSample.txt');
  opts.resultDir = fullfile(opts.dataDir,'Encoder',opts.prefix) ;
  opts.imdbPath = fullfile(opts.resultDir, 'imdb.mat') ;
  opts.encoderPath = fullfile(opts.resultDir, 'encoder.mat') ;
  opts.modelPath = fullfile(opts.resultDir, 'model.mat') ;
  opts.diaryPath = fullfile(opts.resultDir, 'diary.txt') ;
  opts.cacheDir = fullfile(opts.resultDir, 'cache') ;
end

disp(opts);

%% get Image DataSet
nameList = [];
% if exist(opts.annotationPath)
%     i = 1;
%     fid = fopen(opts.annotationPath);
%     tline = fgetl(fid);
%     while ischar(tline)
%         if ~isempty(tline)
%             str = strsplit(tline,',');
%             nameList{i} = str{1};
%             i = i + 1;
%         end
%         tline = fgetl(fid);
%     end
%     fclose(fid);
% end



if exist(opts.datasetDir) == 7
    list = dir(opts.datasetDir);
    j = 1;
    for i=1:size(list,1)
        k = strfind(list(i).name,'.jpg');
        if ~isempty(k)
            nameList{j} = list(i).name;
            j = j+1;
        end
    end
end

numOfSamples = length(nameList);
imList = cell(1,numOfSamples);
for i = 1:numOfSamples
    path = fullfile(opts.datasetDir, nameList{i});
    imList{i} = imread(path);
end

%% train Encoder
if exist(opts.encoderPath)
  encoder = load(opts.encoderPath) ;
else
    numTrain = numOfSamples ;
    if opts.lite, numTrain = 10 ; end
    encoder = trainEncoder(imList, ...
                         opts.encoderParams{:}, ...
                         'lite', opts.lite) ;
  save(opts.encoderPath, '-struct', 'encoder') ;
  diary off ;
  diary on ;
end