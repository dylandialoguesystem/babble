function [ B , bbox ] = image2edge( image )
%IMAGE2EDGE get bounding box and edge vector of image into 9 unsigened
%bins
% Input: image -> original image from database or webcamera
% Output: feat -> edges into 8 unsigned bins 
%         bbox -> bounding box of object in original box

% Author: Yanchao Yu
% setup;
% image = imread('data/tmp/20150313T140754.jpg');

%% process original image 
[im_height, im_width, dim] = size(image);

% get gray scale of image
gray_image = rgb2gray(image);
%Median filtering the image to remove noise%
gray_image = medfilt2(gray_image,[3 3]); 

%% finding Edges of gray image
BW = edge(gray_image,'sobel'); 
[imx,imy] = size(BW);
msk = [0 0 0 0 0;
       0 1 1 1 0;
       0 1 1 1 0;
       0 1 1 1 0;
       0 0 0 0 0;];
%Smoothing  image to reduce the number of connected components
B = conv2(double(BW),double(msk)); 
% Calculating connected components
L = bwlabel(B,8);
mex = max(max(L))
% Extract the number plate completely.
[r,c] = find(L==17);  
rc = [r c];
[sx sy] = size(rc);
n1 = zeros(imx,imy); 

%% calculating the coordinate of points on edges 
[y,x] = find(B);
[rows columns] = size(x);
new_x = [];
new_y = [];

for i = 1: rows
   if (x(i,1) <= 30) && (y(i,1) <= 30)
       continue;
   elseif (x(i,1) <= 30) && (y(i,1) >= (im_height-30))
       continue;
   elseif (x(i,1) >= (im_width-30)) && (y(i,1) <= 30)
       continue;
   elseif (x(i,1) >= (im_width-30)) && (y(i,1) >= (im_height-30))
       continue;
   else
       new_x = [new_x;x(i,1)];
       new_y = [new_y;y(i,1)];
   end
end

%% return bounding box and Edge vector
max_x = max(new_x);
max_y = max(new_y);
min_x = min(new_x);
min_y = min(new_y);
width = max_x - min_x;
height =  max_y - min_y;
bbox = [min_x,min_y,width,height];

% figure;
% imshow(B);
end

