function index = getNearest( feat, centers )
%getNearest get nearest neighbors of feat 

% Author: Yanchao Yu

 if 0  
     index = kdtreeidx(double(centers), double(feat));
 end

centers = centers';
centerssq = sum(centers.^2, 1);

index = zeros(size(feat, 1), 1);
for k = 1:size(feat, 1)
    dist = centerssq - 2*feat(k, :) * centers;
    [tmp, index(k)] = min(dist);
end
end
