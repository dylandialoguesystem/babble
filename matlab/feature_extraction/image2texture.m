% function feat_texture = image2texture( image )
%IMAGE2TEXTURE Summary of this function goes here
%   Detailed explanation goes here

setup;
image = imread('data/tmp/20150313T140754.jpg');

model.numWords = 256 ;
model.quantizer = 'kdtree';

%% calculating Texture Centers
im = standardizeImage(image);
if size(im, 3)==3
    im = rgb2gray(image);
end

gaussianImage = imfilter(im, fspecial('gaussian', 3, 1));
feat = single(MRS4fast(gaussianImage));

[centers, assignments] = vl_kmeans(feat', model.numWords,'Algorithm','ANN','MaxNumIterations',50);
model.centers = centers;
if strcmp(model.quantizer, 'kdtree')
   model.kdtree = vl_kdtreebuild(centers) ;
end



% end

