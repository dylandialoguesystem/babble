%% demo

plotFlag = 2;
depth = 8;

alef1 = imread('/Users/yy147/PhD Proj/git/babble/resource/2016-instances/pretrain-instances/sample_4.jpg');   %% Binary image
alef2 = imread('/Users/yy147/PhD Proj/git/babble/resource/2016-instances/pretrain-instances/sample_46.jpg');   %% Intensity image
tav = imread('/Users/yy147/PhD Proj/git/babble/resource/2016-instances/pretrain-instances/sample_0.jpg');       %% Binary image


 alef1 = standardizeImage(alef1);
 [ Balef1 , bbox ] = image2edge( alef1 );
 alef1 = imcrop(Balef1, bbox);
 figure
 imshow(alef1);
    
 alef2 = standardizeImage(alef2);
 [ Balef2 , bbox ] = image2edge( alef2 );
 alef2 = imcrop(Balef2, bbox);
 figure
 imshow(alef2);
 
 tav = standardizeImage(tav);
 [ Btav , bbox ] = image2edge( tav );
 tav = imcrop(Btav, bbox);
 figure
 imshow(tav);
    
 
 figure
subplot(1,3,1);
vec1 = hierarchicalCentroid(alef1,depth,plotFlag);
subplot(1,3,2);
vec2 = hierarchicalCentroid(alef2,depth,plotFlag);
subplot(1,3,3);
vec3 = hierarchicalCentroid(tav,depth,plotFlag);

dist_1_2 = sum((vec1 - vec2) .^ 2);
dist_1_3 = sum((vec1 - vec3) .^ 2);

fprintf('The distance between alef1 and alef2: %1.3f\n', dist_1_2);
fprintf('The distance between alef1 and tav: %1.3f\n', dist_1_3);
