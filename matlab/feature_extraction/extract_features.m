%  function feature = extract_features( image )
%EXTRACT_FEATURES extract visual features of image, containing L*A*B Color 
% Space, BoVW(HOG) and Edges;
% Input: image -> original image from database or webcamera
% Output: vector -> visual features
%              contains LAB Color Space quntatized into 256 kmeans centers  
%                       PHOW descriptors quntatized into 1000 kmeans centers  
%                       Edges quntatized into 16 unsigned bins   

% Author: Yanchao Yu
setup;


for i= 581 : 582
    
    path = sprintf('resource/2016-instances/trainset/instance_%d.jpg',i);
    image = imread(path);
    image = standardizeImage(image);

%     figure(1),title('original'),imshow(image);

    [ B , bbox ] = image2edge( image );

%     figure(2),title('B'),imshow(B);
    
    subimage = imcrop(image, bbox);
%     figure(3),title('subimage'),imshow(subimage);

    % feat_bovw = image2bovw( subimage );
    feat_color = image2hsv( subimage )' ;
end