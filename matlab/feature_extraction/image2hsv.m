function hsvColorHistogram = image2hsv(image)
% input: image to be quantized in hsv color space into 8x2x2 equal bins
% output: 1x32 vector indicating the features extracted from hsv color
% space

%% build new color matrix in edges
I_cropped = rgb2gray(image);
I_eq = adapthisteq(I_cropped);
bw = im2bw(I_eq, graythresh(I_eq));
figure, imshow(image), title('Back to Original Image')
overlay1 = imoverlay(image, bw, [1 1 1]);

R = overlay1(:,:,1);
G = overlay1(:,:,2);
B = overlay1(:,:,3);

R = R(R ~= 255);
G = G(G ~= 255);
B = B(B ~= 255);

i = floor(sqrt(length(R)));

R = R(1:i*i,1);
R = reshape(uint8(R), i,i);
G = G(1:i*i,1);
G = reshape(uint8(G), i,i);
B = B(1:i*i,1);
B = reshape(uint8(B), i,i);

image = cat(3, R, G, B);
% figure, imshow(image), title('Processed Color')

[rows, cols, ~] = size(image);
% totalPixelsOfImage = rows*cols*numOfBands;
image = rgb2hsv(image);

% split image into h, s & v planes
h = image(:, :, 1);
s = image(:, :, 2);
v = image(:, :, 3);

% quantize each h,s,v equivalently to 8x2x2
% Specify the number of quantization levels.
% thresholdForH = multithresh(h, 7);  % 7 thresholds result in 8 image levels
% thresholdForS = multithresh(s, 1);  % Computing one threshold will quantize ...
%                                     % the image into three discrete levels
% thresholdForV = multithresh(v, 1);  % 7 thresholds result in 8 image levels
%
% seg_h = imquantize(h, thresholdForH); % apply the thresholds to obtain segmented image
% seg_s = imquantize(s, thresholdForS); % apply the thresholds to obtain segmented image
% seg_v = imquantize(v, thresholdForV); % apply the thresholds to obtain segmented image

% quantize each h,s,v to 8x2x2
% Specify the number of quantization levels.
numberOfLevelsForH = 16;
numberOfLevelsForS = 4;
numberOfLevelsForV = 4;

% Find the max.
maxValueForH = max(h(:));
maxValueForS = max(s(:));
maxValueForV = max(v(:));

% create final histogram matrix of size 8x2x2
hsvColorHistogram = zeros(numberOfLevelsForH, numberOfLevelsForS, numberOfLevelsForV);

% create col vector of indexes for later reference
index = zeros(rows*cols, 3);

% Put all pixels into one of the "numberOfLevels" levels.
count = 1;
for row = 1:size(h, 1)
    for col = 1 : size(h, 2)
        quantizedValueForH(row, col) = ceil(numberOfLevelsForH * h(row, col)/maxValueForH);
        quantizedValueForS(row, col) = ceil(numberOfLevelsForS * s(row, col)/maxValueForS);
        quantizedValueForV(row, col) = ceil(numberOfLevelsForV * v(row, col)/maxValueForV);
        
        % keep indexes where 1 should be put in matrix hsvHist
        index(count, 1) = quantizedValueForH(row, col);
        index(count, 2) = quantizedValueForS(row, col);
        index(count, 3) = quantizedValueForV(row, col);
        count = count+1;
    end
end

% put each value of h,s,v to matrix 8x2x2
% (e.g. if h=7,s=2,v=1 then put 1 to matrix 8x2x2 in position 7,2,1)
for row = 1:size(index, 1)
    if (index(row, 1) == 0 || index(row, 2) == 0 || index(row, 3) == 0)
        continue;
    end
    hsvColorHistogram(index(row, 1), index(row, 2), index(row, 3)) = ... 
        hsvColorHistogram(index(row, 1), index(row, 2), index(row, 3)) + 1;
end

% normalize hsvHist to unit sum
hsvColorHistogram = hsvColorHistogram(:)';
hsvColorHistogram = hsvColorHistogram/sum(hsvColorHistogram);

% clear workspace
clear('row', 'col', 'count', 'numberOfLevelsForH', 'numberOfLevelsForS', ...
    'numberOfLevelsForV', 'maxValueForH', 'maxValueForS', 'maxValueForV', ...
    'index', 'rows', 'cols', 'image', 'quantizedValueForH', ...
    'quantizedValueForS', 'quantizedValueForV');

% figure('Name', 'Quantized leves for H, S & V');
% subplot(2, 3, 1);
% imshow(seg_h, []);
% subplot(2, 3, 2);
% imshow(seg_s, []);
% title('Quatized H,S & V by matlab function imquantize');
% subplot(2, 3, 3);
% imshow(seg_v, []);
% subplot(2, 3, 4);
% imshow(quantizedValueForH, []);
% subplot(2, 3, 5);
% imshow(quantizedValueForS, []);
% title('Quatized H,S & V by my function');
% subplot(2, 3, 6);
% imshow(quantizedValueForV, []);

end