 function HOG = image2bovw( image )
%IMAGE2BOVW get Bag?of-Visual-Words HOG of image into 1000 kmeans centers 
% uisng Vlfeat Toolbox
% Input: image -> subImage inside bounding box 
% Output: HOG -> BoVW histgram into 256 demension matrix 

% Author: Yanchao Yu

%  setup;
%  image = imread('data/tmp/20150313T140754.jpg');

% load('hogClusters.mat');
% 
% % model.numWords = 1000 ;
% model.numSpatialX = 4 ;
% model.numSpatialY = 4 ;
% model.quantizer = 'kdtree' ;
% 
% image = standardizeImage(image);
% % [key_points, descriptors] = vl_phow(im, 'step', 4, 'floatdescriptors', true) ;
% % %descriptors = vl_colsubset(cat(2, descriptors{:}), 10e4) ;
% % descriptors = single(descriptors) ;
% % 
% % % Quantize the descriptors to get the visual words
% % centers = vl_kmeans(descriptors, model.numWords,'Initialization', 'plusplus','Algorithm','ANN');
% model.centers = centers;
% if strcmp(model.quantizer, 'kdtree')
%    model.kdtree = vl_kdtreebuild(centers) ;
% end
% 
% HOG = getHOGDescriptors(image,model);

im = standardizeImage(image);
im_size = size(im) ;
encoder = load(sprintf('data/Encoder/bovw/encoder.mat'));

% [keypoints, descriptors] = computeFeatures(im);

features = encoder.extractorFn(im) ;
imageSize = size(im) ;
HOG = {} ;
for i = 1:size(encoder.subdivisions,2)
  minx = encoder.subdivisions(1,i) * imageSize(2) ;
  miny = encoder.subdivisions(2,i) * imageSize(1) ;
  maxx = encoder.subdivisions(3,i) * imageSize(2) ;
  maxy = encoder.subdivisions(4,i) * imageSize(1) ;

  ok = ...
    minx <= features.frame(1,:) & features.frame(1,:) < maxx  & ...
    miny <= features.frame(2,:) & features.frame(2,:) < maxy ;

  descrs = encoder.projection * bsxfun(@minus, ...
                                       features.descr(:,ok), ...
                                       encoder.projectionCenter) ;
  if encoder.renormalize
    descrs = bsxfun(@times, descrs, 1./max(1e-12, sqrt(sum(descrs.^2)))) ;
  end

  w = size(im,2) ;
  h = size(im,1) ;
  frames = features.frame(1:2,:) ;
  frames = bsxfun(@times, bsxfun(@minus, frames, [w;h]/2), 1./[w;h]) ;

  descrs = extendDescriptorsWithGeometry(encoder.geometricExtension, frames, descrs) ;

  
  switch encoder.type
    case 'bovw'
      [words,distances] = vl_kdtreequery(encoder.kdtree, encoder.words, ...
                                         descrs, ...
                                         'MaxComparisons', 100) ;
      z = vl_binsum(zeros(encoder.numWords,1), 1, double(words)) ;
      z = sqrt(z) ;

    case 'fv'
      z = vl_fisher(descrs, ...
                    encoder.means, ...
                    encoder.covariances, ...
                    encoder.priors, ...
                    'Improved') ;
    case 'vlad'
      [words,distances] = vl_kdtreequery(encoder.kdtree, encoder.words, ...
                                         descrs, ...
                                         'MaxComparisons', 15) ;
      assign = zeros(encoder.numWords, numel(words), 'single') ;
      assign(sub2ind(size(assign), double(words), 1:numel(words))) = 1 ;
      z = vl_vlad(descrs, ...
                  encoder.words, ...
                  assign, ...
                  'SquareRoot', ...
                  'NormalizeComponents') ;
  end
  
%   [words,distances] = vl_kdtreequery(encoder.kdtree, encoder.words, ...
%                                          descrs, ...
%                                          'MaxComparisons', 100) ;
%       z = vl_binsum(zeros(encoder.numWords,1), 1, double(words)) ;
%       z = sqrt(z) ;
    
  z = z / max(sqrt(sum(z.^2)), 1e-12) ;
  HOG{i} = z(:);
end
HOG = cat(1, HOG{:});