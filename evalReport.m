function [averPrecision, averRecall, averF1, averAccuracy] = evalReport( ontology, attrs, id, acutual, predict, policy, other)
%EVALREPORT Summary of this function goes here
    setup;
    formatOut = 'mm-dd-yy';
    dir_root = fileparts(which('setup.m'));
    filename = sprintf('/data/Experiments/offline/expReport_%s.txt',policy);
    path = fullfile(dir_root,filename);
    disp(path);
    
    if exist(path, 'file') == 0
        writeInReport(path,'/********************************************************************/');
        writeInReport(path,'  Classifier Summary Version -- Logistic Regression with SGD');
        writeInReport(path,'  @ Author:  Yanchao Yu');
        writeInReport(path,'  @ Version: 0.1');
        writeInReport(path,'  @ Date:    March 2016');
        writeInReport(path,sprintf('  @ Policy Type:  %s ',policy));
        writeInReport(path,'/********************************************************************/');
    end
    
    writeToRow(path,{'',''});
    writeInReport(path,sprintf('/*********************** Test %d fold ***********************/', id));
    
    writeInReport(path,'');
    onlotogy_str = sprintf('@@ Ontology: %s',ontology);
    writeInReport(path,onlotogy_str);
    writeInReport(path,'');

    acutual = cell2mat(acutual');
    [row,cols] = size(acutual);
    acutual = acutual *2 -1;

    predict = cell2mat(predict');
    [row,cols] = size(predict);
    predict = predict *2 -1;

    precision = {};
    recall = {};
    microF1 = {};
    accuracy = {};
    for i = 1 : cols
        X = acutual(:,i);
        Y = predict(:,i);
        [ precision{i}, recall{i}, microF1{i}, accuracy{i}, ~ ] = im2evaluation( X',Y' );

        line_str = sprintf('[%s]  Precision = %.3g,    Recall = %.3g,    Macro-F1 = %.3g,    Accuracy = %.3g',attrs{i}, precision{i}, recall{i}, microF1{i}, accuracy{i});
        writeInReport(path,line_str);
    end
%   [ averPrecision, averRecall, averF1, averAccuracy, infor ] = im2evaluation( acutual',predict' );
    averPrecision = averEval(precision);
    averRecall= averEval(recall);
    averF1= averEval(microF1);
    averAccuracy = averEval(accuracy);
    
    line_str = sprintf('[%s]  Precision = %.3g,    Recall = %.3g,    Macro-F1 = %.3g,    Accuracy = %.3g','Average', averPrecision, averRecall, averF1, averAccuracy);
    writeInReport(path,line_str);
    
    description = sprintf('Sample Distribution across %s: %s', ontology, other);
    writeInReport(path,'');
    writeInReport(path,description);
end

function value = averEval(data)
    num = length(data)
    value = 0;

    for i = 1 : num
        if(isnan(data{i}))
            data{i} = 0;
        end
        
        value = value + data{i};
    end
    
    value = value / num;
end

function writeInReport(path, line)
    if iscell(line)
        line = line{1};
    end
    
    fid = fopen( path, 'a' );
    
    fprintf(fid, '%s\n', line);
    fclose(fid);
end

